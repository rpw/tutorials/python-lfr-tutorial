# -*- coding: utf-8 -*-

import numpy as np
    
def diff(item1, item2, relative=True, both=True, first=True, ignoringZeros=True, epsiZeros=0.,
         ignoringNaNs=True, digit=17, fmt='.3f', outputs=2, echo=True):    
    if relative:
        D = (np.abs(item1) + np.abs(item2)) / 2 if both else (item1 if first else item2)
        if ignoringZeros:
            itemZeros = item1 if first else item2
            idi = np.nonzero(itemZeros) if epsiZeros == 0 else np.nonzero(np.abs(itemZeros) >= epsiZeros)
            nb_zeros = itemZeros.size - (np.count_nonzero(itemZeros) if epsiZeros == 0 else len(idi[0]))             
        else:
            idi = ...
        diff = (item1[idi] - item2[idi]) / D[idi]    
    else:
        idi = ...        
        diff = item1 - item2 
                                   
    q = abs(diff)*100 if relative else abs(diff)    
    
    if ignoringNaNs:
        imax = np.nanargmax(q)
        imin = np.nanargmin(q)    
    else:
        imax = np.argmax(q)
        imin = np.argmin(q)
    
    if idi == ...:
        ind_max = np.unravel_index(imax, q.shape)    
        ind_min = np.unravel_index(imin, q.shape) 
    else:
        ind_max = [idi[k][imax] for k in range(len(idi))]
        ind_min = [idi[k][imin] for k in range(len(idi))]        
    
    if echo:    
        print('shape of the items:', item1.shape)
        format_item1_item2 = '(item1 {}:.{}f{}   item2 {}:.{}f{})'.format('{', str(digit), '}', '{', str(digit), '}')
        format_max_diff = 'max relative difference: %'+fmt+' %%' if relative else 'max difference: %'+fmt
        format_min_diff = 'min relative difference: %.3e %%' if relative else 'min difference: %.3e' 
        if relative:
            print('ignoring NaNs: ', ignoringNaNs, '  ignoring Zeros: ', 
                  ignoringZeros, '=> %d zeros'%(nb_zeros) if ignoringZeros else '')            
        else:
             print('ignoring NaNs: ', ignoringNaNs)
        print(format_max_diff%(q.flatten()[imax]), '    for index:', ind_max)
        print(format_item1_item2.format(item1[idi].flatten()[imax], item2[idi].flatten()[imax]))  
        print(format_min_diff%(q.flatten()[imin]), '    for index:', ind_min)
        print(format_item1_item2.format(item1[idi].flatten()[imin], item2[idi].flatten()[imin]))    
              
    return q.flatten()[imax] if outputs == 1 else (q.flatten()[imax], ind_max) 
