# -*- coding: utf-8 -*-

import numpy as np
from lib_plasma.dispersion import m, M, epsi0, e, kB


def linfromdB(*val):    
    return [np.power(10,np.array(v)/20) for v in val] if len(val) > 1 else np.power(10,np.array(val[0])/20)
    
def dBfromlin(*val):    
    return [20*np.log10(np.array(v)) for v in val] if len(val) > 1 else 20*np.log10(np.array(val[0]))
    

def minus180_plus180(phi):
    return phi%360-360 if phi%360 > 180 else phi%360
        
def standard_sheet(phase):
    return ((phase-180.) %360.)-180.    


def Fahrenheit_to_Celsius(T):
    return (T-32.) * 5./9

def Celsius_to_Fahrenheit(T):
    return 32. + T*9./5    
    
    
def Fpe_to_Ne(fpe, kHz=True, cm3=True):    
    wpe = 2*np.pi * (fpe*1000 if kHz else fpe)
    return m * epsi0 * (wpe/e)**2 * (1e-6 if cm3 else 1)
    
def Ne_to_lambda_D(n, Te=10, cm3=True, eV=True):    
    wpe = np.sqrt((n*1e6 if cm3 else n) * e**2 / epsi0 / m)
    vthe = np.sqrt((Te*e if eV else kB*Te) / m)     
    return vthe / wpe 
    
def B_to_Fci(B, nT=True, kHz=False):  
    fci = e * (B*1e-9 if nT else B) / M / (2*np.pi)    
    return fci * (1e-3 if kHz else 1)    
    
def B_to_Fce(B, nT=True, kHz=False):  
    fce = e * (B*1e-9 if nT else B) / m / (2*np.pi)    
    return fce * (1e-3 if kHz else 1)        
