# -*- coding: UTF-8 -*-

# routines pour la manipulation des bits
# début: 9  janvier 2015 
# suite: 19 janvier 2015
#        11 février 2015 (from hex to int)


def int2bin(n,nbits):
    """ 
    affiche la représentation binaire d'un nombre int avec complément à 2  
    pour les valeurs négatives
    début: 9 janvier 2015
    """
    spec='0'+str(nbits)+'b'
    if n >= (-2**(nbits-1)) and n <= (2**(nbits-1)-1) :
        return format(n, spec) if n>=0 else format(2**nbits+n, spec)
    else :
        return 'ERROR!'
        
        
def int2hex(n,nbits):
    """ 
    affiche la représentation hexadécimale d'un nombre int avec complément à 2  
    pour les valeurs négatives
    début: 9 janvier 2015
    """
    if (nbits % 4) != 0 :
        return 'ERROR 1!'
    spec='0'+str(nbits/4)+'x'
    if n >= (-2**(nbits-1)) and n <= (2**(nbits-1)-1) :
        return format(n, spec) if n>=0 else format(2**nbits+n, spec)
    else :
        return 'ERROR 2!'
        

def int32hex(n):
    """ 
    affiche la représentation hexadécimale d'un nombre int avec complément à 2  
    pour les valeurs négatives ; force une représentation à 32 bits; marche donc
    pour des int signés 32 bit maximum
    début: 14 janvier 2015 (Alexis)
    suite: 19 janvier 2015
    """
    return "%0x"%(n&0xffffffff)
                
def twos_comp(val, nbits):
    """
    compute the 2's complement of int value val
    http://stackoverflow.com/questions/1604464/twos-complement-in-python
    """
    if (val & (1 << (nbits - 1))) != 0: # if sign bit is set e.g., 8bit: 128-255
        val = val - (1 << nbits)        # compute negative value
    return val     
    
def twos_comp_np(vals, nbits):
    """
    compute the 2's complement of array of int values vals
    http://stackoverflow.com/questions/1604464/twos-complement-in-python
    """
    vals[vals & (1<<(nbits-1)) != 0] -= (1<<nbits)
    return vals

def hex2int(hex_str,nbits):
    """ 
    convertit en int sur nbits, avec complément à 2 pour les valeurs négatives, un string hexadécimale 
    début: 10 février 2015
    """
    return twos_comp(int(hex_str,16), nbits)
                                         
def sign_extend(value, bits):
    """
    semblable à twos_comp ?
    """
    sign_bit = 1 << (bits - 1)
    return (value & (sign_bit - 1)) - (value & sign_bit)                    
                        

"""   

from lib_bin import *

int2bin(2**15-1,16)

n=2**31-1
int2hex(n,32)
int2hex32bits(n)

hex2int('8000',16)
Out[21]: -32768

hex2int('7fff',16)
Out[22]: 32767

https://docs.python.org/2/library/string.html#formatexamples

http://stackoverflow.com/questions/1604464/twos-complement-in-python

https://wiki.python.org/moin/BitManipulation

https://wiki.python.org/moin/BitwiseOperators

Two's Complement binary for Negative Integers:

Negative numbers are written with a leading one instead of a leading zero. So if you are using only 8 bits for your twos-complement numbers, then you treat patterns from "00000000" to "01111111" as the whole numbers from 0 to 127, and reserve "1xxxxxxx" for writing negative numbers. A negative number, -x, is written using the bit pattern for (x-1) with all of the bits complemented (switched from 1 to 0 or 0 to 1). So -1 is complement(1 - 1) = complement(0) = "11111111", and -10 is complement(10 - 1) = complement(9) = complement("00001001") = "11110110". This means that negative numbers go all the way down to -128 ("10000000").

Of course, Python doesn't use 8-bit numbers. It USED to use however many bits were native to your machine, but since that was non-portable, it has recently switched to using an INFINITE number of bits. Thus the number -5 is treated by bitwise operators as if it were written "...1111111111111111111011". 

x << y
    Returns x with the bits shifted to the left by y places (and new bits on the right-hand-side are zeros). This is the same as multiplying x by 2**y. 
x >> y
    Returns x with the bits shifted to the right by y places. This is the same as //'ing x by 2**y. 
x & y
    Does a "bitwise and". Each bit of the output is 1 if the corresponding bit of x AND of y is 1, otherwise it's 0. 
x | y
    Does a "bitwise or". Each bit of the output is 0 if the corresponding bit of x AND of y is 0, otherwise it's 1. 
~ x
    Returns the complement of x - the number you get by switching each 1 for a 0 and each 0 for a 1. This is the same as -x - 1. 
x ^ y
    Does a "bitwise exclusive or". Each bit of the output is the same as the corresponding bit in x if that bit in y is 0, and it's the complement of the bit in x if that bit in y is 1. 

Just remember about that infinite series of 1 bits in a negative number, and these should all make sense. 


"""















