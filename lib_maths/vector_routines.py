# -*- coding: utf-8 -*-

import numpy as np
from numpy.linalg import norm

class TotoError(Exception):
    pass

def angle(vec1, vec2, deg=True, decimals=15):
    return np.rad2deg(np.arccos((np.inner(vec1, vec2)/norm(vec1)/norm(vec2)).round(decimals=decimals))) if deg else \
                      np.arccos((np.inner(vec1, vec2)/norm(vec1)/norm(vec2)).round(decimals=decimals))
                      
def v_angle(vec1, vec2, deg=True, decimals=15):
    """
    return the angles between a set of two vectors based of their vectorial inner product; 
    works for any shape of the vector fields provided that the vector axis is the last one:
    vec1(..., dim)
    vec2(..., dim) 
    """
    if vec1.shape == vec2.shape: 
        dim = vec1.shape[-1]
    else: 
        raise TotoError('problematic dimension of inputs!!')
    inner_prod = vec1[..., 0]*vec2[..., 0]    
    for i in range(1, dim, 1):
        inner_prod += vec1[..., i]*vec2[..., i] 
    theta = np.arccos((inner_prod / norm(vec1, axis=-1) / norm(vec2, axis=-1)).round(decimals=decimals))
    return np.rad2deg(theta) if deg else theta                        
                      
def vector_variance_analysis(vec, echo=False):
    """
    vec(n, dim): input vector field on which variance analysis will be performed
    """
    n, dim = vec.shape
    if echo: print('shape of the data:', n, dim)
        
    # variance analysis for a centered field (i.e for dvec = vec-vec0)
    dvec = vec - np.mean(vec, axis=0)
    
    # compute the covariance matrix < dvec dvec.T >   
    mat = np.dot(dvec.T, dvec) / n
    # or:
    # mat = np.cov(dvec, rowvar=False, bias=True)    
    # or (...): 
    # mat = np.zeros((dim,dim))
    # for i in range(n):
    #     mat += np.tensordot(dvec[i,:], dvec[i,:], axes=0)
    # mat /= n 
    
    # computation of the eigenvalues and eigenvectors     
    evals, evecs = np.linalg.eigh(mat)
    
    if echo:
        print("eigenvalues  :\n", evals)
        print("eigenvectors :\n", evecs)    
    return evals, evecs

def mdirect(m, icolumn_to_reverse=0, epsi=1e-4):
    """
    Return input unitary matrix with the specified column
    reversed if det(m) == -1 in order to get an axis system
    oriented in the direct sense, i.e. det(md) == +1. 
    Return a copy otherwise.
    """
    if abs(abs(np.linalg.det(m))-1) >= epsi:
        raise TotoError("Non unitary matrix !!!")
    md = m.copy()    
    if np.linalg.det(m) < 0:           
        md[:, icolumn_to_reverse] = -m[:, icolumn_to_reverse]  
    return md  
    
def rotation_matrix(axis, theta, deg=True):
    """
    Using the Euler-Rodrigues formula, return the rotation matrix 
    associated with counterclockwise rotation about the given axis by angle theta.    
    https://stackoverflow.com/questions/6802577/rotation-of-3d-vector    
    https://en.wikipedia.org/wiki/Euler%E2%80%93Rodrigues_formula
    """       
    axis = np.asarray(axis)
    axis = axis / np.sqrt(np.dot(axis, axis))
    tt = np.deg2rad(theta) if deg else theta
    a = np.cos(tt / 2.0)
    b, c, d = -axis * np.sin(tt / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])
            
def v_uvectors_REF_to_VEC(vec, axis='eZref'):
    """
    Takes a set of vectors expressed in a given (direct and orthonormal) coordinate system, called REF, 
    and returns a set of matrices for transformation to new coordinate systems, called VEC, 
    where the Z axes are // to the input vectors; works for any shape of the vector field provided 
    that the vector axis is the last one.
INPUT:    
    vec[..., 3]: array of vectors expressed in a REF coordinate system from which a field of 
                 VEC coordinate systems is built  
    axis: option for building the new X axes        
OUTPUT:              
    M_REF_to_VEC[..., 3, 3]: array of matrices for transformation from the REF coordinate system 
                             to the VEC coordinate systems          
    """
    eAxis = {'eZref': [0., 0., 1.], 
             'eYref': [0., 1., 0.],
             'eXref': [1., 0., 0.]}[axis] 
    eZvec = np.moveaxis([vec[...,i] / norm(vec, axis=-1) for i in range(3)], 0, -1)    
    eXvec = np.cross(eAxis, eZvec)    
    eXvec = np.moveaxis([eXvec[...,i] / norm(eXvec, axis=-1) for i in range(3)], 0, -1)          
    eYvec = np.cross(eZvec, eXvec) 
    return np.stack([eXvec, eYvec, eZvec], axis=-2)       
    
     
