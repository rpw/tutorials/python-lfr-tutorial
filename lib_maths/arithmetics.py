# -*- coding: utf-8 -*-

import numpy as np

class TotoError(Exception):
    pass

def is_prime(n):
    """
    Return True if n is a prime number
    (n shall be a positive integer)
    """
    if not isinstance(n, int):        
        raise TypeError('Argument n={} is not an integer ! It is of type {}'.format(n, type(n)))
    if n <= 0:
        raise TotoError('Argument n={} is not a positive integer !'.format(n))        
    if n == 1:
        return True
    if n%2 == 0: 
        return False    
    for i in range(3, n, 1):
        if n%i == 0:
            return False
    return True   
    
def sign_func(x, trinary=False):
    s = (1-np.sign(x))/2.
    if trinary: 
        return s
    if s.ndim == 0: 
        return 0 if s == 0.5 else s 
    else:
        s[s==0.5] = 0.
        return s    
