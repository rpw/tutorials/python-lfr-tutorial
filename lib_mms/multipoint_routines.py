# -*- coding: utf-8 -*-

import numpy as np

def surface_velocity(dij, tij):
    """            
    Détermine la normale, e_n, d'une surface supposée plane qui traverse 4 satellites, 
    ainsi que sa vitesse normale, v_n, relative à ces satellites, à partir des distances 
    intersatellites et du timing de la traversée:
    
        e_n . [dij_a, dij_b, dij_c] = [tij_a, tij_b, tdij_c] * v_n
        
    Le vecteur normal, e_n, est ainsi defini pointant dans le sens du mouvement apparant de la surface 
    (vu dans le repère des 4 satellites)          
        
    Suppose que les 4 satellites ont la même vitesse, vsat (km/s, GSE, par exemple, en tout  
    cas dans un repère orthonormé), et que les distances intersatellites sont constantes (km):    
        v_n = vGSE_n - vsat_n                    
    Generalisable au cas ou les vitesses sont differentes avec:    
        j tel que tj = tmax
        tij = ti - tj
        dij = ri(ti) - rj(ti)
        v_n = vGSE_n - vsat_n_j
    La vitesse normale de la surface, vGSE_n, est ainsi restituée dans le repere de référence (GSE, par ex.).
    
    La solution obtenue est unique et ne depend pas du choix des 3 paires dij et tij utilisées 
    si ce n'est qu'elles doivent correspondre à une geometrie non coplanaire.                      
    
INPUT:
    dij: structure contenant 3 distances intersatellites ri - rj (paires nommées 'a', 'b', 'c') 
         dans le repère orthonormé de référence (GSE, par ex., en km) 
    tij: structure contenant les 3 durées ti - tj correspondants aux écarts temporels observés 
         par les 3 paires de satellites 'a', 'b', 'c', en secondes      

OUTPUT:
    e_n: vecteur unitaire normal à la surface qui se déplace relativement aux 4 satellites
    v_n: vitesse de la surface donnée dans sa direction normale et relativement aux satellites                
    
    version 1.0: first version with Python from the last version of the idl routine [3 avril 2019]        
    """
       
    D = np.array([dij['a'], dij['b'], dij['c'] ]).T   # dij vecteurs colonnes 
    T = np.array([tij['a'], tij['b'], tij['c']])      # vecteur ligne
    
    Dinv = np.linalg.inv(D)
    x = np.dot(T,  Dinv )
    v_n = 1/np.linalg.norm(x)
    e_n = v_n * x    
    return e_n, v_n
    
def get_meanvalue_field(vec_arr, time_arr, t1, t2, echo=False):
    """
    vec_arr[isat](n, dim): vector array for each S/C
    time_arr[isat](n): corresponding time array (can either be tt2000 or datetime)
    t1, t2: time interval over which to compute the mean values (same type as time_arr elements)
    """
    if t2 <= t1:
        raise TotoError("t2 <= t1 !")     
    meanval_arr = []
    for vec, time in zip(vec_arr, time_arr):
        if (time[-1] < t1) or (time[0] > t2):
            raise TotoError("[%s, %s] est en dehors de la période des données ..."%(t1, t2))
        i1 = np.searchsorted(time, t1, side='left')  
        i2 = np.searchsorted(time, t2, side='right')
        meanval_arr.append(np.mean(vec[i1:i2, :], axis=0) if (i1 != i2) else vec[i1, :])
        if echo: print('i1:', i1, '  i2:', i2, '  meanvalue (over time):', meanval_arr[-1])
    meanval = np.mean(meanval_arr, axis=0)
    if echo: print('\nmeanvalue over time and s/c:', meanval, '\n')        
    return meanval     
