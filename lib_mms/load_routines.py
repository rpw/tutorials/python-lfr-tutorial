# -*- coding: utf-8 -*-

import os

import numpy as np
from spacepy import pycdf
import bisect

class TotoError(Exception):
    pass

############### MMS CDF files #######################


def load_mec_CDF_L2(file, t1=None, t2=None, coord='gse', raw_Epoch=False, echo=False, key_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)
    # find leftmost Epoch greater than or equal to t1 (t1 <= a[i1])
    # find rightmost Epoch less than or equal to t2 (a[i2-1] <= t2)  
    i1 = 0 if t1 == None else bisect.bisect_left(cdf['Epoch'], t1)
    i2 = len(cdf['Epoch']) if t2 == None else bisect.bisect_right(cdf['Epoch'], t2)        
    if echo:
        print(cdf)
        if key_attrs != None: print('\n{}: {}'.format(key_attrs, cdf.attrs[key_attrs]))
        print("start:", cdf["Epoch"][i1])
        print("stop: ", cdf["Epoch"][i2-1])
        print("=> %d elements"%(i2-i1))                             
    sat = os.path.basename(file)[3]        
    rvec = np.array(cdf["mms%s_mec_r_%s"%(sat, coord)][i1:i2,:])
    time = np.array(cdf.raw_var("Epoch")[i1:i2])
    if echo:
        print('Sampling frequency:', 1/np.median(np.gradient(1e-9*time))) 
    cdf.close()
    return rvec, time if raw_Epoch else pycdf.lib.v_tt2000_to_datetime(time)

def load_fgm_CDF_L2(file, t1=None, t2=None, coord='gse', raw_Epoch=False, echo=False, key_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)
    # find leftmost Epoch greater than or equal to t1 (t1 <= a[i1])
    # find rightmost Epoch less than or equal to t2 (a[i2-1] <= t2)  
    i1 = 0 if t1 == None else bisect.bisect_left(cdf['Epoch'], t1)
    i2 = len(cdf['Epoch']) if t2 == None else bisect.bisect_right(cdf['Epoch'], t2)        
    if echo:
        print(cdf)
        if key_attrs != None: print('\n{}: {}'.format(key_attrs, cdf.attrs[key_attrs]))
        print("start:", cdf["Epoch"][i1])
        print("stop: ", cdf["Epoch"][i2-1]) 
        print("=> %d elements"%(i2-i1))                              
    sat = os.path.basename(file)[3]        
    bvec = np.array(cdf["mms%s_fgm_b_%s_brst_l2"%(sat, coord)][i1:i2,0:3])
    time = np.array(cdf.raw_var("Epoch")[i1:i2])
    if echo:
        print('Sampling frequency:', 1/np.median(np.gradient(1e-9*time))) 
    cdf.close()
    return bvec, time if raw_Epoch else pycdf.lib.v_tt2000_to_datetime(time)

def load_scm_CDF_L2(file, t1=None, t2=None, raw_Epoch=True, echo=False, key_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)
    # find leftmost Epoch greater than or equal to t1 (t1 <= a[i1])
    # find rightmost Epoch less than or equal to t2 (a[i2-1] <= t2)    
    i1 = 0 if t1 == None else bisect.bisect_left(cdf['Epoch'], t1)
    i2 = len(cdf['Epoch']) if t2 == None else bisect.bisect_right(cdf['Epoch'], t2)        
    if echo:
        print(cdf)
        if key_attrs != None: print('\n{}: {}'.format(key_attrs, cdf.attrs[key_attrs]))
        print("start:", cdf["Epoch"][i1])
        print("stop: ", cdf["Epoch"][i2-1])
        print("=> %d elements"%(i2-i1))                
    sat = os.path.basename(file)[3]        
    bvec = np.array(cdf["mms%s_scm_acb_gse_scb_brst_l2"%(sat)][i1:i2,:])
    time = np.array(cdf.raw_var("Epoch")[i1:i2]) 
    if echo:
        print('Sampling frequency:', 1/np.median(np.gradient(1e-9*time))) 
    cdf.close()
    return bvec, time if raw_Epoch else pycdf.lib.v_tt2000_to_datetime(time)

def load_edp_CDF_L2(file, t1=None, t2=None, raw_Epoch=True, echo=False, key_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)
    sat = os.path.basename(file)[3]
    # find leftmost Epoch greater than or equal to t1 (t1 <= a[i1])
    # find rightmost Epoch less than or equal to t2 (a[i2-1] <= t2)  
    i1 = 0 if t1 == None else bisect.bisect_left(cdf['mms%s_edp_epoch_brst_l2'%(sat)], t1)
    i2 = len(cdf['mms%s_edp_epoch_brst_l2'%(sat)]) if t2 == None else \
         bisect.bisect_right(cdf['mms%s_edp_epoch_brst_l2'%(sat)], t2)        
    if echo:
        print(cdf)
        if key_attrs != None: print('\n{}: {}'.format(key_attrs, cdf.attrs[key_attrs]))
        print("start:", cdf['mms%s_edp_epoch_brst_l2'%(sat)][i1])
        print("stop: ", cdf['mms%s_edp_epoch_brst_l2'%(sat)][i2-1]) 
        print("=> %d elements"%(i2-i1))                                       
    evec = np.array(cdf["mms%s_edp_dce_gse_brst_l2"%(sat)][i1:i2,:])
    time = np.array(cdf.raw_var('mms%s_edp_epoch_brst_l2'%(sat))[i1:i2]) 
    if echo:
        print('Sampling frequency:', 1/np.median(np.gradient(1e-9*time))) 
    cdf.close()
    return evec, time if raw_Epoch else pycdf.lib.v_tt2000_to_datetime(time)

    
    
