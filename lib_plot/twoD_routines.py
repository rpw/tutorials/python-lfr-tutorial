# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

def contour2D(func2D, arg_xy=True, n_xscales=3, n_yscales=3, num=100, nlevels=10, str_title='', cmap=plt.cm.jet,
              str_xlabel='x ($L_{x}$)', str_ylabel='y ($L_{y}$)', fontsize=12, filled=True, str_colorbar='',
              figsize=(5,5), fig_ratio=True, fname=None, **kwds):        
    keys = [k for k in kwds.keys()]
    lx = kwds[keys[0]]
    ly = kwds[keys[1]]
    ampl = kwds[keys[2]]
    x = np.linspace(-n_xscales, n_xscales, num)
    y = np.linspace(-n_yscales, n_yscales, num)     
    X, Y = np.meshgrid(x*lx, y*ly, indexing='xy', sparse=True)    
    P = func2D(X, Y, **kwds) if arg_xy else func2D(Y, X, **kwds)            
    fig, ax = plt.subplots(figsize=(figsize[0], figsize[1]*(ly/lx if fig_ratio else 1.)), constrained_layout=True)    
    if filled:
        clines = ax.contour(x, y, P, nlevels, colors='k', linewidths=1)
        ax.clabel(clines, inline=1, fontsize=10)
        cfilled = ax.contourf(x, y, P, clines.levels, vmin=None, vmax=None, colors=None, cmap=cmap)
        cbar = fig.colorbar(cfilled, ax=ax)
        cbar.set_label(str_colorbar, fontsize=fontsize)
    else:
        clines = ax.contour(x, y, P, nlevels, vmin=None, vmax=None, colors=None, cmap=cmap)
        ax.clabel(clines, inline=1, fontsize=10)                   
    ax.set_title('\nContour lines ($L_{x}$=%3.1f,  $L_{y}$=%3.1f, Amplitude=%.1f)'%(lx, ly, ampl) if not \
                 str_title else str_title, fontsize=fontsize)
    ax.set_xlabel(str_xlabel, fontsize=fontsize)
    ax.set_ylabel(str_ylabel, fontsize=fontsize)
    if fname != None: plt.savefig(fname, bbox_inches='tight', pad_inches=0.2)        
    plt.show()       
                    
def stream2D(field2D, icomp=0, arg_xy=True, comp_xy=True, n_xscales=3, n_yscales=3, num=100, fig_ratio=True,
             str_xlabel='x ($L_{x}$)', str_ylabel='y ($L_{y}$)', fontsize=12, str_title='', str_colorbar='',
              fname=None, cmap=plt.cm.PiYG, density=[3.,2.], arrowsize=0.8, figsize=(8,8), **kwds):
    keys = [k for k in kwds.keys()]
    lx = kwds[keys[0]]
    ly = kwds[keys[1]]
    ampl = kwds[keys[2]]    
    x = np.linspace(-n_xscales, n_xscales, num)
    y = np.linspace(-n_yscales, n_yscales, num)     
    X, Y = np.meshgrid(x*lx, y*ly, indexing='xy', sparse=True)        
    E = field2D(X, Y, **kwds) if arg_xy else field2D(Y, X, **kwds)
    comps = [k for k in E.keys()]
    if not comp_xy: comps.reverse()
    fig, ax = plt.subplots( figsize=(figsize[0], figsize[1]*(ly/lx if fig_ratio else 1.)), constrained_layout=True)
    strm = ax.streamplot(x, y, E[comps[0]], E[comps[1]], color=E[comps[icomp]],
                         density=density, arrowsize=arrowsize, cmap=cmap)
    cbar = fig.colorbar(strm.lines, ax=ax)
    cbar.set_label(comps[icomp] if not str_colorbar else str_colorbar, fontsize=fontsize)
    ax.set_title('\n\n Field lines\n($L_{x}$=%3.1f,  $L_{y}$=%3.1f, Amplitude=%.1f)'%(lx, ly, ampl) if not \
                 str_title else str_title, fontsize=fontsize) 
    ax.set_xlabel(str_xlabel, fontsize=fontsize)
    ax.set_ylabel(str_ylabel, fontsize=fontsize)
    if fname != None: plt.savefig(fname, bbox_inches='tight', pad_inches=0.2)        
    plt.show() 
