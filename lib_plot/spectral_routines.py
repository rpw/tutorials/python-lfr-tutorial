# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.dates as mdt
from matplotlib.ticker import MultipleLocator
import numpy as np
from datetime import timedelta

def fig_spectrograms1(spectrogram_list, time, freq, ampl_range=None, freq_range=None, log=None, psd=None, nop=None,
                      names=None, units=None, fname=None, comment=None, vline_time=None, sharex=True, sharey=True,
                      figsize=(10,16), hspace=0.05, fontsize=12, cmap=plt.cm.jet, fill_value=None, time_gap=False, 
                      gap_dt_percent=1e-3, gap_echo=False, date_fmt=None, df_fmt='.1f', dt_fmt='.1f', show=False, 
                      no_ishow=False, close=False, ylabel='frequency (Hz)', logy=False): 
    """
     spectrogram_list[i].shape = nspec, nfreq (same for all i)
     len(time) = nspec
     len(freq) = nfreq
     version 1.0: implement insertion of time gaps + fix a point on 'log' and 'ampl_range' options + option 'show' [26 février 2020]
     version 1.1: copy of the input arrays and conversion into float when inserting gaps                           [27 février 2020]
     version 1.2: implement custom date formatting using DateFormatter                                             [25 mars 2020]
     version 1.3: implement no_ishow option by wrapping with plt.ioff() and plt.ion() + df_fmt option              [ 7 avril 2020] 
     version 1.4: implement close option                                                                           [16 avril 2020]                                                                              
     version 1.5: fix a bug in gap insertion (last element in dt was missing ...)                                  [21 avril 2020]
     version 1.6: implement dt_fmt option                                                                          [21 mai 2020]
     version 1.7: implement the posibility of different cmap specific for each spectrogram                         [ 8 juin 2020]
     version 1.8: fix a bug when reading latex strings for "names" and "units" keywords                            [ 2 octobre 2020]
     version 1.9: implement logy option and fix a corresponding bug with yrange + fix a set_xlabel bug             [11 décembre 2020]
    """                                               
    def insert_time_gaps(spectrogram_list, time, gap_dt_percent=1e-3, echo=True):
        dt = time[1:] - time[0:-1]
        ind_insert = np.nonzero(dt > dt[0]+dt[0]*gap_dt_percent)[0]
        time_gap = np.insert(time, ind_insert+1, time[ind_insert]+dt[0], axis=0)
        spectrogram_list_gap = [np.insert(spectrogram.astype(float), ind_insert+1, np.nan, axis=0) for spectrogram in spectrogram_list] 
        if echo: 
            print('gap insertion here:')
            print((len(ind_insert)*'{}\n').format(*time[ind_insert]))
            print('gap interval:') 
            print((len(ind_insert)*'{}\n').format(*dt[ind_insert]))            
        return spectrogram_list_gap, time_gap         
    if no_ishow: plt.ioff()    
    ndata = len(spectrogram_list)
    fig, axarr = plt.subplots(ndata, 1, squeeze=False, sharex=sharex, sharey=sharey, figsize=figsize)  
    plt.subplots_adjust(hspace=hspace)  
    if not isinstance(cmap, list): cmap = ndata*[cmap]         
    if names == None: names = ndata*['']
    if log == None: log = ndata*[False]          
    if psd == None: psd = ndata*[False]   
    if nop == None: nop = ndata*[False]                  
    if ampl_range == None:
        varr = [[None, None] for i in range(ndata)]
    else:
        varr =  [[(np.log10(a) if a != None else None) for a in ampl_range[i]] if log[i] else ampl_range[i] for i in range(ndata)]
    yrange = (freq[1] if freq[0] == 0 else freq[0] if logy else freq[0], freq[-1]) if freq_range == None else freq_range
    # insert NaNs in the spectrograms where time gaps occur if the option is set
    if time_gap:
            spectrogram_list2, time2 = insert_time_gaps(spectrogram_list, time, gap_dt_percent=gap_dt_percent, echo=gap_echo) 
    else:
            spectrogram_list2, time2 = np.copy(spectrogram_list), np.copy(time)        
    # generate the two 2D grids for the x & y bounds of the color spectrograms
    dt = time2[1] - time2[0]           
    df = freq[-1] - freq[-2]    
    x, y = np.meshgrid(np.append(time2, time2[-1]+dt), np.append(freq, freq[-1]+df)-df/2)
    # plotting
    if units == None: units = ndata*['?']
    for i in range(ndata):         
        if fill_value != None:
            spectrogram_list2[i][spectrogram_list2[i] < fill_value] = np.nan             
        z = np.log10(spectrogram_list2[i]) if log[i] else spectrogram_list2[i]
        spectro = axarr[i, 0].pcolormesh(x, y, z.T, vmin=varr[i][0], vmax=varr[i][1], cmap=cmap[i])
        if logy: axarr[i, 0].semilogy()                  
        axarr[i, 0].set_ylim(yrange)  
        axarr[i, 0].set_ylabel(ylabel, fontsize=fontsize)
        axarr[i, 0].xaxis.set_ticks_position('both')
        axarr[i, 0].yaxis.set_ticks_position('both')
        if vline_time != None:
            for t in vline_time:
                axarr[i, 0].axvline(t, color='white', lw=1)
        if (comment != None) and (i == 0): 
            axarr[i, 0].set_title(('{}     ($\Delta f$ = {:'+df_fmt+'} Hz, $\Delta t$ = {:'+dt_fmt+'} s)') \
                           .format(comment, df, dt.total_seconds() if isinstance(dt, timedelta) else dt), fontsize=fontsize)                     
        cbar = fig.colorbar(spectro, ax=axarr[i, 0])        
        if psd[i]:
            cbar.set_label('log$_{10}$('+units[i]+'$^2$/Hz)\n'+names[i] if log[i] else units[i]+'$^2$/Hz\n'+names[i], fontsize=fontsize)        
        elif not nop[i]:
            cbar.set_label('log$_{10}$('+units[i]+'$^2$)\n'+names[i] if log[i] else units[i]+'$^2$\n'+names[i], fontsize=fontsize)            
        else:   
            cbar.set_label('log$_{10}$('+units[i]+')\n'+names[i] if log[i] else units[i]+'\n'+names[i], fontsize=fontsize)
                
        if (date_fmt != None) and isinstance(dt, timedelta):
            date_format = mdt.DateFormatter(date_fmt)
            axarr[i, 0].xaxis.set_major_formatter(date_format) 
        if not sharex:
            plt.setp(axarr[i, 0].get_xticklabels(), rotation=30, ha='right')           
    if not isinstance(dt, timedelta): 
        axarr[ndata-1, 0].set_xlabel('time (s)', fontsize=fontsize)   
    if sharex: fig.autofmt_xdate(rotation=30, ha='right')    
               #plt.xticks(rotation=70, ha='right')                
    # saving      
    if fname != None:
        plt.savefig(fname, bbox_inches='tight', pad_inches=0.2)
    if show: plt.show()    
    if close: plt.close(fig)
    if no_ishow: plt.ion()       
    
def fig_spectrograms(spectrogram_list, time, freq_list, ampl_range=None, freq_range=None, logy=None, log=None, 
                     psd=None, nop=None, names=None, units=None, fname=None, comment=None, vline_time=None, 
                     sharex=True, sharey=True, figsize=(10,16), hspace=0.05, fontsize=12, fs_name=12, fs_title=10, 
                     fill_value=None, time_gap=False, gap_dt_percent=1e-3, gap_echo=False, dt_i1i2=None, which_df='bottom',
                     date_fmt=None, df_fmt='.1f', dt_fmt='.1f', show=False, no_ishow=False, close=False, 
                     cbar_aspect=20, cbar_pad=0.05, cbar_ticklabel_fs=None, ticklabel_fs=None, cmap=plt.cm.jet, dpi=None,
                     ylabel='frequency (Hz)', xtl_rot=30., xtl_ha='right', xlabel=None, nlocator=None, style='default',
                     rec_wf_list=None): 
    """
     spectrogram_list[i].shape = nspec, nfreq[i]
     len(time) = nspec
     len(freq_list[i]) = nfreq[i] if freq_list is a list else len(freq_list) = nfreq[i] = constant    
     rec_wf_list = [{'ipanel': i, 'multi_x': 2*[sm_l2_swf_epoch_datetime2E_SRF[2][sx]], 
                                  'multi_y': [0.05*mag_fce, 0.2*mag_fce],                                       
                                  'multi_color': 2*['white'], 'multi_linestyle': 2*['-'], 
                                  'multi_marker': 2*[''], 'multi_linewidth': 2*[1.5]} for i in range(5)]
                     
     version 2.0: implementation from version 1.9 of possibly different frequency axes + xlabel, xtl_rot and nlocator options  [14 décembre  2020]    
     version 2.1: implementation of style option + xtl_ha option few weeks before ...                                          [14 janvier   2021]       
     version 2.2: implementation of dt_i1i2 option                                                                             [21 janvier   2021]      
     version 2.3: implementation of which_df option  ('top', 'bottom', or its value)  + dpi option                             [30 janvier   2021]       
     version 2.4: implementation of cbar_aspect=20, cbar_pad=0.05, cbar_ticklabel_fs=None, ticklabel_fs=None   options         [ 2 février   2021]
     version 2.5: implemention of rec_wf_list (enfin !)                                                                        [14 juin      2021]     
     version 2.6: extension of fontsize and ticklabel_fs keywords as tuples to distinguish x and y axes + dt_i1i2 with i0      [30 septembre 2021]    
     version 2.7: fix a bug with rec_wf_list time range, set_xlim(xmin, xmax) was missing ...                                  [ 7 octobre   2021]  
    """                                               
    def insert_time_gaps(spectrogram_list, time, gap_dt_percent=1e-3, echo=True):
        dt = time[1:] - time[0:-1]
        ind_insert = np.nonzero(dt > dt[0]+dt[0]*gap_dt_percent)[0]
        time_gap = np.insert(time, ind_insert+1, time[ind_insert]+dt[0], axis=0)
        spectrogram_list_gap = [np.insert(spectrogram.astype(float), ind_insert+1, np.nan, axis=0) for spectrogram in spectrogram_list] 
        if echo: 
            print('gap insertion here:')
            print((len(ind_insert)*'{}\n').format(*time[ind_insert]))
            print('gap interval:') 
            print((len(ind_insert)*'{}\n').format(*dt[ind_insert]))            
        return spectrogram_list_gap, time_gap         
    def min_max_dt(time, i1=0, i2=-2):
        dt = time[i1+1:i2+1] - time[i1:i2]            
        return [np.min(dt), np.max(dt)]     
    if no_ishow: plt.ioff()
    if style != 'default': plt.style.use(style)     
    ndata = len(spectrogram_list)
    fig, axarr = plt.subplots(ndata, 1, squeeze=False, sharex=sharex, sharey=sharey, figsize=figsize)        
    plt.subplots_adjust(hspace=hspace)  
    if not isinstance(cmap, list): cmap = ndata*[cmap]         
    if names == None: names = ndata*['']
    if log == None: log = ndata*[False]          
    if psd == None: psd = ndata*[False]   
    if nop == None: nop = ndata*[False]                  
    if ampl_range == None:
        varr = [[None, None] for i in range(ndata)]
    else:
        varr =  [[(np.log10(a) if a != None else None) for a in ampl_range[i]] if log[i] else ampl_range[i] for i in range(ndata)]    
    if logy == None: logy = ndata*[False] 
    if freq_range == None:
        freq_range = ndata*[None]
    elif not isinstance(freq_range[0], list): 
        freq_range = ndata*[freq_range]             
    if not isinstance(freq_list, list): 
        freq_list = ndata*[freq_list]
        ndf = 1                      
    else:
        ndf = ndata    
    if isinstance(ylabel, str): ylabel = ndata*[ylabel]    
    if isinstance(fontsize, tuple): 
        fs_xlabel, fs_ylabel = fontsize[0], fontsize[1]  
    else:
        fs_xlabel, fs_ylabel = fontsize, fontsize
    if isinstance(ticklabel_fs, tuple): 
        xticklabel_fs, yticklabel_fs = ticklabel_fs[0], ticklabel_fs[1]  
    else:
        xticklabel_fs, yticklabel_fs = ticklabel_fs, ticklabel_fs                       
    yrange = []    
    for i, freq in enumerate(freq_list):
        yrange.append((freq[1] if freq[0] == 0 else freq[0] if logy[i] else freq[0], freq[-1]) if freq_range[i] == None else freq_range[i])                        
    # insert NaNs in the spectrograms where time gaps occur if the option is set
    if time_gap:
            spectrogram_list2, time2 = insert_time_gaps(spectrogram_list, time, gap_dt_percent=gap_dt_percent, echo=gap_echo) 
    else:
            spectrogram_list2, time2 = np.copy(spectrogram_list), np.copy(time)        
    # generate the two 2D grids for the x & y bounds of the color spectrograms
    if isinstance(dt_i1i2, tuple): 
        ndt = 2
        dt = min_max_dt(time, i1=dt_i1i2[0], i2=dt_i1i2[1])
    else:
        ndt = 1
        i0 = 0 if dt_i1i2 == None else dt_i1i2
        dt = [time[1+i0] - time[0+i0]]                              
    df, x, y = ndata*[None], ndata*[None], ndata*[None]
    for i, freq in enumerate(freq_list):
        df[i] = freq[-1] - freq[-2] if which_df == 'top' else freq[1] - freq[-0] if  which_df == 'bottom' else which_df    
        x[i], y[i] = np.meshgrid(np.append(time2, time2[-1]+dt[0]), np.append(freq, freq[-1]+df[i])-df[i]/2)
    if ndf == 1: df = [df[0]]    
    # plotting
    if isinstance(dt[0], timedelta):    
        for i in range(len(dt)): dt[i] = dt[i].total_seconds()
        dt_timedelta = True
    if units == None: units = ndata*['?']
    for i in range(ndata):         
        if fill_value != None:
            spectrogram_list2[i][spectrogram_list2[i] < fill_value] = np.nan             
        z = np.log10(spectrogram_list2[i]) if log[i] else spectrogram_list2[i]
        spectro = axarr[i, 0].pcolormesh(x[i], y[i], z.T, vmin=varr[i][0], vmax=varr[i][1], cmap=cmap[i])
        if logy[i]: axarr[i, 0].semilogy()  
        axarr[i, 0].set_ylim(yrange[i])  
        axarr[i, 0].set_ylabel(ylabel[i], fontsize=fs_ylabel)        
        axarr[i, 0].xaxis.set_ticks_position('both')
        axarr[i, 0].yaxis.set_ticks_position('both')
        axarr[i, 0].tick_params(axis='x', labelsize=xticklabel_fs)
        axarr[i, 0].tick_params(axis='y', labelsize=yticklabel_fs)                        
        if vline_time != None:
            for t in vline_time:
                axarr[i, 0].axvline(t, color='white', lw=1)           
        if (comment != None) and (i == 0): 
            axarr[i, 0].set_title(('{}     ($\Delta f$ = {:'+df_fmt+'}'+(ndf-1)*(', {:'+df_fmt+'}')+' Hz, $\Delta t$ = {:'+dt_fmt+'}'+\
                           (ndt-1)*(', {:'+dt_fmt+'}')+' s)').format(comment, *df, *dt), loc='center', fontsize=fs_title)                                                                           
        cbar = fig.colorbar(spectro, ax=axarr[i, 0], aspect=cbar_aspect, pad=cbar_pad)    
        cbar.ax.tick_params(labelsize=cbar_ticklabel_fs)    
        if psd[i]:
            cbar.set_label('log$_{10}$('+units[i]+'$^2$/Hz)\n'+names[i] if log[i] else units[i]+'$^2$/Hz\n'+names[i], fontsize=fs_name)        
        elif not nop[i]:
            cbar.set_label('log$_{10}$('+units[i]+'$^2$)\n'+names[i] if log[i] else units[i]+'$^2$\n'+names[i], fontsize=fs_name)            
        else:   
            cbar.set_label('log$_{10}$('+units[i]+')\n'+names[i] if log[i] else units[i]+'\n'+names[i], fontsize=fs_name)                
        if (date_fmt != None) and dt_timedelta:
            date_format = mdt.DateFormatter(date_fmt)
            axarr[i, 0].xaxis.set_major_formatter(date_format) 
            #if nlocator != None: axarr[i, 0].xaxis.set_major_locator(MaxNLocator(nbins=nlocator))  
        if nlocator != None: axarr[i, 0].xaxis.set_major_locator(MultipleLocator(1/nlocator))                         
        if not sharex:
            plt.setp(axarr[i, 0].get_xticklabels(), rotation=xtl_rot, ha=xtl_ha)     
    if isinstance(xlabel, str):              
            axarr[ndata-1, 0].set_xlabel(xlabel, fontsize=fs_xlabel) 
    elif xlabel == True:                     
        if dt_timedelta:
            axarr[ndata-1, 0].set_xlabel(str(time2[len(time2)//4])[0:10], fontsize=fs_xlabel)                   
        else:
            axarr[ndata-1, 0].set_xlabel('time (s)', fontsize=fs_xlabel)                           
    if sharex: 
        fig.autofmt_xdate(rotation=xtl_rot, ha=xtl_ha)    
        #plt.xticks(rotation=70, ha=xtl_ha)    
    # if curves to overplot with their own time
    if rec_wf_list != None:
        for rec_wf in rec_wf_list:
            i = rec_wf['ipanel']
            nb_wf = len(rec_wf['multi_x'])     
            multi_color = nb_wf*[None] if rec_wf['multi_color'] == None else rec_wf['multi_color']       
            multi_linestyle = nb_wf*[None] if rec_wf['multi_linestyle'] == None else rec_wf['multi_linestyle'] 
            multi_marker = nb_wf*[None] if rec_wf['multi_marker'] == None else rec_wf['multi_marker']
            multi_linewidth = nb_wf*[None] if rec_wf['multi_linewidth'] == None else rec_wf['multi_linewidth']
            for j in range(nb_wf):
                xmin, xmax = axarr[i, 0].get_xlim()
                axarr[i, 0].plot(rec_wf['multi_x'][j], rec_wf['multi_y'][j], 
                                 color=multi_color[j],linestyle=multi_linestyle[j],
                                 marker=multi_marker[j], linewidth=multi_linewidth[j])  
                axarr[i, 0].set_xlim(xmin, xmax)                                                        
    # saving      
    if fname != None:
        default_dpi = plt.rcParams["savefig.dpi"]        
        plt.rcParams["savefig.dpi"] = 'figure' if dpi == None else dpi 
        plt.savefig(fname, bbox_inches='tight', pad_inches=0.2)
        plt.rcParams["savefig.dpi"] = default_dpi
    if show: plt.show()    
    if close: plt.close(fig)
    if no_ishow: plt.ion()
    if style != 'default': plt.style.use('default')   
        
 
               
