# -*- coding: utf-8 -*-

from matplotlib.widgets import SpanSelector

def select_span_panel(axarr, panel=1, multi=False, echo=True):
    """
    A routine that selects with the mouse a single time span or several ones from the given axis system 'axarr'.
    5, 25 october 2018
    """            
    time_span = []
    def onselect(tmin, tmax):            
        if multi or len(time_span) == 0:
            time_span.append([tmin, tmax])    
        else:
            tspan_previous = time_span.pop()
            time_span.append([tmin, tmax])    
            for i in range(6):
                axarr[i].axvspan(tspan_previous[0], tspan_previous[1], facecolor='white')
        for i in range(6):
            axarr[i].axvspan(tmin, tmax, facecolor='red', alpha=0.5)                
    span = SpanSelector(axarr[panel-1], onselect, 'horizontal', useblit=False, span_stays=False,
                        rectprops=dict(alpha=0.5, facecolor='blue'))        
    return time_span, span
