# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import matplotlib.dates as mdt
from matplotlib.ticker import MultipleLocator
import numpy as np
from datetime import datetime

def fig_waveforms1(multi_wf_list, multi_label_list, time_list, ampl_range=None, log=None, mll_loc=None, panel_grid=None,
                   units=None, colors=None, linestyles=None, markers=None, sharex=True, sharey=False, date_fmt=None,
                   fname=None, figsize=(7,12), hspace=0.05, fill_value=None, xtl_rot=30., xtl_ha='right', xlabel=None, nlocator=None,
                   comment=None, ycmt=0.06, pcmt=0, title='', fontsize=12, show=True, no_ishow=False, close=False):
    """
     multi_wf_list[i].shape =  (nb_pt[i], nb_wf[i]) or just nb_pt[i] (nb_wf[i]=1 implicitely)
     multi_label_list[i] ...
     time_list[i] ...        
     version 1.0: implementation in the following of fig_spectrograms v1.0                                [   février 2020]  
     version 1.1: implementation of log option + other ones in the following of fig_spectrograms v2.0     [15 décembre 2020]     
    """                                        
    nb_panel = len(multi_wf_list)
    fig, axarr = plt.subplots(nb_panel, 1, squeeze=False, figsize=figsize, sharex=sharex, sharey=sharey)
    plt.subplots_adjust(hspace=hspace) 
    if ampl_range == None: 
        ampl_range = [[None, None] for i in range(nb_panel)]                                  
    if units == None: units = nb_panel*['']
    if colors == None: colors = nb_panel*[None]   
    if linestyles == None: linestyles = nb_panel*[None] 
    if markers == None: markers = nb_panel*[None]
    if log == None: log = nb_panel*[False]   
    if mll_loc == None: mll_loc = nb_panel*['best']             
    if panel_grid == None: panel_grid = nb_panel*[False]                    
    axarr[0, 0].set_title(title, fontsize=fontsize)     
    for i, (multi_wf, multi_label, time, multi_color, multi_linestyle, multi_marker) in enumerate(zip(multi_wf_list, multi_label_list, time_list, 
                                                                                        colors, linestyles, markers)):
        if fill_value != None:
            multi_wf2 = multi_wf.astype(float)
            multi_wf2[multi_wf2 < fill_value] = np.nan  
        else:
            multi_wf2 = multi_wf         
        if multi_wf2.ndim == 1: multi_wf2 = multi_wf2[:, None]    
        nb_pt, nb_wf = multi_wf2.shape 
        if multi_color == None: multi_color = nb_wf*[None]        
        if multi_linestyle == None: multi_linestyle = nb_wf*[None] 
        if multi_marker == None: multi_marker = nb_wf*[None]                               
        for j in range(nb_wf):
            axarr[i, 0].plot(time, multi_wf2[:, j], label=multi_label[j], color=multi_color[j], 
                             linestyle=multi_linestyle[j], marker=multi_marker[j], linewidth=2)
            axarr[i, 0].set_ylim(ampl_range[i])  
            axarr[i, 0].set_ylabel(units[i], fontsize=None)
            axarr[i, 0].legend(loc=mll_loc[i]) 
        axarr[i, 0].grid(panel_grid[i])    
        axarr[i, 0].xaxis.set_ticks_position('both')    
        axarr[i, 0].yaxis.set_ticks_position('both') 
        if log[i]: axarr[i, 0].semilogy()          
        if (date_fmt != None) and isinstance(time[0], datetime):
            date_format = mdt.DateFormatter(date_fmt)
            axarr[i, 0].xaxis.set_major_formatter(date_format)           
        if nlocator != None: axarr[i, 0].xaxis.set_major_locator(MultipleLocator(1/nlocator))                       
        if not sharex:
            plt.setp(axarr[i, 0].get_xticklabels(), rotation=xtl_rot, ha=xtl_ha)     
    if isinstance(xlabel, str):              
            axarr[-1, 0].set_xlabel(xlabel, fontsize=fontsize) 
    elif xlabel == True:                     
        if isinstance(time[0], datetime):
            axarr[-1, 0].set_xlabel(str(time_list[-1][len(time_list[-1])//4])[0:10], fontsize=fontsize)                   
        else:
            axarr[-1, 0].set_xlabel('time (s)', fontsize=fontsize)   
    if sharex: fig.autofmt_xdate(rotation=xtl_rot, ha=xtl_ha)
               #plt.xticks(rotation=30, ha=xtl_ha)             
    # saving      
    if fname != None:
        plt.savefig(fname, bbox_inches='tight', pad_inches=0.2)
    if show: plt.show()    
    if close: plt.close(fig)
    if no_ishow: plt.ion()   
    
def fig_waveforms(multi_wf_list, multi_label_list, time_list, ampl_range=None, xrange=None, log=None, mll_loc=None, panel_grid=None,
                  units=None, colors=None, linestyles=None, markers=None, linewidths=None, sharex=True, sharey=False, date_fmt=None,
                  fname=None, figsize=(7,12), hspace=0.05, fill_value=None, xtl_rot=30., xtl_ha='right', xlabel=None, nlocator=None,
                  comment=None, ycmt=0.06, pcmt=0, title='', fs_xlabel=12, fs_ylabel=12,  xticklabel_fs=None, yticklabel_fs=None, 
                  fs_legend=None, fs_title=11,
                  show=True, no_ishow=False, close=False, dpi=None, ncol_legend=None,                  
                  rec_wf_list=None):
    """
     multi_wf_list[i].shape =  (nb_pt[i], nb_wf[i]) or just nb_pt[i] (nb_wf[i]=1 implicitely)
     multi_label_list[i] ...
     time_list[i] ...    
     example:         
     rec_wf_list = [{'ipanel': 1, 'twinx': True, 'log': False, 'ampl_range': [None, None],
                     'mlabel_loc': 'lower right', 'unit': 'cm$^{-3}$',
                     'multi_x': [tnr_datetime[sNfp], bias_datetime[sNsc]], 
                     'multi_y': [Fpe_to_Ne(tnr_fp[sNfp]), bias_ne[sNsc]], 
                     'multi_label': ['Ne Fp Peak', 'Ne S/C Pot'],                 
                     'multi_color': ['blue', 'C3'], 'multi_linestyle': ['--', '-'], 
                     'multi_marker': ['+', ''], 'multi_linewidth': None, 'ncol_legend': 2},
                    {'ipanel': 2, 'twinx': True, 'log':False, 'ampl_range': None,
                     'mlabel_loc': 'lower right', 'unit': 'cm$^{-3}$',
                     'multi_x': [bias_datetime[sNsc]], 
                     'multi_y': [bias_ne[sNsc]], 
                     'multi_label': ['Ne S/C Pot'], 'ncol_legend': None,                
                     'multi_color': None, 'multi_linestyle': None, 
                     'multi_marker': None, 'multi_linewidth': None}]    
     comment = {'ipanel': 0, 'xy': [0.02, 0.95], 'text': 'comment', 'fontsize': 12}                
         
     version 2.0: from version 1.1 implemention of rec_wf_list !                                            [23 janvier 2021]  
     version 2.1: implementation of dpi option                                                              [31 janvier 2021]             
     version 2.2: implementation of comment option                                                          [14 mars 2021]     
     version 2.3: implementation of ncol_legend option and severar fontsize options                         [17 mai 2021]          
    """                                        
    nb_panel = len(multi_wf_list)
    fig, axarr = plt.subplots(nb_panel, 1, squeeze=False, figsize=figsize, sharex=sharex, sharey=sharey)
    plt.subplots_adjust(hspace=hspace) 
    if ampl_range == None: 
        ampl_range = [[None, None] for i in range(nb_panel)]
    if xrange == None: 
        xrange = [[None, None] for i in range(nb_panel)]                                        
    if units == None: units = nb_panel*['']
    if colors == None: colors = nb_panel*[None]   
    if linestyles == None: linestyles = nb_panel*[None] 
    if linewidths == None: linewidths = nb_panel*[None]     
    if markers == None: markers = nb_panel*[None]
    if log == None: log = nb_panel*[False]   
    if mll_loc == None: mll_loc = nb_panel*['best']             
    if panel_grid == None: panel_grid = nb_panel*[False]  
    if ncol_legend == None: ncol_legend = nb_panel*[1]                  
    axarr[0, 0].set_title(title, fontsize=fs_title)     
    for i, (multi_wf, multi_label, time, multi_color, multi_linestyle, multi_marker, multi_linewidth) \
           in enumerate(zip(multi_wf_list, multi_label_list, time_list, colors, linestyles, markers, linewidths)):
        if fill_value != None:
            multi_wf2 = multi_wf.astype(float)
            multi_wf2[multi_wf2 < fill_value] = np.nan  
        else:
            multi_wf2 = multi_wf         
        if multi_wf2.ndim == 1: multi_wf2 = multi_wf2[:, None]    
        nb_pt, nb_wf = multi_wf2.shape 
        if multi_color == None: multi_color = nb_wf*[None]        
        if multi_linestyle == None: multi_linestyle = nb_wf*[None] 
        if multi_marker == None: multi_marker = nb_wf*[None]          
        if multi_linewidth == None: multi_linewidth = nb_wf*[None]                      
        for j in range(nb_wf):
            axarr[i, 0].plot(time, multi_wf2[:, j], label=multi_label[j], color=multi_color[j], 
                             linestyle=multi_linestyle[j], marker=multi_marker[j], linewidth=multi_linewidth[j])
            axarr[i, 0].set_xlim(xrange[i])                   
            axarr[i, 0].set_ylim(ampl_range[i])  
            axarr[i, 0].set_ylabel(units[i], fontsize=fs_ylabel)
            axarr[i, 0].tick_params(axis='x', labelsize=xticklabel_fs)
            axarr[i, 0].tick_params(axis='y', labelsize=yticklabel_fs)
            axarr[i, 0].legend(loc=mll_loc[i], fontsize=fs_legend, ncol=ncol_legend[i]) 
        axarr[i, 0].grid(panel_grid[i])    
        axarr[i, 0].xaxis.set_ticks_position('both')    
        axarr[i, 0].yaxis.set_ticks_position('both') 
        if log[i]: axarr[i, 0].semilogy()          
        if (date_fmt != None) and isinstance(time[0], datetime):
            date_format = mdt.DateFormatter(date_fmt)
            axarr[i, 0].xaxis.set_major_formatter(date_format)           
        if nlocator != None: axarr[i, 0].xaxis.set_major_locator(MultipleLocator(1/nlocator))                       
        if not sharex:
            plt.setp(axarr[i, 0].get_xticklabels(), rotation=xtl_rot, ha=xtl_ha)     
    if isinstance(xlabel, str):              
            axarr[-1, 0].set_xlabel(xlabel, fontsize=fs_xlabel) 
    elif xlabel == True:                     
        if isinstance(time[0], datetime):
            axarr[-1, 0].set_xlabel(str(time_list[-1][len(time_list[-1])//4])[0:10], fontsize=fs_xlabel)                   
        else:
            axarr[-1, 0].set_xlabel('time (s)', fontsize=fs_xlabel)   
    if sharex: fig.autofmt_xdate(rotation=xtl_rot, ha=xtl_ha)
               #plt.xticks(rotation=30, ha=xtl_ha)
    # if other curves to overplot with their own time or with another scale range
    if rec_wf_list != None:
        for rec_wf in rec_wf_list:
            i = rec_wf['ipanel']
            nb_wf = len(rec_wf['multi_x'])     
            multi_color = nb_wf*[None] if rec_wf['multi_color'] == None else rec_wf['multi_color']       
            multi_linestyle = nb_wf*[None] if rec_wf['multi_linestyle'] == None else rec_wf['multi_linestyle'] 
            multi_marker = nb_wf*[None] if rec_wf['multi_marker'] == None else rec_wf['multi_marker']
            multi_linewidth = nb_wf*[None] if rec_wf['multi_linewidth'] == None else rec_wf['multi_linewidth']    
            ncol_rec_wf = 1 if rec_wf['ncol_legend'] == None else rec_wf['ncol_legend']      
            if not rec_wf['twinx']:   
                for j in range(nb_wf):
                    axarr[i, 0].plot(rec_wf['multi_x'][j], rec_wf['multi_y'][j], label=rec_wf['multi_label'][j], color=multi_color[j], 
                                     linestyle=multi_linestyle[j], marker=multi_marker[j], linewidth=multi_linewidth[j])                                 
                axarr[i, 0].legend(loc=rec_wf['mlabel_loc'], fontsize=fs_legend, ncol=ncol_rec_wf)                                                                                          
            else:
                ax2 = axarr[i, 0].twinx()
                for j in range(nb_wf):
                    ax2.plot(rec_wf['multi_x'][j], rec_wf['multi_y'][j], label=rec_wf['multi_label'][j], color=multi_color[j], 
                             linestyle=multi_linestyle[j], marker=multi_marker[j], linewidth=multi_linewidth[j])                                                                                                                  
                ax2.set_ylim([None, None] if rec_wf['ampl_range'] == None else rec_wf['ampl_range'])                  
                if rec_wf['log']: ax2.semilogy()  
                if rec_wf['unit'] != None: ax2.set_ylabel(rec_wf['unit'], fontsize=fs_ylabel)
                ax2.legend(loc=rec_wf['mlabel_loc'], fontsize=fs_legend,ncol=ncol_rec_wf)    
                ax2.tick_params(axis='y', labelsize=yticklabel_fs)
                if (date_fmt != None) and isinstance(rec_wf['multi_x'][0][0], datetime):
                    date_format = mdt.DateFormatter(date_fmt)
                    #axarr[i, 0].xaxis.set_major_formatter(date_format)  c'est pareil a priori ...   
                    ax2.xaxis.set_major_formatter(date_format)     
    # if one comment to be displayed on a given panel
    if comment != None:
        plt.text(comment['xy'][0], comment['xy'][1], comment['text'],
                 horizontalalignment='left', verticalalignment='top', 
                 transform=axarr[comment['ipanel'], 0].transAxes, fontsize=comment['fontsize'])                                                                                                                                                                                                              
    # saving      
    if fname != None:
        default_dpi = plt.rcParams["savefig.dpi"]        
        plt.rcParams["savefig.dpi"] = 'figure' if dpi == None else dpi 
        plt.savefig(fname, bbox_inches='tight', pad_inches=0.2)
        plt.rcParams["savefig.dpi"] = default_dpi
    if show: plt.show()    
    if close: plt.close(fig)
    if no_ishow: plt.ion()       
    
    
    
