#!/bin/bash

# script to get data required to run the tutorial
# Data files are requested using the SOAR TAP API (see http://soar.esac.esa.int/soar/#home)
# This script requires wget tool to work.

# Author: Xavier Bonnin (LESIA, CNRS)
# Date: 14/09/2022


# Define the path of the directory where downloaded file will be saved
DATA_DIR="${PWD}/data"
# Create the directory where data will be downloaded if it doesn't exist
mkdir -p "${DATA_DIR}"

# Build SOAR API
API_URL_TEMPLATE="http://soar.esac.esa.int/soar-sl-tap/data?retrieval_type=LAST_PRODUCT&product_type=SCIENCE&data_item_id="

# List of data products to download
DATA_ITEM_LIST="solo_L2_mag-srf-normal-internal_20220326 solo_L2_rpw-lfr-surv-asm_20220326 solo_L2_rpw-lfr-surv-bp1_20220326 solo_L2_rpw-lfr-surv-bp2_20220326 solo_L2_rpw-lfr-surv-cwf-b_20220326 solo_L2_rpw-lfr-surv-cwf-e_20220326 solo_L2_rpw-lfr-surv-swf-b_20220326 solo_L2_rpw-lfr-surv-swf-e_20220326"

# Loop over the data items we want to download
for DATA_ITEM in $DATA_ITEM_LIST;do
  # Make a web request to receive the data product in this case using wget
  wget -nc --content-disposition -P "${DATA_DIR}" "${API_URL_TEMPLATE}${DATA_ITEM}" --progress=bar:force:noscroll
done

echo "Done"
