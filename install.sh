#!/bin/bash

# Create and add .local/bin to PATH
mkdir -p ~/.local/bin
export PATH=~/.local/bin":$PATH"

# Download, install and load NASA CDF software
if [[ ! -f ./lib/cdf38_1-dist-all.tar.gz ]];then
  CDF_URL="https://spdf.gsfc.nasa.gov/pub/software/cdf/dist/cdf38_1/cdf38_1-dist-all.tar.gz"
  mkdir -p ./lib
  cd ./lib
  wget $CDF_URL
  cd ../
else
  echo "cdf38_1-dist-all.tar.gz found in ./lib/"
fi
cd ./lib
tar -xf cdf38_1-dist-all.tar.gz
cd ./cdf38_1-dist
make OS=linux ENV=gnu CURSES=yes FORTRAN=no UCOPTIONS=-O2 SHARED=yes all
make install
cd ../../
source ./lib/cdf38_1-dist/bin/definitions.B

# Update pip
python3 -m pip install pip -U

# Install the some modules using pip
python3 -m pip install numpy
python3 -m pip install matplotlib
python3 -m pip install spacepy
python3 -m pip install pynverse



# Install jupyter lab and jupyter notebook
python3 -m pip install jupyterlab
python3 -m pip install notebook

echo "Installation completed"
