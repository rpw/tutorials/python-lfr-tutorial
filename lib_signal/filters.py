class TotoError(Exception):
    pass

import numpy as np
from scipy import linalg, signal, optimize

from lib_parsing.parse_routines import is_scalar

def invfreqs(h, w, nB, nA, wf=None, nk=0, equation_type='real', least_squares=False, method='lm'):
    """    
       A routine devoted to be the inverse operation of scipy.signal.freqs. Given 
       the complex frequency response h(w) of an analog filter, it computes its  
       continuous time transfer function in the nB-order numerator and nA-order 
       denominator coefficient vectors `b` and `a` form:        
       
                             nB      nB-1                   nk-1           nk
               B(s)    (b[0]s + b[1]s   + .... + b[nB-nk-1]s   + b[nB-nk])s
       H(s) = ---- = -----------------------------------------------------
                             nA      nA-1              1     
               A(s)     a[0]s + a[1]s + .... + a[nA-1]s + a[nA]

       with a[0]=1 and b[j]=0 for j > nB-nk (i.e., nk trailing zeros), and s = j w. 

       Coefficients are determined by minimizing sum(wf |B-HA|**2).
       Basic equation to be solved : A.H * A * C = A.H * y ...
       See "solving linear least-squares problems and pseudo-inverses":   
       https://docs.scipy.org/doc/scipy/reference/tutorial/linalg.html
              
    Inputs:
       h -- The frequency response.    
       w -- The angular frequencies at which h is defined, in radians/s.

    Outputs:
       b, a --- The numerator and denominator of the linear filter,
                of complex type if equation_type == 'complex'.
    
    Adapated from :
    https://github.com/awesomebytes/parametric_modeling/blob/master/src/invfreqs.py
    
    version 1.0: first version                                 [23 january  2018] 
    version 1.1: implementation of the 'least-squares' option  [ 5 february 2018] 
    version 1.2: fix bug on the weight 'wf' implementation     [ 7 february 2018]
    """
    h = np.atleast_1d(h)
    w = np.atleast_1d(w)
    if is_scalar(wf):
        wf = np.ones_like(w)
    if len(h) != len(w) or len(w) != len(wf):
        raise TotoError("The lengths of h, w and wf must coincide.")
    if np.any(w<0):
        raise TotoError("w has negative values.")
    s = 1j*w     
    nm = np.maximum(nA, nB)
    mV = np.mat( np.vander(s, nm+1, increasing=False) )
    mH = np.mat( np.diag(h) )
    mA = np.mat( np.hstack( (mH*mV[:,-nA:], -mV[:,-nB-1:nm+1-nk]) ) )
    mW = np.mat( np.diag(wf) )
    if equation_type == 'real':
        C = linalg.solve( np.real(mA.H*mW*mA), -np.real(mA.H*mW*mH*mV[:,-nA-1]) )
        dtype = float
    elif equation_type == 'imag':    
        C = linalg.solve( np.imag(mA.H*mW*mA), -np.imag(mA.H*mW*mH*mV[:,-nA-1]) )
        dtype = float
    elif equation_type == 'complex':
        C = linalg.solve( mA.H*mW*mA, -mA.H*mW*mH*mV[:,-nA-1] )
        dtype = complex
    a = np.ones(nA+1, dtype=dtype)    
    a[1:] = C[:nA].flatten()
    b = np.zeros(nB+1, dtype=dtype)         
    b[:nB-nk+1] = C[nA:].flatten()
    if least_squares:
        def fun_real_imag(p):
            _, hh = signal.freqs(p[0:nB+1], p[nB+1:nB+1+nA+1], worN=w)
            dh =  np.ravel( mW * np.mat(hh-h).T )
            return np.hstack([dh.real, dh.imag])
        p1 = np.hstack([b, a])
        res = optimize.least_squares(fun_real_imag, p1, jac='3-point', method=method)
        p2 = res.x / res.x[nB+1]
        b, a = p2[0:nB+1], p2[nB+1:nB+1+nA+1]      
    return b, a        

def invfreqz(h, w, nB, nA, wf=None, nk=0, fs=1., equation_type='real', least_squares=False, method='lm'):
    """    
       A routine devoted to be the inverse operation of scipy.signal.freqz. Given 
       the complex frequency response h(w) of a digital filter, it computes its  
       discret time transfer function in the nB-order numerator and nA-order 
       denominator coefficient vectors `b` and `a` form:   
       
                  jwT          -j(nk)wT     -j(nk+1)wT            -j(nB)wT
          jwT  B(e   )    b[0]e     +  b[1]e   +  ...  + b[nB-nk]e
       H(e) = -------- = -----------------------------------------------
                  jwT                 -jwT              -j(nA-1)wT   -j(nA)wT
               A(e   )    a[0] + a[1]e  + ... + a[nA-1]e     + a[nA]e       
     
       with a[0]=1 and b[j]=0 for j > nB-nk (i.e., nk leading zeros).

       Coefficients are determined by minimizing sum(wf |B-HA|**2).
       Basic equation to be solved : A.H * A * C = A.H * y ...
       See "solving linear least-squares problems and pseudo-inverses":   
       https://docs.scipy.org/doc/scipy/reference/tutorial/linalg.html
              
    Inputs:
       h  -- The frequency response.    
       w  -- The angular frequencies at which h is defined, in radians/s.
       fs -- Sampling frequency, in sample/s

    Outputs:
       b, a --- The numerator and denominator of the linear filter,
                of complex type if equation_type == 'complex'.
    
    Adapated from 'invfreqs' routine
    
    version 1.0: first version                                 [25 january  2018]   
    version 1.1: implementation of the 'least-squares' option  [ 5 february 2018]      
    version 1.2: fix bug on the weight 'wf' implementation     [ 7 february 2018]
    """
    h = np.atleast_1d(h)    
    w = np.atleast_1d(w)
    if is_scalar(wf):
        wf = np.ones_like(w)
    if len(h) != len(w) or len(w) != len(wf):
        raise TotoError("The lengths of h, w and wf must coincide.")
    if np.any(w<0):
        raise TotoError("w has negative values.")
    s = np.exp(-1j*w/fs)     
    nm = np.maximum(nA, nB)
    mV = np.mat( np.vander(s, nm+1, increasing=True) )
    mH = np.mat( np.diag(h) )
    mA = np.mat( np.hstack( (mH*mV[:, 1:nA+1], -mV[:, nk:nB+1]) ) )
    cY = np.mat(h).T
    mW = np.mat( np.diag(wf) )
    if equation_type == 'real':
        C = linalg.solve( np.real(mA.H*mW*mA), -np.real(mA.H*mW*cY) )
        dtype = float
    elif equation_type == 'imag':    
        C = linalg.solve( np.imag(mA.H*mW*mA), -np.imag(mA.H*mW*cY) )
        dtype = float
    elif equation_type == 'complex':
        C = linalg.solve( mA.H*mW*mA, -mA.H*mW*cY )
        dtype = complex
    a = np.ones(nA+1, dtype=dtype)        
    a[1:] = C[:nA].flatten()
    b = np.zeros(nB+1, dtype=dtype)         
    b[nk:] = C[nA:].flatten()
    if least_squares:
        def fun_real_imag(p):
            _, hh = signal.freqz(p[0:nB+1], p[nB+1:nB+1+nA+1], worN=w/fs)
            dh =  np.ravel( mW * np.mat(hh-h).T )
            return np.hstack([dh.real, dh.imag])
        p1 = np.hstack([b, a])
        res = optimize.least_squares(fun_real_imag, p1, jac='3-point', method=method)
        p2 = res.x / res.x[nB+1]
        b, a = p2[0:nB+1], p2[nB+1:nB+1+nA+1]  
    return b, a     
       
def hanning_idl(n):
    return np.sin(np.pi*np.arange(n)/n)**2  

def trapezoid_window(n, nedge, ampl=1.):
    if nedge > n//2:
        raise TotoError("nedge > n//2 !")
    triangle = signal.windows.triang(2*nedge, sym=True)
    trapezoid = np.ones(n) 
    trapezoid[0:nedge] = triangle[0:nedge]
    trapezoid[n-nedge:] = triangle[nedge:]
    return trapezoid if ampl == 1. else ampl * trapezoid

def win_filter(nwin, n1, n2, window_type='rectangular'):    
    win = np.zeros(nwin)    
    i0 =int((nwin-1)/2)
    q = int(nwin/2)    
    if not ( 0 <= n1 <= n2 <= q ):
        raise TotoError("condition 0 <= n1 <= n2 <= q is not matched !")
    id1 = i0 + n1        
    id2 = i0 + n2   
    ig1 = i0 - n1
    # i0 == q means nwin is odd ...
    ig2 = i0 - n2 if (n2 < q) or (i0 == q) else i0 - n2 + 1   
    win[id1:id2+1] = 1.    
    win[ig2:ig1+1] = 1.     
    return win
    
def filtre_passe_bande(data, fs, f1=None, f2=None, fwindow='rectangular', twindow='trapezoid5', 
                       rm_dc=False, echo=False):
    """
    data:     ndarray(n, dim_vector) or ndarray(n)
    fs:       sampling frequency in Herz  
        
    version 1.0: first version adapated from my old IDL routine ...         [2 october 2018]       
    """
    if echo:
        print( 'INPUT' )
        print( 'data: array{}'.format(data.shape) )    

    # get the number of data points
    try:
        n = data.shape[0]
    except AttributeError as e:
        raise AttributeError('data cannot be a scalar...')
    # get the vector dimension and reshape the data to a 2D-array if not the case  ...
    flag_ndimEQ1 = False
    if data.ndim == 2:
        dim_vector = data.shape[1]
    elif data.ndim == 1:
        dim_vector = 1
        data.shape = n, 1
        flag_ndimEQ1 = True
    # sinon on lève une exception
    else:
        raise TotoError('dimension of the data shall be 1 or 2 ...')        
    
    df = fs/n
    if f1 == None: f1 = 0.
    if f2 == None: f2 = df * int(n/2)        
    # 0 <= f1 < f2   
    if not ( 0 <= f1 < f2 ):    
        raise TotoError("condition 0 <= f1 < f2 is not matched !")
    if f2 > df*int(n/2): f2 = df * int(n/2) 
    if echo:
        print('bandpass from %.2f Hz to %.2f Hz'%(f1, f2) )
        
    # transformation into the frequency domain
    """
    REMIND: the forward np.fft.fft and backward np.fft.ifft
    have the opposite sign convention of the exponent than for IDL & PV-Wave:
    q(j) = FFT(+1) = Sum(i=0 to nw-1) of p(i)*Exp(-2Pi I i*j/nw)
    q(j) = FFT(-1) = Sum(i=0 to nw-1) of p(i)*Exp(+2Pi I i*j/nw) / nw
    thus here, with np.fft.fft,  one retains the "positives" frequencies
               with np.fft.ifft, one retains the "negatives" frequencies
    for the NumPy FFT, complex (thus "double precision") is native ...
    """   
    if twindow[0:9] == 'trapezoid':
        percentage_nedge = int(twindow[9:])
        nedge = int(n*percentage_nedge/100)
        window = trapezoid_window(n, nedge, ampl=1.)
    elif twindow == 'hanning':
        window = hanning_idl(n)
    else:
        twindow = False
    if echo:    
        print('twindow =>', twindow)
    if rm_dc: 
        data2 = data - np.mean(data, axis=0)
    else:    
        data2 = data 
    #data = np.zeros((n, dim_vector), dtype=complex)
    #for m in range(dim_vector): 
    #    data[:, m] = np.fft.fft(data2[:, m]*window) if twindow else np.fft.fft(data2[:, m])
    DATA = np.fft.fft(data2.T*window, axis=1).T if twindow else np.fft.fft(data2, axis=0)   
        
    # negative frequencies on the left ! [there are int((n-1)/2) ...]    
    #data = np.concatenate((data[int(n/2)+1:, :], data[0:int(n/2)+1, :]), axis=0)
    DATA = np.roll(DATA, int((n-1)/2), axis=0)
    
    # the frequencies less than f1 and greater than f2 are filtered out    
    DATA = (DATA.T * win_filter(n, int(f1/df+.5), int(f2/df+.5), window_type=fwindow)).T   
    
    # backward tranformamation to the time domain    
    #data = np.concatenate((data[int((n-1)/2):, :], data[0:int((n-1)/2), :]), axis=0)
    DATA = np.roll(DATA, -int((n-1)/2), axis=0)    
    # and restore the original shape if changed ...
    if flag_ndimEQ1:
        data.shape = n                    
    return np.real(np.fft.ifft(DATA, axis=0)).reshape(n) if flag_ndimEQ1 else np.real(np.fft.ifft(DATA, axis=0))
    
           
