# -*- coding: utf-8 -*-

import numpy as np

def wsinterp1(x, xp, yp, left=None, right=None):
    """
    One-dimensional Whittaker-Shannon interpolation   
     
    f(t) = Sum_{n=-inf}^{+inf} f(nT) * sinc[(t-nT)/T] with sinc(x) = sin(pi x)/(pi x)
    
INPUT:        
    x     : array_like (the x-coordinates of the interpolated values)
    xp    : 1-D sequence of floats (x-coordinates of the data points)
    yp    : 1-D sequence of floats (y-coordinates of the data points, same length as xp)
    left  : float, value to return for x < xp[0], default is yp[0]    
    right : float, value to return for x > xp[-1], default is yp[-1]   
         
OUTPUT:    
    y     : float or ndarray (the interpolated values at x, same shape as x)
    
    http://www.diffpy.org/diffpy.utils/api/diffpy.utils.parsers.html#diffpy.utils.parsers.resample.wsinterp
    https://docs.scipy.org/doc/numpy/reference/generated/numpy.interp.html#numpy.interp
        
    version 1.0: first version largely inspired from www.diffpy.org                      [19 december 2016]            
    """    
    scalar = np.isscalar(x)
    if scalar:
        x = np.array(x)
        x.resize(1)
    # nxp copies of x data, shape = (nx, nxp)
    x_nxp = np.resize(x, (len(xp), len(x))).T
    v = (x_nxp - xp) / (xp[1] - xp[0])
    # sum over n (i.e. nxp)
    y_at_x = np.sum(yp * np.sinc(v), axis = 1)
    
    # enforce left and right
    if left is None: left = yp[0]
    y_at_x[x < xp[0]] = left
    if right is None: right = yp[-1]
    y_at_x[x > xp[-1]] = right
    
    # return a float if we got a float
    if scalar: return float(y_at_x)
    
    return y_at_x
    
def wsinterp(x, xp, yp, nwidth=1000, array2D=False, left=None, right=None, echo=True):
    """
    One-dimensional Whittaker-Shannon interpolation   
     
    f(t) = Sum_{n=-inf}^{+inf} f(nT) * sinc[(t-nT)/T] with sinc(x) = sin(pi x)/(pi x)
    
INPUT:        
    x      : array_like (the x-coordinates of the interpolated values)
    xp     : 1-D ndarray of floats (x-coordinates of the data points)
    yp     : 1-D ndarray of floats (y-coordinates of the data points, same length as xp)
    nwidth : finite width used for the summation over n
    left   : float, value to return for x < xp[0], default is yp[0]    
    right  : float, value to return for x > xp[-1], default is yp[-1]   
         
OUTPUT:    
    y     : float or ndarray (the interpolated values at x, same shape as x)
            
    version 1.0: improvement of wsinterp1 (more efficient and consume less memory)     [31 december 2016] 
    version 1.1: implementation without 2-D array (necessitate still less memory ...)  [04 janvier  2016]                
    """    
    scalar = np.isscalar(x)
    if scalar:
        x = np.array(x)
        x.resize(1)
    nx , nxp, dx, xp_start, xp_end = len(x), len(xp), xp[1]-xp[0], xp[0], xp[-1]        
    dx, xp_start, xp_end =  xp[1]-xp[0], xp[0], xp[-1]        

    # nx indexes of closest xp   
    i0xp_nx = np.array([np.argmin(abs(xp - _x)) for _x in x])    
    
    # left and right edges ...
    imin = min(i0xp_nx) - nwidth
    imax = max(i0xp_nx) + nwidth
    if left is None: left = yp[0]
    if right is None: right = yp[-1]
    if imin < 0:
        yp = np.concatenate((np.array(-imin*[left]), yp), axis=0)
        xp = np.concatenate((xp_start+np.arange(imin,0)*dx, xp), axis=0)
        i0xp_nx += -imin
        if echo: print('WARNING (left edge effects): min(i0xp_nx)-nwidth = {} < 0'.format(imin))
    if imax > nxp-1:
        yp = np.concatenate((yp, np.array((imax-nxp+1)*[right])), axis=0)
        xp = np.concatenate((xp, xp_end+np.arange(1,imax-nxp+2)*dx), axis=0)
        if echo: print('WARNING (right edge effects): max(i0xp_nx)+nwidth = {} > nxp-1 = {}'.format(imax, nxp-1))
    
    if array2D:            
        # nx selection of 2*nwidth+1-large (xp, yp) data, shape = (2*nwidth+1, nx)
        xp_nx = np.array([xp[i0-nwidth:i0+nwidth+1] for i0 in i0xp_nx]).T
        yp_nx = np.array([yp[i0-nwidth:i0+nwidth+1] for i0 in i0xp_nx]).T       
        v = (x - xp_nx) / dx
        # sum over n (i.e. 2*nwidth+1)
        y_at_x = np.sum(yp_nx * np.sinc(v), axis = 0)
    else:    
        # same but directly (without huge 2-D array)
        y_at_x = np.array([np.sum(yp[i0-nwidth:i0+nwidth+1] * np.sinc((_x - xp[i0-nwidth:i0+nwidth+1]) / dx)) for _x, i0 in zip(x, i0xp_nx)])

    # enforce left and right
    out_left = x < xp_start
    out_right = x > xp_end
    if echo and (True in out_left) : print('WARNING (out of bounds!): on left ...')
    if echo and (True in out_right): print('WARNING (out of bounds!): on right ...')
    y_at_x[out_left] = left
    y_at_x[out_right] = right
    
    # return a float if we got a float
    if scalar: return float(y_at_x)
    
    return y_at_x
    
        
