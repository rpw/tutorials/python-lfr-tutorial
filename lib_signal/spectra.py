# -*- coding: utf-8 -*-

import numpy as np

class TotoError(Exception):
    pass

def computeFFT(wf, fs=1., window=False, normalized_with_amplitude=True, removeMean=False, deg=True):
    """
    CAUTION: the forward np.fft.fft and backward np.fft.ifft
    have the opposite sign convention of the exponent than for IDL & PV-Wave:
    q(j) = FFT(+1) = Sum(i=0 to nw-1) of p(i)*Exp(-2Pi I i*j/nw)
    q(j) = FFT(-1) = Sum(i=0 to nw-1) of p(i)*Exp(+2Pi I i*j/nw) / nw
    thus here, with np.fft.fft,  one retains the "positives" frequencies
               with np.fft.ifft, one retains the "negatives" frequencies
    for the NumPy FFT, complex (thus "double precision") is native ...
    """
    def hanning_idl(n):
        return np.sin(np.pi*np.arange(n)/n)**2    
    n = len(wf)
    if removeMean: wf = wf - np.mean(wf)
    if window: fft = np.fft.fft(wf*hanning_idl(n)) / n * 2 if normalized_with_amplitude else \
                     np.fft.fft(wf*hanning_idl(n)) / n * np.sqrt((8./3))
    else: fft = np.fft.fft(wf) / n
    freq = np.fft.fftfreq(n, 1/fs)
    freq[int(n/2)] = abs(freq[int(n/2)])
    return [freq[0:int(n/2)+1], abs(fft[0:int(n/2)+1]), np.angle(fft[0:int(n/2)+1], deg=deg)]
    
def hanning_idl(n):
    return np.sin(np.pi*np.arange(n)/n)**2

def spectral_matrices(data, fs=1., nw=None, na=None, nsa=None, nsc=None, \
                       win=None, rm_dc=False, fft_dir=+1, echo=True):
    """
    Routine that compute time-averaged spectral matrices from vectorial time series data

INPUT:
    data:     ndarray(n, dim_vector) or ndarray(n)
    fs:       sampling frequency in Herz
    nw:       window size of the elementary fft's
    na:       number of fft's to be averaged
    nsa:      shift between the fft windows to be averaged
    nsc:      shift between the "cells" of averaged spectral matrices
    win:      window specification for apodisation
    rm_dc:    True for removing the DC component before fft
    fft_dir:  forward (+1) or backward (-1) fft
    echo:     True if echo, False otherwise

OUTPUT:
    asm:      time-averaged spectral matrices [ndarray(nspec, nfreq, dim_vector, dim_vector)]
    time_asm: their associated time           [ndarray(nspec, 3)]

    {'asm': asm, 'time': time_asm}

    version 1.0: first version with Python (!) from the last version of the idl routine     [5-10 janvier 2016]

    version 1.1: slight improvement of performance                                          [12 janvier 2016]
                 by replacing
                 " asm_cell[:,:,l] = asm_cell[:,:,l] + np.dot( q[:,l].reshape(dim_vector,1), \
                                                       np.conj(q[:,l]).reshape(1,dim_vector) )  "
                 by
                 " q_l = q[:,l:l+1].copy()
                   asm_cell[...,l] = asm_cell[...,l] + np.dot( q_l, np.conj(q_l.T) ) "

    version 1.2: another slight improvement of performance                                  [13 janvier 2016]
                 by replacing last change by
                 " q_l = q[l:l+1, :]
                   asm_cell[l, ...] += np.dot( q_l.T, np.conj(q_l) ) "
                 and better implementation of the windowing and the substraction of DC
    version 1.3  python3 compatibility ...                                                  [19 mars 2016]
    version 1.4  change noecho (int) to echo (boolean) + rm_dc from int to boolean          [28 mars 2016]
    version 1.5: add TotoError Exception class                                              [13 avril 2016]
    """

    if echo:
        print( "Compute time-averaged spectral matrices")
        print( 'INPUT' )
        print( ' data: array{}'.format(data.shape) )

    # get the number of data points
    try:
        n = data.shape[0]
    except AttributeError as e:
        raise AttributeError('data cannot be a scalar...')
    # get the vector dimension and reshape the data to a 2D-array if not the case  ...
    flag_ndimEQ1 = False
    if data.ndim == 2:
        dim_vector = data.shape[1]
    elif data.ndim == 1:
        dim_vector = 1
        data.shape = n, 1
        flag_ndimEQ1 = True
    # sinon on lève une exception
    else:
        raise TotoError('dimension of the data shall be 1 or 2 ...')

    # when the data are of integer type they shall be converted into float ...
    if data.dtype.name[0:3] == 'int':
        dtype = float
    elif data.dtype.name[0:5] == 'float':
        dtype = float
    elif data.dtype.name[0:7] == 'complex':
        dtype = complex
    else:
        raise TotoError('unknown type of data ?!')

    # determine the number of time-averaged spectral matrices ("cells")
    if nw == None:
        nw  = n
    if na == None:
        na  = 1
    if nsa == None:
        nsa  = nw
    # size of one "cell" = nw + (na-1)*nsa
    if (nw + (na-1)*nsa) > n:
        raise TotoError('inconsistent parameters: no spectra computed')
    if nsc == None:
        nsc  = nw + (na-1)*nsa
    # number of "cells"
    nspec = int(1 + (n - nw - (na-1)*nsa)/nsc)
    # n - nsc < nw + (na-1)*nsa + (nspec-1)*nsc =< n

    if echo:
        print( ' nw: {}\n na: {}\n nsa: {}\n nsc: {}'.format(nw, na, nsa, nsc) )

    # define the dimension of the outputs
    nfreq = int(nw/2+1)  
      # number of frequencies retained from the nw-point fft (q = FFT(p))
      # here, one selects the first half of the available bins including the bin f=0
      # since, for real signals (p), one has conjugate(q(j)) = q(nfreq-j)
      # for complex signals its still possible to get the other bins
      # by combining the forward (+1) and backward (-1) fft ...
    asm = np.zeros((nspec, nfreq, dim_vector, dim_vector), dtype=complex)
    time_asm  = np.zeros((nspec, 3), dtype=float)

    if echo:
        print( 'OUPUT' )
        print( ' asm: array{}'.format(asm.shape) )
        print( ' time_asm: array{}'.format(time_asm.shape) )

    # prepare the implementation of a window if asked
    if win == None:
        pass
    elif win == 'hanning':
        win_values = np.hanning(nw)
    elif win == 'hanning_idl':
        win_values = hanning_idl(nw)
    else:
        raise TotoError('this window is not implemented ...')

    # for each cell
    for j in range(nspec):
        asm_cell = np.zeros((nfreq, dim_vector, dim_vector), dtype=complex)
        q = np.zeros((nfreq, dim_vector), dtype=complex)

        # compute one time-averaged (windowed) spectral matrix
        for k in range(na):
            p = data[(j*nsc)+(k*nsa):(j*nsc)+(k*nsa)+nw,:].astype(dtype, copy=True)
            # remove DC value if asked
            if rm_dc:
                for m in range(dim_vector):
                    p[:, m] -= np.sum(p[:, m])/ nw
            # implement a window if asked
            if win != None:
                for m in range(dim_vector):
                    p[:, m] *= win_values
            # CAUTION: the forward np.fft.fft and backward np.fft.ifft
            # have the opposite sign convention of the exponent than for IDL & PV-Wave:
            # q(j) = FFT(+1) = Sum(i=0 to nw-1) of p(i)*Exp(-2Pi I i*j/nw)
            # q(j) = FFT(-1) = Sum(i=0 to nw-1) of p(i)*Exp(+2Pi I i*j/nw) / nw
            # thus here, with fft_dir=+1, one retains the "positives" frequencies
            #            with fft_dir=-1, one retains the "negatives" frequencies
            # for the NumPy FFT, complex (thus "double precision") is native ...
            for m in range(dim_vector):
                if fft_dir == -1:
                    spoon = np.fft.ifft(p[:, m])
                elif fft_dir == +1:
                    spoon = np.fft.fft(p[:, m])
                else:
                    raise TotoError('forward or backward only ...')
                q[:, m] = spoon[0:nfreq]
            # column-row matrix multiplication for each frequency & sum for averaging
            for l in range(nfreq):
                q_l = q[l:l+1, :]
                asm_cell[l, ...] += np.dot( q_l.T, np.conj(q_l) )

        # normalisation of the power spectra
        if fft_dir == +1:                           # FFT(+) => q_continous = q_discrete / fs
           asm[j, ...] = asm_cell / na / (fs*nw)    # psd = |q_continuous|^2 / T with T = nw/fs
                                                    #  =>  psd = |q_discrete|^2 / (fs*nw)
                                                    # <=>  psd = |q_discrete|^2 / nw^2 / df
        if fft_dir == -1:                           # FFT(-) => q_continous = q_discrete * nw/fs
           asm[j, ...] = asm_cell / na / (fs/nw)    # psd = |q_continuous|^2 / T with T = nw/fs
                                                    #  =>  psd = |q_discrete|^2 / (fs/nw)
                                                    # <=>  psd = |q_discrete|^2 / df

        # time of the asm
        time_asm[j, 0] =  j*nsc / fs                                # time of the first sample
        time_asm[j, 1] = ( (j*nsc)+((na-1)*nsa)+nw-1 ) / fs         # time of the last sample
        time_asm[j, 2] = ( time_asm[j, 0] + time_asm[j, 1] ) / 2.   # mean time

    # restore the original shape if ...
    if flag_ndimEQ1:
        data.shape = n

    return {'asm': asm, 'time': time_asm} 

def FT_w1(f, fe=1., N0=256, a=0.5):
    """
OUTPUT  : theoretical Fourier Transform of the rectangular window function w1

INPUT   
f       : frequency of the computed FT (in unit of fe)
NO      : width in unit of Te of the rectangular function ( w1(t) = 1 if -a*Te <= t < -a*Te + N0*Te )
                                                          ( w1(t) = 0 otherwise                     )
a       : parameter for time delay                        ( where            0 <= a < 1             )   
fe      : sampling frequency equal to 1 by default
          (accordingly sampling period Te equal to 1 by default)
    """
    ampl = N0/fe
    sinc = np.sin(np.pi*f*N0) / (np.pi*f*N0) if np.all(f != 0) else 1.
    phase = -np.pi*f*(N0-2*a)
    exp = np.exp(1j*phase)
    return ampl * sinc * exp          
        
def X_FFT_sine_w(FFT_k, k, FT_w, N=256, fe=1., fi=0.25, a=0.5, nmax=0):
    """
OUTPUT  : X = A*exp(j*phi), the complex amplitude of a sine function of frequency fi 
                            deduced from its windowed FFT value at frequency index k
INPUT
k         : frequency index of the computed FTT (for k/N in unit of fe)
N         : number of points of the FFT 
fi        : frequency parameter of the sine function (in unit of fe)  
p         : binary parameter (+1 or -1) for selecting the positive or negative frequency part
FT_w      : theoretical Fourier Transform of a window function w
a         : parameter for time delay of the window
fe        : sampling frequency equal to 1 by default
           (accordingly sampling period Te equal to 1 by default)
    """
    def Wp_FFT_sine_w(k, p, FT_w, N=256, fe=1., fi=0.25, a=0.5, nmax=0):
        """
        OUTPUT  : generic W+/- function defined from the theoretical FFT of a windowed sine function
          [ W+_k = fe/N * SUM_over_n{ 1/2j * W(k*fe/N-fi-n*fe) } where W = FT(w) ]
          [ W-_k = fe/N * SUM_over_n{ 1/2j * W(k*fe/N+fi-n*fe) } where W = FT(w) ]
        """          
        Wp_k = FT_w(k/N-p*fi, fe=fe, N0=N, a=a)
        for n in range(1, nmax+1): 
            Wp_k += FT_w(k/N-p*fi-n, fe=fe, N0=N, a=a) + FT_w(k/N-p*fi+n, fe=fe, N0=N, a=a) 
        return Wp_k * fe/N / 2j
    Wpositive = Wp_FFT_sine_w(k, +1, FT_w, N=N, fe=fe, fi=fi, a=a, nmax=nmax)
    Wnegative = Wp_FFT_sine_w(k, -1, FT_w, N=N, fe=fe, fi=fi, a=a, nmax=nmax)    
    return ( (FFT_k*np.conjugate(Wpositive) + np.conjugate(FFT_k)*Wnegative) / 
             (abs(Wpositive)**2 - abs(Wnegative)**2) )    

