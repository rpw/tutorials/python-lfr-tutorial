# -*- coding: utf-8 -*-

import numpy as np

from numpy.linalg import norm
from pynverse import inversefunc

### Contantes de base ###
"""
National Institute of Standards and Technology: http://physics.nist.gov/cuu/Constants/index.html
CODATA Internationally recommended 2014 values of the Fundamental Physical Constants (12/11/2018)
"""

m  = 9.10938356e-31           # kg               electron mass
M  = 1.672621898e-27          # kg               proton mass
e  = 1.6021766208e-19         # C                elementary charge    
c  = 299792458                # m/s              speed of light in vacuum
kB = 1.38064852e-23           # J/K              Boltzmann constant
G  = 6.67408e-11              # m^3/kg/s^2       Newtonian constant of gravitation

mu = M/m                      #                  proton-electron mass ratio 

mu0 = 4*np.pi*1e-7            # N/A^2            magnetic constant
epsi0 = 1/(mu0*c*c)           # F/m              electric constant

### Paramètres plasma ####

def plasma_parameters(B=1, n=1, Ti=1, Te=1, nT=True, cm3=True, eV=True, format='10.2f', echo=True):
    """
    wci = e B / M      =>    fci [Hz] ~ 0.01525 B [nT]  ~ B / 66
    wce = e B / m      =>    fce [Hz] ~ 27.99 B [nT]  
    1 Gauss = 10^-4 Tesla
    M/m ~ 1836.15
    
    wpe = (n e^2 / epsi0 m)^1/2   => fpe [Hz] ~ 8.98 (ne)^1/2 [m-3]  = 8979 (ne)^1/2 [cm-3 ]  
    wpi = Wpe (m/M)^1/2
    (M/m)^1/2 ~ 42.85
    
    vthi = (kB Ti / M)^1/2        => vthi [m/s] ~ 9.8 10^3 (Ti)^1/2 [eV]    
    vthe = (kB Te / m)^1/2        => vthe [m/s] ~ 4.2 10^5 (Te)^1/2 [eV]
    1 eV ~ 11605 K * kB
    cs   = (kB Te / M)^1/2        => cs = vthe / (M/m)^1/2
    
    vAlf = B /(μ0 n M)^1/2        => vAlf [m/s] ~ 21.8 10^3 B/(n)^1/2 [nT/cm-3/2]
         = c wci / wpi
    
    rho_i = vthi / wci
    rho_e = vthe / wce
    
    lambda_i = c / wpi
    lambda_e = c / wpe    
    
    lambda_D = vthe / wpe = cs / wpi = smallest IAW scale
    
    beta_i = (n kB Ti) / (B^2 / 2μ0) = 2 * (vthi / vAlf)^2
    beta_e = (n kB Te) / (B^2 / 2μ0) = 2 * (vthe / vAlf)^2 * m/M
    """
    fci = e * (B*1e-9 if nT else B) / M / (2*np.pi)
    fce = fci * mu
    
    fpe = np.sqrt((n*1e6 if cm3 else n) * e**2 / epsi0 / m) / (2*np.pi)
    fpi = fpe * np.sqrt(m/M)

    vthi = np.sqrt((Ti*e if eV else kB*Ti) / M) 
    vthe = np.sqrt((Te*e if eV else kB*Te) / m) 
    cs   = np.sqrt((Te*e if eV else kB*Te) / M) 
    vAlf = (B*1e-9 if nT else B) / np.sqrt(mu0*(n*1e6 if cm3 else n)*M)
    
    rhoi = vthi / (2*np.pi*fci) 
    rhoe = vthe / (2*np.pi*fce)     
    lbdi = c / (2*np.pi*fpi) 
    lbde = c / (2*np.pi*fpe)   
    lbdD = vthe / (2*np.pi*fpe)   
    
    betai = 2*(vthi/vAlf)**2  
    betae = 2*(vthe/vAlf)**2 / mu 
    
    if echo:
        print('INPUTS:')
        print('Magnetic field intensity     (B) = %15.2f %s'%(B, 'nT' if nT else 'T'))
        print('Plasma density               (n) = %15.2f %s'%(n, '1/cm^3' if cm3 else '1/m^3'))
        print('Proton temperature          (Ti) = %15.2f %s'%(Ti, 'eV' if eV else 'K'))
        print('Electron temperature        (Te) = %15.2f %s'%(Te, 'eV' if eV else 'K'))
        
        
        print()
        print('OUTPUTS:')
        print(('Proton gyrofrequency       (fci) = %'+format+' Hz')%(fci))
        print(('Electron gyrofrequency     (fce) = %'+format+' Hz')%(fce))
        print(('Proton plama frequency     (fpi) = %'+format+' Hz')%(fpi))
        print(('Electron plasma frequency  (fpe) = %'+format+' Hz')%(fpe))
        print(('Proton thermal velocity   (vthi) = %'+format+' m/s')%(vthi))
        print(('Electron thermal velocity (vthe) = %'+format+' m/s')%(vthe))
        print(('Sound speed                 (cs) = %'+format+' m/s')%(cs))        
        print(('Alfvén velocity           (vAlf) = %'+format+' m/s')%(vAlf))
        print(('Proton Larmor radius      (rhoi) = %'+format+' m')%(rhoi))
        print(('Electron Larmor radius    (rhoe) = %'+format+' m')%(rhoe))
        print(('Proton inertial length    (lbdi) = %'+format+' m')%(lbdi))
        print(('Electron inertial length  (lbde) = %'+format+' m')%(lbde))
        print(('Debye length              (lbdD) = %'+format+' m')%(lbdD))
        print(('Proton beta              (betai) = %'+format+'  ')%(betai))
        print(('Electron beta             (betae) = %'+format+'  ')%(betae))
        
    return {'inputs':  {'B': B, 'n': n, 'Ti': Ti, 'Te': Te,
                        'nT': nT, 'cm3': cm3, 'eV': eV},
            'outputs': {'fci': fci,
                        'fce': fce,
                        'fpi': fpi,
                        'fpe': fpe,
                        'vthi': vthi,
                        'vthe': vthe,
                          'cs': cs,
                        'vAlf': vAlf,
                        'rhoi': rhoi,
                        'rhoe': rhoe,
                        'lbdi': lbdi,
                        'lbde': lbde,
                        'lbdD': lbdD,
                        'betai': betai,
                        'betae': betae}
           }                   
                        
### Relation de dispersion d'un magnetoplasma froid homogène et plus ####

def cold_plasma_params(B0=1e-9, 
                       n0=1e6, 
                       ns=[1., 1., 0., 0., 0.], 
                       qs=[-1., +1., +2., 0., 0.], 
                       ms=[1./mu, 1., 4., 16., 32.],
                       echo=False): 
    """
INPUT    
    B0: large scale static magnetic field (T)
    n0: large scale static plasma density (m-3)
    ns[0:nspecies]: percentage of each species with respect to n0
                    population 0 : e-
                    population 1 : H+
                    population 2 : He++
                    population 3 : ...
                    population 4 : ... 
    qs[0:nspecies]: signed charge number of each species in unit of e                    
    ms[0:nspecies]: mass number of each species in unit of M           
OUTPUT
    Wps[0:nspecies]: plasma frequency for each species (rad/s)
    Wcs[0:nspecies]: gyrofrequency for each species (rad/s)                                                   
    """    
    Ns = n0 * np.array(ns) 
    Qs =  e * np.array(qs) 
    Ms =  M * np.array(ms)             
    Wps = np.sqrt(Ns * Qs**2 / Ms / epsi0)
    Wcs = -Qs * B0 / Ms      # convention opposée à celle de Stix (et de Le Quéau) ! ...                   
    if echo:
        print("charge neutrality: %g n0 e"%(np.sum(np.array(qs)*np.array(ns))))
        print()
        fpe = Wps[0] / (2*np.pi)
        fce = Wcs[0] / (2*np.pi)
        vAlf = c * fce / fpe / np.sqrt(mu)
        print("fce:     %10.2f Hz"%fce)
        print("fpe:     %10.2f Hz"%fpe)        
        print("fpe/fce: %10.2f"%(fpe/fce))
        print("vAlf:    %10.2f m/s"%(vAlf))
        print()
        print("fréquences de coupure (N=0):")
        fR = np.sqrt(fce**2/4 + fpe**2) + fpe/2
        fL = np.sqrt(fce**2/4 + fpe**2) - fpe/2       
        print("fR:      %10.2f Hz"%fR)
        print("fL:      %10.2f Hz"%fL)         
    return Wps, Wcs
    
def cold_plasma_dispersion_stuff(Wps, Wcs):
    """
Stix et Le Quéau: solutions en exp[i(k.r-wt)]

arg(Ey/Ex) dans ]0°, +180°[ indique un polarisation Droite (sens indirect)
arg(Ey/Ex) dans ]-180°, 0°[ indique un polarisation Gauche (sens direct)

x + i y tourne à droite (composante E+ = Ex-iEy)
x - i y tourne à gauche (composante E- = Ex+iEy)
|E+| > |E-| <=> polarisation Droite
|E+| < |E-| <=> polarisation Gauche
|E+| = |E-| <=> polarisation rectiligne

mais ATTENTION: lors de l'analyse de données, la FFT utilisée en général décompose 
sur les fréquences positives, c-à-d sur des exp[i(wt-k.r)]. Il faut donc inverser  ....
    """
    def R(w):
        return 1 - np.sum(Wps**2 / (w - Wcs) / w)            
    def L(w):
        return 1 - np.sum(Wps**2 / (w + Wcs) / w)    
    def P(w):
        return 1 - np.sum(Wps**2 / w*2)    
    def S(w):
        return (R(w) + L(w)) / 2    
    def D(w):
        return (R(w) - L(w)) / 2
    
    def A(w, theta):
        return S(w)*np.sin(theta)**2 + P(w)*np.cos(theta)**2    
    def B(w, theta):
        return R(w)*L(w)*np.sin(theta)**2 + P(w)*S(w)*(1+np.cos(theta)**2)    
    def C(w):
        return P(w)*R(w)*L(w)    
    def F(w, theta):
        return np.sqrt( ((R(w)*L(w)-P(w)*S(w))**2 * np.sin(theta)**4) + (2*P(w)*D(w)*np.cos(theta))**2 )      
    def N2plus(w, theta):
        return (B(w, theta) + F(w, theta)) / (2*A(w, theta))     
    def N2moins(w, theta):
        return (B(w, theta) - F(w, theta)) / (2*A(w, theta))     
    
    def Ey_over_Ex(w, theta, N2):
        return 1j*D(w) / (N2(w, theta) - S(w))        
    def Eplus_over_Emoins(w, theta, N2):
        return (N2(w, theta) - L(w)) / (N2(w, theta) - R(w))    
    def By_over_Bx(w, theta, N2):
        return P(w)*(N2(w, theta) - S(w)) /  (1j*D(w)*(N2(w, theta)*np.sin(theta)**2 - P(w)))    
    def Bplus_over_Bmoins(w, theta, N2):
        return (L(w) - N2(w, theta) + D(w)*N2(w, theta)*np.sin(theta)**2/P(w)) / \
               (N2(w, theta) - R(w) + D(w)*N2(w, theta)*np.sin(theta)**2/P(w))    
    
    def Ez_over_Ex(w, theta, N2):
        return  N2(w, theta)*np.sin(theta)*np.cos(theta) / (N2(w, theta)*np.sin(theta)**2 - P(w))      
    def Ez_over_Ey(w, theta, N2):    
        return  N2(w, theta)*np.sin(theta)*np.cos(theta)*(N2(w, theta) - S(w)) / \
                (1j*D(w)*(N2(w, theta)*np.sin(theta)**2 - P(w)))    
    def Ez_over_Exy(w, theta, N2):
        return  N2(w, theta)*np.sin(theta)*np.cos(theta) / np.abs(N2(w, theta)*np.sin(theta)**2 - P(w)) / \
                np.sqrt(1+ D(w)**2/(N2(w, theta) - S(w))**2)    
    def Ez_over_Exy_bis(w, theta, N2):
        return  N2(w, theta)*np.sin(theta)*np.cos(theta)*np.sqrt(2)*np.abs(N2(w, theta) - S(w)) / \
                np.abs(N2(w, theta)*np.sin(theta)**2 - P(w)) / \
                np.sqrt((N2(w, theta) - R(w))**2 + (N2(w, theta) - L(w))**2)    
                    
    def Epara_over_Eperp(w, theta, N2):
        return  np.abs(np.sin(theta)*(N2(w, theta) - P(w))*(N2(w, theta) - S(w))) / \
                np.sqrt((P(w)*np.cos(theta)*(N2(w, theta) - S(w)))**2 + \
                        (D(w)*(N2(w, theta)*np.sin(theta)**2 - P(w)))**2)
    def Ey_over_Eperpx(w, theta, N2):
        return 1j*D(w)*(N2(w, theta)*np.sin(theta)**2 - P(w)) / (N2(w, theta) - S(w)) / (-P(w)*np.cos(theta))    
    def By_over_Bperpx(w, theta, N2):
        return -1/Ey_over_Eperpx(w, theta, N2)    
        
    return {"N2plus": N2plus, 
            "N2moins": N2moins,
            "Ey_over_Ex": Ey_over_Ex,            
            "By_over_Bx": By_over_Bx,
            "Eplus_over_Emoins": Eplus_over_Emoins,
            "Bplus_over_Bmoins": Bplus_over_Bmoins,
            "Ez_over_Exy": Ez_over_Exy,
            "Epara_over_Eperp": Epara_over_Eperp,
            "Ey_over_Eperpx": Ey_over_Eperpx,
            "By_over_Bperpx": By_over_Bperpx}

def f_S0_whistler(f_SW, theta_kB0=10., N2moins=None, Vsw=[-350., 0., 0.], kvec=[-1.,-1.,-1.], echo=False):
    """
    Given the frequency of a whistler mode wave expressed in the solar wind frame, and its angle of 
    propagation with respect to B0, computes the wave frequency in the spacecraft frame (here Solar 
    Orbiter) using the cold plasma dispersion relation for estimating the doppler shift.
INPUT:    
    f_SW:       wave frequency in the solar wind frame (Hz)
    theta_kB0:  angle between the wave vector k and the local large scale magnetic field B0 (degree) 
    N2moins:    a function giving the square of the refractive index of the whistler mode wave          
    Vsw:        solar wind velocity vector relative to S0 expressed in a given reference frame (km/s)
    kvec:       vector parallel to the wave vector k given in the same reference frame as Vsw

OUTPUT:  
    f_S0:       wave frequency in the SO frame (Hz)
    """
    vphi = c/np.sqrt(N2moins(2*np.pi*f_SW, np.deg2rad(theta_kB0))) / 1e3
    cos = np.inner(kvec, Vsw)/norm(kvec)/norm(Vsw)
    if echo:
        print('vphi (in SW frame):   %.1f km/s'%vphi)
        print('Vsw  (in SO frame): [{:.1f}, {:.1f}, {:.1f}] km/s (SRF)'.format(*Vsw))
        print('(k/|k|).Vsw/vphi:     %.2f'%(cos*norm(Vsw)/vphi))
        print('f_SW:                                %7.2f Hz'%(f_SW))
        print('f_S0 = f_SW * (1 +(k/|k|).Vsw/vphi): %7.2f Hz'%(f_SW*(1 + cos*norm(Vsw)/vphi)))
    return f_SW*(1 + cos*norm(Vsw)/vphi)

def inverse_f_SO_whistler(theta_kB0, N2moins, Vsw, kvec, domain=[0,None], open_domain=True):
    return inversefunc(f_S0_whistler, args=(theta_kB0, N2moins, Vsw, kvec), domain=domain, open_domain=open_domain)

def f_SW_whistler(f_S0, theta_kB0=10., N2moins=None, Vsw=[-350., 0., 0.], kvec=[-1., -1., -1.], domain=[0,None], open_domain=True):
    return inverse_f_SO_whistler(theta_kB0, N2moins, Vsw, kvec, domain=domain, open_domain=open_domain)(f_S0)    
                         
def vphi_S0_whistler(f_S0, theta_kB0=10., N2moins=None, Vsw=[-350., 0., 0.], kvec=[-1.,-1.,-1.], 
                     domain=[0,None], open_domain=True):    
    """
    Phase velocity of the whistler mode wave in the S/C frame (km/s). See comments of "f_S0_whistler".
    """                     
    f_SW = f_SW_whistler(f_S0, theta_kB0=theta_kB0, N2moins=N2moins, Vsw=Vsw, kvec=kvec,
                         domain=domain, open_domain=open_domain)   
    vphi_SW_whistler = c / np.sqrt(N2moins(2*np.pi*f_SW, np.deg2rad(theta_kB0))) / 1000. 
    return f_S0/f_SW *  vphi_SW_whistler    

def vphi_SW_whistler(f_SW, theta_kB0, N2moins):
    """
    Phase velocity of the whistler mode wave in the plasma frame (km/s). See comments of "f_S0_whistler".
    """  
    return c/np.sqrt(N2moins(2*np.pi*f_SW, np.deg2rad(theta_kB0))) / 1e3
    
def vphi_whistler_para_LF(f, wpe, wce):
    """
    Gives the phase velocity w/k of a whistler mode wave for parallel propagation 
    using a low-frequency approximation (Ichimaru p. 97): w ~ wce (c*k/wpe)^2
    => w/k ~ c/wpe * sqrt(wce*w) = sqrt(w/wce) * sqrt(M/m) * vAlf
    """
    return c/wpe * np.sqrt(wce*2*np.pi*f)    



