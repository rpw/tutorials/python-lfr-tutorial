# -*- coding: utf-8 -*-

import numpy as np 

def is_scalar(var):
    return var.ndim == 0 if isinstance(var, np.ndarray) else not isinstance(var, (tuple, list))  
    
def is_iterable(var):
    return hasattr(var, '__iter__') if not isinstance(var, np.ndarray) else var.ndim > 0    

"""
http://stackoverflow.com/questions/16807011/python-how-to-identify-if-a-variable-is-an-array-or-a-scalar

var.ndim
var.shape
var.size
type(var).__name__
np.isscalar(var)

import collections
import numpy as np
isinstance(P, (collections.Sequence, np.ndarray))

if hasattr(N, "__len__")
"""
