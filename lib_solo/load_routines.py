# -*- coding: utf-8 -*-

import os

import numpy as np
from spacepy import pycdf
import bisect

from lib_lfr.load_routines import print_cdf

############### Solar Orbiter CDF files #######################

def load_mag_CDF_L2(file, t1=None, t2=None, raw_Epoch=False, echo=False, key_attrs=None, key_field=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)
    # find leftmost Epoch greater than or equal to t1 (t1 <= a[i1])
    # find rightmost Epoch less than or equal to t2 (a[i2-1] <= t2)  
    i1 = 0 if t1 == None else bisect.bisect_left(cdf['EPOCH'], t1)
    i2 = len(cdf['EPOCH']) if t2 == None else bisect.bisect_right(cdf['EPOCH'], t2)        
    if echo:
        print_cdf(cdf, key_attrs=key_attrs, key_field=key_field)        
        print("start:", cdf["EPOCH"][i1])
        print("stop: ", cdf["EPOCH"][i2-1]) 
        print("=> %d elements"%(i2-i1))                              
    bvec = np.array(cdf["B_SRF"][i1:i2,0:3])
    time = np.array(cdf.raw_var("EPOCH")[i1:i2])
    if echo:
        print('Sampling frequency:', 1/np.median(np.gradient(1e-9*time))) 
    cdf.close()
    return bvec, time if raw_Epoch else pycdf.lib.v_tt2000_to_datetime(time)

############### Solar Orbiter ASCII CDPP/AMDA files #######################
    
def load_pas_v(file):
    """
    Velocity moment from SWA/PAS
    PARAMETER_ID : pas_momgr_v
    PARAMETER_NAME : pas_momgr_v
    PARAMETER_SHORT_NAME : v_s/c
    PARAMETER_COMPONENTS : vx,vy,vz
    PARAMETER_UNITS : km/s
    """
    return np.genfromtxt(file, comments='#', delimiter=None, 
                         dtype='S23,f8,f8,f8',
                         names=['time', 'vx', 'vy', 'vz'])          

def load_pas_n(file):
    """
    Density moment from SWA/PAS
    PARAMETER_ID : pas_momgr_n
    PARAMETER_NAME : pas_momgr_n
    PARAMETER_SHORT_NAME : density
    PARAMETER_UNITS : cm-3    
    """
    return np.genfromtxt(file, comments='#', delimiter=None, 
                         dtype='S23,f8',
                         names=['time', 'n',])    
                         
  
