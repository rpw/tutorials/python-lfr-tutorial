# -*- coding: utf-8 -*-

LFR_Fs0 = 24576.
LFR_Fs1 = 4096.
LFR_Fs2 = 256.
LFR_Fs3 = 16.
LFR_Fs = [LFR_Fs0, LFR_Fs1, LFR_Fs2, LFR_Fs3]
def LFR_Fs2F(fs):
    return {LFR_Fs0: 0, LFR_Fs1: 1, LFR_Fs2: 2, LFR_Fs3: 3}[fs]

chs_sm     = ['B1', 'B2', 'B3', 'E1', 'E2', 'V'] 
markers_sm = [ '+',  '*',  '.',  '8',  'x', 'o']    
colors_sm  = [ 'b',  'g',  'r',  'm',  'y', 'c']   
def name2index_sm(n):
    return {'B1': 0, 'B2': 1, 'B3': 2, 'E1': 3, 'E2': 4,  'V': 5, 'calib': 6}[n]
def index2name_sm(i):
        return ['B1', 'B2', 'B3', 'E1', 'E2', 'V', 'calib'][i]    
    
chs_wf     = ['V', 'E1', 'E2', 'B1', 'B2', 'B3'] 
markers_wf = ['o',  '8',  'x',  '+',  '*',  '.']    
colors_wf  = ['c',  'm',  'y',  'b',  'g',  'r']
def name2index_wf(n):    
    return { 'V': 0, 'E1': 1, 'E2': 2, 'B1': 3, 'B2': 4, 'B3': 5, 'calib': 6}[n]    
def index2name_wf(i):
    return ['V', 'E1', 'E2', 'B1', 'B2', 'B3', 'calib'][i]   
            
chs_bias     = ['BIAS1', 'BIAS2', 'BIAS3', 'BIAS4', 'BIAS5']    
chs_vhf     = ['VHF1', 'VHF2', 'VHF3']      
markers_bias = ['o',  '8',  'x',  '+',  '*']    
colors_bias  = ['c',  'm',  'y',  'm',  'y']
chs_se_dc =   [ 'V1_DC',  'V2_DC',  'V3_DC']
chs_diff_dc = ['V12_DC', 'V13_DC', 'V23_DC']
chs_diff_ac = ['V12_AC', 'V13_AC', 'V23_AC']
colors_bias_l2  = ['m', 'c', 'y']

chs_scm_uarf = ['Bx_UARF', 'By_UARF', 'Bz_UARF']
chs_scm_rtn  = ['Bx_RTN' , 'By_RTN' , 'Bz_RTN' ] 
chs_scm_srf  = ['Bx_SRF' , 'By_SRF' , 'Bz_SRF' ] 
chs_ant_srf  = ['Ey_SRF' , 'Ez_SRF'] 
