# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
#plt.rcParams['figure.figsize'] = (10, 6)
import numpy as np

from lib_lfr.global_params import *
from lib_parsing.parse_routines import is_scalar
from lib_maths.conversion_routines import dBfromlin, standard_sheet
from lib_signal.filters import invfreqs, invfreqz

import warnings
warnings.simplefilter('ignore', np.RankWarning)
from scipy.optimize import curve_fit
from scipy import signal

def mean_TF(calib_arr, comp_arr=['B1', 'B2', 'B3'], F=1, idrop=None, ampl_range=None, freq_range=None, 
            dB=False, unwrap_plus=None):
    nb_of_measurements = len(calib_arr)
    icomp_arr = [name2index_sm(n) for n in comp_arr]  
    l_allCal = [5, 10, 14, 17, 19, 20]
    l_compCal = [l_allCal[i] for i in icomp_arr]
    ncomp = len(comp_arr)
    f_injected_arr = nb_of_measurements * [None]
    calib_V2count_arr = nb_of_measurements * [None]
    cross_norm_arr = nb_of_measurements * [None]    
    delta_phi_add_arr = nb_of_measurements * [None]    
    phi_compCal_arr = nb_of_measurements * [None]            
        
    for calib, k in zip(calib_arr, range(nb_of_measurements)):
        print(calib[0]['comp'], len(calib), k+1)
        f_injected_arr[k] = [cal['f_injected'] for cal in calib]
        calib_V2count_arr[k] = np.array([[cal['calib_V2count'][i] for i in icomp_arr] for cal in calib])
        print( not False in list(f_injected_arr[0] == np.array(f_injected_arr[k])), calib_V2count_arr[k].shape )
        plt.plot(f_injected_arr[k], dBfromlin(calib_V2count_arr[k]) if dB else calib_V2count_arr[k], linewidth=2)                
    plt.title("{} TF amplitude measurements for {} @F{}".format(nb_of_measurements, comp_arr, F), fontsize=14)             
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('Amplitude (count/V)' if not dB else 'Amplitude (dB[count/V])', fontsize=14)
    if ampl_range != None:
        plt.ylim(ampl_range if not dB else np.rint(dBfromlin(ampl_range)+np.array([-0.5,0.5])))    
    if freq_range != None:
        plt.xlim(freq_range)             
    plt.show()    
        
    f_injected = f_injected_arr[0]
    nfreq = len(f_injected)
    ifreq_arr = np.arange(nfreq) if idrop == None else np.delete(np.arange(nfreq), idrop)    
    
    calib_V2count_dataset_arr = np.array(calib_V2count_arr)    
    print(calib_V2count_dataset_arr.shape)    
    calib_V2count_arr_mean = np.mean(calib_V2count_dataset_arr, axis=0)
    calib_V2count_arr_error = 100.*np.std(calib_V2count_dataset_arr, axis=0)/calib_V2count_arr_mean
    print("Max calibration error (std) by component: {} (% of amplitude in count/V)".format(np.amax(calib_V2count_arr_error[ifreq_arr,:], axis=0)))
    print("Corresponding index: {}".format(ifreq_arr[np.argmax(calib_V2count_arr_error[ifreq_arr,:], axis=0)]))    
    plt.plot(f_injected, dBfromlin(calib_V2count_arr_mean) if dB else calib_V2count_arr_mean, linewidth=2)
    plt.title("Mean TF amplitudes (over measurements) for {} @F{}".format(comp_arr, F), fontsize=14)             
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('Amplitude (count/V)' if not dB else 'Amplitude (dB[count/V])', fontsize=14)
    if ampl_range != None:
        plt.ylim(ampl_range if not dB else np.rint(dBfromlin(ampl_range)+np.array([-0.5,0.5])))    
    if freq_range != None:
        plt.xlim(freq_range)             
    plt.show()    
        
    calib_V2count_mean = np.mean(calib_V2count_arr_mean, axis=1)
    calib_V2count_diff = 100.*np.std(calib_V2count_arr_mean, axis=1)/calib_V2count_mean
    print("Max calibration difference (std) between component: {} (% of amplitude in count/V) \n".format(np.amax(calib_V2count_diff[ifreq_arr], axis=0)))
    plt.plot(f_injected, dBfromlin(calib_V2count_mean) if dB else calib_V2count_mean, linewidth=2)
    n1ine = int(nfreq / 15)
    nlast = nfreq % 15
    print( (n1ine*(15*'{:8.2f}'+'\n') + (nlast*'{:8.2f}'+'\n')).format(*calib_V2count_mean) )
    print( (n1ine*(15*'{:8.1f}'+'\n') + (nlast*'{:8.1f}'+'\n')).format(*f_injected) )
    print( (n1ine*(15*'{:8.2f}'+'\n') + (nlast*'{:8.2f}'+'\n')).format(*calib_V2count_diff) )
    plt.title("Mean TF amplitude (over {} and measurements) @F{}".format(comp_arr, F), fontsize=14)               
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('Amplitude (count/V)' if not dB else 'Amplitude (dB[count/V])', fontsize=14)
    if ampl_range != None:
        plt.ylim(ampl_range if not dB else np.rint(dBfromlin(ampl_range)+np.array([-0.5,0.5])))    
    if freq_range != None:
        plt.xlim(freq_range)             
    plt.show()
    
    for calib, k in zip(calib_arr, range(nb_of_measurements)):
        cross_norm_arr[k] = np.array([[cal['cross_norm'][l] for l in l_compCal] for cal in calib])
        delta_phi_add_arr[k] = np.array([cal['delta_phi_add'] if (cal['delta_phi_add'] != None) else 0. for cal in calib])   
        phi_compCal_arr[k] = np.array([[np.angle(cross_n[j], deg=False) + np.deg2rad(dphi2add) for j in range(ncomp)] \
                        for cross_n, dphi2add in zip(cross_norm_arr[k], delta_phi_add_arr[k])])        
        for j in range(ncomp):
            phi_compCal_arr[k][:,j] = np.rad2deg(np.unwrap(phi_compCal_arr[k][:,j], discont=np.pi))   
            if unwrap_plus != None:
                index_toapply = unwrap_plus[0]
                m_2pi_toadd = unwrap_plus[1]              
                phi_compCal_arr[k][index_toapply,j] = phi_compCal_arr[k][index_toapply,j] + m_2pi_toadd                             
        print(phi_compCal_arr[k].shape)    
        plt.plot(f_injected, phi_compCal_arr[k], linewidth=2)
    plt.title("{} TF phase measurements for {} @F{}".format(nb_of_measurements, comp_arr, F), fontsize=14)             
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('Phase (°)', fontsize=14)  
    plt.show()    
    
    phi_dataset_compCal_arr = np.array(phi_compCal_arr)           
    print(phi_dataset_compCal_arr.shape)       
    phi_compCal_arr_mean = np.mean(phi_dataset_compCal_arr, axis=0)
    phi_compCal_arr_error = np.std(phi_dataset_compCal_arr, axis=0)
    print("Max phase error (std) by component: {} (°)".format(np.amax(phi_compCal_arr_error[ifreq_arr,:], axis=0)))
    print("Corresponding index: {}".format(ifreq_arr[np.argmax(phi_compCal_arr_error[ifreq_arr,:], axis=0)]))
    print('Fine time error: {} (°)'.format(np.rad2deg(2*np.pi*(LFR_Fs[F]/2.5)*(2**(-16)))/2))
    plt.plot(f_injected, phi_compCal_arr_mean, linewidth=2)
    plt.title("Mean TF phases (over measurements) for {} @F{}".format(comp_arr, F), fontsize=14)             
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('Phase (°)', fontsize=14)    
    plt.show()    
      
    phi_compCal_mean = np.mean(phi_compCal_arr_mean, axis=1)
    phi_compCal_diff = np.std(phi_compCal_arr_mean, axis=1)
    print("Max phase difference (std) between component: {} (°) \n".format(np.amax(phi_compCal_diff[ifreq_arr], axis=0)))
    plt.plot(f_injected, phi_compCal_mean, linewidth=2)
    print( (n1ine*(15*'{:8.2f}'+'\n') + (nlast*'{:8.2f}'+'\n')).format(*phi_compCal_mean) )
    print( (n1ine*(15*'{:8.1f}'+'\n') + (nlast*'{:8.1f}'+'\n')).format(*f_injected) )
    print( (n1ine*(15*'{:8.2f}'+'\n') + (nlast*'{:8.2f}'+'\n')).format(*phi_compCal_diff) )
    plt.title("Mean TF phase (over {} and measurements) @F{}".format(comp_arr, F), fontsize=14)               
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('Phase (°)', fontsize=14)    
    plt.show()
    return f_injected, calib_V2count_mean.tolist(), phi_compCal_mean.tolist(), (nb_of_measurements, comp_arr, F, idrop)

def fitfunc(freq, fitfunc1, fitfunc2, freq_sep):
    if is_scalar(freq):                    
        return fitfunc1(freq) if freq <= freq_sep else fitfunc2(freq)
    else:
        return [fitfunc1(_freq) if _freq <= freq_sep else fitfunc2(_freq) for _freq in freq]
        
def fit_TF_phase(mean_TF_result, ie=29, i1=0, i2=133, order=6):
    freqs = mean_TF_result[0]
    phases = mean_TF_result[2]
    comp_arr = mean_TF_result[3][1]
    F = mean_TF_result[3][2]
    print('Linear fit from {} Hz to {} Hz :'.format(freqs[0], freqs[ie]))
    z = np.polyfit(freqs[0:ie+1], phases[0:ie+1], 1)  
    print('{} X + {}'.format(*z))  
    print('Data time shift: {:6.2f} micro secondes <=> {:6.3f} T{}'.format(1e6*z[0]/360., LFR_Fs[F]*z[0]/360., F))    
    phase_fit_low = np.poly1d(z) 
    plt.plot(freqs, phases, linewidth=2)  
    plt.plot(freqs[0:ie+1], phase_fit_low(freqs[0:ie+1]), linewidth=2) 
    print('Polynomial fit of order {} from {} Hz to {} Hz :'.format(order, freqs[i1], freqs[i2]))    
    z = np.polyfit(freqs[i1:i2+1], phases[i1:i2+1], order)
    phase_fit_high = np.poly1d(z) 
    plt.plot(freqs[ie:i2+1], phase_fit_high(freqs[ie:i2+1]), linewidth=2)   
    plt.title("Fit of the mean TF phase (over {} and measurements) @F{}".format(comp_arr, F), fontsize=14)                 
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('Phase (°)', fontsize=14)        
    plt.show()
    def phase_fit(freq):
        return fitfunc(freq, phase_fit_low, phase_fit_high, freqs[ie])                   
    diff = phase_fit(freqs[0:i2+1]) - np.array(phases[0:i2+1])    
    max_diff_abso = np.amax(abs(diff))
    print("Max difference between fit and original: {:.2f} (°)".format(max_diff_abso))
    plt.plot(freqs[0:i2+1], diff, linewidth=2)
    plt.title("Difference between fit and original of the mean TF phase (over {} and measurements) @F{}".format(comp_arr, F), fontsize=14)                 
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('$\Delta$ (°)', fontsize=14)    
    plt.show()          
    return phase_fit 
    
def rational(x, p, q):
    """
    The general rational function description.
    p is a list with the polynomial coefficients in the numerator
    q is a list with the polynomial coefficients in the denominator 
    after the first one (i.e. the zero order one), which is fixed at 1.    
    """
    return np.polyval(p, x) / np.polyval(q + [1], x)

def rational3_3(x, p0, p1, p2, p3, q0, q1, q2):
    return rational(x, [p0, p1, p2, p3], [q0, q1, q2])

def rational4_4(x, p0, p1, p2, p3, p4, q0, q1, q2, q3):
    return rational(x, [p0, p1, p2, p3, p4], [q0, q1, q2, q3])    

def rational6_6(x, p0, p1, p2, p3, p4, p5, p6, q0, q1, q2, q3, q4, q5):
    return rational(x, [p0, p1, p2, p3, p4, p5, p6], [q0, q1, q2, q3, q4, q5])

def rational8_8(x, p0, p1, p2, p3, p4, p5, p6, p7, p8, q0, q1, q2, q3, q4, q5, q6, q7):
    return rational(x, [p0, p1, p2, p3, p4, p5, p6, p7, p8], [q0, q1, q2, q3, q4, q5, q6, q7])

def rational10_10(x, p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, q0, q1, q2, q3, q4, q5, q6, q7, q8, q9):
    return rational(x, [p0, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10], [q0, q1, q2, q3, q4, q5, q6, q7, q8, q9])

def fit_TF_ampli(mean_TF_result, i1=0, i2=110, i3=133, ratio_order=6, poly_order=6, p0_p_order=8800, 
                 ampl_range=None, dB=False):  
    freqs = mean_TF_result[0]
    amplitudes = mean_TF_result[1] if not dB else dBfromlin(mean_TF_result[1])
    comp_arr = mean_TF_result[3][1]
    F = mean_TF_result[3][2] 
    print('Rational function fit of order {} from {} Hz to {} Hz'.format(ratio_order, freqs[i1], freqs[i2]))      
    func_gen = {3: rational3_3, 4: rational4_4, 6: rational6_6, 8: rational8_8, 10: rational10_10}
    p0_p = (ratio_order+1)*[0]
    p0_q = ratio_order*[0]
    p0_p[ratio_order] = p0_p_order if not dB else dBfromlin(p0_p_order)
    p0_q[ratio_order-1] = 1
    popt, pcov = curve_fit(func_gen[ratio_order], freqs[i1:i2+1], amplitudes[i1:i2+1], p0 = p0_p+p0_q)
    def amplitude_fit_low(freq):
        if is_scalar(freq):                    
            return func_gen[ratio_order](freq, *popt) if freq >= freqs[i1] else np.interp(freq, freqs, amplitudes)
        else:
            return [func_gen[ratio_order](_freq, *popt) if _freq >= freqs[i1] else np.interp(_freq, freqs, amplitudes) for _freq in freq]            
    plt.plot(freqs, amplitudes, linewidth=2)  
    plt.plot(freqs[i1:i2+1], [amplitude_fit_low(f) for f in freqs[i1:i2+1]], linewidth=2) 
    if ampl_range != None:
        plt.ylim(ampl_range if not dB else np.rint(dBfromlin(ampl_range)+np.array([-0.5,0.5])))    
    print('Polynomial fit of order {} from {} Hz to {} Hz'.format(poly_order, freqs[i2], freqs[i3]))    
    z = np.polyfit(freqs[i2:i3+1], amplitudes[i2:i3+1], poly_order)
    amplitude_fit_high = np.poly1d(z) 
    plt.plot(freqs[i2:i3+1], amplitude_fit_high(freqs[i2:i3+1]), linewidth=2)  
    plt.title("Fit of the mean TF amplitude (over {} and measurements) @F{}".format(comp_arr, F), fontsize=14)                 
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('Amplitude (count/V)' if not dB else 'Amplitude (dB[count/V])', fontsize=14)
    plt.show()
    def ampli_fit(freq):
        return fitfunc(freq, amplitude_fit_low, amplitude_fit_high, freqs[i2])              
    diff = ampli_fit(freqs[0:i3+1]) - np.array(amplitudes[0:i3+1])         
    imax = np.argmax(abs(diff))    
    max_diff_abso = abs(diff[imax])
    if not dB:
        max_diff_rela = abs(max_diff_abso/amplitudes[imax])
        print("Max difference between fit and original: {:.2f} count/V (i.e {:.2} %)".format(max_diff_abso, 100*max_diff_rela))
    else:
        max_diff_rela = abs(10**(max_diff_abso/20.) -1)
        print("Max difference between fit and original: {:.4f} dB (i.e. {:.2} % of amplitude in count/V)".format(max_diff_abso, 100*max_diff_rela))
    plt.plot(freqs[0:i3+1], diff, linewidth=2)  
    plt.title("Difference between fit and original of the mean TF amplitude (over {} and measurements) @F{}".format(comp_arr, F), fontsize=14)                 
    plt.xlabel('Frequency (Hz)', fontsize=12)
    plt.ylabel('$\Delta$ (count/V)' if not dB else '$\Delta$ (dB[count/V])', fontsize=14)     
    plt.show()      
    return ampli_fit    

def compare_TF_figures(freqs, h1, h2, figsize=(14,4), abs_diff_range=None, phi_diff_range=None):   
    fig, axarr = plt.subplots(1, 2, figsize=figsize)
    axarr[0].plot(freqs, np.real(h1))
    axarr[0].plot(freqs, np.real(h2))
    axarr[1].plot(freqs, np.imag(h1))
    axarr[1].plot(freqs, np.imag(h2))
    plt.show()     
    diff = np.abs(h2) - np.abs(h1)
    imax = np.argmax(abs(diff))    
    max_diff_abso = diff[imax] 
    max_diff_rela = max_diff_abso/abs(h1[imax])
    print("Max difference between fit and original: {:.2f} count/V (i.e {:.2f} %)".format(max_diff_abso, 100*max_diff_rela))      
    fig, axarr = plt.subplots(1, 2, figsize=figsize)
    axarr[0].plot(freqs, np.abs(h1))
    axarr[0].plot(freqs, np.abs(h2))
    axarr[1].plot(freqs, diff)
    axarr[1].set_ylim(abs_diff_range)
    plt.show()
    diff = (np.angle(h2, deg=True) - np.angle(h1, deg=True))
    diff = np.array([standard_sheet(d) for d in diff])
    imax = np.argmax(np.abs(diff))    
    max_diff_abso = diff[imax]
    print("Max difference between fit and original: {:.2f} (°)".format(max_diff_abso))    
    fig, axarr = plt.subplots(1, 2, figsize=figsize)
    axarr[0].plot(freqs, np.rad2deg(np.unwrap(np.angle(h1))))
    axarr[0].plot(freqs, np.rad2deg(np.unwrap(np.angle(h2))))
    axarr[1].plot(freqs, diff)
    axarr[1].set_ylim(phi_diff_range)    
    plt.show()
                
def fit_TF_rational_freqs(mean_TF_result, nB=4, nA=6, wf=None, nk=0, equation_type='real', fs=LFR_Fs0, 
                          leastsq_method='lm', fit_method='trf', maxfev=3000, p0_tau=-0.1, idrop=[]):     
    freqs = np.delete(np.array(mean_TF_result[0]), idrop)
    ampli = np.delete(np.array(mean_TF_result[1]), idrop)
    phase = np.delete(np.array(mean_TF_result[2]), idrop)
    wfreqs = 2*np.pi*freqs
    h = ampli * np.exp(1j*np.deg2rad(phase))

    b1, a1 = invfreqs(h, wfreqs, nB, nA, wf=wf, nk=nk, equation_type=equation_type,
                      least_squares=False, method=leastsq_method)
    print('b (linear):', b1)
    print('a (linear):', a1)
    w1, h1 = signal.freqs(b1, a1, worN=wfreqs)
    compare_TF_figures(w1/np.pi/2, h, h1, figsize=(14,4))    
    b1_lstsqr, a1_lstsqr = invfreqs(h, wfreqs, nB, nA, wf=wf, nk=nk, equation_type=equation_type,
                      least_squares=True, method=leastsq_method)
    print('b (least_squares):', b1_lstsqr)
    print('a (least_squares):', a1_lstsqr)
    w1, h1 = signal.freqs(b1_lstsqr, a1_lstsqr, worN=wfreqs)
    compare_TF_figures(w1/np.pi/2, h, h1, figsize=(14,4))    
    
    def H0(w, *p):
        _, h = signal.freqs(p[0:nB+1], p[nB+1:nB+1+nA+1], worN=w)
        return h 
    def H0abs(w, *p):
        return np.abs(H0(w, *p))
    def H0phi(w, *p):
        return np.angle(H0(w, *p)) % (2*np.pi)      
    p0_p = b1_lstsqr.tolist() 
    p0_q = a1_lstsqr.tolist()
    popt0, pcov0 = curve_fit(H0abs, wfreqs, ampli, p0 = p0_p + p0_q, maxfev=maxfev, method=fit_method)
    #popt0, pcov0 = curve_fit(H0abs, wfreqs, ampli, p0 = popt0)        
    #popt0, pcov0 = curve_fit(H0phi, wfreqs, np.deg2rad(phase) % (2*np.pi), p0 = p0_p + p0_q)
    #popt0, pcov0 = curve_fit(H0phi, wfreqs, np.deg2rad(phase) % (2*np.pi), p0 = popt0)    
    popt0 = popt0 / popt0[nB+1]                            
    print('popt0 (H0abs) :', popt0)
    compare_TF_figures(freqs, h, H0(wfreqs, *popt0), figsize=(14,4))        
     
    def H(w, tau):    
        return H0(w, *popt0) * np.exp(1j*w*tau/fs)        
    def Hphi(w, tau):
        return np.angle(H(w, tau)) % (2*np.pi)
    def Hreal(w, tau):
        return np.real(H(w, tau))
    def Himag(w, tau):
        return np.imag(H(w, tau))    
    #popt_tau, pcov = curve_fit(Hphi, wfreqs, np.deg2rad(phase) % (2*np.pi), p0 =[p0_tau], maxfev=maxfev, method=fit_method)
    #popt_tau, pcov = curve_fit(Hphi, wfreqs, np.deg2rad(phase) % (2*np.pi), p0 =[popt_tau], maxfev=maxfev, method=fit_method)
    #print('popt_tau :', popt_tau)
    #compare_TF_figures(freqs, h, H(wfreqs, popt_tau), figsize=(14,4))                      
    
    def expH0(w, *p):
        _, h = signal.freqs(p[0:nB+1], p[nB+1:nB+1+nA+1], worN=w)
        return h * np.exp(1j*w*p[-1]/fs)
    def expH0abs(w, *p):
        return np.abs(expH0(w, *p))
    def expH0phi(w, *p):
        return np.angle(expH0(w, *p)) % (2*np.pi)
    def expH0real(w, *p):
        return np.real(expH0(w, *p))
    def expH0imag(w, *p):
        return np.imag(expH0(w, *p))  
    #popt0exp, pcov0 = curve_fit(expH0real, wfreqs, np.real(h), p0 = p0_p + p0_q + [p0_tau], maxfev=maxfev, method=fit_method)
    #popt0exp, pcov0 = curve_fit(expH0real, wfreqs, np.real(h), p0 = popt0.tolist() + [p0_tau])
    #popt0exp, pcov0 = curve_fit(expH0imag, wfreqs, np.imag(h), p0 = p0_p + p0_q + [p0_tau])    
    #print('popt0exp :', popt0exp)    
    #popt0exp, pcov0 = curve_fit(expH0real, wfreqs, np.real(h), p0 = popt0exp)
    #print('popt0exp :', popt0exp)     
    #popt0exp, pcov0 = curve_fit(expH0imag, wfreqs, np.imag(h), p0 = popt0exp, maxfev=maxfev, method=fit_method)
    #print('popt0exp :', popt0exp)
    #popt0exp, pcov0 = curve_fit(expH0real, wfreqs, np.real(h), p0 = popt0exp, maxfev=maxfev, method=fit_method)
    #print('popt0exp :', popt0exp) 
    #compare_TF_figures(freqs, h, expH0(wfreqs, *popt0exp), figsize=(14,4))
    
    return b1_lstsqr, a1_lstsqr, popt0[0:nB+1], popt0[nB+1:nB+1+nA+1]                                     
                                  
def fit_TF_rational_freqz(mean_TF_result, nB=7, nA=4, wf=None, nk=0, equation_type='real', fs=4*LFR_Fs0, idrop=[],
                          p0_tau=-0.1, opt='b1+a1', leastsq_method='lm', fit_method='trf', maxfev=3000):     
    freqs = np.delete(np.array(mean_TF_result[0]), idrop)
    ampli = np.delete(np.array(mean_TF_result[1]), idrop)
    phase = np.delete(np.array(mean_TF_result[2]), idrop)
    wfreqs = 2*np.pi*freqs
    h = ampli * np.exp(1j*np.deg2rad(phase))

    b1, a1 = invfreqz(h, wfreqs, nB, nA, wf=wf, nk=nk, fs=fs, equation_type=equation_type, 
                      least_squares=False, method=leastsq_method)
    print('b (linear):', b1)
    print('a (linear):', a1)
    #w1, h1 = signal.freqz(b1, a1, worN=None)
    w1, h1 = signal.freqz(b1, a1, worN=wfreqs/fs)
    compare_TF_figures((w1/np.pi)*fs/2, h, h1, figsize=(14,4))  
    b1_lstsqr, a1_lstsqr = invfreqz(h, wfreqs, nB, nA, wf=wf, nk=nk, fs=fs, equation_type=equation_type, 
                         least_squares=True, method=leastsq_method)
    print('b (least_squares):', b1_lstsqr)
    print('a (least_squares):', a1_lstsqr)
    w1, h1 = signal.freqz(b1_lstsqr, a1_lstsqr, worN=wfreqs/fs)
    compare_TF_figures((w1/np.pi)*fs/2, h, h1, figsize=(14,4))      
                      
    def H0(w, *p):
        _, h = signal.freqz(p[0:nB+1], p[nB+1:nB+1+nA+1], worN=w/fs)
        return h 
    def H0abs(w, *p):
        return np.abs(H0(w, *p))
    def H0phi(w, *p):
        return np.angle(H0(w, *p)) % (2*np.pi)
    def H0real(w, *p):
        return np.real(H0(w, *p))                  
    p0_p = b1_lstsqr.tolist() 
    p0_q = a1_lstsqr.tolist()
    popt0, pcov0 = curve_fit(H0abs, wfreqs, ampli, p0 = p0_p + p0_q, maxfev=maxfev, method=fit_method)    
    #popt0, pcov0 = curve_fit(H0real, wfreqs, ampli, p0 = popt0)        
    #popt0, pcov0 = curve_fit(H0abs, wfreqs, ampli, p0 = popt0)        
    #popt0, pcov0 = curve_fit(H0phi, wfreqs, np.deg2rad(phase) % (2*np.pi), p0 = p0_p + p0_q)
    #popt0, pcov0 = curve_fit(H0phi, wfreqs, np.deg2rad(phase) % (2*np.pi), p0 = popt0)    
    #popt0, pcov0 = curve_fit(H0abs, wfreqs, ampli, p0 = popt0)
    #popt0, pcov0 = curve_fit(H0phi, wfreqs, np.deg2rad(phase) % (2*np.pi), p0 = popt0)    
    popt0 = popt0 / popt0[nB+1]                        
    print('popt0 (H0abs) :', popt0)  
    compare_TF_figures(freqs, h, H0(wfreqs, *popt0), figsize=(14,4))            

    def H(w, tau):    
        return H0(w, *popt0) * np.exp(1j*w*tau/fs)        
    def Hphi(w, tau):
        return np.angle(H(w, tau)) % (2*np.pi)                
    #popt_tau, pcov = curve_fit(Hphi, wfreqs, np.deg2rad(phase) % (2*np.pi), p0 =[p0_tau])    
    #print('popt_tau :', popt_tau)          
    #compare_TF_figures(freqs, h, H(wfreqs, popt_tau), figsize=(14,4))            
        
    def expH0(w, *p):
        _, h = signal.freqz(p[0:nB+1], p[nB+1:nB+1+nA+1], worN=w/fs)
        return h * np.exp(1j*w*p[-1]/fs)
    def expH0abs(w, *p):
        return np.abs(expH0(w, *p))
    def expH0phi(w, *p):
        return np.angle(expH0(w, *p)) % (2*np.pi)
    def expH0real(w, *p):
        return np.real(expH0(w, *p))
    def expH0imag(w, *p):
        return np.imag(expH0(w, *p))
    #if opt == 'b1+a1': 
    #    popt0exp, pcov0 = curve_fit(expH0real, wfreqs, np.real(h), p0 = p0_p + p0_q + [p0_tau])
    #if opt == 'popt0':
    #    popt0exp, pcov0 = curve_fit(expH0real, wfreqs, np.real(h), p0 = popt0.tolist() + [p0_tau])
    #print('popt0exp :', popt0exp)        
    #popt0exp, pcov0 = curve_fit(expH0imag, wfreqs, np.imag(h), p0 = p0_p + p0_q + [p0_tau])    
    #print('popt0exp :', popt0exp)    
    #popt0exp, pcov0 = curve_fit(expH0real, wfreqs, np.real(h), p0 = popt0exp)
    #print('popt0exp :', popt0exp)         
    #popt0exp, pcov0 = curve_fit(expH0imag, wfreqs, np.imag(h), p0 = popt0exp)
    #print('popt0exp :', popt0exp)        
    #popt0, pcov0 = curve_fit(H0abs, wfreqs, ampli, p0 = popt0exp[:-1])
    #popt0exp[:-1] = popt0  
    #print('popt0exp :', popt0exp)     
    #compare_TF_figures(freqs, h, expH0(wfreqs, *popt0exp), figsize=(14,4))
    
    return b1_lstsqr, a1_lstsqr, popt0[0:nB+1], popt0[nB+1:nB+1+nA+1]










    

