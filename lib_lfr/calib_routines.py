# -*- coding: utf-8 -*-

from lib_lfr.global_params import *
from lib_lfr.load_routines import get_lfr_decom_l1_swf, get_lfr_decom_l1_cwf
from lib_signal.spectra import spectral_matrices
from lib_lfr.load_routines import load_LFR_TF_B_CDF, load_LFR_TF_E_CDF, load_field_CDF, load_SCM_TF_comp_CDF
from lib_lfr.interp_routines import ANT_TF_HF_PA_interp, LFR_TF_interp, SCM_TF_mat_interp

import numpy as np
from numpy.polynomial.polynomial import polyval
from fractions import Fraction

class TotoError(Exception):
    pass
    
def calib_one_freq_sinus_swf(source, f_injected, signals=[('B1', 1.), ('B2', 1.), ('B3', 1.), ('E1', 1.), ('E2', 1.), ('V', 1.)], \
                             df_asked=None, calib_max=False, trigger_time=None, time_shift=0., part_snap=None, echo=True):
    """
    Routine qui calcule les autocorrelations et les crosscorrelations moyennées
    des données snapshot waveforms SWF (i.e. fichiers de type .sfi, i= 0, 1 ou 2)
    enregistrées avec une fréquence d'étalonnage. En pratique, on calcule des matrices
    spectrales moyennées. En sont déduits les facteurs de calibration [counts/Volt]
    (à partir des auto) et les phases relatives [°] (à partir des cross).
    Marche aussi pour les fréquences >= fe/2 (teste/évalue donc le repliement).
    En option: détermination des phases absolues par simulation/reconstruction du
    signal de calibration et son adjonction dans le calcul des matrices spectrales.
    Routine à utiliser fichier par fichier, i.e. fréquence par fréquence.

INPUT:
    source:         either a file name of data (ex: 'data/2015_10_19_15_28_08_packet_record_NORMAL.sf0') 
                    or a SWF data structure already available by a previous use of get_lfr_decom_l1_swf
    f_injected:     frequency of the injected sinusoïdal signal (in Hertz)
    signals:        list of tuples whose first coordinate gives the name of the input signal and the second one
                    its associated complex pic amplitude in Volt (ex: [('B1', 1.), ('B2', 1.)], thus same phases = 0);                 
    df_asked:       frequency resolution asked for the fft computation (in Hertz); if None the default value, fe/256, is used
    calib_max:      True if bin with max amplitude is used for calibration, False if bin at expected place is used
    trigger_time:   if set, time in seconds for absolut phase determination; works only with the first snapshot
                    (the other snapshots if any are not used); if None, relative phase determination only
    time_shift:     possible time shift correction in order to get at f=0 an absolute phase value of 0 degree
    part_snap:      if None full snapshot is taken into account; if set to (ipart, lpart) only the snapshot part
                    starting at index ipart over a length of lpart points is used
    echo:           echo if True, no echo otherwise

OUTPUT:
    calib_V2count:   conversion factors from Volt to count (count/Volt) [list(ncomp), float]
                    [ncomp = nb of input signals = len(icomp) = len(ampl), min=1, max=6]
                    [        + 1 calib signal if keyword 'trigger_time' is set ...     ]
    cross_norm:     normalized cross correlations [list(ncomp*(ncomp-1)/2), complex]
    delta_phi_add:  if applicable, an unique phase value correction, in degre, to be added to the absolute phases
                    associated to 'calib' signal; if not applicable, it is set to None                   
    status_param:   OK if correct set of frequency parameters, i.e. if one FFT frequency can match
                    the injected frequency, KO otherwise
    status_auto:    for each auto, OK if the maximum of the FFT bins corresponds to the injected frequency, KO otherwise
    status_cross:   forn each cross, OK if both maxima of the FFT bins correspond to the injected frequency, KO otherwise
    freq_max:       list of frequencies corresponding to the max amplitudes of component
    icomp:          index list of the input signals (ex: [1, 2, 5, 6]; 6 stands for the calib signal if absolute phase determination)
    comp:           name list of the input signals (ex: ['B2', 'B3', 'V', 'calib'])
    ampl:           list of their complex pic amplitude (ex: [1., 1., 1., 9000.] in Volt except for the calib signal)
    datatype:       string quoting the datatype used for the calibration ('SWF_F0', 'SWF_F1', 'SWF_F2')
    
    return {'calib_V2count':      calib_V2count,     
            'cross_norm':         cross_norm,        
            'delta_phi_add':      None, TBW ...                              
            'status_param':       status_param,      
            'status_auto':        status_auto,       
            'status_cross':       status_cross,      
            'freq_max':           freq_max,          
            'icomp':              icomp,             
            'comp':               comp,              
            'ampl':               ampl,              
            'f_injected':         f_injected,        
            'df_analysis':        df_analysis,       
            'trigger_time':       trigger_time,      
            'calib_max':          calib_max,         
            'datatype':           datatype           
            'part_snap':          part_snap}                       
            
    version 1.0: first version with Python (!) from the last version of the idl routine             [... 29 février 2016]
    version 1.1: file input has been changed to source input (two possible ways for getting the data ...)          
                 + python3 compatibility + aliasing debug (two cases instead of one ...)                   [28 mars 2016]
    version 1.2: bug fixed on signal name 'V' + cosmetics                                                  [31 mars 2016]
    version 1.3: remove '_swf' in output nomenclature and add 'datatype' keyword                           [4 avril 2016]
    version 1.3: add TotoError Exception class                                                            [12 avril 2016]
    version 1.4: implementation of keyword 'delta_phi_add' TBW ...                                        [13 avril 2016] 
    version 1.5: implementation of a complex amplitude of the input signal                                 [7  juin 2016]
    version 1.6: implementation of 'half_snap' & 'ihalf' keywords                                       [12 juillet 2016]
                 (works with one snapshot; TBW if more ...)
    version 1.7: numpy type data converted into Python type data (compatibility with jsonpickle ...)    [22 juillet 2016]   
    version 1.8: improvement: f_injected%1 == 0 TBW if needed ...                                       [28 juillet 2016]
    version 1.9: 'half_snap' & 'ihalf' keywords improved to 'part_snap'keyword                              [1 août 2016]                                  
    """

    # convert name list into index list
    def name2index_sm(n):
        return {'B1': 0,'B2': 1, 'B3': 2, 'E1': 3,'E2': 4, 'V': 5}[n]
    isignals = [(name2index_sm(n),a) for (n, a) in signals]

    # for convenience (...) sort the isignals along their index
    def coord0(element):
        return element[0]
    isignals.sort(key=coord0)
    icomp = [s[0] for s in isignals]
    ampl = [s[1] for s in isignals]

    # load SWF data if not already available
    # data_block:     v, e1, e2, b1, b2, b3 (in count unit => 16-bit integer array)
    #                 [ndarray(nb_of_data, 6), with nb_of_data = number of samples by component]
    # relative_time:  time of the data in unit of seconds counted from the first sample (float array)
    #                 [ndarray(nb_of_data)]
    # start_time:     time of the first sample (seconds from 2000/00/01 00:00)
    # index_snapshot: index of the first element of each snapshot in data_block and time (integer array)
    #                 [ndarray(nb_of_snapshots+1)]
    #                 i. e., for k from 0 to nb_of_snapshots-1, snapshot(k) and its time(k) are given by
    #                        data_block[index_snapshot[k]:index_snapshot[k+1], *] and
    #                        relative_time[index_snapshot[k]:index_snapshot[k+1]]
    # fe:             fréquence d'échantillonnage (sampling frequency) read from the file name
    if isinstance(source, str):
        if echo:
            print( 'Lecture du fichier: {}'.format(source) )
        swf = get_lfr_decom_l1_swf(source)
    else:
        swf = source        
    data_block = swf['data_block'].copy()
    # from v, e1, e2, b1, b2, b3 ...
    spoon           = data_block[:,0].copy()  #  v, e1, e2, b1, b2, b3
    data_block[:,0] = data_block[:,3]         # b1, e1, e2, b1, b2, b3
    data_block[:,3] = data_block[:,1]         # b1, e1, e2, e1, b2, b3
    data_block[:,1] = data_block[:,4]         # b1, b2, e2, e1, b2, b3
    data_block[:,4] = data_block[:,2]         # b1, b2, e2, e1, e2, b3
    data_block[:,2] = data_block[:,5]         # b1, b2, b3, e1, e2, b3
    data_block[:,5] = spoon                   # b1, b2, b3, e1, e2,  v
    # ... to b1, b2, b3, e1, e2, v !
    (i_b1, i_b2, i_b3, i_e1, i_e2,  i_v) = (0, 1, 2, 3, 4, 5)
    
    # consider possibly just a part of the snapshot
    if part_snap:
        ipart = part_snap[0]
        lsnap = part_snap[1]    
        data_block = data_block[ipart:ipart+lsnap,:]        
        if echo:
            print( 'Only the snapshot part from index {} over a length of {} points will be considered'.format(ipart, lsnap) + \
                   '(work just with one snapshot; TBW otherwise ...)' )
    else:
        ipart = 0
        lsnap = 2048                                                       

    # number of SWF and number of samples by component in the data file
    index_snapshot = swf['index_snapshot']
    nsnap = len(index_snapshot) - 1
    ndata = data_block.shape[0]
    
    # which sampling frequency ? f0, f1 or f2 ?
    if swf['fe'] == 24576.:
        str_fe = 'f0'
    elif swf['fe'] == 4096.:
        str_fe = 'f1'
    elif swf['fe'] == 256.:
        str_fe = 'f2'
    # sinon on lève une exception
    else:
        raise TotoError('problematic sampling frequency !?')
    datatype = 'SWF_'+str_fe.upper()
    
    if echo:
        print( ('Contains: {} snapshot {}' if nsnap <= 1 else \
                'Contains: {} snapshots {}').format(nsnap, datatype) )

    if trigger_time != None:
        # absolute phase will be determined by simulating the calibration signal at the first snapshot
        # and by correlating it to the measured signals; for doing that one simply includes, as another
        # signal component, the calibration signal in the computation of the averaged spectral matrix;
        # CAUTION: here averaging is consistent with phase determination only if the duration of the individual, 
        # 'instantaneous', spectral matrix (SM) is an entire number of the wave period (same thing should be
        # reproduced from one SM to the other ...)
        # time information is therefore crucial:
        # packet_headers: headers of the packets read in the file (string array)
        #                 [ndarray(nb_of_packets, 6)]
        #       example : cnt  nr  nb   syn_t   int_t (2^⁻16 sec)      sec_t (seconds)
        #                 7    1   304  1       93255699               1422.9690399169921875
        #                 int_t => time of the first sample of the packet in unit of 2^-16 seconds (integer)
        #                 sec_t => same but in unit of seconds (floating)
        isnap = 0
        time_from_ttrig_to_tsnap = int(swf['packet_headers'][isnap*7, 4])*2.**(-16) - trigger_time
        if echo:
            print( 'Absolute phase will be determined from the first snapshot' )
            print( 'The trigger time is {:d} s and corresponds to {:.6f} s ahead'.format(trigger_time, time_from_ttrig_to_tsnap)  )
        if f_injected%1 == 0:        
            if echo:
                print("This trigger time may be erroneous by an entire number of seconds: BUT this does not matter \n" \
                    + "since f_injected = {}Hz is a mutiple of 1Hz !...".format(f_injected))                   
        else:
            if echo:
                print("This trigger time may be erroneous by an entire number of seconds: BUT this can be corrected \n" \
                    + "since f_injected = {}Hz is an entire fraction of 1Hz ({}/{}Hz)!...".\
                       format(f_injected, Fraction(str(f_injected)).numerator, Fraction(str(f_injected)).denominator))
                print("An entire number of 360*f_injected={:.2f}° can indeed be erroneous and SHOULD be retrieved ... (TBW)".\
                       format(360.*f_injected))                   
        # time correction can be taken into account; it can be estimated from the slope dphi/df; ideally it should be zero;
        # a negative slope means the data are recorded with some delay, and inversely, a positive slope means a datation ahead
        # in fact, e.g., for f0 one measures an absolute time shift of about -T0; it corresponds to some structural delay in the VHDL ...
        time_shift += float(ipart)/swf['fe']       
        time_calib = np.arange(lsnap)/swf['fe'] + time_shift         # in seconds
        ampl_calib = 9000.                                           # in count unit (arbitrary value ...)
        phase_calib = 2*np.pi*f_injected*time_from_ttrig_to_tsnap    # in radian (here intial phase is suppposed to be 0)
        sinus_calib = ampl_calib * np.sin(2*np.pi*f_injected*time_calib + phase_calib)
        sinus_calib.shape = lsnap , 1
        data_block = np.concatenate((data_block, sinus_calib), axis=1)
        icomp.append(6)
        ampl.append(ampl_calib)

    def index2name_sm(i):
        return ['B1', 'B2', 'B3', 'E1', 'E2', 'V', 'calib'][i]
    comp = [ index2name_sm(i) for i in icomp]

    # computation of 1 averaged spectral matrix from the snapshot waveform SWF
    # ndarray(1, nfreq, 6, 6): for [B1 B2 B3 E1 E2 V]       # CONJ([B1 B2 B3 E1 E2 V])       (complex) if trigger_time == None
    # ndarray(1, nfreq, 7, 7): for [B1 B2 B3 E1 E2 V calib] # CONJ([B1 B2 B3 E1 E2 V calib]) (complex) if trigger_time != None

    # we set the parameters in order to get the maximum independent spectral matrices to be averaged
    if df_asked == None:
        df_asked = swf['fe'] / 256.  # default
    else:
        df_asked = float(df_asked)   # make sure to be a float
    nw = int(swf['fe'] / df_asked + 0.5)
    if nw <= 2048:
        if (nsnap > 1) and (2048 % nw != 0):
            raise TotoError('(nsnap > 1) and (2048 % nw={} != 0) => problematic frequency resolution asked (df_asked) !'.format(nw))
        na = int(ndata / nw)
        # fs=nw  => df=1Hz in order to get count^2 units ...
        sm = spectral_matrices(data_block, fs=nw, nw=nw, na=na, echo=echo)
        asm = sm['asm']
    else:
        raise TotoError('nw={} > 2048 => problematic frequency resolution asked (df_asked) !'.format(nw))
    df_analysis = swf['fe'] / nw
    if echo:
        print( 'Résolution de fréquence demandée pour l\'analyse : {} Hz'.format(df_asked) )
        print( 'Résolution de fréquence effective dans l\'analyse: {} Hz'.format(df_analysis) )

    # determination of the frequency bin which is the closest to f_injected
    f_injected = float(f_injected)
    ifreq = int( f_injected / df_analysis + 0.5)
    status_param = 'OK' if f_injected == ifreq*df_analysis else 'KO'
    if echo:
        print( 'Fréquence injectée pour la calibration : {} Hz'.format(f_injected) )
        print( 'Raie la plus proche de la freq injectée: {} Hz, i.e raie {} (indice)'.format(ifreq*df_analysis, ifreq) )
        print( 'Statut sur paramètres ok si ces 2 fréquences sont identiques: {}'.format(status_param) )
    if f_injected >= swf['fe']/2:
        f_injected_modulo = (f_injected%swf['fe'])
        ifreq_modulo = (ifreq%nw)
        if f_injected_modulo < swf['fe']/2:
            f_aliasing = f_injected_modulo
            ifreq_aliasing = ifreq_modulo            
        else:                    
            f_aliasing = swf['fe'] - f_injected_modulo
            ifreq_aliasing = nw - ifreq_modulo
        if echo:
            print( 'Repliement de la fréquence injectée: {} Hz'.format(f_aliasing) )
            print( 'Raie la plus proche correspondante : {} Hz, i.e raie {} (indice)'.\
                                format(ifreq_aliasing*df_analysis, ifreq_aliasing) )
        ifreq = ifreq_aliasing

    # the normalized FFT of a sinusoïdal signal of frequency f, of peak amplitude A, and phase phi,
    # computed with a frequency resolution df such as m*df = f, m being an integer,
    # has a unique frequency bin at index m with an amplitude equal to A/2 and phase phi
    # if the FFT used retains the "positives" frequencies; phase -phi otherwise.
    # (the default FFT used here in 'spectral_matrices' is the forward np.fft.fft,
    #  q(j) = FFT(+1) = Sum(i=0 to nw-1) of p(i)*Exp(-2Pi I i*j/nw), which retains
    #  the "positives" frequencies)

    # amplitude calibration determined from the computed SWF power spectra (auto-correlations)
    if not calib_max:
        if echo:
            print('Compute calibration with the bin at expected place ...')
        calib_V2count = [ float( np.sqrt(asm[0, ifreq, icomp_j, icomp_j].real / (abs(ampl_j)/2.)**2) ) \
                          for (icomp_j, ampl_j) in zip(icomp, ampl) ]
    else:
        if echo:
            print('Compute calibration with the most intense bin ...')
        calib_V2count = [ float( np.sqrt(np.max(asm[0, 1:, icomp_j, icomp_j].real) / (abs(ampl_j)/2.)**2) )\
                          for (icomp_j, ampl_j) in zip(icomp, ampl) ]
    ifreq_max = [ int(1+np.argmax(asm[0, 1:, icomp_j, icomp_j].real)) for icomp_j in icomp  ]
    status_auto = [ 'OK' if i == ifreq else 'KO' for i in ifreq_max ]
    freq_max = [ind*df_analysis for ind in ifreq_max]

    if echo:
        print( '##### AMPLITUDE CALIBRATION FROM SWF COMPUTED POWER-SPECTRA (AUTOCORRELATION) #####' )
        print( 'Component (name)           :', end ='' )
        for name in comp:
            print( ' {:>8s}'.format(name), end ='' )
        print()
        print( 'Component (index)          :', end ='' )
        for index in icomp:
            print( ' {:8d}'.format(index), end ='' )
        print()
        print( 'Amplitude (V)              :', end ='' )
        for val in ampl:
            print( ' {:8.2f}'.format(abs(val)), end ='' )
        print()
        print( 'Conversion factor (count/V):', end ='' )
        for val in calib_V2count:
            print( ' {:8.2f}'.format(val), end ='' )
        print()
        print( 'Frequency max auto (index) :', end ='' )
        for ind in ifreq_max:
            print( ' {:8d}'.format(ind), end ='' )
        print()
        print( 'Frequency max auto (Hz)    :', end ='' )
        for freq in freq_max:
            print( ' {:8.1f}'.format(freq), end ='' )
        print()
        print( 'Autocorrelation status     :', end ='' )
        for cal in status_auto:
            print( ' {:>8s}'.format(cal), end ='' )
        print()
        print( '                                   (ok if injected frequency corresponds to a max)' )

    # relative/absolute phase calibration determined from the computed SWF cross-spectra (cross-correlations)
    # complex values of ampl allow to correct the relative phases from phase differencies between the input 
    # signals: multiplying by np.conj(ampl_i/abs(ampl_i))*(ampl_j/abs(ampl_j)) permits to get free from them ...
    rg = range(len(ifreq_max))
    if not calib_max:
        if echo:
            print('Compute phase with the bin at expected place ...')
        cross_norm = [ complex( np.conj(ampl_i/abs(ampl_i))*asm[0, ifreq, icomp_i, icomp_j]*(ampl_j/abs(ampl_j)) /   \
                                np.sqrt(asm[0, ifreq, icomp_i, icomp_i].real*   \
                                        asm[0, ifreq, icomp_j, icomp_j].real) ) \
                       for icomp_i, ampl_i in zip(icomp, ampl) for icomp_j, ampl_j in zip(icomp, ampl) if icomp_j > icomp_i ]
    else:
        if echo:
            print('Compute phase with the most intense bin ...')
        cross_norm = [ complex( np.conj(ampl[i]/abs(ampl[i]))*asm[0, ifreq_max[i], icomp[i], icomp[j]]*(ampl[j]/abs(ampl[j])) /  \
                                np.sqrt(asm[0, ifreq_max[i], icomp[i], icomp[i]].real*   \
                                        asm[0, ifreq_max[j], icomp[j], icomp[j]].real) ) \
                       for i in rg for j in rg  if j > i ]
    status_cross = [ 'OK' if (ifreq_max[i] == ifreq and ifreq_max[j] == ifreq) else 'KO' for i in rg for j in rg  if j > i ]

    if echo:
        print( '##### PHASE CALIBRATION FROM SWF COMPUTED CROSS-SPECTRA (CROSSCORRELATION) #####' )
        print( 'icomp[i] icomp[j]    l     S_ij/SQRT(S_ii*Sjj)     rho       phi    cross status' )
        print( '                               (normalized)                  (°)                ' )

        l = 0
        for icomp_i in icomp:
            for icomp_j in icomp:
                if icomp_j > icomp_i:
                    print( '{:7d}{:9d}{:6d} {:22.5f} {:9.5f} {:9.3f} {:>8s}'. \
                            format(icomp_i, icomp_j, l, cross_norm[l],    
                                   abs(cross_norm[l]),                    
                                   np.angle(cross_norm[l], deg=True),     
                                   status_cross[l]) )
                    l = l + 1

    return {'calib_V2count':  calib_V2count, 
            'cross_norm':     cross_norm,    
            'delta_phi_add':  None,          
            'status_param':   status_param,  
            'status_auto':    status_auto,   
            'status_cross':   status_cross,  
            'freq_max':       freq_max,      
            'icomp':          icomp,         
            'comp':           comp,          
            'ampl':           ampl,          
            'f_injected':     f_injected,    
            'df_analysis':    df_analysis,   
            'trigger_time':   trigger_time,  
            'calib_max':      calib_max,     
            'datatype':       datatype,      
            'part_snap':      part_snap} 
                        
def calib_one_freq_sinus_cwf(source, f_injected, signals=[('B1', 1.), ('B2', 1.), ('B3', 1.), ('E1', 1.), ('E2', 1.), ('V', 1.)], \
                             df_asked=None, calib_max=False, trigger_time=None, phi_target=0., time_shift=0., echo=True):
    """
    Routine qui calcule les autocorrelations et les crosscorrelations moyennées
    des données continuous waveforms CWF (i.e. fichiers de type .cfi, i= 1, 2 ou 3)
    enregistrées avec une fréquence d'étalonnage. En pratique, on calcule des matrices
    spectrales moyennées. En sont déduits les facteurs de calibration [counts/Volt]
    (à partir des auto) et les phases relatives [°] (à partir des cross).
    Marche aussi pour les fréquences >= fe/2 (teste/évalue donc le repliement).
    En option: détermination des phases absolues par simulation/reconstruction du
    signal de calibration et son adjonction dans le calcul des matrices spectrales.
    Routine à utiliser fichier par fichier, i.e. fréquence par fréquence.

INPUT:
    source:         either a file name of data (ex: 'data/2015_10_19_15_28_08_packet_record_NORMAL.cf2') 
                    or a CWF data structure already available by a previous use of get_lfr_decom_l1_cwf
    f_injected:     frequency of the injected sinusoïdal signal (in Hertz)
    signals:        list of tuples whose first coordinate gives the name of the input signal and the second one
                    its associated complex pic amplitude in Volt (ex: [('B1', 1.), ('B2', 1.)], thus same phases = 0); 
    df_asked:       frequency resolution asked for the fft computation (in Hertz); if None the default value, fe/256, is used
    calib_max:      True if bin with max amplitude is used for calibration, False if bin at expected place is used
    trigger_time:   if set, time in seconds for absolut phase determination; works only with the first snapshot
                    (the other snapshots if any are not used); if None, relative phase determination only
    phi_target:     concerns the phase correction target, usually 0° but can be 90° for BIAS4 and BIAS5 if alone ...               
    time_shift:     possible time shift correction in order to get at f=0 an absolute phase value of 0 degree
    echo:           True if echo, False otherwise

OUTPUT:
    calib_V2count:  conversion factors from Volt to count (count/Volt) [list(ncomp), float]
                    [ncomp = nb of input signals = len(icomp) = len(ampl), min=1, max=6]
                    [        + 1 calib signal if keyword 'trigger_time' is set ...     ]
    cross_norm:     normalized cross correlations [list(ncomp*(ncomp-1)/2), complex]
    delta_phi_add:  if applicable, an unique phase value correction, in degre, to be added to the absolute phases
                    associated to 'calib' signal; if not applicable, it is set to None                        
    status_param:   OK if correct set of frequency parameters, i.e. if one FFT frequency can match
                    the injected frequency, KO otherwise
    status_auto:    for each auto, OK if the maximum of the FFT bins corresponds to the injected frequency, KO otherwise
    status_cross:   forn each cross, OK if both maxima of the FFT bins correspond to the injected frequency, KO otherwise
    freq_max:       list of frequencies corresponding to the max amplitudes of component
    icomp:          index list of the input signals (ex: [1, 2, 5, 6]; 6 stands for the calib signal if absolute phase determination)
    comp:           name list of the input signals (ex: ['B2', 'B3', 'V', 'calib'])
    ampl:           list of their complex pic amplitude (ex: [1., 1., 1., 9000.] in Volt except for the calib signal)
    datatype:       string quoting the datatype used for the calibration ('CWF_F1', 'CWF_F2', 'CWF_F3')

    return {'calib_V2count':      calib_V2count,     
            'cross_norm':         cross_norm,        
            'delta_phi_add':      delta_phi_add0,             
            'status_param':       status_param,      
            'status_auto':        status_auto,       
            'status_cross':       status_cross,      
            'freq_max':           freq_max,          
            'icomp':              icomp,             
            'comp':               comp,              
            'ampl':               ampl,              
            'f_injected':         f_injected,        
            'df_analysis':        df_analysis,       
            'trigger_time':       trigger_time,      
            'calib_max':          calib_max,         
            'datatype':           datatype}                       

    version 1.0: first version from calib_one_freq_sinus_swf.py (version 1.2) and the last version of the idl routine  [3  avril    2016]
    version 1.1: implementation of keyword 'delta_phi_add'                                                             [6  avril    2016]
    version 1.2: improvements: Fraction(f_injected) => Fraction(str(f_injected)) ... + phi_target ...                  [8  avril    2016]
    version 1.3: add TotoError Exception class                                                                         [12 avril    2016]
    version 1.4: numpy type data converted into Python type data (compatibility with jsonpickle ...)                   [26 juillet  2016]
    version 1.5: improvement: f_injected%1 == 0 ...                                                                    [28 juillet  2016]
    version 1.6: implementation of a complex amplitude of the input signal                                             [24 novembre 2016]
    version 1.7: implementation of the case 'V, E1, E2'                                                                [10 janvier  2017]    
    """
    # convert name list into index list
    def name2index_sm(n):
        return {'B1': 0,'B2': 1, 'B3': 2, 'E1': 3,'E2': 4, 'V': 5}[n]
    isignals = [(name2index_sm(n),a) for (n, a) in signals]

    # for convenience (...) sort the isignals along their index
    def coord0(element):
        return element[0]
    isignals.sort(key=coord0)
    icomp = [s[0] for s in isignals]
    ampl = [s[1] for s in isignals]

    # load CWF data if not already available
    # data_block:     v, e1, e2, b1, b2, b3 (in count unit => 16-bit integer array)
    #                 [ndarray(nb_of_data, 6), with nb_of_data = number of samples by component]
    # relative_time:  time of the data in unit of seconds counted from the first sample (float array)
    #                 [ndarray(nb_of_data)]
    # start_time:     time of the first sample (seconds from 2000/00/01 00:00)
    # fe:             fréquence d'échantillonnage (sampling frequency) read from the file name
    if isinstance(source, str):
        if echo:
            print( 'Lecture du fichier: {}'.format(source) )
        cwf = get_lfr_decom_l1_cwf(source)
    else:
        cwf = source        
    data_block = cwf['data_block'].copy()
    if cwf['names'] == 'V, E1, E2':           
        # from v, e1, e2...
        spoon           = data_block[:,0].copy()  #  v, e1, e2
        data_block[:,0] = data_block[:,1]         # e1, e1, e2
        data_block[:,1] = data_block[:,2]         # e1, e2, e2        
        data_block[:,2] = spoon                   # e1, e2,  v
        # ... to e1, e2, v !
        (i_e1, i_e2,  i_v) = (3, 4, 5)
        icomp0 = 3
    else:    
        # from v, e1, e2, b1, b2, b3 ...
        spoon           = data_block[:,0].copy()  #  v, e1, e2, b1, b2, b3
        data_block[:,0] = data_block[:,3]         # b1, e1, e2, b1, b2, b3
        data_block[:,3] = data_block[:,1]         # b1, e1, e2, e1, b2, b3
        data_block[:,1] = data_block[:,4]         # b1, b2, e2, e1, b2, b3
        data_block[:,4] = data_block[:,2]         # b1, b2, e2, e1, e2, b3
        data_block[:,2] = data_block[:,5]         # b1, b2, b3, e1, e2, b3
        data_block[:,5] = spoon                   # b1, b2, b3, e1, e2,  v
        # ... to b1, b2, b3, e1, e2, v !
        (i_b1, i_b2, i_b3, i_e1, i_e2,  i_v) = (0, 1, 2, 3, 4, 5)        

    # number of samples by component in the data file
    ndata = data_block.shape[0]

    # which sampling frequency ? f1, f2 or f3 ?
    if cwf['fe'] == 4096.:
        str_fe = 'f1'
    elif cwf['fe'] == 256.:
        str_fe = 'f2'
    elif cwf['fe'] == 16.:
        str_fe = 'f3'
    # sinon on lève une exception
    else:
        raise TotoError('problematic sampling frequency !?')
    datatype = 'CWF_'+str_fe.upper()

    if echo:
        print( ('Contains: {} samplings of {}').format(ndata, datatype) )    
        
    phi_to_be_corrected = False
    delta_phi_add0 = None     
    if trigger_time != None:                  
        # absolute phase will be determined by simulating the calibration signal at the time of the cwf
        # and by correlating it to the measured signals; for doing that one simply includes, as another
        # signal component, the calibration signal in the computation of the averaged spectral matrix;
        # CAUTION: here averaging is consistent with phase determination only if the duration of the individual, 
        # 'instantaneous', spectral matrix (SM) is an entire number of the wave period (same thing should be
        # reproduced from one SM to the other ...)
        # time information is therefore crucial:
        # packet_headers: headers of the packets read in the file (string array)
        #                 [ndarray(nb_of_packets, 6)]
        #       example : cnt  nr  nb   syn_t   int_t (2^⁻16 sec)      sec_t (seconds)
        #                 7    1   304  1       93255699               1422.9690399169921875
        #                 int_t => time of the first sample of the packet in unit of 2^-16 seconds (integer)
        #                 sec_t => same but in unit of seconds (floating)
        time_from_ttrig_to_tcwf = cwf['start_time']+cwf['relative_time'][0] - trigger_time
        if echo:
            print( 'Absolute phase will be determined by simulating the calibration signal at the time of the cwf' )
            print( 'The trigger time is {:d} s and corresponds to {:.6f} s ahead'.format(trigger_time, time_from_ttrig_to_tcwf)  )
        if f_injected%1 == 0:
            if echo:
                print("This trigger time may be erroneous by an entire number of seconds: BUT this does not matter \n" \
                    + "since f_injected = {}Hz is a mutiple of 1Hz !...".format(f_injected))                   
        else:
            phi_to_be_corrected = True                          
            if echo:
                print("This trigger time may be erroneous by an entire number of seconds: BUT this can be corrected \n" \
                    + "since f_injected = {}Hz is an entire fraction of 1Hz ({}/{}Hz)!...".\
                       format(f_injected, Fraction(str(f_injected)).numerator, Fraction(str(f_injected)).denominator))
                print("An entire number of 360*f_injected={:.2f}° can indeed be erroneous and will be retrieved afterwards...".\
                       format(360.*f_injected))                       
            def delta_phi_add(phi, f_injected, target=0.): 
                """
                function that defines the delta phase to be added for correction 
                360*f_injected is a multiple of 360./Fraction(str(f_injected)).denominator)
                The minus sign is because we are searching for negative values (small phase delays) 
                """        
                return (phi-target)%(-360./Fraction(str(f_injected)).denominator) - (phi-target)                                                     
        # time correction can be taken into account; it can be estimated from the slope dphi/df; ideally it should be zero;
        # a negative slope means the data are recorded with some delay, and inversely, a positive slope means a datation ahead
        # in fact, e.g., for f0 one measures an absolute time shift of about -T0; it corresponds to some structural delay in the VHDL ...
        time_calib = np.arange(ndata)/cwf['fe'] + time_shift         # in seconds
        ampl_calib = 9000.                                           # in count unit (arbitrary value ...)
        phase_calib = 2*np.pi*f_injected*time_from_ttrig_to_tcwf     # in radian (here intial phase is suppposed to be 0)
        sinus_calib = ampl_calib * np.sin(2*np.pi*f_injected*time_calib + phase_calib)
        sinus_calib.shape = ndata, 1
        data_block = np.concatenate((data_block[0:ndata,:], sinus_calib), axis=1)
        icomp.append(6)
        ampl.append(ampl_calib)                        
            
    def index2name_sm(i):
        return ['B1', 'B2', 'B3', 'E1', 'E2', 'V', 'calib'][i]
    comp = [ index2name_sm(i) for i in icomp]

    # computation of 1 averaged spectral matrix from the continuous waveform CWF
    # ndarray(1, nfreq, 6, 6): for [B1 B2 B3 E1 E2 V]       # CONJ([B1 B2 B3 E1 E2 V])       (complex) if trigger_time == None
    # ndarray(1, nfreq, 7, 7): for [B1 B2 B3 E1 E2 V calib] # CONJ([B1 B2 B3 E1 E2 V calib]) (complex) if trigger_time != None

    # we set the parameters in order to get the maximum independent spectral matrices to be averaged
    if df_asked == None:
        df_asked = cwf['fe'] / 256.  # default
    else:
        df_asked = float(df_asked)   # make sure to be a float
    nw = int(cwf['fe'] / df_asked + 0.5)
    if nw <= ndata:
        na = int(ndata / nw)
        # fs=nw  => df=1Hz in order to get count^2 units ...
        sm = spectral_matrices(data_block, fs=nw, nw=nw, na=na, echo=echo)
        asm = sm['asm']
    else:
        raise TotoError('nw={} > ndata={} => problematic frequency resolution asked (df_asked) !'.format(nw, ndata))
    df_analysis = cwf['fe'] / nw
    if echo:
        print( 'Résolution de fréquence demandée pour l\'analyse : {} Hz'.format(df_asked) )
        print( 'Résolution de fréquence effective dans l\'analyse: {} Hz'.format(df_analysis) )

    # determination of the frequency bin which is the closest to f_injected
    f_injected = float(f_injected)
    ifreq = int( f_injected / df_analysis + 0.5)
    status_param = 'OK' if f_injected == round(ifreq*df_analysis,6) else 'KO'
    if echo:
        print( 'Fréquence injectée pour la calibration : {} Hz'.format(f_injected) )
        print( 'Raie la plus proche de la freq injectée: {} Hz, i.e raie {} (indice)'.format(round(ifreq*df_analysis,6), ifreq) )
        print( 'Statut sur paramètres ok si ces 2 fréquences sont identiques: {}'.format(status_param) )
    if f_injected >= cwf['fe']/2:
        f_injected_modulo = (f_injected%cwf['fe'])
        ifreq_modulo = (ifreq%nw)
        if f_injected_modulo < cwf['fe']/2:
            f_aliasing = f_injected_modulo
            ifreq_aliasing = ifreq_modulo            
        else:                    
            f_aliasing = cwf['fe'] - f_injected_modulo
            ifreq_aliasing = nw - ifreq_modulo
        if echo:
            print( 'Repliement de la fréquence injectée: {} Hz'.format(f_aliasing) )
            print( 'Raie la plus proche correspondante : {} Hz, i.e raie {} (indice)'.\
                                format(ifreq_aliasing*df_analysis, ifreq_aliasing) )
        ifreq = ifreq_aliasing

    # the normalized FFT of a sinusoïdal signal of frequency f, of peak amplitude A, and phase phi,
    # computed with a frequency resolution df such as m*df = f, m being an integer,
    # has a unique frequency bin at index m with an amplitude equal to A/2 and phase phi
    # if the FFT used retains the "positives" frequencies; phase -phi otherwise.
    # (the default FFT used here in 'spectral_matrices' is the forward np.fft.fft,
    #  q(j) = FFT(+1) = Sum(i=0 to nw-1) of p(i)*Exp(-2Pi I i*j/nw), which retains
    #  the "positives" frequencies)

    # amplitude calibration determined from the computed CWF power spectra (auto-correlations)
    if not calib_max:
        if echo:
            print('Compute calibration with the bin at expected place ...')
        calib_V2count = [ float( np.sqrt(asm[0, ifreq, icomp_j-icomp0, icomp_j-icomp0].real / (abs(ampl_j)/2.)**2) ) \
                          for (icomp_j, ampl_j) in zip(icomp, ampl) ]
    else:
        if echo:
            print('Compute calibration with the most intense bin ...')
        calib_V2count = [ float( np.sqrt(np.max(asm[0, 1:, icomp_j-icomp0, icomp_j-icomp0].real) / (abs(ampl_j)/2.)**2) ) \
                          for (icomp_j, ampl_j) in zip(icomp, ampl) ]
    ifreq_max = [ 1+np.argmax(asm[0, 1:, icomp_j-icomp0, icomp_j-icomp0].real) for icomp_j in icomp ]
    status_auto = [ 'OK' if i == ifreq else 'KO' for i in ifreq_max ]
    freq_max = [ index*df_analysis for index in ifreq_max ]

    if echo:
        print( '##### AMPLITUDE CALIBRATION FROM CWF COMPUTED POWER-SPECTRA (AUTOCORRELATION) #####' )
        print( 'Component (name)           :', end ='' )
        for name in comp:
            print( '{:>9s}'.format(name), end ='' )
        print()
        print( 'Component (index)          :', end ='' )
        for index in icomp:
            print( '{:9d}'.format(index), end ='' )
        print()
        print( 'Amplitude (V)              :', end ='' )
        for val in ampl:
            print( '{:9.2f}'.format(abs(val)), end ='' )
        print()
        print( 'Conversion factor (count/V):', end ='' )
        for val in calib_V2count:
            print( '{:9.2f}'.format(val), end ='' )
        print()
        print( 'Frequency max auto (index) :', end ='' )
        for ind in ifreq_max:
            print( '{:9d}'.format(ind), end ='' )
        print()
        print( 'Frequency max auto (Hz)    :', end ='' )
        for freq in freq_max:
            print( '{:9.2f}'.format(freq), end ='' )
        print()
        print( 'Autocorrelation status     :', end ='' )
        for cal in status_auto:
            print( '{:>9s}'.format(cal), end ='' )
        print()
        print( '                                   (ok if injected frequency corresponds to a max)' )

    # relative/absolute phase calibration determined from the computed CWF cross-spectra (cross-correlations)
    # complex values of ampl allow to correct the relative phases from phase differencies between the input 
    # signals: multiplying by np.conj(ampl_i/abs(ampl_i))*(ampl_j/abs(ampl_j)) permits to get free from them ...
    rg = range(len(ifreq_max))
    if not calib_max:
        if echo:
            print('Compute phase with the bin at expected place ...')
        cross_norm = [ complex( np.conj(ampl_i/abs(ampl_i))*asm[0, ifreq, icomp_i-icomp0, icomp_j-icomp0]*(ampl_j/abs(ampl_j)) /  \
                        np.sqrt(asm[0, ifreq, icomp_i-icomp0, icomp_i-icomp0].real*  \
                                asm[0, ifreq, icomp_j-icomp0, icomp_j-icomp0].real) ) \
                       for icomp_i, ampl_i in zip(icomp, ampl) for icomp_j, ampl_j in zip(icomp, ampl) if icomp_j > icomp_i ]                                                
    else:
        if echo: print('Compute phase with the most intense bin ...')
        cross_norm = [ complex( np.conj(ampl[i]/abs(ampl[i]))*asm[0, ifreq_max[i], icomp[i]-icomp0, icomp[j]-icomp0]*(ampl[j]/abs(ampl[j])) /  \
                                np.sqrt(asm[0, ifreq_max[i], icomp[i]-icomp0, icomp[i]-icomp0].real*  \
                                        asm[0, ifreq_max[j], icomp[j]-icomp0, icomp[j]-icomp0].real) ) \
                      for i in rg for j in rg  if j > i ]
    status_cross = [ 'OK' if (ifreq_max[i] == ifreq and ifreq_max[j] == ifreq) else 'KO' for i in rg for j in rg  if j > i ]    

    if phi_to_be_corrected:
        def minus180_plus180(phi):
            return phi%360-360 if phi%360 > 180 else phi%360
        if echo:            
            print( '##### PHASE CALIBRATION FROM CWF COMPUTED CROSS-SPECTRA (CROSSCORRELATION) #####' )
            print( 'icomp[i] icomp[j]    l     S_ij/SQRT(S_ii*Sjj)     rho       phi    phi_corr  cross status' )
            print( '                               (normalized)                  (°)       (°)                ' )
        l = 0
        for icomp_i in icomp:
            for icomp_j in icomp:
                if icomp_j > icomp_i:
                    phi = np.angle(cross_norm[l], deg=True) 
                    phi_corr = phi                                   
                    if icomp_j == 6:
                        if icomp_i == min(icomp): 
                            delta_phi_add0 = float(delta_phi_add(phi, f_injected, phi_target))                                               
                        phi_corr = minus180_plus180(phi + delta_phi_add0)                                                                      
                    if echo:
                        print( '{:7d}{:9d}{:6d} {:22.5f} {:9.5f} {:9.3f} {:9.3f} {:>8s}'.      \
                                format(icomp_i, icomp_j, l, cross_norm[l], abs(cross_norm[l]), 
                                phi, phi_corr, status_cross[l]) )
                    l = l + 1
        if echo:
            print("### A correction of {:+.2f}° has been applied in the determination of the absolute phases ###".\
                   format(delta_phi_add0))
                    
    if echo and not phi_to_be_corrected:
        print( '##### PHASE CALIBRATION FROM CWF COMPUTED CROSS-SPECTRA (CROSSCORRELATION) #####' )
        print( 'icomp[i] icomp[j]    l     S_ij/SQRT(S_ii*Sjj)     rho       phi    cross status' )
        print( '                               (normalized)                  (°)                ' )
        l = 0
        for icomp_i in icomp:
            for icomp_j in icomp:
                if icomp_j > icomp_i:
                    print( '{:7d}{:9d}{:6d} {:22.5f} {:9.5f} {:9.3f} {:>8s}'. \
                            format(icomp_i, icomp_j, l, cross_norm[l],    
                                   abs(cross_norm[l]),                    
                                   np.angle(cross_norm[l], deg=True),     
                                   status_cross[l]) )
                    l = l + 1
           
    return {'calib_V2count':  calib_V2count,  
            'cross_norm':     cross_norm,     
            'delta_phi_add':  delta_phi_add0, 
            'status_param':   status_param,   
            'status_auto':    status_auto,    
            'status_cross':   status_cross,   
            'freq_max':       freq_max,       
            'icomp':          icomp,          
            'comp':           comp,           
            'ampl':           ampl,           
            'f_injected':     f_injected,     
            'df_analysis':    df_analysis,    
            'trigger_time':   trigger_time,
            'calib_max':      calib_max,      
            'datatype':       datatype}
            
def calib_one_freq_sinus_asm(source, f_injected, signals=[('B1', 1.), ('B2', 1.), ('B3', 1.), ('E1', 1.), ('E2', 1.)], 
                             iasm=0, calib_max=False, hanning=True, echo=True):
    """
    Routine qui analyse les matrices spectrales moyennées ASM (i.e. fichiers de type .afi, i= 1, 2 ou 3).    
    On déduit des termes diagonaux (les autocorrelations ) les facteurs de calibration [counts/Volt] et 
    des termes non diagonaux (les crosscorrelations) les phases relatives [°].
    Marche aussi pour les fréquences >= fe/2 (teste/évalue donc le repliement).    
    Routine à utiliser fichier par fichier, i.e. fréquence par fréquence.

INPUT:
    source:         either a file name of data (ex: 'data/2015_10_19_15_28_08_packet_record_NORMAL.af2') 
                    or a ASM data structure already available by a previous use of get_lfr_decom_l1_asm
    f_injected:     frequency of the injected sinusoïdal signal (in Hertz)
    signals:        list of tuples whose first coordinate gives the name of the input signal and the second one
                    its associated complex pic amplitude in Volt (ex: [('B1', 1.), ('B2', 1.)], thus same phases = 0)
    calib_max:      True if bin with max amplitude is used for calibration, False if bin at expected place is used
    iasm:           index of the asm used for the calculation    
    echo:           True if echo, False otherwise

OUTPUT:
    calib_V2count:  conversion factors from Volt to count (count/Volt) [list(ncomp), float]
                    [ncomp = nb of input signals = len(icomp) = len(ampl), min=1, max=5]
    cross_norm:     normalized cross correlations [list(ncomp*(ncomp-1)/2), complex]                       
    status_param:   OK if correct set of frequency parameters, i.e. if one ASM FFT frequency can match
                    the injected frequency, KO otherwise
    status_auto:    for each auto, OK if the maximum of the ASM FFT bins corresponds to the injected frequency, KO otherwise
    status_cross:   forn each cross, OK if both maxima of the ASM FFT bins correspond to the injected frequency, KO otherwise
    freq_max:       list of frequencies corresponding to the max amplitudes of component
    icomp:          index list of the input signals (ex: [1, 2, 4])
    comp:           name list of the input signals (ex: ['B2', 'B3', 'E2'])
    ampl:           list of their complex pic amplitude (ex: [1., 1., 1.] in Volt)
    datatype:       string quoting the datatype used for the calibration ('ASM_F1', 'ASM_F2', 'ASM_F3')

    return {'calib_V2count':  calib_V2count, 
            'cross_norm':     cross_norm,    
            'status_param':   status_param,  
            'status_auto':    status_auto,   
            'status_cross':   status_cross,  
            'freq_max':       freq_max,      
            'icomp':          icomp,         
            'comp':           comp,          
            'ampl':           ampl,          
            'f_injected':     f_injected,    
            'df_asm':         df_asm,   
            'calib_max':      calib_max,
            'iasm':           iasm,     
            'datatype':       datatype}                 

    version 1.0: first version from calib_one_freq_sinus_swf.py (version 1.9) and the last version of the idl routine  [9 janvier 2017]    
    """

    # convert name list into index list
    def name2index_sm(n):
        return {'B1': 0,'B2': 1, 'B3': 2, 'E1': 3,'E2': 4, 'V': 5}[n]
    isignals = [(name2index_sm(n),a) for (n, a) in signals]
    # for convenience (...) sort the isignals along their index
    def coord0(element):
        return element[0]
    isignals.sort(key=coord0)
    icomp = [s[0] for s in isignals]
    ampl = [s[1] for s in isignals]
    # back to name list
    def index2name_sm(i):
        return ['B1', 'B2', 'B3', 'E1', 'E2', 'V'][i]
    comp = [ index2name_sm(i) for i in icomp]
    
    # load ASM data if not already available
    # {'asm_idl':         asm_idl,                    
    #  'packet_headers':  packet_headers,             
    #  'relative_time':   relative_time,              
    #  'start_time':      start_time,                 
    #  'freq':            freq                        
    #  'fe':              fe,                         
    #  'fromfile':        file,                       
    #  'names':           '[B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])'}
    if isinstance(source, str):
        if echo: print( 'Lecture du fichier: {}'.format(source) )
        asm = get_lfr_decom_l1_asm(source)
    else:
        asm = source
        
    # shape of the of ASM data     
    nasm, nfreq, dim, dim = asm['asm_idl'].shape    
    # which sampling frequency ? f0, f1 or f2 ?
    F = LFR_Fs2F(asm['fe'])
    datatype = 'ASM_F%s'%F   
    if echo: 
        print( ('Contains: {} averaged spectral matrice {}' if nasm <= 1 else \
                'Contains: {} averaged spectral matrices {}').format(nasm, datatype) ) 
        print( 'Index of the ASM used: {}'.format(iasm) )        
    # frequency resolution of the ASM data
    nw = 256
    df_asm = asm['fe'] / nw
    if echo: print( 'Résolution de fréquence des {}: {} Hz'.format(datatype, df_asm) )                         
        
    # determination of the frequency bin which is the closest to f_injected
    f_injected = float(f_injected)
    ifreq = int(f_injected / df_asm + 0.5)
    status_param = 'OK' if f_injected == ifreq*df_asm else 'KO'
    if echo:
        print( 'Fréquence injectée pour la calibration : {} Hz'.format(f_injected) )
        print( 'Raie la plus proche de la freq injectée: {} Hz, i.e raie {} (indice)'.format(ifreq*df_asm, ifreq) )
        print( 'Statut sur paramètres ok si ces 2 fréquences sont identiques: {}'.format(status_param) )
    if f_injected >= asm['fe']/2:
        f_injected_modulo = (f_injected%asm['fe'])
        ifreq_modulo = (ifreq%nw)
        if f_injected_modulo < asm['fe']/2:
            f_aliasing = f_injected_modulo
            ifreq_aliasing = ifreq_modulo            
        else:                    
            f_aliasing = asm['fe'] - f_injected_modulo
            ifreq_aliasing = nw - ifreq_modulo
        if echo:
            print( 'Repliement de la fréquence injectée: {} Hz'.format(f_aliasing) )
            print( 'Raie la plus proche correspondante : {} Hz, i.e raie {} (indice)'.\
                                format(ifreq_aliasing*df_asm, ifreq_aliasing) )
        ifreq = ifreq_aliasing
    # corresponding ASM index (LFR drop first and last bins among the 128 ones)
    i0  = [17, 6, 7][F]
    imax  = [17+88-1, 6+104-1, 7+96-1][F]      
    ifreq_asm = ifreq - i0 if ifreq < imax else imax - i0
        
    # calibration of the ASM power spectra (auto-correlations)    
    corr = 0.5*(1-1./256) if hanning else 1.
    if not calib_max:
        if echo: print('Compute calibration with the bin at expected place ...')
        calib_V2count = [ float( np.sqrt(asm['asm_idl'][iasm, ifreq_asm, icomp_j, icomp_j].real / (corr*abs(ampl_j)/2.)**2) ) \
                          for (icomp_j, ampl_j) in zip(icomp, ampl) ]
    else:
        if echo: print('Compute calibration with the most intense bin ...')
        calib_V2count = [ float( np.sqrt(np.max(asm['asm_idl'][iasm, :, icomp_j, icomp_j].real) / (corr*abs(ampl_j)/2.)**2) )\
                          for (icomp_j, ampl_j) in zip(icomp, ampl) ]
    ifreq_max = [ np.argmax(asm['asm_idl'][iasm, :, icomp_j, icomp_j].real) for icomp_j in icomp  ]
    status_auto = [ 'OK' if i == ifreq_asm else 'KO' for i in ifreq_max ]
    freq_max = [asm['freq'][i] for i in ifreq_max]
    
    if echo:
        print( '##### CALIBRATION OF the ASM POWER-SPECTRA (AUTOCORRELATION) #####' )
        print( 'Component (name)           :', end ='' )
        for name in comp:
            print( ' {:>8s}'.format(name), end ='' )
        print()
        print( 'Component (index)          :', end ='' )
        for index in icomp:
            print( ' {:8d}'.format(index), end ='' )
        print()
        print( 'Amplitude (V)              :', end ='' )
        for val in ampl:
            print( ' {:8.2f}'.format(abs(val)), end ='' )
        print()
        print( 'Conversion factor (count/V):', end ='' )
        for val in calib_V2count:
            print( ' {:8.2f}'.format(val), end ='' )
        print()
        print( 'Frequency max auto (index) :', end ='' )
        for ind in ifreq_max:
            print( ' {:8d}'.format(ind), end ='' )
        print()
        print( 'Frequency max auto (Hz)    :', end ='' )
        for freq in freq_max:
            print( ' {:8.1f}'.format(freq), end ='' )
        print()
        print( 'Autocorrelation status     :', end ='' )
        for cal in status_auto:
            print( ' {:>8s}'.format(cal), end ='' )
        print()
        print( '                                   (ok if injected frequency corresponds to a max)' )
        
    # calibration of the ASM relative phases (cross-correlations)
    # complex values of ampl allow to correct the relative phases from phase differencies between the input 
    # signals: multiplying by np.conj(ampl_i/abs(ampl_i))*(ampl_j/abs(ampl_j)) permits to get free from them ...
    rg = range(len(ifreq_max))
    if not calib_max:
        if echo: print('Compute phase with the bin at expected place ...')
        cross_norm = [ complex( np.conj(ampl_i/abs(ampl_i))*asm['asm_idl'][iasm, ifreq_asm, icomp_i, icomp_j]*(ampl_j/abs(ampl_j)) /   \
                                np.sqrt(asm['asm_idl'][iasm, ifreq_asm, icomp_i, icomp_i].real*   \
                                        asm['asm_idl'][iasm, ifreq_asm, icomp_j, icomp_j].real) ) \
                       for icomp_i, ampl_i in zip(icomp, ampl) for icomp_j, ampl_j in zip(icomp, ampl) if icomp_j > icomp_i ]
    else:
        if echo: print('Compute phase with the most intense bin ...')
        cross_norm = [ complex( np.conj(ampl[i]/abs(ampl[i]))*asm['asm_idl'][iasm, ifreq_max[i], icomp[i], icomp[j]]*(ampl[j]/abs(ampl[j])) /  \
                                np.sqrt(asm['asm_idl'][iasm, ifreq_max[i], icomp[i], icomp[i]].real*   \
                                        asm['asm_idl'][iasm, ifreq_max[j], icomp[j], icomp[j]].real) ) \
                       for i in rg for j in rg  if j > i ]
    status_cross = [ 'OK' if (ifreq_max[i] == ifreq_asm and ifreq_max[j] == ifreq_asm) else 'KO' for i in rg for j in rg  if j > i ]
    
    if echo:
        print( '##### CALIBRATION OF THE ASM CROSS-SPECTRA (CROSSCORRELATION) #####' )
        print( 'icomp[i] icomp[j]    l     S_ij/SQRT(S_ii*Sjj)     rho       phi    cross status' )
        print( '                               (normalized)                  (°)                ' )

        l = 0
        for icomp_i in icomp:
            for icomp_j in icomp:
                if icomp_j > icomp_i:
                    print( '{:7d}{:9d}{:6d} {:22.5f} {:9.5f} {:9.3f} {:>8s}'. \
                            format(icomp_i, icomp_j, l, cross_norm[l],    \
                                   abs(cross_norm[l]),                    \
                                   np.angle(cross_norm[l], deg=True),     \
                                   status_cross[l]) )
                    l = l + 1

    return {'calib_V2count':  calib_V2count, 
            'cross_norm':     cross_norm,    
            'status_param':   status_param,  
            'status_auto':    status_auto,   
            'status_cross':   status_cross,  
            'freq_max':       freq_max,      
            'icomp':          icomp,         
            'comp':           comp,          
            'ampl':           ampl,          
            'f_injected':     f_injected,    
            'df_asm':         df_asm,   
            'calib_max':      calib_max,  
            'iasm':           iasm,                    
            'datatype':       datatype}                                                    
    
def calib_matrix_sm(*tf_freqs_set, echo=False):
    ncomp = len(tf_freqs_set)    
    nfreq = len(tf_freqs_set[0])
    calib_mat_freqs = np.array([np.diagflat([1/tf_freqs[i] for tf_freqs in tf_freqs_set]) for i in range(nfreq)])
    if echo:
        print('%d components with %d frequencies'%(ncomp, nfreq))
        print('shape of output: %s'%(str(calib_mat_freqs.shape)))
    return calib_mat_freqs
    
def calibrate_sm_old(sm, calib_mat, echo=False):
    nspec, nfreq, dim, _ = sm.shape    
    nfreq_cal, dim_cal, _ = calib_mat.shape
    if nfreq != nfreq_cal or dim != dim_cal:
        raise TotoError('problematic data instance !?')
    sm_calibrated = np.zeros((nspec, nfreq, dim, dim), dtype=complex)
    for i in range(nfreq):
        _ = np.dot( sm[:, i, :, :], np.conj(calib_mat[i, :, :].T) )    
        sm_calibrated[:, i, :, :] = np.swapaxes(np.dot( calib_mat[i, :, :], _ ), 0, 1)
    if echo:
        print("calibrated sm shape: ", sm_calibrated.shape)
    return sm_calibrated
    
def calibrate_sm(sm, calib_mat, echo=False):
    nspec, nfreq, dim, _ = sm.shape  
    ndim = calib_mat.ndim
    if ndim == 3:        
        nfreq_cal, dim_cal, _ = calib_mat.shape        
    elif ndim == 4:
        nspec_cal, nfreq_cal, dim_cal, _ = calib_mat.shape
    else:
        raise TotoError('problematic data instance !?', calib_mat.shape)                  
    if nfreq != nfreq_cal or dim != dim_cal:
            raise TotoError('problematic data instance !?', (nfreq, nfreq_cal), (dim, dim_cal)) 
    if ndim == 4 and nspec != nspec_cal:        
        raise TotoError('problematic data instance !?', nspec, nspec_cal)                      
    sm_calibrated = np.zeros((nspec, nfreq, dim, dim), dtype=complex)
    if ndim == 4:
        for i in range(nspec):
            for j in range(nfreq):            
                _ = np.dot( sm[i, j, :, :], np.conj(calib_mat[i, j, :, :].T) )
                sm_calibrated[i, j, :, :] = np.dot( calib_mat[i, j, :, :], _ )
    else:
        for j in range(nfreq):
            _ = np.dot( sm[:, j, :, :], np.conj(calib_mat[j, :, :].T) )    
            sm_calibrated[:, j, :, :] = np.swapaxes(np.dot( calib_mat[j, :, :], _ ), 0, 1)                    
    if echo:
        print("calibrated sm shape: ", sm_calibrated.shape)
    return sm_calibrated    
    
def calibrate_psd_auto(psd_auto, calib_tf_gain, echo=False):
    nspec, nfreq = psd_auto.shape
    ndim = 1
    if calib_tf_gain.ndim == 2:
        ndim = 2
        nspec_cal, nfreq_cal = calib_tf_gain.shape 
    else:
        nfreq_cal = len(calib_tf_gain)                 
    if nfreq != nfreq_cal:        
        raise TotoError('problematic data instance !?', nfreq, nfreq_cal)
    if ndim == 2 and nspec != nspec_cal:        
        raise TotoError('problematic data instance !?', nspec, nspec_cal)    
    psd_auto_calibrated = psd_auto / calib_tf_gain**2              
    if echo:
        print("L1 input psd_auto shape:   ", psd_auto.shape)
        print("calib_tf_gain shape:       ", calib_tf_gain.shape)
        print("calibrated psd_auto shape: ", psd_auto_calibrated.shape)
    return psd_auto_calibrated 
    
def calibrate_psd_cross(psd_cross, calib_tf_gain_l, calib_tf_gain_r, echo=False):
    nspec, nfreq = psd_cross.shape    
    ndim_l, ndim_r = 1, 1
    if calib_tf_gain_l.ndim == 2:
        ndim_l = 2
        nspec_cal_l, nfreq_cal_l = calib_tf_gain_l.shape 
    else:
        nfreq_cal_l = len(calib_tf_gain_l)     
    if calib_tf_gain_r.ndim == 2:
        ndim_r = 2
        nspec_cal_r, nfreq_cal_r = calib_tf_gain_r.shape 
    else:
        nfreq_cal_r = len(calib_tf_gain_r)         
    if nfreq != nfreq_cal_l or nfreq != nfreq_cal_r:        
        raise TotoError('problematic data instance !?', nfreq, nfreq_cal_l, nfreq_cal_r)
    if (ndim_l == 2 and nspec != nspec_cal_l) or (ndim_r == 2 and nspec != nspec_cal_r):        
        raise TotoError('problematic data instance !?', nspec, nspec_cal_l, nspec_cal_r)      
    psd_cross_calibrated = psd_cross / calib_tf_gain_l / calib_tf_gain_r        
    if echo:
        print("L1 input psd_cross shape:   ", psd_cross.shape)
        print("calib_tf_gain_l shape:      ", calib_tf_gain_l.shape)
        print("calib_tf_gain_r shape:      ", calib_tf_gain_r.shape)
        print("calibrated psd_cross shape: ", psd_cross_calibrated.shape)       
    return psd_cross_calibrated 

def freq_average_of_asm(asm_data, nfreq_ave=8, psd=True):
    """
    The output unit (asm_data_ave) is the same as the input unit (asm_data).
    When the unit of asm is a psd (power spectral density) the average formula
    is given  by Sum(asm*df)/nfreq_ave / (nfreq_ave*df) otherwise just by Sum(asm)/nfreq_ave.    
    A REVOIR a priori erreur !! ... 21/10/2020
    """
    nspec_asm, nfreq_asm, dim_asm, _ = asm_data.shape 
    nfreq_asm_ave = int(nfreq_asm/nfreq_ave)
    asm_data_ave = np.zeros((nspec_asm, nfreq_asm_ave, dim_asm, dim_asm), dtype=complex)
    for i in range(nfreq_asm_ave):
        asm_data_ave[:, i, ...] = np.sum(asm_data[:, i*nfreq_ave:i*nfreq_ave+nfreq_ave, ...], axis=1) / (nfreq_ave**2 if psd else nfreq_ave)
    return asm_data_ave
    
def freq_average_of_sm(sm, freq_sm, nfreq_ave=8, j1=None, j2=None, echo=False):
    """
    Compute the frequency average of a given spectral matrix (sm) over packets 
    of nfreq_ave consecutive bins. The average formula is the same whatever the unit 
    of the spectral matrix (psd, power spectral density; or just spectral power): 
    sm_ave = Sum(sm*df) / (nfreq_ave*df) = Sum(sm)/nfreq_ave
    The unit of output (sm_ave) is the same as the unit of input (sm).
    version 1.0: correction and improvement of "freq_average_of_asm" ...  [27 octobre 2021]    
    """
    nspec_sm, nfreq_sm, dim_sm, _ = sm.shape 
    if j1 == None: j1 = 0
    if j2 == None: j2 = nfreq_sm       
    nfreq_sm_ave = (j2 - j1) // nfreq_ave
    freq_sm_ave = np.array([freq_sm[j1+k*nfreq_ave] for k in range(nfreq_sm_ave)]) \
                  + (freq_sm[nfreq_ave-1]- freq_sm[0])/2
    sm_ave = np.zeros((nspec_sm, nfreq_sm_ave, dim_sm, dim_sm), dtype=complex)
    if echo:
        print('sm_ave      :', sm_ave.shape)
        print('freq_sm_ave :', freq_sm_ave.shape)        
    for k in range(nfreq_sm_ave):
        sm_ave[:, k, ...] = np.sum(sm[:, (j1+k*nfreq_ave):(j1+k*nfreq_ave+nfreq_ave), ...], axis=1) \
                            / nfreq_ave    
    return sm_ave, freq_sm_ave    
    
def sm_from_swf_old(swf_B, swf_E, swf_V, swf_time, F=0, na=8, echo=False, DC=True, win=None, 
                    unit='count^2/Hz', background=True):
    """
    The normalization aims at correcting the biasing effect of apodization. 
    If background option is set the spectra give an unbiased estimate of the power spectral densities of 
    broadband waves (the factor 2 take into account the negative frequencies, the factor 8/3 the energy attenuation 
    due to the application of the Hanning window itself). If not, they give an estimate of the amplitudes of 
    coherent waves as if no window were applied, i.e. as (A/2)**2.
    """
    data_block = np.concatenate((swf_B, swf_E, swf_V), axis=1)
    nw = int(2048/na)
    rm_dc = False if win == None else True    
    fs = LFR_Fs[F] if unit == 'count^2/Hz' else nw   # fs=nw  => df=1Hz in order to get count^2 units ...
    if win in ['hanning', 'hanning_idl']: 
        norm = np.sqrt(2.*8./3) if background else 2. # normalization with energy (background) or with amplitude
    else: 
        norm = 1.    
    sm = spectral_matrices(norm*data_block, fs=fs, nw=nw, na=na, echo=echo, win=win, rm_dc=rm_dc)       
    sm_from_swf = sm['asm'] if DC else sm['asm'][:, (2 if rm_dc else 1):, ...]     
    sm_time_from_swf = swf_time[::2048]
    df = LFR_Fs[F] / nw
    sm_freq_from_swf = np.arange(0 if DC else (2 if rm_dc else 1 )*df, int(nw/2 + 1)*df, df)
    return sm_from_swf, sm_time_from_swf, sm_freq_from_swf      
    
def sm_from_swf(swf_B, swf_E, swf_V, swf_time, F=0, nw=256, na=8, echo=False, DC=True, win=None, 
                rm_dc=None, unit='count^2/Hz', background=True):
    """
    The normalization aims at correcting the biasing effect of apodization. 
    If background option is set the spectra give an unbiased estimate of the power spectral densities of 
    broadband waves (the factor 2 take into account the negative frequencies, the factor 8/3 the energy attenuation 
    due to the application of the Hanning window itself). If not, they give an estimate of the amplitudes of 
    coherent waves as if no window were applied, i.e. as (A/2)**2.
    """
    data_block = np.concatenate((swf_B, swf_E, swf_V), axis=1)
    if rm_dc == None:
        rm_dc = False if win == None else True        
    fs = LFR_Fs[F] if unit == 'count^2/Hz' else nw   # fs=nw  => df=1Hz in order to get count^2 units ...
    if win in ['hanning', 'hanning_idl']: 
        norm = np.sqrt(2.*8./3) if background else 2. # normalization with energy (background) or with amplitude
    else: 
        norm = 1.    
    if echo:
        print('window: %s'%(win))
        print('rm_dc : %s'%(rm_dc))
        print('norm  : %.2f'%(norm))            
    sm = spectral_matrices(norm*data_block, fs=fs, nw=nw, na=na, echo=echo, win=win, rm_dc=rm_dc)       
    sm_from_swf = sm['asm'] if DC else sm['asm'][:, (2 if rm_dc else 1):, ...]     
    sm_time_from_swf = swf_time[::na*nw]
    df = LFR_Fs[F] / nw
    sm_freq_from_swf = np.arange(0 if DC else (2 if rm_dc else 1 )*df, int(nw/2 + 1)*df, df)
    return sm_from_swf, sm_time_from_swf, sm_freq_from_swf     
    
def sm_from_cwf(cwf_B, cwf_E, cwf_V, cwf_time, F=3, nw=256, na=8, echo=False, DC=True, win=None, 
                 rm_dc=None, unit='count^2/Hz', background=True):
    """
    The normalization aims at correcting the biasing effect of apodization. 
    If background option is set the spectra give an unbiased estimate of the power spectral densities of 
    broadband waves (the factor 2 take into account the negative frequencies, the factor 8/3 the energy attenuation 
    due to the application of the Hanning window itself). If not, they give an estimate of the amplitudes of 
    coherent waves as if no window were applied, i.e. as (A/2)**2.
    """
    data_block = np.concatenate((cwf_B, cwf_E, cwf_V), axis=1)
    if rm_dc == None:
        rm_dc = False if win == None else True    
    fs = LFR_Fs[F] if unit == 'count^2/Hz' else nw    # fs=nw  => df=1Hz in order to get count^2 units ...
    if win in ['hanning', 'hanning_idl']: 
        norm = np.sqrt(2.*8./3) if background else 2. # normalization with energy (background) or with amplitude
    else: 
        norm = 1.
    if echo:
        print('window: %s'%(win))
        print('rm_dc : %s'%(rm_dc))
        print('norm  : %.2f'%(norm))       
    sm = spectral_matrices(norm*data_block, fs=fs, nw=nw, na=na, echo=echo, win=win, rm_dc=rm_dc)       
    sm_from_cwf = sm['asm'] if DC else sm['asm'][:, (2 if rm_dc else 1):, ...]     
    sm_time_from_cwf = cwf_time[::na*nw][0:-1 if len(cwf_time) % (na*nw) else None]
    df = LFR_Fs[F] / nw
    sm_freq_from_cwf = np.arange(0 if DC else (2 if rm_dc else 1 )*df, int(nw/2 + 1)*df, df)
    return sm_from_cwf, sm_time_from_cwf, sm_freq_from_cwf          
    
def calib_sm_lfr_sensor(sm_data, sm_freq, sm_R, sm_BW, sm_mux, sm_g, cdffiles_TF, list_F=[0,1,2], echo=False):
    """
    Calibrate the "original" 5x5 electromagnetic spectral matrices (3B+2E) in two steps. 
    First applying the LFR transfer functions then the sensor transfer functions.
    
INPUT:
    F = 0, 1 or 2 
    sm_data[F]: electromagnetic spectral matrices [B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])     
                where the magnetic components are given in the SCM B1-B2-B3 coordinate system 
                and the electric components in the ANT V12-V23 coordinate system                 
                [ndarray(nspec, nfreq, dim_vector, dim_vector)
                    with nspec = nb of spectral matrices,
                         nfreq = nb of frequency bins,
                         dim_vector = 5 for 3b + 2e,             
                 sm_data_b = sm[..., 0:3, 0:3]
                 sm_data_e = sm[..., 3:, 3:], complex and in count^2 or count^2/Hz unit]                 
    sm_freq[F]: ndarray(nfreq)          
    sm_R[F]:    ndarray(nspec) [routing parameter of the electric signals]             
    sm_BW[F]:   ndarray(nspec) [parameter for bias working or not]
    sm_mux[F]:  ndarray(nspec) [bias mux mode parameter]            
    sm_g[F]:    ndarray(nspec) [bias differential gain parameter]
    cdffiles_TF: list of the absolute paths for loading the lfr and sensor tranfert functions                          
    
OUPUT:
    sm_data_cal_sensor[F]: ndarray(nspec, nfreq, dim_vector, dim_vector)        
    """                        
    # absolute paths of the lfr and sensor TF files
    [cdffile_LFR_TF_B, cdffile_LFR_TF_E, cdffile_LFR_TF_VHF, 
     cdffile_SCM_TF,   cdffile_BIAS_TF,  cdffile_ANT_HF_PREAMP_TF] = cdffiles_TF

    # loading LFR TF (SCM, BIAS and VHF channels)                       
    (LFR_TF_B_ampli, LFR_TF_B_phase, LFR_TF_B_freqs) = load_LFR_TF_B_CDF(cdffile_LFR_TF_B, echo=echo)  
    LFR_TF_B = 4*[None]
    for F in range(4): 
        LFR_TF_B[F] = { 'Freq':  LFR_TF_B_freqs[F], 
                        'Gain':  LFR_TF_B_ampli[F],
                        'Phase': LFR_TF_B_phase[F] }                           
    (LFR_TF_E_ampli, LFR_TF_E_phase, LFR_TF_E_freqs) = load_LFR_TF_E_CDF(cdffile_LFR_TF_E, echo=echo)  
    LFR_TF_E = 4*[None]
    for F in range(4): 
        LFR_TF_E[F] = { 'Freq':  LFR_TF_E_freqs[F], 
                        'Gain':  LFR_TF_E_ampli[F],
                        'Phase': LFR_TF_E_phase[F]}                
    LFR_TF_VHF_ampli = 3*[None]
    LFR_TF_VHF_phase = 3*[None]
    LFR_TF_VHF_freqs = 3*[None]    
    for F in [0, 1, 2]:    
        [LFR_TF_VHF_ampli[F], LFR_TF_VHF_phase[F], LFR_TF_VHF_freqs[F]] = \
        load_field_CDF(cdffile_LFR_TF_VHF, ["TF_VHF_123_amplitude_F%s"%(F), 
                                            "TF_VHF_123_phase_F%s"%(F), 
                                            "Freqs_F%s"%(F)], echo=echo)       
    LFR_TF_VHF = 3*[None]
    for F in range(3): 
        LFR_TF_VHF[F] = { 'Freq': LFR_TF_VHF_freqs[F], 
                          'Gain': LFR_TF_VHF_ampli[F],
                          'Phase': LFR_TF_VHF_phase[F]}      
        
    # loading SCM TF    
    SCM_TF_comp = load_SCM_TF_comp_CDF(cdffile_SCM_TF, temp=-50., lfr_on=True, echo=echo)   

    # loading BIAS TF        
    TF_coeffs = load_field_CDF(cdffile_BIAS_TF, ['TRANSFER_FUNCTION_COEFFS'], echo=echo)[0]
    def H_DC_SE_G1s17(f):
        s = 1j*2*np.pi*f 
        return polyval(s, TF_coeffs[0, :, 0]) / polyval(s, TF_coeffs[1, :, 0])
    def H_DC_DIFF_G1(f):
        s = 1j*2*np.pi*f 
        return polyval(s, TF_coeffs[0, :, 1]) / polyval(s, TF_coeffs[1, :, 1])
    def H_AC_DIFF_G5(f):
        s = 1j*2*np.pi*f 
        return polyval(s, TF_coeffs[0, :, 2]) / polyval(s, TF_coeffs[1, :, 2])
    def H_AC_DIFF_G100(f):
        s = 1j*2*np.pi*f 
        return polyval(s, TF_coeffs[0, :, 3]) / polyval(s, TF_coeffs[1, :, 3])
                 
    # loading ANT HF-PREAMP TF        
    [ANT_TF_HF_PREAMP_ampli, ANT_TF_HF_PREAMP_phase, ANT_TF_HF_PREAMP_freqs] = \
        load_field_CDF(cdffile_ANT_HF_PREAMP_TF, ["CALIBRATION_AMPLITUDE", 
                                                  "CALIBRATION_PHASE", 
                                                  "CALIBRATION_FREQUENCY"], echo=echo)  
    ANT_TF_HF_PREAMP_ampli = np.squeeze(ANT_TF_HF_PREAMP_ampli)
    ANT_TF_HF_PREAMP_phase = np.squeeze(ANT_TF_HF_PREAMP_phase)
    ANT_TF_HF_PREAMP_freqs = np.squeeze(ANT_TF_HF_PREAMP_freqs)
    ANT_TF_HF_PREAMP_freqs = np.insert(ANT_TF_HF_PREAMP_freqs, 0, 1)
    ANT_TF_HF_PREAMP_phase = np.insert(ANT_TF_HF_PREAMP_phase, 0, -130, axis=1)
    ANT_TF_HF_PREAMP_ampli = np.insert(ANT_TF_HF_PREAMP_ampli, 0,   -2, axis=1)
    ANT_TF_HF_PA = { 'Freq': ANT_TF_HF_PREAMP_freqs, 
                     'Gain': ANT_TF_HF_PREAMP_ampli,
                     'Phase': ANT_TF_HF_PREAMP_phase} 

    # define a function returning the right electric TF depending on the configuration parameters
    def BIAS_ANT_HF_PA_TF_E1E2(freqs, bias_work, lfr_R_param, bias_mux_mode, bias_diff_gain):
        if bias_work == 0:
            TF_VHF2 = ANT_TF_HF_PA_interp(freqs, ANT_TF_HF_PA, 1, dB=True, polar=False)            # VHF2
            TF_VHF3 = ANT_TF_HF_PA_interp(freqs, ANT_TF_HF_PA, 2, dB=True, polar=False)            # VHF3
            return [TF_VHF2, TF_VHF3]
        if lfr_R_param == 0:                                   
            TF_E_AC = H_AC_DIFF_G100(freqs) if bias_diff_gain else H_AC_DIFF_G5(freqs)             # BIAS4 BIAS5
            return [TF_E_AC, TF_E_AC] 
        if lfr_R_param == 1:               
            TF_E1_DC = H_DC_DIFF_G1(freqs) if bias_mux_mode == 0 else H_DC_SE_G1s17(freqs)         # BIAS2
            TF_E2_DC = H_DC_SE_G1s17(freqs) if bias_mux_mode in [4, 5, 6] else H_DC_DIFF_G1(freqs) # BIAS3  
            return [TF_E1_DC, TF_E2_DC]   
            
    # calibration at LFR level        
    calib_tf_freqs_sm_scm_lfr = 3*[None]
    calib_tf_freqs_sm_bias_lfr = 3*[None]
    calib_tf_freqs_sm_vhf_lfr = 3*[None]
    sm_data_cal_lfr = 3*[None]
    for F in list_F:
        calib_tf_freqs_sm_scm_lfr[F] = np.array( \
                        [[LFR_TF_interp(f, LFR_TF_B[F], 0) for f in sm_freq[F]],
                         [LFR_TF_interp(f, LFR_TF_B[F], 1) for f in sm_freq[F]],
                         [LFR_TF_interp(f, LFR_TF_B[F], 2) for f in sm_freq[F]]] )
        calib_tf_freqs_sm_bias_lfr[F] = np.array( \
                        [[LFR_TF_interp(f, LFR_TF_E[F], 1) for f in sm_freq[F]],   # BIAS2 
                         [LFR_TF_interp(f, LFR_TF_E[F], 2) for f in sm_freq[F]],   # BIAS3 
                         [LFR_TF_interp(f, LFR_TF_E[F], 3) for f in sm_freq[F]],   # BIAS4 
                         [LFR_TF_interp(f, LFR_TF_E[F], 4) for f in sm_freq[F]]] ) # BIAS5 
        calib_tf_freqs_sm_vhf_lfr[F] = np.array( \
                        [[LFR_TF_interp(f, LFR_TF_VHF[F], 0) for f in sm_freq[F]],   # VHF1 
                         [LFR_TF_interp(f, LFR_TF_VHF[F], 1) for f in sm_freq[F]],   # VHF2 
                         [LFR_TF_interp(f, LFR_TF_VHF[F], 2) for f in sm_freq[F]]] ) # VHF3 
        nspec = len(sm_R[F])
        nfreq = len(sm_freq[F])
        calib_mat_freqs_sm_BE_lfr = np.zeros((nspec, nfreq, 5, 5), dtype=complex)        
        for i in range(nspec):
            calib_mat_freqs_sm_BE_lfr[i, :, 0, 0] = 1/calib_tf_freqs_sm_scm_lfr[F][0, :]
            calib_mat_freqs_sm_BE_lfr[i, :, 1, 1] = 1/calib_tf_freqs_sm_scm_lfr[F][1, :]
            calib_mat_freqs_sm_BE_lfr[i, :, 2, 2] = 1/calib_tf_freqs_sm_scm_lfr[F][2, :]
            r = sm_R[F][i]  
            bw = sm_BW[F][i]
            if bw == 1:
                # BIAS4 BIAS5 if r == 0 
                # BIAS2 BIAS3 if r == 1
                calib_mat_freqs_sm_BE_lfr[i, :, 3, 3] = 1/calib_tf_freqs_sm_bias_lfr[F][2-2*r, :]
                calib_mat_freqs_sm_BE_lfr[i, :, 4, 4] = 1/calib_tf_freqs_sm_bias_lfr[F][3-2*r, :]
            else:
                # VHF2 VHF3 if bw == 0 (the 3 HF-PA are indeed almost the same ...)
                calib_mat_freqs_sm_BE_lfr[i, :, 3, 3] = 1/calib_tf_freqs_sm_vhf_lfr[F][1, :]
                calib_mat_freqs_sm_BE_lfr[i, :, 4, 4] = 1/calib_tf_freqs_sm_vhf_lfr[F][2, :]        
        sm_data_cal_lfr[F] = calibrate_sm(sm_data[F], calib_mat_freqs_sm_BE_lfr, echo=echo)      
                
    # calibration at sensor level                
    calib_mat_freqs_sm_scm_sensor = 3*[None]
    calib_tf_freqs_sm_bias_hf_pa_sensor_E1E2 = 3*[None]
    sm_data_cal_sensor = 3*[None]
    for F in list_F:
        calib_mat_freqs_sm_scm_sensor[F] = SCM_TF_mat_interp(sm_freq[F], SCM_TF_comp, reverse=True, dB=True, debug=False)
        calib_tf_freqs_sm_bias_hf_pa_sensor_E1E2[F] = np.array([BIAS_ANT_HF_PA_TF_E1E2(sm_freq[F], bw, r, mux, g) \
                                                      for (bw, r, mux, g) in zip(sm_BW[F], sm_R[F], sm_mux[F], sm_g[F])])    
        nspec = len(sm_R[F])
        nfreq = len(sm_freq[F])                                              
        calib_mat_freqs_sm_BE_sensor = np.zeros((nspec, nfreq, 5, 5), dtype=complex)
        for i in range(nspec):
            calib_mat_freqs_sm_BE_sensor[i, :, 0:3, 0:3] = calib_mat_freqs_sm_scm_sensor[F]
        calib_mat_freqs_sm_BE_sensor[:, :, 3, 3] = 1/calib_tf_freqs_sm_bias_hf_pa_sensor_E1E2[F][:,0,:]
        calib_mat_freqs_sm_BE_sensor[:, :, 4, 4] = 1/calib_tf_freqs_sm_bias_hf_pa_sensor_E1E2[F][:,1,:]
        sm_data_cal_sensor[F] = calibrate_sm(sm_data_cal_lfr[F], calib_mat_freqs_sm_BE_sensor, echo=echo)                    
    return sm_data_cal_sensor           
    

















    

