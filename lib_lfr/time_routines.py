# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import numpy as np

class TotoError(Exception):
    pass  
    
def date_from_sec2000(t, string=False):
    """
    t is a time in second from 2000-01-01:00:00:00.000000 UTC
    """
    tref = datetime(2000, 1, 1)
    d = tref + timedelta(seconds=t)    
    return datetime.strftime(d, '%Y-%m-%dT%H:%M:%S.%f') if string else d
    
def sec2000_from_date(d):
    """
    d is a date (UTC) either as a string (format YYYY-MM-DDThh:mm:ss.uuuuuu) or as a datetime
    """
    tref = datetime(2000, 1, 1) # UTC
    if isinstance(d, str): t = datetime.strptime(d[0:26], '%Y-%m-%dT%H:%M:%S.%f')                
    if isinstance(d, datetime):  t = d                
    return (t-tref).total_seconds()
    
def v_datestring_to_datetime(dates, str_format='%Y-%m-%dT%H:%M:%S.%f'):
    """
    dates are dates as string with a given format
    default format: 'YYYY-MM-DDThh:mm:ss.uuuuuu'
    """                 
    return [datetime.strptime(d, str_format) for d in np.array(dates, ndmin=1).astype(str)]    
            
def v_hour_to_datetime(hours, day=datetime(2000, 1, 1, 0, 0, 0)):
    """
    hours of the day
    """
    return [day + timedelta(hours=h) for h in np.array(hours, ndmin=1).astype(float)]      
    
def v_sec_to_datetime(sec, tref=datetime(2020, 5, 30, 0, 0, 0)):
    """
    sec are seconds from tref UTC
    """
    return [tref + timedelta(seconds=s) for s in np.array(sec, ndmin=1).astype(float)]        
    
def index_from_date(date, date_arr, epsi=None):
    """
    Determine the index corresponding to a date in date_arr 
    """   
    if epsi == None:
        epsi = date - date
    if (date_arr[-1] < date-epsi) or (date_arr[0] > date+epsi):
            raise TotoError("%s est en dehors de la période de données ..."%(date))    
    index = np.argmin(np.abs(np.asarray(date_arr) - date))
    return index 

def index_same_date(date1, date2, first=True, epsi=None):
    """
    in the case where the date1 and date2 series are included in each other
    """
    if first:     
        # date1 is included in date2
        ik_date2 = [index_from_date(d1, date2, epsi=epsi) for d1 in date1]
    else:
        # date2 is included in date1        
        ik_date1 = [index_from_date(d2, date1, epsi=epsi) for d2 in date2]
    return ik_date2 if first else ik_date1         
    
def index_asm_included_in_swf(date_asm, date_snap, d1=None, d2=None):
    """
    Search for asm time intervals that are included in swf time intervals.
    Work for F2 data (ASM time resolution is 4s and SWF duration is 8s).    
    The dates correpond to the time of the first sampling of the time series
    on which the data are constructed
    """        
    if d1 == None: d1 = date_asm[0]
    if d2 == None: d2 = date_asm[-1]    
    i_asm_list = []
    i_snap_list = []
    for i_asm, d_asm in enumerate(date_asm):
        if d1 <= d_asm <= d2:         
            i_snap = len(date_snap[date_snap <= d_asm]) - 1
            if date_snap[i_snap] + timedelta(seconds=4) >= d_asm:
                i_asm_list.append(i_asm)
                i_snap_list.append(i_snap)            
    return i_asm_list, i_snap_list     
    

