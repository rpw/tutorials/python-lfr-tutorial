# -*- coding: utf-8 -*-

import numpy as np

from lib_parsing.parse_routines import is_scalar
from lib_maths.conversion_routines import B_to_Fce

from datetime import datetime
from numpy.linalg import norm
from scipy.stats import linregress



class TotoError(Exception):
    pass

def sm_3b2e_bp1_FSW_R3_2_0_24(sm, k44_pe=1., k55_pe=1., k45_pe=1.+1.j,           
                                  k14_sx=1.+1.j, k15_sx=1.+1.j, k24_sx=1.+1.j, k25_sx=1.+1.j, k34_sx=1.+1.j, k35_sx=1.+1.j,                          
                                  k24_ny=1.+1.j, k25_ny=1.+1.j, k34_ny=1.+1.j, k35_ny=1.+1.j,                          
                                  k24_nz=1.+1.j, k25_nz=1.+1.j, k34_nz=1.+1.j, k35_nz=1.+1.j, 
                                  alphaM = +45., ok_dop_e=False, echo=False):
    """
    Routine qui calcule les basic parameters BP1 à partir d'une matrice spectrale 3B+2E en entrée 
    (ASM moyennée ou BP2 dénormalisé)
    
INPUT:
    sm:     electromagnetic spectral matrices [B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])                      
            [ndarray(nspec, nfreq, dim_vector, dim_vector)
                with nspec = nb of spectral matrices,
                     nfreq = nb of frequency bins,
                     dim_vector = 5 for 3b + 2e,             
             sm_b = sm[..., 0:3, 0:3]
             sm_e = sm[..., 3:, 3:], complex and a priori in count^2 unit]
               
    kij_pe: kcoefficients for PE (Power spectrum of the electric field)
    kij_sx: kcoefficients for SX (X_SO-component of the Poynting vector)
    kij_ny, kij_nz: kcoefficients for the nY and nZ terms of VPHI (Phase velocity estimator)    
    [ndarray(nfreq)]
    // Im(S_ji) = -Im(S_ij)
    // k_ji = k_ij has been used in the FSW implementation ...
    
    alphaM: rotation angle of SCM2 (+Y_M) around SCM1 axis (+X_M // +Z_SO) [in degrees] 
                   
OUTPUT:    
    trace_sm_b:   total magnetic wave power     (positive float)  [ndarray(nspec,nfreq)]
    trace_sm_e:   total electric wave power     (positive float)  [ndarray(nspec,nfreq)] 
    degree_polar: degree of polarization        ( 0 ≤ float ≤ 1)  [ndarray(nspec,nfreq)]        
    wave_nvector: wave normal vector components (-1 ≤ float ≤ 1)  [ndarray(nspec,nfreq,3)]
    wave_ellipti: wave ellipticity estimator    ( 0 ≤ float ≤ 1)  [ndarray(nspec,nfreq)]
    sx:           x-component of the Poynting vector (float)      [ndarray(nspec,nfreq)]
    sx_arg:       1 argument bit value          (complex plane divided into two sectors) [ndarray(nspec,nfreq)]
    vphi:         phase velocity estimator      (float)           [ndarray(nspec,nfreq)]
    vphi_arg:     1 argument bit value          (complex plane divided into two sectors) [ndarray(nspec,nfreq)] 

    Degree of polarization:
        It detects pure state waves. 100% indicates a pure state wave. 
        Less than 60-70% indicates a non pure state wave (noise, two or 
        more waves, ...). It is invariant under unitary coordinate transformation.
        For more information see J. C. Samson and J. V. Olson, 'Some comments
        on the descriptions of the polarization states of waves', 
        Geophys. J. R. Astr. Soc. (1980) V61, 115-129.
        It works for general n-dimensional processes. 
        Here we compute both assessments: with 3b, 2e and 3b+2e, respectively, if ok_dop_e is set.

    Wave normal vector:
        The components of the wave normal vector calculated from the complex 
        off-diagonal elements of the magnetic spectral matrix.
        For more information see Means, 'Use of the Three-dimensional covariance 
        matrix in analyzing the polarization properties of plane waves', JGR, 1972.
        It is not invariant under coordinate transformation: it assumes
        an orthogonal frame and a same calibration factor for each component ...

    Wave ellipticity:
        An estimate of the wave ellipticity calculated from the complex 
        off-diagonal elements of the magnetic spectral matrix. 
        => e = 2 / (a/b + b/a)
        It is based on the article of Means (see above, same remarks). 

    {'psd_b_bp1':         trace_sm_b,              
     'psd_e_bp1':         trace_sm_e, 
     'degree_polar_3b':   degree_polar_3b, 
     'degree_polar_2e':   degree_polar_2e, 
     'degree_polar_3b2e': degree_polar_3b2e, 
     'dop_bp1':           degree_polar_3b**2,                  
     'nvec_bp1':          wave_nvector_3b, 
     'ellip_bp1':         wave_ellipti_3b,
     'sx':                sx,
     'sx_bp1':            sx.real,  
     'sx_arg_bp1':        sx_arg,                      
     'vphi':              vphi,            
     'vphi_bp1':          vphi.real,
     'vphi_arg_bp1':      vphi_arg}
                                        
    version 1.0: first version with Python from the last version of the idl routine sm_3b2e_characterization_lfr.pro 
                                                                                               [17-19 décembre 2018]                                                                                               
    version 1.1: implementation of the frequency dependence of the kcoefficients                   [18 février 2020]
    """    
    # get the matrix dimension and the number of matrices and frequencies
    nspec, nfreq, dim_vector, _ = sm.shape
    if echo:
        print('Shape of the spectral matrix structure (BP2 or ASM_ave) from which the basic parameters BP1 will be computed :')              
        print('nspec: %d, nfreq: %d, dim: %d'%(nspec, nfreq, dim_vector))
                                    
    # kcoefficient initialisation    
    if is_scalar(k44_pe): k44_pe = np.zeros(nfreq) + k44_pe
    if is_scalar(k55_pe): k55_pe = np.zeros(nfreq) + k55_pe    
    if is_scalar(k45_pe): k45_pe = np.zeros(nfreq) + k45_pe 
       
    if is_scalar(k14_sx): k14_sx = np.zeros(nfreq) + k14_sx    
    if is_scalar(k15_sx): k15_sx = np.zeros(nfreq) + k15_sx    
    if is_scalar(k24_sx): k24_sx = np.zeros(nfreq) + k24_sx    
    if is_scalar(k25_sx): k25_sx = np.zeros(nfreq) + k25_sx    
    if is_scalar(k34_sx): k34_sx = np.zeros(nfreq) + k34_sx                
    if is_scalar(k35_sx): k35_sx = np.zeros(nfreq) + k35_sx                
    
    if is_scalar(k24_ny): k24_ny = np.zeros(nfreq) + k24_ny    
    if is_scalar(k25_ny): k25_ny = np.zeros(nfreq) + k25_ny    
    if is_scalar(k34_ny): k34_ny = np.zeros(nfreq) + k34_ny    
    if is_scalar(k35_ny): k35_ny = np.zeros(nfreq) + k35_ny
        
    if is_scalar(k24_nz): k24_nz = np.zeros(nfreq) + k24_nz    
    if is_scalar(k25_nz): k25_nz = np.zeros(nfreq) + k25_nz    
    if is_scalar(k34_nz): k34_nz = np.zeros(nfreq) + k34_nz    
    if is_scalar(k35_nz): k35_nz = np.zeros(nfreq) + k35_nz        
        
    # compute the degree of polarization with 3 magnetic field components 
    # and the total magnetic power 
    # PB = <BxBx*> + <ByBy*> + <BzBz*>  
    trS = np.zeros((nspec,nfreq))
    for k in [0,1,2]: trS += sm[..., k, k].real            # sm_b = sm[:, :, 0:3, 0:3]
    trS_power2 = trS**2    
    trS2 = np.sum( abs(sm[..., 0:3, 0:3])**2, axis=(2,3) ) # S2 = S # S
                                                           # trS2 = S2(0,0) + S2(1,1) + S2(2,2) ...
                                                           #      = Sum_ij of |S(i,j)|^2
    degree_polar_3b = np.sqrt( (3*trS2 - trS_power2) / ((3-1)*trS_power2) )
    trace_sm_b = trS        
    
    # compute the degree of polarization with 2 electric field components
    # and the total electric power
    # PE = <ExEx*> + <EyEy*>  
    trS = np.zeros((nspec,nfreq))                          # sm_e = sm[..., 3:, 3:]
    for i in range(nfreq): trS[:, i] = k44_pe[i]*sm[:, i, 3, 3].real + \
                                       k55_pe[i]*sm[:, i, 4, 4].real + \
                                      (k45_pe[i]*sm[:, i, 3, 4]).real                                       
    trS_power2 = trS**2
    trS2 = np.sum( abs(sm[..., 3:, 3:])**2, axis=(2,3) )   # S2 = S # S
                                                           # trS2 = S2(0,0) + S2(1,1) + S2(2,2) ...
                                                           #      = Sum_ij of |S(i,j)|^2     
    degree_polar_2e = np.sqrt( (2*trS2 - trS_power2) / ((2-1)*trS_power2) ) \
                      if ok_dop_e else np.nan * np.ones((nspec,nfreq))
    trace_sm_e = trS

    # compute the degree of polarization with 5 electromagnetic field components 
    # with a normalisation between E and B
    sm_be_norm = sm.copy()
    coeff = np.sqrt((2./3)*trace_sm_b / trace_sm_e)
    for i in [3,4]:
        for j in [3,4]:
            sm_be_norm[..., i, j] = sm[..., i, j] * coeff**2 
    for i in [0,1,2]:
        for j in [3,4]:
            sm_be_norm[..., i, j] = sm[..., i, j] * coeff 
            sm_be_norm[..., j, i] = sm[..., j, i] * coeff                
    trS = np.zeros((nspec,nfreq))
    for k in range(dim_vector): trS += sm_be_norm[..., k, k].real
    trS_power2 = trS**2
    trS2 = np.sum( abs(sm_be_norm)**2, axis=(2,3) )        # S2 = S # S
                                                           # trS2 = S2(0,0) + S2(1,1) + S2(2,2) ...
                                                           #      = Sum_ij of |S(i,j)|^2             
    degree_polar_3b2e = np.sqrt( (dim_vector*trS2 - trS_power2) / ((dim_vector-1)*trS_power2) ) \
                        if ok_dop_e else np.nan * np.ones((nspec,nfreq))

    # compute the wave normal vector
    wave_nvector_3b = np.zeros((nspec, nfreq, 3))          # sm_b = sm[:, :, 0:3, 0:3]
    offdiagonal_im = np.array([sm[..., 0, 1], sm[..., 0, 2], sm[..., 1, 2]]).imag
    #              =          [       n3    ,       -n2    ,         n1   ] * ab
    ab = np.sqrt( np.sum( offdiagonal_im**2, axis=0 ) )
    wave_nvector_3b[..., 0]= +offdiagonal_im[2, ...] / ab  # n1
    wave_nvector_3b[..., 1]= -offdiagonal_im[1, ...] / ab  # n2
    wave_nvector_3b[..., 2]= +offdiagonal_im[0, ...] / ab  # n3
    
    # compute the wave ellipticity
    wave_ellipti_3b = 2*ab / trace_sm_b                    # e = 2 / (a/b + b/a)

    # compute the x-component of the Poynting vector 
    # sx =  <EyBz*> - <EzBy*>   
    # sm_b = sm[:, :, 0:3, 0:3]
    # sm_e = sm[..., 3:, 3:]
    sx = np.zeros((nspec,nfreq), dtype=complex)
    for i in range(nfreq): sx[:, i] = k14_sx[i]*sm[:, i, 3, 0] + k15_sx[i]*sm[:, i, 4, 0] + \
                                      k24_sx[i]*sm[:, i, 3, 1] + k25_sx[i]*sm[:, i, 4, 1] + \
                                      k34_sx[i]*sm[:, i, 3, 2] + k35_sx[i]*sm[:, i, 4, 2]   
    sx_arg = np.zeros((nspec,nfreq), dtype=np.int8)
    indices_where_put_one = np.nonzero( abs(sx.imag) > abs(sx.real) )
    sx_arg[ indices_where_put_one ] = 1                                               

    # compute an estimate of the phase velocity 
    # vphi = ( ny <EzBx*> - nz <EyBx*> ) /  <BxBx*>
    # sm_b = sm[:, :, 0:3, 0:3]
    # sm_e = sm[..., 3:, 3:]
    alphaM = np.deg2rad(alphaM)
    ny = np.sin(alphaM)*wave_nvector_3b[..., 1] + np.cos(alphaM)*wave_nvector_3b[..., 2]
    nz = wave_nvector_3b[..., 0]
    bxbx = sm[..., 1, 1].real*np.cos(alphaM)**2 + sm[..., 2, 2].real*np.sin(alphaM)**2  + \
           -2*np.sin(alphaM)*np.cos(alphaM)*sm[..., 1, 2].real        
    vphi = np.zeros((nspec,nfreq), dtype=complex)       
    for i in range(nfreq): vphi[:, i] = ( ny[:, i]*(k24_ny[i]*sm[:, i, 3, 1] + k25_ny[i]*sm[:, i, 4, 1] +  \
                                                    k34_ny[i]*sm[:, i, 3, 2] + k35_ny[i]*sm[:, i, 4, 2])   \
                                        + nz[:, i]*(k24_nz[i]*sm[:, i, 3, 1] + k25_nz[i]*sm[:, i, 4, 1] +  \
                                                    k34_nz[i]*sm[:, i, 3, 2] + k35_nz[i]*sm[:, i, 4, 2]) ) / bxbx[:, i]
    vphi_arg = np.zeros((nspec,nfreq), dtype=np.int8)    
    vphi_arg[ np.nonzero( abs(vphi.imag) > abs(vphi.real) ) ] = 1
            
    return {'psd_b_bp1':         trace_sm_b,                 
            'psd_e_bp1':         trace_sm_e,   
            'degree_polar_3b':   degree_polar_3b,
            'dop_bp1':           degree_polar_3b**2,           
            'degree_polar_2e':   degree_polar_2e, 
            'degree_polar_3b2e': degree_polar_3b2e, 
            'nvec_bp1':          wave_nvector_3b,
            'ellip_bp1':         wave_ellipti_3b,
            'sx':                sx,
            'sx_bp1':            sx.real,  
            'sx_arg_bp1':        sx_arg,                      
            'vphi':              vphi,            
            'vphi_bp1':          vphi.real,
            'vphi_arg_bp1':      vphi_arg}
            
def sm_3b2e_bp1(sm, k44_pe=1.,  k55_pe=1., k45_pe=0.,           
                    k14_sx=0.,  k15_sx=0., k24_sx=0., k25_sx=-1., k34_sx=1., k35_sx=0.,                          
                    k14_ny=0.,  k15_ny=1., k24_ny=0., k25_ny=0.,  k34_ny=0., k35_ny=0.,                          
                    k14_nz=-1., k15_nz=0., k24_nz=0., k25_nz=0.,  k34_nz=0., k35_nz=0., 
                    M_SOprime_SCM = np.diagflat([1,1,1]), M_tilde_SOprime=np.diagflat([1,1,1]),
                    ok_dop_e=False, echo=False):
    """
    Routine qui calcule les basic parameters BP1 à partir d'une matrice spectrale 3B+2E en entrée 
    (ASM moyennée ou BP2 dénormalisé)
    
INPUT:
    sm:     electromagnetic spectral matrices [B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])                      
            [ndarray(nspec, nfreq, dim_vector, dim_vector)
                with nspec = nb of spectral matrices,
                     nfreq = nb of frequency bins,
                     dim_vector = 5 for 3b + 2e,             
             sm_b = sm[..., 0:3, 0:3]
             sm_e = sm[..., 3:, 3:], complex and a priori in count^2 unit]
               
    kij_pe: kcoefficients for PE (Power spectrum of the electric field)
    kij_sx: kcoefficients for SX (X_SO-component of the Poynting vector)
    kij_ny, kij_nz: kcoefficients for the nY and nZ terms of VPHI (Phase velocity estimator)    
    [ndarray(nfreq)]
    // Im(S_ji) = -Im(S_ij)
    // k_ji = k_ij has been used in the FSW implementation ...
    +<EyBz*> = +<E1B3*>  => k34_sx = +1 by default (in case the transformation into SRF is already done)
    -<EzBy*> = -<E2B2*>  => k25_sx = -1 by default (in case the transformation into SRF is already done)
    +<EzBx*> = +<E2B1*>  => k15_ny = +1 by default (in case the transformation into SRF is already done)
    -<EyBx*> = -<E1B1*>  => k14_nz = -1 by default (in case the transformation into SRF is already done)
    
    M_SOprime_SCM: transformation matrix from the SCM to the SO’ coordinate system 
    M_tilde_SOprime: transfer matrix of the SCM antennas and transformation into the SO' coordinate system 
                     (see "BP_2020-06-18_V8.pdf")                        
                   
OUTPUT:    
    trace_sm_b:   total magnetic wave power     (positive float)  [ndarray(nspec,nfreq)]
    trace_sm_e:   total electric wave power     (positive float)  [ndarray(nspec,nfreq)] 
    degree_polar: degree of polarization        ( 0 ≤ float ≤ 1)  [ndarray(nspec,nfreq)]        
    wave_nvector: wave normal vector components (-1 ≤ float ≤ 1)  [ndarray(nspec,nfreq,3)]
    wave_ellipti: wave ellipticity estimator    ( 0 ≤ float ≤ 1)  [ndarray(nspec,nfreq)]
    sx:           x-component of the Poynting vector (float)      [ndarray(nspec,nfreq)]
    sx_arg:       1 argument bit value          (complex plane divided into two sectors) [ndarray(nspec,nfreq)]
    vphi:         phase velocity estimator      (float)           [ndarray(nspec,nfreq)]
    vphi_arg:     1 argument bit value          (complex plane divided into two sectors) [ndarray(nspec,nfreq)] 

    Degree of polarization:
        It detects pure state waves. 100% indicates a pure state wave. 
        Less than 60-70% indicates a non pure state wave (noise, two or 
        more waves, ...). It is invariant under unitary coordinate transformation.
        For more information see J. C. Samson and J. V. Olson, 'Some comments
        on the descriptions of the polarization states of waves', 
        Geophys. J. R. Astr. Soc. (1980) V61, 115-129.
        It works for general n-dimensional processes. 
        Here we compute both assessments: with 3b, 2e and 3b+2e, respectively, if ok_dop_e is set.

    Wave normal vector:
        The components of the wave normal vector calculated from the complex 
        off-diagonal elements of the magnetic spectral matrix.
        For more information see Means, 'Use of the Three-dimensional covariance 
        matrix in analyzing the polarization properties of plane waves', JGR, 1972.
        It is not invariant under coordinate transformation: it assumes
        an orthogonal frame and a same calibration factor for each component ...

    Wave ellipticity:
        An estimate of the wave ellipticity calculated from the complex 
        off-diagonal elements of the magnetic spectral matrix. 
        => e = 2 / (a/b + b/a)
        It is based on the article of Means (see above, same remarks). 

    {'psd_b_bp1':         trace_sm_b,              
     'psd_e_bp1':         trace_sm_e, 
     'degree_polar_3b':   degree_polar_3b, 
     'degree_polar_2e':   degree_polar_2e, 
     'degree_polar_3b2e': degree_polar_3b2e, 
     'dop_bp1':           degree_polar_3b**2,                  
     'nvec_bp1':          wave_nvector_3b, 
     'ellip_bp1':         wave_ellipti_3b,
     'sx':                sx,
     'sx_bp1':            sx.real,  
     'sx_arg_bp1':        sx_arg,                      
     'vphi':              vphi,            
     'vphi_bp1':          vphi.real,
     'vphi_arg_bp1':      vphi_arg}
                                        
    version 1.0: first version with Python from the last version of the idl routine sm_3b2e_characterization_lfr.pro 
                                                                                               [17-19 décembre 2018]
    version 1.1: implementation of the frequency dependance of the kcoefficients                   [18 février 2020]
    version 2.0: update of vphi with a more general formula                                           [30 juin 2020]
    """    
    # get the matrix dimension and the number of matrices and frequencies
    nspec, nfreq, dim_vector, _ = sm.shape
    if echo:
        print('Shape of the spectral matrix structure (BP2 or ASM_ave) from which the basic parameters BP1 will be computed :')              
        print('nspec: %d, nfreq: %d, dim: %d'%(nspec, nfreq, dim_vector))
                                    
    # kcoefficients and M_tilde_SOprime shall have a frequency dependance    
    if is_scalar(k44_pe): k44_pe = np.zeros(nfreq) + k44_pe
    if is_scalar(k55_pe): k55_pe = np.zeros(nfreq) + k55_pe    
    if is_scalar(k45_pe): k45_pe = np.zeros(nfreq) + k45_pe 
       
    if is_scalar(k14_sx): k14_sx = np.zeros(nfreq) + k14_sx    
    if is_scalar(k15_sx): k15_sx = np.zeros(nfreq) + k15_sx    
    if is_scalar(k24_sx): k24_sx = np.zeros(nfreq) + k24_sx    
    if is_scalar(k25_sx): k25_sx = np.zeros(nfreq) + k25_sx    
    if is_scalar(k34_sx): k34_sx = np.zeros(nfreq) + k34_sx                
    if is_scalar(k35_sx): k35_sx = np.zeros(nfreq) + k35_sx                
    
    if is_scalar(k14_ny): k14_ny = np.zeros(nfreq) + k14_ny    
    if is_scalar(k15_ny): k15_ny = np.zeros(nfreq) + k15_ny   
    if is_scalar(k24_ny): k24_ny = np.zeros(nfreq) + k24_ny    
    if is_scalar(k25_ny): k25_ny = np.zeros(nfreq) + k25_ny    
    if is_scalar(k34_ny): k34_ny = np.zeros(nfreq) + k34_ny    
    if is_scalar(k35_ny): k35_ny = np.zeros(nfreq) + k35_ny
    
    if is_scalar(k14_nz): k14_nz = np.zeros(nfreq) + k14_nz    
    if is_scalar(k15_nz): k15_nz = np.zeros(nfreq) + k15_nz        
    if is_scalar(k24_nz): k24_nz = np.zeros(nfreq) + k24_nz    
    if is_scalar(k25_nz): k25_nz = np.zeros(nfreq) + k25_nz    
    if is_scalar(k34_nz): k34_nz = np.zeros(nfreq) + k34_nz    
    if is_scalar(k35_nz): k35_nz = np.zeros(nfreq) + k35_nz    
    
    if M_tilde_SOprime.ndim == 2: M_tilde_SOprime = np.array([M_tilde_SOprime for i in range(nfreq)])    
        
    # compute the degree of polarization with 3 magnetic field components 
    # and the total magnetic power 
    # PB = <BxBx*> + <ByBy*> + <BzBz*>  
    trS = np.zeros((nspec,nfreq))
    for k in [0,1,2]: trS += sm[..., k, k].real            # sm_b = sm[:, :, 0:3, 0:3]
    trS_power2 = trS**2    
    trS2 = np.sum( abs(sm[..., 0:3, 0:3])**2, axis=(2,3) ) # S2 = S # S
                                                           # trS2 = S2(0,0) + S2(1,1) + S2(2,2) ...
                                                           #      = Sum_ij of |S(i,j)|^2
    degree_polar_3b = np.sqrt( (3*trS2 - trS_power2) / ((3-1)*trS_power2) )
    trace_sm_b = trS        
    
    # compute the degree of polarization with 2 electric field components
    # and the total electric power
    # PE = <ExEx*> + <EyEy*>  
    trS = np.zeros((nspec,nfreq))                          # sm_e = sm[..., 3:, 3:]
    for i in range(nfreq): trS[:, i] = k44_pe[i]*sm[:, i, 3, 3].real + \
                                       k55_pe[i]*sm[:, i, 4, 4].real + \
                                      (k45_pe[i]*sm[:, i, 3, 4]).real                                       
    trS_power2 = trS**2
    trS2 = np.sum( abs(sm[..., 3:, 3:])**2, axis=(2,3) )   # S2 = S # S
                                                           # trS2 = S2(0,0) + S2(1,1) + S2(2,2) ...
                                                           #      = Sum_ij of |S(i,j)|^2     
    degree_polar_2e = np.sqrt( (2*trS2 - trS_power2) / ((2-1)*trS_power2) ) \
                      if ok_dop_e else np.nan * np.ones((nspec,nfreq))
    trace_sm_e = trS

    # compute the degree of polarization with 5 electromagnetic field components 
    # with a normalisation between E and B
    sm_be_norm = sm.copy()
    coeff = np.sqrt((2./3)*trace_sm_b / trace_sm_e)
    for i in [3,4]:
        for j in [3,4]:
            sm_be_norm[..., i, j] = sm[..., i, j] * coeff**2 
    for i in [0,1,2]:
        for j in [3,4]:
            sm_be_norm[..., i, j] = sm[..., i, j] * coeff 
            sm_be_norm[..., j, i] = sm[..., j, i] * coeff                
    trS = np.zeros((nspec,nfreq))
    for k in range(dim_vector): trS += sm_be_norm[..., k, k].real
    trS_power2 = trS**2
    trS2 = np.sum( abs(sm_be_norm)**2, axis=(2,3) )        # S2 = S # S
                                                           # trS2 = S2(0,0) + S2(1,1) + S2(2,2) ...
                                                           #      = Sum_ij of |S(i,j)|^2             
    degree_polar_3b2e = np.sqrt( (dim_vector*trS2 - trS_power2) / ((dim_vector-1)*trS_power2) ) \
                        if ok_dop_e else np.nan * np.ones((nspec,nfreq))

    # compute the wave normal vector
    wave_nvector_3b = np.zeros((nspec, nfreq, 3))          # sm_b = sm[:, :, 0:3, 0:3]
    offdiagonal_im = np.array([sm[..., 0, 1], sm[..., 0, 2], sm[..., 1, 2]]).imag
    #              =          [       n3    ,       -n2    ,         n1   ] * ab
    ab = np.sqrt( np.sum( offdiagonal_im**2, axis=0 ) )
    wave_nvector_3b[..., 0]= +offdiagonal_im[2, ...] / ab  # n1
    wave_nvector_3b[..., 1]= -offdiagonal_im[1, ...] / ab  # n2
    wave_nvector_3b[..., 2]= +offdiagonal_im[0, ...] / ab  # n3
    
    # compute the wave ellipticity
    wave_ellipti_3b = 2*ab / trace_sm_b                    # e = 2 / (a/b + b/a)

    # compute the x-component of the Poynting vector 
    # sx =  <EyBz*> - <EzBy*>   
    # sm_b = sm[:, :, 0:3, 0:3]
    # sm_e = sm[..., 3:, 3:]
    sx = np.zeros((nspec,nfreq), dtype=complex)
    for i in range(nfreq): sx[:, i] = k14_sx[i]*sm[:, i, 3, 0] + k15_sx[i]*sm[:, i, 4, 0] + \
                                      k24_sx[i]*sm[:, i, 3, 1] + k25_sx[i]*sm[:, i, 4, 1] + \
                                      k34_sx[i]*sm[:, i, 3, 2] + k35_sx[i]*sm[:, i, 4, 2]   
    sx_arg = np.zeros((nspec,nfreq), dtype=np.int8)
    indices_where_put_one = np.nonzero( abs(sx.imag) > abs(sx.real) )
    sx_arg[ indices_where_put_one ] = 1                                               

    # compute an estimate of the phase velocity 
    # vphi = ( ny <EzBx*> - nz <EyBx*> ) /  <BxBx*>
    # sm_b = sm[:, :, 0:3, 0:3]
    # sm_e = sm[..., 3:, 3:]    
    wave_nvector_3b_SOprime = np.moveaxis(np.dot(M_SOprime_SCM, np.moveaxis(wave_nvector_3b, -1, -2)), 0, -1)
    ny = wave_nvector_3b_SOprime[..., 1]
    nz = wave_nvector_3b_SOprime[..., 2]    
    bxbx = np.zeros(nspec, dtype=float)
    vphi = np.zeros((nspec, nfreq), dtype=complex)       
    for i in range(nfreq):
        bxbx = sm[:, i, 0, 0].real*np.abs(M_tilde_SOprime[i, 0, 0])**2 + \
               sm[:, i, 1, 1].real*np.abs(M_tilde_SOprime[i, 0, 1])**2 + \
               sm[:, i, 2, 2].real*np.abs(M_tilde_SOprime[i, 0, 2])**2 + \
               2*(sm[:, i, 0, 1]*M_tilde_SOprime[i, 0, 0]*np.conj(M_tilde_SOprime[i, 0, 1])).real + \
               2*(sm[:, i, 0, 2]*M_tilde_SOprime[i, 0, 0]*np.conj(M_tilde_SOprime[i, 0, 2])).real + \
               2*(sm[:, i, 1, 2]*M_tilde_SOprime[i, 0, 1]*np.conj(M_tilde_SOprime[i, 0, 2])).real
        vphi[:, i] = ( ny[:, i]*(k14_ny[i]*sm[:, i, 3, 0] + k15_ny[i]*sm[:, i, 4, 0] +  \
                                 k24_ny[i]*sm[:, i, 3, 1] + k25_ny[i]*sm[:, i, 4, 1] +  \
                                 k34_ny[i]*sm[:, i, 3, 2] + k35_ny[i]*sm[:, i, 4, 2])   \
                     + nz[:, i]*(k14_nz[i]*sm[:, i, 3, 0] + k15_nz[i]*sm[:, i, 4, 0] +  \
                                 k24_nz[i]*sm[:, i, 3, 1] + k25_nz[i]*sm[:, i, 4, 1] +  \
                                 k34_nz[i]*sm[:, i, 3, 2] + k35_nz[i]*sm[:, i, 4, 2]) ) / bxbx                                                                                    
    vphi_arg = np.zeros((nspec,nfreq), dtype=np.int8)    
    vphi_arg[ np.nonzero( abs(vphi.imag) > abs(vphi.real) ) ] = 1
            
    return {'psd_b_bp1':         trace_sm_b,                 
            'psd_e_bp1':         trace_sm_e,   
            'degree_polar_3b':   degree_polar_3b,
            'dop_bp1':           degree_polar_3b**2,           
            'degree_polar_2e':   degree_polar_2e, 
            'degree_polar_3b2e': degree_polar_3b2e, 
            'nvec_bp1':          wave_nvector_3b,
            'ellip_bp1':         wave_ellipti_3b,
            'sx':                sx,
            'sx_bp1':            sx.real,  
            'sx_arg_bp1':        sx_arg,                      
            'vphi':              vphi,            
            'vphi_bp1':          vphi.real,
            'vphi_arg_bp1':      vphi_arg}            
                                            
def polarization_ellipse(sm):
    """
    Samson, J. C., & Olson, J. V. 1980, Geophys. J. Int., 61, 115
    In the presence of a pure state, the form of the complex spectral matrix reads: 
    S = lambda u u.T = (r1 + i r2) (r1.T - i r2.T), with r1, r2 real vectors and r1.r2 = 0.
    Application for n=3
INPUT:
    sm(nspec, nfreq, 3, 3): spectral matrix                  
OUTPUT:
    r1(nspec, nfreq, 3): major axis of the polarization ellipse
    r2(nspec, nfreq, 3): minor axis of the polarization ellipse
    """
    nspec, nfreq, dim, _ = sm.shape
    u = sm[..., 0, :].copy()                           # state vector   
    u[..., 0] =  np.sqrt(u[..., 0].real)               # a1 = sqrt(S11)
    u[..., 1] =  np.conjugate(u[..., 1]) / u[..., 0]   # a2 exp(i phi2) = S12*/a1
    u[..., 2] =  np.conjugate(u[..., 2]) / u[..., 0]   # a3 exp(i phi1) = S13*/a1
    r = np.zeros((nspec, nfreq, 3), dtype=complex) 
    r1 =  np.zeros((nspec, nfreq, 3), dtype=float) 
    r2 =  np.zeros((nspec, nfreq, 3), dtype=float) 
    for i in range(nspec):
        for j in range(nfreq):
            gamma = np.arctan(2*np.inner(u[i, j, :].real, u[i, j, :].imag) / \
                              (norm(u[i, j, :].real)**2 - norm(u[i, j, :].imag)**2))
            phi = -gamma/2  # [Pi/2]    
            r[i, j, :] = np.exp(1j*phi) * u[i, j, :]  
            if norm(r[i, j, :].real) >= norm(r[i, j, :].imag):
                r1[i, j, :] = r[i, j, :].real 
                r2[i, j, :] = r[i, j, :].imag
            else:
                r1[i, j, :] = r[i, j, :].imag
                r2[i, j, :] = r[i, j, :].real                            
    return r1, r2
                    
def wave_normal_vector_theta(nvec, time, B0, timeB0, deg=True):
    """
    nvec(nspec, nfreq, 3): wave normal vector components expressed in the same coordinate system as B0
    B0(:, 3): MAG magnetic field vectors from which the angle with nvec is calculated
    work also with any array(nspec, nfreq, 3) like S(:, :, 3)...
    """
    nspec, nfreq, dim_nvec = nvec.shape
    nB0, dim_B0 = B0.shape    
    if (len(time) != nspec) or (len(timeB0) != nB0) or (dim_nvec != 3) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')    
    if isinstance(timeB0[0], datetime) and isinstance(time[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(time[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (time[0] < timeB0[0]) or (time[-1] > timeB0[-1]):
        raise TotoError("time interval is not included within timeB0 interval ...")           
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, time, side='left')    
    bvec = B0[indexes_nearestB0, :]
    theta = np.zeros((nspec, nfreq))
    for i in range(nspec):
        theta[i, :] = np.arccos(np.inner(nvec[i, :, :], bvec[i, :]) / norm(bvec[i, :]) / \
                      norm(nvec[i, :, :], axis=-1))
    return np.rad2deg(theta) if deg else theta      
    
def vector3D_theta(vec, time, B0, timeB0, deg=True, sharp=False):
    """
    vec(nspec, nfreq, 3): vector components expressed in the same coordinate system as B0
    B0(:, 3): MAG magnetic field vectors from which the angle with vec is calculated    
    theta((nspec, nfreq): output within [0, 180] degrees if not sharp option set
                          else within [0, 90] degrees by applying the transformation
                          (180 - theta) if theta is an obtuse angle
    """
    nspec, nfreq, dim_vec = vec.shape
    nB0, dim_B0 = B0.shape    
    if (len(time) != nspec) or (len(timeB0) != nB0) or (dim_vec != 3) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')    
    if isinstance(timeB0[0], datetime) and isinstance(time[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(time[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (time[0] < timeB0[0]) or (time[-1] > timeB0[-1]):
        raise TotoError("time interval is not included within timeB0 interval ...")           
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, time, side='left')    
    bvec = B0[indexes_nearestB0, :]
    theta = np.zeros((nspec, nfreq))
    for i in range(nspec):
        theta[i, :] = np.arccos(np.inner(vec[i, :, :], bvec[i, :]) / norm(bvec[i, :]) / \
                      norm(vec[i, :, :], axis=-1))  
    if sharp: 
        index_obtuse = (theta > np.pi/2)
        theta[index_obtuse] = np.pi - theta[index_obtuse]                                    
    return np.rad2deg(theta) if deg else theta                 
    
def EX_from_EdotB0(E, time, B0, timeB0):
    """
    E(n, 2): Y, Z electric field components expressed in the same (orthonormal) coordinate system as B0
    B0(:, 3): MAG magnetic field vectors from which the X component of E is calculated assuming E.B0=0
    """
    n, dim = E.shape
    nB0, dim_B0 = B0.shape    
    if (len(time) != n) or (len(timeB0) != nB0) or (dim != 2) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')    
    if isinstance(timeB0[0], datetime) and isinstance(time[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(time[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (time[0] < timeB0[0]) or (time[-1] > timeB0[-1]):
        raise TotoError("time interval is not included within timeB0 interval ...")  
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, time, side='left')    
    bvec = B0[indexes_nearestB0, :]   
    EX = np.zeros(n)
    for i in range(n):
        EX[i] = -np.inner(E[i, :], bvec[i, 1:]) / bvec[i, 0]    
    return EX
    
def Fce_from_B0(time, B0, timeB0):
    """
    time(n): time to which the electron gyrofrequency fce is computed 
    B0(:, 3): MAG magnetic field vector from which fce is computed 
    """
    nB0, dim_B0 = B0.shape    
    if (len(timeB0) != nB0) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')    
    if isinstance(timeB0[0], datetime) and isinstance(time[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(time[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (time[0] < timeB0[0]) or (time[-1] > timeB0[-1]):
        raise TotoError("time interval is not included within timeB0 interval ...")  
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, time, side='left')    
    bvec = B0[indexes_nearestB0, :]   
    return B_to_Fce(norm(bvec, axis=-1), nT=True, kHz=False)
                            
def Poynting_vector(sm_be, coeff=1e7/4/np.pi/2):
    """
    From the B-E spectral matrix expressed in a xyz direct orthonormal coordinate
    system, compute the xyz-components of the complex Poynting vector E x B*.
    P = 1/2 Re[E x B*] / mu0 => by default: coeff = 1/2 * 1/mu0 in MKS     
INPUT:
    sm_be(nspec, nfreq, 6, 6): B-E spectral matrix  
    sm_b = sm_be[:  :, 0:3, 0:3]
    sm_e = sm_be[:, :, 3:, 3:]                
OUTPUT:
    S(nspec, nfreq, 3)                      
    by default S is in (nW/m^2)/Hz if E in V/m and B in nT (V/m * nT / mu0)        
    """
    # Sx =  (<EyBz*> - <EzBy*>) * coeff
    # Sy =  (<EzBx*> - <ExBz*>) * coeff     
    # Sz =  (<ExBy*> - <EyBx*>) * coeff 
    nspec, nfreq, dim, _ = sm_be.shape            
    S = np.zeros((nspec, nfreq, 3), dtype=complex)
    S[..., 0] = (sm_be[..., 4, 2] - sm_be[..., 5, 1]) * coeff
    S[..., 1] = (sm_be[..., 5, 0] - sm_be[..., 3, 2]) * coeff       
    S[..., 2] = (sm_be[..., 3, 1] - sm_be[..., 4, 0]) * coeff
    return S  
    
def phaseveloEyBy_cross_EdotB0_SRF(EyBystar, BzBystar, BxBystar, nvec, time, B0, timeB0, coeff=1e9/1e3):
    """
    Since k x E = w*B, if E.B0 = 0, then: E = -(w/k.B0) * (B0 x B)
    In particular: Ey = (w/k.BO) * (B0x*Bz - B0z*Bx)
    k = nvec * K, where K is a signed wavenumber  
    b0 = B0 / |B0|
    Hence: EyBystar = w/K * (1/nvec.b0) * (b0x*BzBystar - b0z*BxBystar)
    The sign of w/K gives the direction of propagation w/r to nvec
INPUT:    
    All vector/tensor components are expressed in SRF  
    EyBystar(nspec, nfreq): crosscorrelation between the Ey and By components
    BzBystar(nspec, nfreq): crosscorrelation between the Bz and By components
    BxBystar(nspec, nfreq): crosscorrelation between the Bx and By components
    nvec(nspec, nfreq, 3): wave normal vector (Means, JGR, 1972), i.e. the unit vector defining the
                           polarization plane and oriented in such a way that δB turns to the right
                           with respect to it (RH, i.e. counter-clock wise or direct direction of rotation)
    time(nspec): time of the spectral products
    B0(npts, 3): MAG magnetic field vector
    timeB0(npts): time of B0        
OUTPUT
    phasevelo(nspec, nfreq): w/K  
    by default the result is given in km/s if E in V/m and B in nT (V/m / T / 1e3)        
    """
    nspec, nfreq, dim_nvec = nvec.shape
    nspecEyBy, nfreqEyBy = EyBystar.shape
    nB0, dim_B0 = B0.shape        
    if (len(time) != nspec) or (len(timeB0) != nB0) or (dim_nvec != 3) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')   
    if (nspecEyBy != nspec) or (nfreqEyBy != nfreq):
        raise TotoError('problematic dimension of inputs!!')       
    if isinstance(timeB0[0], datetime) and isinstance(time[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(time[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (time[0] < timeB0[0]) or (time[-1] > timeB0[-1]):
        raise TotoError("time interval is not included within timeB0 interval ...")           
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, time, side='left')    
    b0 = B0[indexes_nearestB0, :]
    b0 = (b0.T / norm(b0, axis=-1)).T
    phasevelo = np.zeros((nspec, nfreq), dtype=complex)
    for i in range(nspec):
        phasevelo[i, :] = EyBystar[i, :] *  np.inner(nvec[i, :, :], b0[i, :]) / \
                          (b0[i, 0]*BzBystar[i, :] - b0[i, 2]*BxBystar[i, :]) 
    return coeff * phasevelo       
      
def phaseveloEyBz_cross_EdotB0_SRF(EyBzstar, BzBzstar, BxBzstar, nvec, time, B0, timeB0, coeff=1e9/1e3):
    """
    Since k x E = w*B, if E.B0 = 0, then: E = -(w/k.B0) * (B0 x B)
    In particular: Ey = (w/k.BO) * (B0x*Bz - B0z*Bx)
    k = nvec * K, where K is a signed wavenumber  
    b0 = B0 / |B0|
    Hence: EyBzstar = w/K * (1/nvec.b0) * (b0x*BzBzstar - b0z*BxBzstar)
    The sign of w/K gives the direction of propagation w/r to nvec
INPUT:    
    All vector/tensor components are expressed in SRF  
    EyBzstar(nspec, nfreq): crosscorrelation between the Ey and Bz components
    BzBzstar(nspec, nfreq): crosscorrelation between the Bz and Bz components
    BxBzstar(nspec, nfreq): crosscorrelation between the Bx and Bz components
    nvec(nspec, nfreq, 3): wave normal vector (Means, JGR, 1972), i.e. the unit vector defining the
                           polarization plane and oriented in such a way that δB turns to the right
                           with respect to it (RH, i.e. counter-clock wise or direct direction of rotation)
    time(nspec): time of the spectral products
    B0(npts, 3): MAG magnetic field vector
    timeB0(npts): time of B0        
OUTPUT
    phasevelo(nspec, nfreq): w/K  
    by default the result is given in km/s if E in V/m and B in nT (V/m / T / 1e3)        
    """
    nspec, nfreq, dim_nvec = nvec.shape
    nspecEyBz, nfreqEyBz = EyBzstar.shape
    nB0, dim_B0 = B0.shape        
    if (len(time) != nspec) or (len(timeB0) != nB0) or (dim_nvec != 3) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')   
    if (nspecEyBz != nspec) or (nfreqEyBz != nfreq):
        raise TotoError('problematic dimension of inputs!!')       
    if isinstance(timeB0[0], datetime) and isinstance(time[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(time[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (time[0] < timeB0[0]) or (time[-1] > timeB0[-1]):
        raise TotoError("time interval is not included within timeB0 interval ...")           
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, time, side='left')    
    b0 = B0[indexes_nearestB0, :]
    b0 = (b0.T / norm(b0, axis=-1)).T
    phasevelo = np.zeros((nspec, nfreq), dtype=complex)
    for i in range(nspec):
        phasevelo[i, :] = EyBzstar[i, :] *  np.inner(nvec[i, :, :], b0[i, :]) / \
                          (b0[i, 0]*BzBzstar[i, :] - b0[i, 2]*BxBzstar[i, :]) 
    return coeff * phasevelo  

def phaseveloEyBx_cross_EdotB0_SRF(EyBxstar, BzBxstar, BxBxstar, nvec, time, B0, timeB0, coeff=1e9/1e3):
    """
    Since k x E = w*B, if E.B0 = 0, then: E = -(w/k.B0) * (B0 x B)
    In particular: Ey = (w/k.BO) * (B0x*Bz - B0z*Bx)
    k = nvec * K, where K is a signed wavenumber  
    b0 = B0 / |B0|
    Hence: EyBxstar = w/K * (1/nvec.b0) * (b0x*BzBxstar - b0z*BxBxstar)
    The sign of w/K gives the direction of propagation w/r to nvec
INPUT:    
    All vector/tensor components are expressed in SRF  
    EyBxstar(nspec, nfreq): crosscorrelation between the Ey and Bx components
    BzBxstar(nspec, nfreq): crosscorrelation between the Bz and Bx components
    BxBxstar(nspec, nfreq): crosscorrelation between the Bx and Bx components
    nvec(nspec, nfreq, 3): wave normal vector (Means, JGR, 1972), i.e. the unit vector defining the
                           polarization plane and oriented in such a way that δB turns to the right
                           with respect to it (RH, i.e. counter-clock wise or direct direction of rotation)
    time(nspec): time of the spectral products
    B0(npts, 3): MAG magnetic field vector
    timeB0(npts): time of B0        
OUTPUT
    phasevelo(nspec, nfreq): w/K  
    by default the result is given in km/s if E in V/m and B in nT (V/m / T / 1e3)        
    """
    nspec, nfreq, dim_nvec = nvec.shape
    nspecEyBx, nfreqEyBx = EyBxstar.shape
    nB0, dim_B0 = B0.shape        
    if (len(time) != nspec) or (len(timeB0) != nB0) or (dim_nvec != 3) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')   
    if (nspecEyBx != nspec) or (nfreqEyBx != nfreq):
        raise TotoError('problematic dimension of inputs!!')       
    if isinstance(timeB0[0], datetime) and isinstance(time[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(time[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (time[0] < timeB0[0]) or (time[-1] > timeB0[-1]):
        raise TotoError("time interval is not included within timeB0 interval ...")           
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, time, side='left')    
    b0 = B0[indexes_nearestB0, :]
    b0 = (b0.T / norm(b0, axis=-1)).T
    phasevelo = np.zeros((nspec, nfreq), dtype=complex)
    for i in range(nspec):
        phasevelo[i, :] = EyBxstar[i, :] *  np.inner(nvec[i, :, :], b0[i, :]) / \
                          (b0[i, 0]*BzBxstar[i, :] - b0[i, 2]*BxBxstar[i, :]) 
    return coeff * phasevelo    
    
def vphi1_BX_SRF(EzBxstar, EyBxstar, BxBxstar, nvec, coeff=1e9/1e3):
    """    
    w/K = ( ny <EzBx*> - nz <EyBx*> ) /  <BxBx*>
    k = nvec * K, where K is a signed wavenumber      
    The sign of w/K gives the direction of propagation w/r to nvec    
INPUT:    
    All vector/tensor components are expressed in SRF  
    EzBxstar(nspec, nfreq): crosscorrelation between the Ez and Bx components
    EyBxstar(nspec, nfreq): crosscorrelation between the Ey and Bx components   
    BxBxstar(nspec, nfreq): crosscorrelation between the Bx and Bx components => autocorrelation
    nvec(nspec, nfreq, 3): wave normal vector (Means, JGR, 1972), i.e. the unit vector defining the
                           polarization plane and oriented in such a way that δB turns to the right
                           with respect to it (RH, i.e. counter-clock wise or direct direction of rotation)
    time(nspec): time of the spectral products          
OUTPUT
    phasevelo(nspec, nfreq): w/K  
    by default the result is given in km/s if E in V/m and B in nT (V/m / T / 1e3)        
    """
    nspec, nfreq, dim_nvec = nvec.shape    
    phasevelo = np.zeros((nspec, nfreq), dtype=complex)
    for i in range(nspec):
        phasevelo[i, :] = (nvec[i, :, 1]*EzBxstar[i, :] - nvec[i, :, 2]*EyBxstar[i, :]) / BxBxstar[i, :]                           
    return coeff * phasevelo    

def vphi1_BY_SRF(EzBystar, EyBystar, BxBystar, nvec, coeff=1e9/1e3):
    """    
    w/K = ( ny <EzBy*> - nz <EyBy*> ) /  <BxBy*>
    k = nvec * K, where K is a signed wavenumber      
    The sign of w/K gives the direction of propagation w/r to nvec    
INPUT:    
    All vector/tensor components are expressed in SRF  
    EzBystar(nspec, nfreq): crosscorrelation between the Ez and By components
    EyBystar(nspec, nfreq): crosscorrelation between the Ey and By components   
    BxBystar(nspec, nfreq): crosscorrelation between the Bx and By components
    nvec(nspec, nfreq, 3): wave normal vector (Means, JGR, 1972), i.e. the unit vector defining the
                           polarization plane and oriented in such a way that δB turns to the right
                           with respect to it (RH, i.e. counter-clock wise or direct direction of rotation)
    time(nspec): time of the spectral products          
OUTPUT
    phasevelo(nspec, nfreq): w/K  
    by default the result is given in km/s if E in V/m and B in nT (V/m / T / 1e3)        
    """
    nspec, nfreq, dim_nvec = nvec.shape    
    phasevelo = np.zeros((nspec, nfreq), dtype=complex)
    for i in range(nspec):
        phasevelo[i, :] = (nvec[i, :, 1]*EzBystar[i, :] - nvec[i, :, 2]*EyBystar[i, :]) / BxBystar[i, :]                           
    return coeff * phasevelo    

def vphi1_BZ_SRF(EzBzstar, EyBzstar, BxBzstar, nvec, coeff=1e9/1e3):
    """    
    w/K = ( ny <EzBz*> - nz <EyBz*> ) /  <BxBz*>
    k = nvec * K, where K is a signed wavenumber      
    The sign of w/K gives the direction of propagation w/r to nvec    
INPUT:    
    All vector/tensor components are expressed in SRF  
    EzBzstar(nspec, nfreq): crosscorrelation between the Ez and Bz components
    EyBzstar(nspec, nfreq): crosscorrelation between the Ey and Bz components   
    BxBzstar(nspec, nfreq): crosscorrelation between the Bx and Bz components
    nvec(nspec, nfreq, 3): wave normal vector (Means, JGR, 1972), i.e. the unit vector defining the
                           polarization plane and oriented in such a way that δB turns to the right
                           with respect to it (RH, i.e. counter-clock wise or direct direction of rotation)
    time(nspec): time of the spectral products          
OUTPUT
    phasevelo(nspec, nfreq): w/K  
    by default the result is given in km/s if E in V/m and B in nT (V/m / T / 1e3)        
    """
    nspec, nfreq, dim_nvec = nvec.shape    
    phasevelo = np.zeros((nspec, nfreq), dtype=complex)
    for i in range(nspec):
        phasevelo[i, :] = (nvec[i, :, 1]*EzBzstar[i, :] - nvec[i, :, 2]*EyBzstar[i, :]) / BxBzstar[i, :]                           
    return coeff * phasevelo       

def Vsw_EDC_BDC(E0, timeE0, B0, timeB0, dn=1):
    """
    first naïf deHoffmann-Teller analysis ...
    E0(n, 3): BIAS L3 DC X, Y, Z electric field components in SRF (mV/m)
    B0(:, 3): MAG L2 magnetic field vectors in SRF (nT) from which the -Vsw x B0 is calculated 
              assuming a purely radial solar wind velocity 
    dn:       offset for derivative dE/dB           
    OUTPUT:
    E0YoverB0Z(n): +E0Y / B0Z (mV/nT / 1000 => km/s)
    E0ZoverB0Y(n): -E0Z / B0Y (mV/nT / 1000 => km/s)   
    V0X_E0Y((n-1-dn)//dn): +dE0Y / dB0Z (mV/nT / 1000 => km/s)
    V0X_E0Z((n-1-dn)//dn): -dE0Z / dB0Y (mV/nT / 1000 => km/s)   
    timeV0X((n-1-dn)//dn)
    """
    nE0, dim_E0 = E0.shape
    nB0, dim_B0 = B0.shape    
    if (len(timeE0) != nE0) or (len(timeB0) != nB0) or (dim_E0 != 3) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')    
    if isinstance(timeB0[0], datetime) and isinstance(timeE0[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(timeE0[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (timeE0[0] < timeB0[0]) or (timeE0[-1] > timeB0[-1]):
        raise TotoError("timeE0 interval is not included within timeB0 interval ...")  
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, timeE0, side='left')    
    E0YoverB0Z = +E0[:, 1] / B0[indexes_nearestB0, 2] * 1e3  # +E0Y / B0Z (mV/nT / 1000 => km/s)
    E0ZoverB0Y = -E0[:, 2] / B0[indexes_nearestB0, 1] * 1e3  # -E0Z / B0Y (mV/nT / 1000 => km/s) 
    # V0X_E0Y = +dE0Y/dB0Z (mV/nT / 1000 => km/s)
    V0X_E0Y = + (E0[dn:-1:dn, 1] - E0[0:-1-dn:dn, 1]) / (B0[indexes_nearestB0[dn:-1:dn], 2] - \
                                                         B0[indexes_nearestB0[0:-1-dn:dn], 2]) * 1e3
    # V0X_E0Z = -dE0Z/dB0Y (mV/nT / 1000 => km/s)
    V0X_E0Z = - (E0[dn:-1:dn, 2] - E0[0:-1-dn:dn, 2]) / (B0[indexes_nearestB0[dn:-1:dn], 1] - \
                                                         B0[indexes_nearestB0[0:-1-dn:dn], 1]) * 1e3 
    timeV0X = timeE0[0:-1-dn:dn] + (timeE0[dn:-1:dn] - timeE0[0:-1-dn:dn]) / 2    
    return (V0X_E0Y, V0X_E0Z, timeV0X, E0YoverB0Z, E0ZoverB0Y)   
    
def VxzHT_EyDC_BxzDC(E0, timeE0, B0, timeB0, echo=False):
    """
    deHoffmann-Teller analysis with only one electric field component (Ey)
    Paschmann, G. & Sonnerup, B. U. 2008, ISSI Scientific Reports Series, 8, 65
    Steinvall et al. 2021 A&A 
    E0(n, 3): BIAS L3 DC X, Y, Z electric field components in SRF (mV/m)
    B0(:, 3): MAG L2 magnetic field vectors in SRF (nT)
    OUTPUT:
    VxzHT: inv(K0) # < E0y x B0xz>   (* 1e3 => km/s)   
    
    version 1: 14 mars 2021 
    """    
    nE0, dim_E0 = E0.shape
    nB0, dim_B0 = B0.shape    
    if (len(timeE0) != nE0) or (len(timeB0) != nB0) or (dim_E0 != 3) or (dim_B0 != 3):
        raise TotoError('problematic dimension of inputs!!')    
    if isinstance(timeB0[0], datetime) and isinstance(timeE0[0], (int, np.int64)):
        timeB0 = pycdf.lib.v_datetime_to_tt2000(timeB0)
    elif isinstance(timeB0[0], (int, np.int64)) and isinstance(timeE0[0], datetime):  
        timeB0 = pycdf.lib.v_tt2000_to_datetime(timeB0) 
    if (timeE0[0] < timeB0[0]) or (timeE0[-1] > timeB0[-1]):
        raise TotoError("timeE0 interval is not included within timeB0 interval ...")  
    """    
    side    returned index i satisfies
    'left'  a[i-1] < v <= a[i]       
            dt is the half sampling period: (a[1]-a[0])/2
    'left'  a[i1-1] <  t1-dt <= a[i1] => t1 dans ]a[i1]  -dt, a[i1]  +dt] 
    """   
    indexes_nearestB0 = np.searchsorted(timeB0+(timeB0[1]-timeB0[0])/2, timeE0, side='left')   
    B0xz = np.stack([B0[indexes_nearestB0, 0], B0[indexes_nearestB0, 2]], axis=-1)    
    
    # compute the covariance matrix < B0xz B0xz > and the K0 matrix  
    B0B0xz = np.dot(B0xz.T, B0xz) / nE0
    K0 = np.diag(2*[np.trace(B0B0xz)]) - B0B0xz      
    
    # compute the "Poynting vector" P0xz = < E0y x B0xz>  
    P0xz = np.dot(E0[:, 1], np.stack([B0xz[:, 1], -B0xz[:, 0]], axis=-1)) / nE0
    
    # compute the deHoffmann-Teller velocity (km/s)
    VxzHT = np.dot(np.linalg.inv(K0), P0xz) * 1e3   
              
    # linear regression analysis
    slope, y0, corr, p_value, std_err = linregress(E0[:, 1], (VxzHT[0]*B0xz[:,1] - VxzHT[1]*B0xz[:,0]) * 1e-3)    
    
    if echo:
        print('V_HT  =  [{:.1f}, NaN, {:.1f}] km/s  => |V_HT| =   {:.1f} km/s'.format(*VxzHT, norm(VxzHT)))
        print('slope of the regression line:      %10.3f'%(slope))
        print('intercept of the regression line:  %10.3f'%(y0))
        print('correlation coefficient:           %10.3f'%(corr))
        print('standard error of the estimate:    %10.3f'%(std_err)) 
        
    params = {"slope": slope,
              "y0": y0,
              "corr": corr,
              "std_err": std_err
             }    
             
    return VxzHT, params       

def spectral_corrcoeff(sm5x5_BE):
    """
    Compute the complex spectral correlation coefficent rho_ij = <PiPj*> / (<PiPi*> <PjPj*>)^(1/2) 
    sm5x5_BE(nspec, nfreq, 5, 5): spectral matrix with 3B & 2E
    """
    #[[(i,j) for j in [0, 1, 2]] for i in [3, 4]]
    corrcoeff_EiBj = [[sm5x5_BE[..., i, j] / \
                       np.sqrt(sm5x5_BE[..., i, i] * \
                               sm5x5_BE[..., j, j]) for j in [0, 1, 2]] for i in [3, 4]]

    corrcoef_EyBx = corrcoeff_EiBj[0][0]
    corrcoef_EyBy = corrcoeff_EiBj[0][1]
    corrcoef_EyBz = corrcoeff_EiBj[0][2]
    corrcoef_EzBx = corrcoeff_EiBj[1][0]
    corrcoef_EzBy = corrcoeff_EiBj[1][1]
    corrcoef_EzBz = corrcoeff_EiBj[1][2]

    #[[(i,j) for j in [0, 1, 2] if j > i] for i in [0, 1, 2]]
    corrcoeff_BiBj = [[sm5x5_BE[..., i, j] / \
                       np.sqrt(sm5x5_BE[..., i, i] * \
                               sm5x5_BE[..., j, j]) for j in [0, 1, 2] if j > i] for i in [0, 1, 2]]

    corrcoef_BxBy = corrcoeff_BiBj[0][0]
    corrcoef_BxBz = corrcoeff_BiBj[0][1]
    corrcoef_ByBz = corrcoeff_BiBj[1][0]

    corrcoef_EyEz = sm5x5_BE[..., 3, 4] / np.sqrt(sm5x5_BE[..., 3, 3] * \
                                                  sm5x5_BE[..., 4, 4])   
    return (corrcoef_EyBx, corrcoef_EyBy, corrcoef_EyBz, 
            corrcoef_EzBx, corrcoef_EzBy, corrcoef_EzBz,
            corrcoef_BxBy, corrcoef_BxBz, corrcoef_ByBz,
            corrcoef_EyEz)
















