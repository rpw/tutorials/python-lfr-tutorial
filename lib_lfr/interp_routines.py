# -*- coding: utf-8 -*-

import os
import numpy as np
from lib_maths.conversion_routines import linfromdB
from numpy.polynomial.polynomial import polyval

from lib_lfr.load_routines import print_cdf, load_field_CDF


def LFR_TF_interp(freq, tf, icomp, polar=False):    
    gain = np.interp(freq, tf['Freq'] , tf['Gain'][:,icomp])
    phase = np.interp(freq, tf['Freq'] , tf['Phase'][:,icomp])
    return (gain, phase) if polar else gain * np.exp(1j*np.deg2rad(phase))     
    
def LFR_TF_mean_E_interp(freq, tf, R=1):    
    if R == 1:  
        return np.sum([[LFR_TF_interp(f, tf, 1) for f in freq],  # BIAS2 BIAS3
                       [LFR_TF_interp(f, tf, 2) for f in freq]], axis=0) / 2        
    if R == 0:   
        return np.sum([[LFR_TF_interp(f, tf, 3) for f in freq],  # BIAS4 BIAS5
                       [LFR_TF_interp(f, tf, 4) for f in freq]], axis=0) / 2    
    print("Bad R value !!!")      
    
def LFR_TF_mean_B_interp(freq, tf):    
    return np.sum([[LFR_TF_interp(f, tf, 0) for f in freq],              # SCM1
                   [LFR_TF_interp(f, tf, 1) for f in freq],              # SCM2 
                   [LFR_TF_interp(f, tf, 2) for f in freq]], axis=0) / 3 # SCM3          
    
def SCM_TF_interp(freq, tf, dB=False, polar=False):
    if dB:
        gain = 10**(np.interp(freq, tf['Freq'] , tf['Gain'])/20)
    else:
        gain = np.interp(freq, tf['Freq'], 10**(tf['Gain']/20))            
    phase = np.interp(freq, tf['Freq'] , np.rad2deg(np.unwrap(np.deg2rad(tf['Phase']))))
    return (gain, phase) if polar else gain * np.exp(1j*np.deg2rad(phase)) 
    
def SCM_TF_comp_interp(freq, tf, dB=False, polar=False, debug=False):
    nfreq = len(freq)
    if debug:
        return {'freq': freq,
                'rB11_LF': np.ones(nfreq),
                'rB12_LF': np.zeros(nfreq),
                'rB13_LF': np.zeros(nfreq),
                'rB21_LF': np.zeros(nfreq),
                'rB22_LF': np.ones(nfreq),
                'rB23_LF': np.zeros(nfreq),
                'rB31_LF': np.zeros(nfreq),
                'rB32_LF': np.zeros(nfreq),
                'rB33_LF': np.ones(nfreq),
                'B11_LF': np.ones(nfreq),
                'B12_LF': np.zeros(nfreq),
                'B13_LF': np.zeros(nfreq),
                'B21_LF': np.zeros(nfreq),
                'B22_LF': np.ones(nfreq),
                'B23_LF': np.zeros(nfreq),
                'B31_LF': np.zeros(nfreq),
                'B32_LF': np.zeros(nfreq),
                'B33_LF': np.ones(nfreq)} if not polar else {'freq': freq,
                                                             'rB11-LF-gains': np.ones(nfreq),
                                                             'rB22-LF-gains': np.ones(nfreq),
                                                             'rB33-LF-gains': np.ones(nfreq)}    
                                
    if dB:
        tf_interp = {'freq': freq,
                     'rB11-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB11-LF-gains'])),
                     'rB12-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB12-LF-gains'])),
                     'rB13-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB13-LF-gains'])),
                     'rB21-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB21-LF-gains'])),
                     'rB22-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB22-LF-gains'])),
                     'rB23-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB23-LF-gains'])),
                     'rB31-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB31-LF-gains'])),
                     'rB32-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB32-LF-gains'])),
                     'rB33-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['rB33-LF-gains'])),
                     'B11-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B11-LF-gains'])),
                     'B12-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B12-LF-gains'])),
                     'B13-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B13-LF-gains'])),
                     'B21-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B21-LF-gains'])),
                     'B22-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B22-LF-gains'])),
                     'B23-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B23-LF-gains'])),
                     'B31-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B31-LF-gains'])),
                     'B32-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B32-LF-gains'])),
                     'B33-LF-gains': linfromdB(np.interp(freq, tf['LF-Freqs'], tf['B33-LF-gains']))}                          
    else:        
        tf_interp = {'freq': freq,
                     'rB11-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB11-LF-gains'])),
                     'rB12-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB12-LF-gains'])),
                     'rB13-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB13-LF-gains'])),
                     'rB21-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB21-LF-gains'])),
                     'rB22-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB22-LF-gains'])),
                     'rB23-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB23-LF-gains'])),
                     'rB31-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB31-LF-gains'])),
                     'rB32-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB32-LF-gains'])),
                     'rB33-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['rB33-LF-gains'])),
                     'B11-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B11-LF-gains'])),
                     'B12-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B12-LF-gains'])),
                     'B13-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B13-LF-gains'])),
                     'B21-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B21-LF-gains'])),
                     'B22-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B22-LF-gains'])),
                     'B23-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B23-LF-gains'])),
                     'B31-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B31-LF-gains'])),
                     'B32-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B32-LF-gains'])),
                     'B33-LF-gains': np.interp(freq, tf['LF-Freqs'], linfromdB(tf['B33-LF-gains']))}            
    tf_interp['rB11-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB11-LF-phases']))))
    tf_interp['rB12-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB12-LF-phases']))))
    tf_interp['rB13-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB13-LF-phases']))))
    tf_interp['rB21-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB21-LF-phases']))))
    tf_interp['rB22-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB22-LF-phases']))))
    tf_interp['rB23-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB23-LF-phases']))))
    tf_interp['rB31-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB31-LF-phases']))))
    tf_interp['rB32-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB32-LF-phases']))))
    tf_interp['rB33-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['rB33-LF-phases']))))
    tf_interp['B11-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B11-LF-phases']))))
    tf_interp['B12-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B12-LF-phases']))))
    tf_interp['B13-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B13-LF-phases']))))
    tf_interp['B21-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B21-LF-phases']))))
    tf_interp['B22-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B22-LF-phases']))))
    tf_interp['B23-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B23-LF-phases']))))
    tf_interp['B31-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B31-LF-phases']))))
    tf_interp['B32-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B32-LF-phases']))))
    tf_interp['B33-LF-phases'] = np.interp(freq, tf['LF-Freqs'],np.rad2deg(np.unwrap(np.deg2rad(tf['B33-LF-phases']))))
    
    if polar:
        return tf_interp
    else:
        return {'freq': freq,
                'rB11_LF': tf_interp["rB11-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB11-LF-phases"])),
                'rB12_LF': tf_interp["rB12-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB12-LF-phases"])),
                'rB13_LF': tf_interp["rB13-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB13-LF-phases"])),
                'rB21_LF': tf_interp["rB21-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB21-LF-phases"])),
                'rB22_LF': tf_interp["rB22-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB22-LF-phases"])),
                'rB23_LF': tf_interp["rB23-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB23-LF-phases"])),
                'rB31_LF': tf_interp["rB31-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB31-LF-phases"])),
                'rB32_LF': tf_interp["rB32-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB32-LF-phases"])),
                'rB33_LF': tf_interp["rB33-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["rB33-LF-phases"])),
                'B11_LF': tf_interp["B11-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B11-LF-phases"])),
                'B12_LF': tf_interp["B12-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B12-LF-phases"])),
                'B13_LF': tf_interp["B13-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B13-LF-phases"])),
                'B21_LF': tf_interp["B21-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B21-LF-phases"])),
                'B22_LF': tf_interp["B22-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B22-LF-phases"])),
                'B23_LF': tf_interp["B23-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B23-LF-phases"])),
                'B31_LF': tf_interp["B31-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B31-LF-phases"])),
                'B32_LF': tf_interp["B32-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B32-LF-phases"])),
                'B33_LF': tf_interp["B33-LF-gains"] * np.exp(1j*np.deg2rad(tf_interp["B33-LF-phases"]))}   
    
def SCM_TF_mat_interp(freq, tf_comp, reverse=True, dB=False, UARF=False, debug=False):
    """   
    B1 is along Y_UARF
    B2 is along Z_UARF
    B3 is along X_UARF 
    """              
    scalar = np.isscalar(freq)
    if scalar:
        freq = np.array(freq)
        freq.resize(1)
    tf_interp = SCM_TF_comp_interp(freq, tf_comp, dB=dB, polar=False, debug=debug)
    if reverse:
        mat_V_to_nT_LF_interp = np.transpose( np.array( 
                           [[tf_interp["rB11_LF"], tf_interp["rB12_LF"], tf_interp["rB13_LF"]], 
                            [tf_interp["rB21_LF"], tf_interp["rB22_LF"], tf_interp["rB23_LF"]], 
                            [tf_interp["rB31_LF"], tf_interp["rB32_LF"], tf_interp["rB33_LF"]]]), 
                                              (2, 0, 1) )
        if UARF: mat_V_to_nT_LF_interp = np.roll(mat_V_to_nT_LF_interp, 1, axis=-2)                                                    
        return np.resize(mat_V_to_nT_LF_interp, (3,3)) if scalar else mat_V_to_nT_LF_interp
    else:               
        mat_nT_to_V_LF_interp = np.transpose( np.array(
                           [[tf_interp["B11_LF"], tf_interp["B12_LF"], tf_interp["B13_LF"]], 
                            [tf_interp["B21_LF"], tf_interp["B22_LF"], tf_interp["B23_LF"]], 
                            [tf_interp["B31_LF"], tf_interp["B32_LF"], tf_interp["B33_LF"]]]), 
                              (2, 0, 1) )
        if UARF: mat_nT_to_V_LF_interp = np.roll(mat_nT_to_V_LF_interp, 1, axis=-1)                      
        return np.resize(mat_nT_to_V_LF_interp, (3,3)) if scalar else mat_nT_to_V_LF_interp   
        
def load_BIAS_TF_func(file, echo=True):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    if echo: print_cdf(file, already_open=False, key_attrs=None, 
                       key_field=['TRANSFER_FUNCTION_COEFFS', 
                                  'TRANSFER_FUNCTION_COEFF_LABEL',
                                  'TRANSFER_FUNCTION_LABEL',
                                  'TRANSFER_FUNCTION_ND_LABEL'])        
    TF_coeffs = load_field_CDF(file, ['TRANSFER_FUNCTION_COEFFS'], echo=echo)[0]    
    def H_DC_SE_G1s17(f):
        s = 1j*2*np.pi*f 
        return polyval(s, TF_coeffs[0, :, 0]) / polyval(s, TF_coeffs[1, :, 0])
    def H_DC_DIFF_G1(f):
        s = 1j*2*np.pi*f 
        return polyval(s, TF_coeffs[0, :, 1]) / polyval(s, TF_coeffs[1, :, 1])
    def H_AC_DIFF_G5(f):
        s = 1j*2*np.pi*f 
        return polyval(s, TF_coeffs[0, :, 2]) / polyval(s, TF_coeffs[1, :, 2])
    def H_AC_DIFF_G100(f):
        s = 1j*2*np.pi*f 
        return polyval(s, TF_coeffs[0, :, 3]) / polyval(s, TF_coeffs[1, :, 3])
    def BIAS_TF(f, conf='ac_diff_G5')  :
        if conf == 'off': return 1. # VHF TF to be elaborated ...
        if conf in ['dc_se_G1s17']:  return H_DC_SE_G1s17(f)    
        if conf in ['dc_diff_G1']:   return H_DC_DIFF_G1(f)
        if conf in ['ac_diff_G5']:   return H_AC_DIFF_G5(f)
        if conf in ['ac_diff_G100']: return H_AC_DIFF_G100(f)
        print("Bad conf name !!!")
    return BIAS_TF       
    
def ANT_TF_HF_PA_interp(freq, tf, icomp, dB=True, polar=False):
    if dB:
        gain = 10**(np.interp(freq, tf['Freq'] , tf['Gain'][icomp,:])/20)
    else:
        gain = np.interp(freq, tf['Freq'], 10**(tf['Gain'][icomp,:]/20))            
    phase = np.interp(freq, tf['Freq'] , np.rad2deg(np.unwrap(np.deg2rad(tf['Phase'][icomp,:]))))
    return (gain, phase) if polar else gain * np.exp(1j*np.deg2rad(phase))        
