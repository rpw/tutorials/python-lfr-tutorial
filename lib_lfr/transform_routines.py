# -*- coding: utf-8 -*-

import numpy as np

class TotoError(Exception):
    pass
    
def convert_temp_to_deg(temperature_hk_raw):
    """
    Convert SCM temperature in Celsius degrees

    :param temperature_hk_raw: temperature of SCM given in the HK files
    :type temperature_hk_raw: int
    :return: temperature_hk
    :rtype: float
    :Example: temperature_hk = convert_temp_to_deg(temperature_hk_raw)   
         
    From Rodrigue and Bruno
    """
    temperature_hk = (np.float32(temperature_hk_raw)+3400.0)*0.01125
    return temperature_hk    

def transform_from_SCM_to_UARF(B):
    """
    B(:, 3): input array of 3-D vectors expressed in the SCM B1-B2-B3 coordinate system
             to be transformed into the SCM UARF coordinate system
             B1 is along Y_UARF
             B2 is along Z_UARF
             B3 is along X_UARF 
    work also with any array(..., 3) like nvec(:, :, 3)         
    """              
    return np.roll(B, +1, axis=-1)

def transform_from_UARF_to_SCM(B):
    """
    B(:, 3): input array of 3-D vectors expressed in the SCM UARF coordinate system
             to be transformed into the SCM B1-B2-B3 coordinate system
             B1 is along Y_UARF
             B2 is along Z_UARF
             B3 is along X_UARF 
            work also with any array(..., 3) like nvec(:, :, 3)         
    """              
    return np.roll(B, -1, axis=-1)    
    
def transform_from_UARF_to_SRF(B):
    """
    B(:, 3): input array of 3-D vectors expressed in the SCM UARF coordinate system
             to be transformed into the SRF (so called "Spacecraft") coordinate system  
             work also with any array(..., 3) like nvec(:, :, 3)                  
    """    
    M_UARF_to_SRF = np.array([[ 0.50087005,  0.5995206 , -0.62426296],
                              [ 0.74385771, -0.6669109 , -0.04365272],
                              [-0.44249848, -0.44249848, -0.77999372]])        
    #return np.dot(M_UARF_to_SRF, B.T).T   
    return np.moveaxis(np.dot(M_UARF_to_SRF, np.moveaxis(B, -1, -2)), 0, -1)    
    
def transform_sm_ref1_to_ref2_fixed(sm_ref1, M_ref1_to_ref2, echo=False):
    """
    Transform a spectral matrix from one fixed coordinate system to another one
    by operating the matrix multiplication: M . SM . conj(M).T
INPUT:
    sm_ref1[npsec, nfreq, dim, dim]
    M_ref1_to_ref2[dim, dim]    
OUTPUT:
    sm_ref2[npsec, nfreq, dim, dim]    
    """
    nspec, nfreq, dim, _ = sm_ref1.shape  
    Mdim, _ = M_ref1_to_ref2.shape
    if Mdim != dim:                
        raise TotoError('problematic data instance !?', (Mdim, dim))                                         
    sm_ref2 = np.moveaxis(np.dot(M_ref1_to_ref2, np.dot(sm_ref1, np.conj(M_ref1_to_ref2.T))), 0, 2)                               
    if echo:
        print("sm_ref1 shape: ", sm_ref1.shape)
        print("sm_ref2 shape: ", sm_ref2.shape)
    return sm_ref2    
    
def transform_sm6x6_BE_ref1_to_ref2(sm_ref1, M3x3_ref1_to_ref2, echo=False):
    """
    Transform a B-E 6x6 spectral matrix from one (fixed or moving) 3D-coordinate system 
    to another one by operating the matrix multiplication: M . SM . conj(M).T
INPUT:
    sm_ref1[npsec, nfreq, 6, 6]
    M_ref1_to_ref2[npsec, nfreq, 3, 3] (moving coordinate system) 
               or [3, 3]               (fixed coordinate system)
OUTPUT:
    sm_ref2[npsec, nfreq, 6, 6]        
    """
    nspec, nfreq, dim, _ = sm_ref1.shape  
    sm_ref2 = np.zeros((nspec, nfreq, 6, 6), dtype=complex)     
    Mdim, _ = M3x3_ref1_to_ref2.shape[-2:]
    if Mdim != 3 or dim != 6:                
        raise TotoError('problematic data instance !?', (Mdim, dim))                  
    ref_moving = True if M3x3_ref1_to_ref2.ndim == 4 else False  
    if ref_moving:
        Mnspec, Mnfreq, _, _ = M3x3_ref1_to_ref2.shape   
        if Mnspec != nspec or nfreq != Mnfreq:                
            raise TotoError('problematic data instance !?', (Mdim, dim))          
        M6x6_ref1_to_ref2 = np.zeros((nspec, nfreq, 6, 6), dtype=complex)        
        M6x6_ref1_to_ref2[..., 0:3, 0:3] = M3x3_ref1_to_ref2
        M6x6_ref1_to_ref2[..., 3:6, 3:6] = M3x3_ref1_to_ref2
        for i in range(nspec):
            for j in range(nfreq):
                sm_ref2[i, j, ...] = np.dot(M6x6_ref1_to_ref2[i, j, ...],  
                                            np.dot(sm_ref1[i, j, ...], np.conj(M6x6_ref1_to_ref2[i, j, ...].T)))
    else:
        M6x6_ref1_to_ref2 = np.zeros((6, 6), dtype=complex)        
        M6x6_ref1_to_ref2[0:3, 0:3] = M3x3_ref1_to_ref2
        M6x6_ref1_to_ref2[3:6, 3:6] = M3x3_ref1_to_ref2
        sm_ref2 = np.moveaxis(np.dot(M6x6_ref1_to_ref2, np.dot(sm_ref1, np.conj(M6x6_ref1_to_ref2.T))), 0, 2)                               
    if echo:
        print("moving coordinate system: ", ref_moving)
        print("sm_ref1 shape: ", sm_ref1.shape)
        print("sm_ref2 shape: ", sm_ref2.shape)
    return sm_ref2        
