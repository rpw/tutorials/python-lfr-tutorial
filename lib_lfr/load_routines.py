# -*- coding: utf-8 -*-

import os

import numpy as np
import pandas as pd
from spacepy import pycdf

from lib_lfr.global_params import LFR_Fs
from lib_io.read_routines import readlines, seeklines
from lib_maths.conversion_routines import linfromdB

class TotoError(Exception):
    pass
    
############### ASCII files from GSE #######################

def load_swf_foreignGSE(file):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return  
    swf_foreignGSE = pd.read_csv(file, header=0, delim_whitespace=True, index_col=0)
    return swf_foreignGSE

def load_cwf_foreignGSE(file):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return  
    cwf_foreignGSE = pd.read_csv(file, header=0, delim_whitespace=True, index_col=0)
    return cwf_foreignGSE


############### CDF files from RPW #######################

def print_cdf(cdf, key_attrs=None, key_field=None, key_field_attrs=None, already_open=True):
    """
INPUT:
    cdf:   file name of the cdf file if it has to be open => already_open == False
           otherwise it is already an openened cdf file with pycdf.CDF()      
    """
    if not already_open: cdf = pycdf.CDF(cdf)    
    print('PRINT cdf: \n', cdf, '\n')
    if key_field != None: 
        for k in np.array(key_field, ndmin=1): print("{}: {}".format(k, cdf[k][...]))   
    if key_field_attrs != None: 
        for k in np.array(key_field_attrs, ndmin=1): print('\n', "cdf['{}'].attrs: {}".format(k, cdf[k].attrs))            
    if key_attrs != None: 
        if key_attrs in ['*', ['*']] :                 
            print('\nPRINT cdf.attrs: \n', cdf.attrs, '\n')
        else:
            print()               
            for k in np.array(key_attrs, ndmin=1): print('{}: {}'.format(k, cdf.attrs[k]))      
    if not already_open: cdf.close()
    
def load_field_CDF(file, k_field, echo=False, key_echo=False, key_attrs=None, key_field=None, key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if key_echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)            
    data_loaded = []
    for k in np.array(k_field, ndmin=1):
        data_loaded.append(cdf[k][...])                
        if echo: print(k + ' :', cdf[k][...].shape)                                  
    cdf.close()   
    return data_loaded    
    
def load_LFR_TF_B_CDF(file, echo=False, key_field=None, key_field_attrs=None, key_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)  
    tf_ampli = 4*[None]
    tf_phase = 4*[None]
    tf_freqs = 4*[None]                
    for F in [0,1,2,3]:        
        tf_ampli[F] = np.array(cdf["TF_B1B2B3_amplitude_F%s"%(F)][...]) 
        tf_phase[F] = np.array(cdf["TF_B1B2B3_phase_F%s"%(F)][...]) 
        tf_freqs[F] = np.array(cdf["Freqs_F%s"%(F)][:])         
    cdf.close()
    return (tf_ampli, tf_phase, tf_freqs)    
    
def load_LFR_TF_E_CDF(file, echo=False, key_field=None, key_field_attrs=None, key_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)    
    tf_ampli = 4*[None]
    tf_phase = 4*[None]
    tf_freqs = 4*[None]                
    for F in [0,1,2]:        
        tf_ampli[F] = np.array(cdf["TF_BIAS_12345_amplitude_F%s"%(F)][...]) 
        tf_phase[F] = np.array(cdf["TF_BIAS_12345_phase_F%s"%(F)][...]) 
        tf_freqs[F] = np.array(cdf["Freqs_F%s"%(F)][:])
    tf_ampli[3] = np.array(cdf["TF_BIAS_123_amplitude_F%s"%(3)][...]) 
    tf_phase[3] = np.array(cdf["TF_BIAS_123_phase_F%s"%(3)][...]) 
    tf_freqs[3] = np.array(cdf["Freqs_F%s"%(3)][:])         
    cdf.close()
    return (tf_ampli, tf_phase, tf_freqs)            
    
def load_hk_lfr_CDF(file, echo=False, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)        
    # time of the hk samples in unit of sec (coarse time) and 2^-16 sec (fine time)
    hk_T_acqui = cdf['ACQUISITION_TIME'][...]
    # and conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC
    hk_time_acqui = hk_T_acqui[:,0] + hk_T_acqui[:,1]*float(2)**(-16)
    # epoch time of the hk samples [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]     
    hk_time_epoch = cdf.raw_var("Epoch")[...]
    # LFR mode
    hk_lfr_mode = cdf['HK_LFR_MODE'][...]
    # LFR parameters
    hk_R0 = cdf["SY_LFR_R0"][...]
    hk_R1 = cdf["SY_LFR_R1"][...]
    hk_R2 = cdf["SY_LFR_R2"][...]
    hk_SP0 = cdf["SY_LFR_SP0"][...]
    hk_SP1 = cdf["SY_LFR_SP1"][...]
    hk_BW = cdf["SY_LFR_BW"][...]
    hk_lfr_params = [hk_BW, (hk_SP0, hk_SP1), (hk_R0, hk_R1, hk_R2)]    
    # LFR calibration for SCM
    hk_lfr_calib = cdf['HK_LFR_CALIB_ENABLED'][...]    
    # SCM temperature
    hk_lfr_temp_scm = cdf['HK_LFR_TEMP_SCM'][...]   
    if echo:
        print()
        print('hk_time_acqui    :', hk_time_acqui.shape)
        print('hk_time_epoch    :', hk_time_epoch.shape)            
        print('hk_lfr_mode      :', hk_lfr_mode.shape)                                     
        print('hk_lfr_calib     :', hk_lfr_calib.shape)
        print('hk_lfr_temp_scm  :', hk_lfr_calib.shape)                                     
    cdf.close()
    hk_lfr_cdf = hk_lfr_mode, hk_time_acqui, hk_time_epoch, hk_lfr_params, hk_lfr_calib, hk_lfr_temp_scm
    return hk_lfr_cdf
    
def load_hk_bias_CDF(file, echo=False, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)        
    # time of the hk samples in unit of sec (coarse time) and 2^-16 sec (fine time)
    hk_T_acqui = cdf['ACQUISITION_TIME'][...]
    # and conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC
    hk_time_acqui = hk_T_acqui[:,0] + hk_T_acqui[:,1]*float(2)**(-16)
    # epoch time of the hk samples [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]     
    hk_time_epoch = cdf.raw_var("Epoch")[...]
    # BIAS gain
    hk_diff_gain = cdf['HK_BIA_DIFF_GAIN'][...] 
    # BIAS mode_mux_set
    hk_mode_mux_set = cdf['HK_BIA_MODE_MUX_SET'][...]         
    if echo:
        print()
        print('hk_time_acqui   :', hk_time_acqui.shape)
        print('hk_time_epoch   :', hk_time_epoch.shape)            
        print('hk_diff_gain    :', hk_diff_gain.shape)          
        print('hk_mode_mux_set :', hk_mode_mux_set.shape)                                                        
    cdf.close()
    hk_bias_cdf = hk_time_acqui, hk_time_epoch, hk_diff_gain, hk_mode_mux_set
    return hk_bias_cdf    

def load_swf_CDF_L1_old(file, F=0, echo=False, key_attrs=None, key_field=None, reshape=True):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo:
        print(cdf, '\n')
        if key_attrs != None: 
            for k in np.array(key_attrs, ndmin=1): print('{}: {}'.format(k, cdf.attrs[k]))     
        if key_field != None: print('\n{}: {}'.format(key_field, cdf[key_field][:]))
    # sampling frequency F=0,1,2 and number of records
    rec_swf_F = cdf['FREQ']
    irec_range = range(len(rec_swf_F))
    # data @F
    rec_swf_B = cdf["B"]
    rec_swf_E = cdf["E"]
    rec_swf_V = cdf["V"]
    swf_B = np.array([rec_swf_B[i, ...] for i in irec_range if rec_swf_F[i] == F])
    swf_E = np.array([rec_swf_E[i, ...] for i in irec_range if rec_swf_F[i] == F])
    swf_V = np.array([rec_swf_V[i, ...] for i in irec_range if rec_swf_F[i] == F])
    snap_numb = swf_B.shape[0]
    snap_size = swf_B.shape[2]     
    # time of the first sample of the swf in unit of sec (coarse time) and 2^-16 sec (fine time)
    # UTC sec from (2000, 1, 1, 0, 0, 0)_UTC
    rec_swf_T_acqui = cdf['ACQUISITION_TIME']
    swf_T_acqui = np.array([rec_swf_T_acqui[i, ...] for i in irec_range if rec_swf_F[i] == F])  
    # epoch time of the first sample of the swf [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]
    rec_swf_T_epoch = cdf.raw_var("Epoch")[:]
    swf_T_epoch = np.array([rec_swf_T_epoch[i, ...] for i in irec_range if rec_swf_F[i] == F])        
    # time vector
    swf_start_acqui = swf_T_acqui[:,0] + swf_T_acqui[:,1]*float(2)**(-16)
    swf_start_epoch = swf_T_epoch    
    ramp_time = np.arange(snap_size) / LFR_Fs[F]
    swf_time_acqui = np.zeros((snap_numb, snap_size), dtype=float)
    swf_time_epoch = np.zeros((snap_numb, snap_size), dtype=int)            
    for isnap in range(snap_numb):
        swf_time_acqui[isnap, :] = swf_start_acqui[isnap] + ramp_time
        swf_time_epoch[isnap, :] = swf_start_epoch[isnap] + np.asarray(ramp_time*1e9 + 0.5, dtype=int)   
    if echo:
        print()
        print('swf_time_acqui :', swf_time_acqui.shape)
        print('swf_time_epoch :', swf_time_epoch.shape)        
        print('swf_B    :', swf_B.shape)
        print('swf_E    :', swf_E.shape)
        print('swf_V    :', swf_V.shape)        
    # reshaping if asked
    if reshape:        
        swf_time_acqui = swf_time_acqui.flatten(order='C')
        swf_time_epoch = swf_time_epoch.flatten(order='C')
        swf_B = np.reshape(np.swapaxes(swf_B, 1, 0), (3, swf_B.shape[0]*swf_B.shape[2]), order='C').T
        swf_E = np.reshape(np.swapaxes(swf_E, 1, 0), (2, swf_E.shape[0]*swf_E.shape[2]), order='C').T   
        swf_V = np.reshape(            swf_V,        (1, swf_V.shape[0]*swf_V.shape[1]), order='C').T                        
        if echo:
            print()
            print('swf_time_acqui :', swf_time_acqui.shape)
            print('swf_time_epoch :', swf_time_epoch.shape)      
            print('swf_B    :', swf_B.shape)
            print('swf_E    :', swf_E.shape)
            print('swf_V    :', swf_V.shape)    
    # LFR parameters
    rec_swf_R0 = cdf["R0"]
    rec_swf_R1 = cdf["R1"]
    rec_swf_R2 = cdf["R2"]
    rec_swf_SP0 = cdf["SP0"]
    rec_swf_SP1 = cdf["SP1"]
    rec_swf_BW = cdf["BW"]  
    swf_params = [(int(rec_swf_BW[i]), (int(rec_swf_SP0[i]), int(rec_swf_SP1[i])), \
                  (int(rec_swf_R0[i]), int(rec_swf_R1[i]), int(rec_swf_R2[i])))    \
                  for i in irec_range if rec_swf_F[i] == F]    
    cdf.close()
    swf_cdf_l1 = (swf_B, swf_E, swf_V, swf_time_acqui, swf_time_epoch, swf_params)
    return swf_cdf_l1       

def load_swf_CDF_L1(file, echo=False, params=False, params_old=False, params_bias=False, 
                    key_attrs=None, key_field=None, key_field_attrs=None, reshape=True):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)               
    swf_B = 3*[None]   
    swf_E = 3*[None]   
    swf_V = 3*[None]   
    swf_time_epoch = 3*[None]
    swf_time_acqui = 3*[None]   
    swf_params = 3*[None] 
    swf_params_bias = 3*[None]            
    # sampling frequency F=0,1,2
    rec_swf_F = cdf['FREQ'][...]    
    rec_swf_B = cdf["B"][...]   
    rec_swf_E = cdf["E"][...]   
    rec_swf_V = cdf["V"][...]   
    rec_swf_T_acqui = cdf['ACQUISITION_TIME'][...]
    rec_swf_T_epoch = cdf.raw_var("Epoch")[...]      
    if params:  
        # LFR parameters
        rec_swf_R0 = cdf["R0"][...]
        rec_swf_R1 = cdf["R1"][...]
        rec_swf_R2 = cdf["R2"][...]
        rec_swf_SP0 = cdf["SP0"][...]
        rec_swf_SP1 = cdf["SP1"][...]
        rec_swf_BW = cdf["BW"][...]    
    if params_bias:                       
        rec_swf_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]    
    for F in [0,1,2]:        
        # data
        swf_index = (rec_swf_F == F)
        swf_B[F] = rec_swf_B[swf_index, ...]        
        swf_E[F] = rec_swf_E[swf_index, ...]        
        swf_V[F] = rec_swf_V[swf_index, ...]                
        # time of the first sample of the swf in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        swf_t0_acqui = rec_swf_T_acqui[swf_index, 0] + rec_swf_T_acqui[swf_index, 1]*float(2)**(-16)                                         
        # epoch time of the first sample of the swf [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        swf_t0_epoch = rec_swf_T_epoch[swf_index]  
        # time vector
        snap_numb, _, snap_size = swf_B[F].shape
        ramp_time = np.arange(snap_size) / LFR_Fs[F]
        swf_time_acqui[F] = np.zeros((snap_numb, snap_size), dtype=float)
        swf_time_epoch[F] = np.zeros((snap_numb, snap_size), dtype=int)            
        for isnap in range(snap_numb):
            swf_time_acqui[F][isnap, :] = swf_t0_acqui[isnap] + ramp_time
            swf_time_epoch[F][isnap, :] = swf_t0_epoch[isnap] + np.asarray(ramp_time*1e9 + 0.5, dtype=int)                   
        if params:
            if params_old:
                irec_range = range(len(rec_swf_F))
                swf_params[F] = [(int(rec_swf_BW[i]), (int(rec_swf_SP0[i]), int(rec_swf_SP1[i])), \
                                 (int(rec_swf_R0[i]), int(rec_swf_R1[i]), int(rec_swf_R2[i])))    \
                                  for i in irec_range if rec_swf_F[i] == F]                                 
            else:
                swf_params[F] = [ rec_swf_BW[swf_index], (rec_swf_SP0[swf_index], rec_swf_SP1[swf_index]), 
                                 (rec_swf_R0[swf_index], rec_swf_R1[swf_index], rec_swf_R2[swf_index])  ]    
        if params_bias:                       
            swf_params_bias[F] = [ rec_swf_bias_mux[swf_index] ]                                    
        if echo:
            print()
            print('swf_time_acqui  F%s:'%(F), swf_time_acqui[F].shape)
            print('swf_time_epoch  F%s:'%(F), swf_time_epoch[F].shape)        
            print('swf_B     F%s:'%(F), swf_B[F].shape)
            print('swf_E     F%s:'%(F), swf_E[F].shape)
            print('swf_V     F%s:'%(F), swf_V[F].shape)                                
        # reshaping if asked
        if reshape:        
            swf_time_acqui[F] = swf_time_acqui[F].flatten(order='C')
            swf_time_epoch[F] = swf_time_epoch[F].flatten(order='C')
            swf_B[F] = np.reshape(np.swapaxes(swf_B[F], 1, 0), (3, swf_B[F].shape[0]*swf_B[F].shape[2]), order='C').T
            swf_E[F] = np.reshape(np.swapaxes(swf_E[F], 1, 0), (2, swf_E[F].shape[0]*swf_E[F].shape[2]), order='C').T   
            swf_V[F] = np.reshape(            swf_V[F],        (1, swf_V[F].shape[0]*swf_V[F].shape[1]), order='C').T                        
            if echo:
                print()
                print('swf_time_acqui  F%s:'%(F), swf_time_acqui[F].shape)
                print('swf_time_epoch  F%s:'%(F), swf_time_epoch[F].shape)        
                print('swf_B     F%s:'%(F), swf_B[F].shape)
                print('swf_E     F%s:'%(F), swf_E[F].shape)
                print('swf_V     F%s:'%(F), swf_V[F].shape)          
    cdf.close()
    swf_cdf_l1 = (swf_B, swf_E, swf_V, swf_time_acqui, swf_time_epoch, swf_params, swf_params_bias)
    return swf_cdf_l1   

def load_cwf_CDF_L1_old(file, F=3, echo=False, key_attrs=None, key_field=None,  key_field_attrs=None):
    """
    cela ne marche pas s'il y plusieurs F différent dans le CDF ;
    le CAUTION est impératif ...
    """
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)   
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                
    # sampling frequency F=1,2,3 and number of records
    if True in [cdf['FREQ'][i] != F for i in range(len(cdf['FREQ']))]:
        print("CAUTION: there is F not equal to {}".format(F))    
        return
    # data @F    
    cwf_B = cdf["B"][:]
    cwf_E = cdf["E"][:]
    cwf_V = cdf["V"][:]
    cwf_V.shape = (cwf_V.shape[0], 1)
    # time of the first sample of the cwf packets in unit of sec (coarse time) and 2^-16 sec (fine time)
    # UTC sec from (2000, 1, 1, 0, 0, 0)_UTC
    cwf_T_acqui = cdf['ACQUISITION_TIME']
    # acquisition time vector of the cwf samples 
    packet_size = 336
    cwf_start_acqui = cwf_T_acqui[::336,0] + cwf_T_acqui[::336,1]*float(2)**(-16)
    packet_numb = len(cwf_start_acqui)
    ramp_time = np.arange(packet_size) / LFR_Fs[F]    
    cwf_time_acqui = np.zeros(packet_numb*packet_size, dtype=float)   
    for i in range(packet_numb): cwf_time_acqui[i*packet_size:(i+1)*packet_size] = ramp_time + cwf_start_acqui[i]         
    # epoch time of the cwf samples [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]
    cwf_time_epoch = cdf.raw_var("Epoch")[:]           
    if echo:
        print()
        print('cwf_time_acqui.shape = {}'.format(cwf_time_acqui.shape))
        print('cwf_time_epoch.shape = {}'.format(cwf_time_epoch.shape))        
        print('cwf_B.shape = {}'.format(cwf_B.shape))
        print('cwf_E.shape = {}'.format(cwf_E.shape))
        print('cwf_V.shape = {}'.format(cwf_V.shape))
    # LFR parameters
    cwf_params = [int(cdf["BW"][0]), (int(cdf["SP0"][0]), int(cdf["SP1"][0])), 
                  (int(cdf["R0"][0]), int(cdf["R1"][0]), int(cdf["R2"][0])) ]    
    cdf.close()
    return (cwf_B, cwf_E, cwf_V, cwf_time_acqui, cwf_time_epoch, cwf_params)
    
def load_cwf_CDF_L1_old(file, echo=False, params=False, params_old=False, params_bias=False, 
                    key_attrs=None, key_field=None,  key_field_attrs=None):
    """
    336 points in SBM1, SBM2, or NORMAL_LONG mode packets with 6 components (1V + 2E + 3B)    
    672 points in NORMAL mode packets with 3 components (1V + 2E)
    https://gitlab.obspm.fr/ROC/RCS/LFR_CALBUT/-/issues/29
    """                
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return 
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)   
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)       
    cwf_B = 4*[None]   
    cwf_E = 4*[None]   
    cwf_V = 4*[None]   
    cwf_time_epoch = 4*[None]
    cwf_time_acqui = 4*[None]   
    cwf_params = 4*[None]
    cwf_params_bias = 4*[None]                  
    # sampling frequency F=1,2,3
    rec_cwf_F = cdf['FREQ'][...]    
    rec_cwf_B = cdf["B"][...]   
    rec_cwf_E = cdf["E"][...]   
    rec_cwf_V = cdf["V"][...] 
    rec_cwf_V.shape = (rec_cwf_V.shape[0], 1)    
    rec_cwf_T_acqui = cdf['ACQUISITION_TIME'][...]
    rec_cwf_T_epoch = cdf.raw_var("Epoch")[...]
    packet_size = 336    
    if len(rec_cwf_T_epoch) % packet_size != 0:
        print("CAUTION: there is not an exact number of packets !!!?")   
        print("CAUTION: {} / 336 = {} !!??".format(len(rec_cwf_T_epoch), len(rec_cwf_T_epoch)/packet_size))
        return
    if params:  
        # LFR parameters
        rec_cwf_R0 = cdf["R0"][...]
        rec_cwf_R1 = cdf["R1"][...]
        rec_cwf_R2 = cdf["R2"][...]
        rec_cwf_SP0 = cdf["SP0"][...]
        rec_cwf_SP1 = cdf["SP1"][...]
        rec_cwf_BW = cdf["BW"][...]         
    if params_bias:                       
        rec_cwf_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]       
    for F in [1,2,3]:        
        # data
        cwf_index = (rec_cwf_F == F)        
        cwf_B[F] = rec_cwf_B[cwf_index, ...]        
        cwf_E[F] = rec_cwf_E[cwf_index, ...]        
        cwf_V[F] = rec_cwf_V[cwf_index, ...]   
        # time of the first sample of the cwf in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        cwf_time_acqui[F] = rec_cwf_T_acqui[cwf_index, 0] + rec_cwf_T_acqui[cwf_index, 1]*float(2)**(-16)   
        # acquisition time vector of the cwf samples 
        ramp_time = np.arange(packet_size) / LFR_Fs[F]   
        packet_numb = len(cwf_time_acqui[F]) // packet_size
        for i in range(packet_numb):            
            second_half_672_point_packet = (cwf_time_acqui[F][(i-1)*packet_size] == \
                                            cwf_time_acqui[F][i*packet_size]) if i else False
            cwf_time_acqui[F][i*packet_size:(i+1)*packet_size] = cwf_time_acqui[F][i*packet_size] + (ramp_time \
                                                                      if not second_half_672_point_packet else \
                                                                            (ramp_time + packet_size/LFR_Fs[F]))         
        # epoch time of the cwf samples [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]      
        cwf_time_epoch[F] = rec_cwf_T_epoch[cwf_index] 
        if params:
            if params_old:
                irec_range = range(len(rec_cwf_F))
                cwf_params[F] = [(int(rec_cwf_BW[i]), (int(rec_cwf_SP0[i]), int(rec_cwf_SP1[i])), \
                                 (int(rec_cwf_R0[i]), int(rec_cwf_R1[i]), int(rec_cwf_R2[i])))    \
                                  for i in irec_range if rec_cwf_F[i] == F]                                 
            else:
                cwf_params[F] = [ rec_cwf_BW[cwf_index], (rec_cwf_SP0[cwf_index], rec_cwf_SP1[cwf_index]), 
                                 (rec_cwf_R0[cwf_index], rec_cwf_R1[cwf_index], rec_cwf_R2[cwf_index])  ]                
        if params_bias:                       
            cwf_params_bias[F] = [ rec_cwf_bias_mux[cwf_index] ]                                            
        if echo:
            print()
            print('cwf_time_acqui  F%s:'%(F), cwf_time_acqui[F].shape)
            print('cwf_time_epoch  F%s:'%(F), cwf_time_epoch[F].shape)        
            print('cwf_B     F%s:'%(F), cwf_B[F].shape)
            print('cwf_E     F%s:'%(F), cwf_E[F].shape)
            print('cwf_V     F%s:'%(F), cwf_V[F].shape)                                 
    cdf.close()
    return (cwf_B, cwf_E, cwf_V, cwf_time_acqui, cwf_time_epoch, cwf_params, cwf_params_bias)
        
def load_cwf_CDF_L1(file, fs_field='FREQ', echo=False, params=False, params_old=False, params_bias=False, 
                    key_attrs=None, key_field=None,  key_field_attrs=None, doesnotmatters=False):
    """
    336 points in SBM1, SBM2, or NORMAL_LONG mode packets with 6 components (1V + 2E + 3B)    
    672 points in NORMAL mode packets with 3 components (1V + 2E)
    https://gitlab.obspm.fr/ROC/RCS/LFR_CALBUT/-/issues/29
    """                
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return 
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)   
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)       
    cwf_B = 4*[None]   
    cwf_E = 4*[None]   
    cwf_V = 4*[None]   
    cwf_time_epoch = 4*[None]
    cwf_time_acqui = 4*[None]   
    cwf_params = 4*[None]
    cwf_params_bias = 4*[None]                  
    # sampling frequency F=1,2,3
    rec_cwf_F = cdf[fs_field][...]    
    rec_cwf_B = cdf["B"][...]   
    rec_cwf_E = cdf["E"][...]   
    rec_cwf_V = cdf["V"][...] 
    rec_cwf_V.shape = (rec_cwf_V.shape[0], 1)    
    rec_cwf_T_acqui = cdf['ACQUISITION_TIME'][...]
    rec_cwf_T_epoch = cdf.raw_var("Epoch")[...]
    packet_size = 336    
    if len(rec_cwf_T_epoch) % packet_size != 0:
        print("CAUTION: there is not an exact number of packets !!!?")   
        print("CAUTION: {} / 336 = {} !!??".format(len(rec_cwf_T_epoch), len(rec_cwf_T_epoch)/packet_size))
        if doesnotmatters: 
            pass
        else:
            return
    if params:  
        # LFR parameters
        rec_cwf_R0 = cdf["R0"][...]
        rec_cwf_R1 = cdf["R1"][...]
        rec_cwf_R2 = cdf["R2"][...]
        rec_cwf_SP0 = cdf["SP0"][...]
        rec_cwf_SP1 = cdf["SP1"][...]
        rec_cwf_BW = cdf["BW"][...]         
    if params_bias:                       
        rec_cwf_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]       
    for F in [1,2,3]:        
        # data
        cwf_index = (rec_cwf_F == F) if fs_field == 'FREQ' else (rec_cwf_F == LFR_Fs[F])       
        cwf_B[F] = rec_cwf_B[cwf_index, ...]        
        cwf_E[F] = rec_cwf_E[cwf_index, ...]        
        cwf_V[F] = rec_cwf_V[cwf_index, ...]   
        # time of the first sample of the cwf in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        cwf_time_acqui[F] = rec_cwf_T_acqui[cwf_index, 0] + rec_cwf_T_acqui[cwf_index, 1]*float(2)**(-16)   
        # acquisition time vector of the cwf samples 
        ramp_time = np.arange(packet_size) / LFR_Fs[F]   
        packet_numb = len(cwf_time_acqui[F]) // packet_size
        for i in range(packet_numb):            
            second_half_672_point_packet = (cwf_time_acqui[F][(i-1)*packet_size] == \
                                            cwf_time_acqui[F][i*packet_size]) if i else False
            cwf_time_acqui[F][i*packet_size:(i+1)*packet_size] = cwf_time_acqui[F][i*packet_size] + (ramp_time \
                                                                      if not second_half_672_point_packet else \
                                                                            (ramp_time + packet_size/LFR_Fs[F]))         
        # epoch time of the cwf samples [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]      
        cwf_time_epoch[F] = rec_cwf_T_epoch[cwf_index] 
        if params:
            if params_old:
                irec_range = range(len(rec_cwf_F))
                cwf_params[F] = [(int(rec_cwf_BW[i]), (int(rec_cwf_SP0[i]), int(rec_cwf_SP1[i])), \
                                 (int(rec_cwf_R0[i]), int(rec_cwf_R1[i]), int(rec_cwf_R2[i])))    \
                                  for i in irec_range if rec_cwf_F[i] == (F if fs_field == 'FREQ' else LFR_Fs[F])]                                 
            else:
                cwf_params[F] = [ rec_cwf_BW[cwf_index], (rec_cwf_SP0[cwf_index], rec_cwf_SP1[cwf_index]), 
                                 (rec_cwf_R0[cwf_index], rec_cwf_R1[cwf_index], rec_cwf_R2[cwf_index])  ]                
        if params_bias:                       
            cwf_params_bias[F] = [ rec_cwf_bias_mux[cwf_index] ]                                            
        if echo:
            print()
            print('cwf_time_acqui  F%s:'%(F), cwf_time_acqui[F].shape)
            print('cwf_time_epoch  F%s:'%(F), cwf_time_epoch[F].shape)        
            print('cwf_B     F%s:'%(F), cwf_B[F].shape)
            print('cwf_E     F%s:'%(F), cwf_E[F].shape)
            print('cwf_V     F%s:'%(F), cwf_V[F].shape)                                 
    cdf.close()
    return (cwf_B, cwf_E, cwf_V, cwf_time_acqui, cwf_time_epoch, cwf_params, cwf_params_bias)

def lfr2idl(asm_128x25, F=0):
    """
    routine for the conversion of the 25 real matrix componnets implemented by lfr:
       B1      B2     B3     E1     E2  
    B1 00      01-02  03-04  05-06  07-08
    B2 cc-cc   09     10-11  12-13  14-15
    B3 cc-cc   cc-cc  16     17-18  19-20
    E1 cc-cc   cc-cc  cc-cc  21     22-23
    E2 cc-cc   cc-cc  cc-cc  cc-cc  24
    into the 5x5 idl matrix format + extraction of the useful bins 
    and the corresponding frequency values
    """
    nfreq = [88, 104, 96][F]
    df = [96., 16., 1.][F]
    j0 = [17, 6, 7][F]        
    freq = np.arange(j0*df, j0*df+nfreq*df, df)
    i0  = [17, 6, 7][F]    
    #i0  = [0, 0, 0][F]        
    nspec = asm_128x25.shape[0]
    asm_idl = np.zeros((nspec, nfreq, 5, 5), dtype=np.complex64)
    for l in range(nspec):
        for k in range(nfreq):  
            asm_idl[l,k,0,0] = asm_128x25[l,k+i0,0 ] 
            asm_idl[l,k,1,1] = asm_128x25[l,k+i0,9 ]
            asm_idl[l,k,2,2] = asm_128x25[l,k+i0,16]
            asm_idl[l,k,3,3] = asm_128x25[l,k+i0,21]
            asm_idl[l,k,4,4] = asm_128x25[l,k+i0,24]
        
            asm_idl[l,k,0,1] = asm_128x25[l,k+i0,1 ] + 1j*asm_128x25[l,k+i0,2 ]
            asm_idl[l,k,0,2] = asm_128x25[l,k+i0,3 ] + 1j*asm_128x25[l,k+i0,4 ]
            asm_idl[l,k,0,3] = asm_128x25[l,k+i0,5 ] + 1j*asm_128x25[l,k+i0,6 ]
            asm_idl[l,k,0,4] = asm_128x25[l,k+i0,7 ] + 1j*asm_128x25[l,k+i0,8 ]
            asm_idl[l,k,1,2] = asm_128x25[l,k+i0,10] + 1j*asm_128x25[l,k+i0,11]
            asm_idl[l,k,1,3] = asm_128x25[l,k+i0,12] + 1j*asm_128x25[l,k+i0,13]
            asm_idl[l,k,1,4] = asm_128x25[l,k+i0,14] + 1j*asm_128x25[l,k+i0,15]
            asm_idl[l,k,2,3] = asm_128x25[l,k+i0,17] + 1j*asm_128x25[l,k+i0,18] 
            asm_idl[l,k,2,4] = asm_128x25[l,k+i0,19] + 1j*asm_128x25[l,k+i0,20]
            asm_idl[l,k,3,4] = asm_128x25[l,k+i0,22] + 1j*asm_128x25[l,k+i0,23] 
        
            asm_idl[l,k,1,0] = np.conj(asm_idl[l,k,0,1]) 
            asm_idl[l,k,2,0] = np.conj(asm_idl[l,k,0,2])
            asm_idl[l,k,3,0] = np.conj(asm_idl[l,k,0,3])  
            asm_idl[l,k,4,0] = np.conj(asm_idl[l,k,0,4])  
            asm_idl[l,k,2,1] = np.conj(asm_idl[l,k,1,2]) 
            asm_idl[l,k,3,1] = np.conj(asm_idl[l,k,1,3]) 
            asm_idl[l,k,4,1] = np.conj(asm_idl[l,k,1,4])  
            asm_idl[l,k,3,2] = np.conj(asm_idl[l,k,2,3])  
            asm_idl[l,k,4,2] = np.conj(asm_idl[l,k,2,4])  
            asm_idl[l,k,4,3] = np.conj(asm_idl[l,k,3,4])
    return freq, asm_idl
        
def load_asm_CDF_L1_old(file, F=0, echo=False, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                  
    # sampling frequency F=0,1,2 and number of records
    rec_asm_F = cdf['FREQ']
    irec_range = range(len(rec_asm_F))
    # data @F
    rec_asm = cdf["ASM"]
    asm_128x25 = np.array([rec_asm[i, ...] for i in irec_range if rec_asm_F[i] == F])
    # conversion into the 5x5 idl matrix format and extraction of the useful bins
    asm_freq, asm_idl = lfr2idl(asm_128x25, F=F)    
    # time of the first sample of the asm in unit of sec (coarse time) and 2^-16 sec (fine time)
    # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
    rec_asm_T_acqui = cdf['ACQUISITION_TIME']
    asm_T_acqui = np.array([rec_asm_T_acqui[i, ...] for i in irec_range if rec_asm_F[i] == F])  
    asm_time_acqui = asm_T_acqui[:,0] + asm_T_acqui[:,1]*float(2)**(-16)    
    # epoch time of the first sample of the asm [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
    rec_asm_T_epoch = cdf.raw_var("Epoch")[:]
    asm_time_epoch = np.array([rec_asm_T_epoch[i] for i in irec_range if rec_asm_F[i] == F])     
    if echo:
        print()
        print('asm_time_acqui :', asm_time_acqui.shape)
        print('asm_time_epoch :', asm_time_epoch.shape)      
        print('asm_128x25 :', asm_128x25.shape)          
        print('asm_idl    :', asm_idl.shape)
        print('asm_freq   :', asm_freq.shape)
    # LFR parameters
    rec_asm_R0 = cdf["R0"]
    rec_asm_R1 = cdf["R1"]
    rec_asm_R2 = cdf["R2"]
    rec_asm_SP0 = cdf["SP0"]
    rec_asm_SP1 = cdf["SP1"]
    rec_asm_BW = cdf["BW"]  
    asm_params = [(int(rec_asm_BW[i]), (int(rec_asm_SP0[i]), int(rec_asm_SP1[i])), \
                  (int(rec_asm_R0[i]), int(rec_asm_R1[i]), int(rec_asm_R2[i])))    \
                  for i in irec_range if rec_asm_F[i] == F]    
    cdf.close()
    asm_cdf_l1 = (asm_idl, asm_time_acqui, asm_time_epoch, asm_freq, asm_params)
    return asm_cdf_l1
    
def load_asm_CDF_L1(file, echo=False, params=False, params_old=False, params_bias=False, 
                    key_attrs=None, key_field=None, key_field_attrs=None, fill_value=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # http://spacepy.lanl.gov/doc/pycdf.html
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                 
    asm_freq = 3*[None]   
    asm_data = 3*[None]   
    asm_time_epoch = 3*[None]
    asm_time_acqui = 3*[None]    
    asm_params = 3*[None]  
    asm_params_bias = 3*[None]       
    # sampling frequency F=0,1,2
    rec_asm_F = cdf['FREQ'][...]    
    rec_asm_data = cdf["ASM"][...]
    rec_asm_T_acqui = cdf['ACQUISITION_TIME'][...]
    rec_asm_T_epoch = cdf.raw_var("Epoch")[...]      
    if params:  
        # LFR parameters
        rec_asm_R0 = cdf["R0"][...]
        rec_asm_R1 = cdf["R1"][...]
        rec_asm_R2 = cdf["R2"][...]
        rec_asm_SP0 = cdf["SP0"][...]
        rec_asm_SP1 = cdf["SP1"][...]
        rec_asm_BW = cdf["BW"][...]    
    if params_bias:                       
        rec_asm_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]             
    for F in [0,1,2]:        
        # data
        asm_index = (rec_asm_F == F)
        asm_128x25 = rec_asm_data[asm_index, ...]      
        if fill_value != None:
            asm_128x25[asm_128x25 < fill_value] = np.nan      
        # conversion into the 5x5 idl matrix format and extraction of the useful bins
        asm_freq[F], asm_data[F] = lfr2idl(asm_128x25, F=F)    
        # time of the first sample of the asm in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        asm_time_acqui[F] = rec_asm_T_acqui[asm_index, 0] + rec_asm_T_acqui[asm_index, 1]*float(2)**(-16)                                         
        # epoch time of the first sample of the asm [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        asm_time_epoch[F] = rec_asm_T_epoch[asm_index]                  
        if params:
            if params_old:
                irec_range = range(len(rec_asm_F))
                asm_params[F] = [(int(rec_asm_BW[i]), (int(rec_asm_SP0[i]), int(rec_asm_SP1[i])), \
                                 (int(rec_asm_R0[i]), int(rec_asm_R1[i]), int(rec_asm_R2[i])))    \
                                  for i in irec_range if rec_asm_F[i] == F]                                 
            else:
                asm_params[F] = [ rec_asm_BW[asm_index], (rec_asm_SP0[asm_index], rec_asm_SP1[asm_index]), 
                                 (rec_asm_R0[asm_index], rec_asm_R1[asm_index], rec_asm_R2[asm_index])  ]          
        if params_bias:                       
            asm_params_bias[F] = [ rec_asm_bias_mux[asm_index] ]                               
        if echo:
            print()
            print('asm_time_acqui   F%s:'%(F), asm_time_acqui[F].shape)
            print('asm_time_epoch   F%s:'%(F), asm_time_epoch[F].shape)      
            print('asm_128x25       F%s:'%(F), asm_128x25.shape)          
            print('asm_data         F%s:'%(F), asm_data[F].shape)                                       
            print('asm_freq         F%s:'%(F), asm_freq[F].shape)                                                                 
    cdf.close()
    asm_cdf_l1 = (asm_data, asm_time_acqui, asm_time_epoch, asm_freq, asm_params, asm_params_bias)
    return asm_cdf_l1    
    
def lfr_autocross_to_5x5mat(bp2_auto, bp2_cross, denorm=True):  
    """
    routine for the conversion of the data format implemented by lfr for bp2 
    [5 auto (real) + 10 cross (complex)]:
          B1      B2     B3     E1     E2  
       B1 00      00     01     02     03
       B2 cc      01     04     05     06
       B3 cc      cc     02     07     08
       E1 cc      cc     cc     03     09
       E2 cc      cc     cc     cc     04
    into the 5x5 matrix format 
    """
    nspec = bp2_auto.shape[0] 
    nfreq = bp2_auto.shape[1] 
    bp2_mat = np.zeros((nspec, nfreq, 5, 5), dtype=np.complex)     
    bp2_mat[...,0,0] = bp2_auto[...,0] 
    bp2_mat[...,1,1] = bp2_auto[...,1]
    bp2_mat[...,2,2] = bp2_auto[...,2]
    bp2_mat[...,3,3] = bp2_auto[...,3] 
    bp2_mat[...,4,4] = bp2_auto[...,4]

    bp2_mat[...,0,1] = bp2_cross[...,0]    
    bp2_mat[...,0,2] = bp2_cross[...,1] 
    bp2_mat[...,0,3] = bp2_cross[...,2]
    bp2_mat[...,0,4] = bp2_cross[...,3] 
    bp2_mat[...,1,2] = bp2_cross[...,4] 
    bp2_mat[...,1,3] = bp2_cross[...,5] 
    bp2_mat[...,1,4] = bp2_cross[...,6] 
    bp2_mat[...,2,3] = bp2_cross[...,7] 
    bp2_mat[...,2,4] = bp2_cross[...,8] 
    bp2_mat[...,3,4] = bp2_cross[...,9]

    bp2_mat[...,1,0] = np.conj(bp2_mat[...,0,1]) 
    bp2_mat[...,2,0] = np.conj(bp2_mat[...,0,2])
    bp2_mat[...,3,0] = np.conj(bp2_mat[...,0,3])  
    bp2_mat[...,4,0] = np.conj(bp2_mat[...,0,4])  
    bp2_mat[...,2,1] = np.conj(bp2_mat[...,1,2]) 
    bp2_mat[...,3,1] = np.conj(bp2_mat[...,1,3]) 
    bp2_mat[...,4,1] = np.conj(bp2_mat[...,1,4])  
    bp2_mat[...,3,2] = np.conj(bp2_mat[...,2,3])  
    bp2_mat[...,4,2] = np.conj(bp2_mat[...,2,4])  
    bp2_mat[...,4,3] = np.conj(bp2_mat[...,3,4])
    
    if denorm: bp2_denormalization(bp2_mat)
    return bp2_mat            
    
def load_bp2_CDF_L1_old(file, echo=False, params=True, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                     
    bp2_n_freq = 3*[None]                            
    bp2_n_auto = 3*[None]
    bp2_n_cross = 3*[None]   
    bp2_n_data = 3*[None]    
    bp2_n_time_epoch = 3*[None]
    bp2_n_time_acqui = 3*[None]
    bp2_n_params = 3*[None]    
    bp2_b_freq = 2*[None]
    bp2_b_auto = 2*[None]
    bp2_b_cross = 2*[None] 
    bp2_b_data = 2*[None]
    bp2_b_time_epoch = 2*[None]
    bp2_b_time_acqui = 2*[None]
    bp2_b_params = 2*[None]
    # sampling frequency F=0,1,2, normal (0) or burst (1) mode, and number of records
    rec_bp2_F = cdf['FREQ']
    rec_bp2_NM = cdf['SURVEY_MODE']        
    irec_range = range(len(rec_bp2_F))   
    rec_bp2_auto = cdf["AUTO"]
    rec_bp2_cross_re = cdf["CROSS_RE"]
    rec_bp2_cross_im = cdf["CROSS_IM"]
    rec_bp2_T_acqui = cdf['ACQUISITION_TIME']
    rec_bp2_T_epoch = cdf.raw_var("Epoch")    
    if params:        
        rec_bp2_R0 = cdf["R0"]
        rec_bp2_R1 = cdf["R1"]
        rec_bp2_R2 = cdf["R2"]
        rec_bp2_SP0 = cdf["SP0"]
        rec_bp2_SP1 = cdf["SP1"]
        rec_bp2_BW = cdf["BW"]  
    # Normal Mode data    
    for F in [0,1,2]:
        # frequency table
        nfreq_n = [11, 13, 12][F]
        df_n = [8*96., 8*16., 8*1.][F]
        fstart_n = [1968, 152, 10.5][F]        
        bp2_n_freq[F] = np.arange(fstart_n, fstart_n+nfreq_n*df_n, df_n) 
        # data
        bp2_n_auto[F] = np.array([ rec_bp2_auto[i, 0:nfreq_n, :] \
                                   for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 0) ])
        bp2_n_cross[F] = np.array([ rec_bp2_cross_re[i, 0:nfreq_n, :] + 1j*rec_bp2_cross_im[i, 0:nfreq_n, :] \
                                    for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 0) ])   
        # conversion into the 5x5 idl matrix format
        bp2_n_data[F] = lfr_autocross_to_5x5mat(bp2_n_auto[F], bp2_n_cross[F])               
        # time of the first sample of the bp2 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp2_n_time_acqui[F] = np.array([ rec_bp2_T_acqui[i, 0] + rec_bp2_T_acqui[i, 1]*float(2)**(-16) \
                                         for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 0) ])  
        # epoch time of the first sample of the bp2 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp2_n_time_epoch[F] = np.array([ rec_bp2_T_epoch[i] \
                                         for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 0) ])                    
        if params:        
            bp2_n_params[F] = [(int(rec_bp2_BW[i]), (int(rec_bp2_SP0[i]), int(rec_bp2_SP1[i])), \
                               (int(rec_bp2_R0[i]), int(rec_bp2_R1[i]), int(rec_bp2_R2[i])))    \
                               for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 0) ]   
        if echo:
            print()
            print('bp2_n_time_acqui F%s:'%(F), bp2_n_time_acqui[F].shape)
            print('bp2_n_time_epoch F%s:'%(F), bp2_n_time_epoch[F].shape)      
            print('bp2_n_auto       F%s:'%(F), bp2_n_auto[F].shape)          
            print('bp2_n_cross      F%s:'%(F), bp2_n_cross[F].shape)  
            print('bp2_n_data       F%s:'%(F), bp2_n_data[F].shape)                      
            print('bp2_n_freq       F%s:'%(F), bp2_n_freq[F].shape)  
    # Burst Mode data               
    for F in [0,1]:
        # frequency table
        nfreq_b = [22, 26][F]
        df_b = [4*96., 4*16., 4*1.][F]
        fstart_b = [1776, 120.][F]        
        bp2_b_freq[F] = np.arange(fstart_b, fstart_b+nfreq_b*df_b, df_b) 
        # data
        bp2_b_auto[F] = np.array([ rec_bp2_auto[i, 0:nfreq_b, :] \
                                   for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 1) ])
        bp2_b_cross[F] = np.array([ rec_bp2_cross_re[i, 0:nfreq_b, :] + 1j*rec_bp2_cross_im[i, 0:nfreq_b, :] \
                                    for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 1) ])   
        # conversion into the 5x5 idl matrix format
        bp2_b_data[F] = lfr_autocross_to_5x5mat(bp2_b_auto[F], bp2_b_cross[F])     
        # time of the first sample of the bp2 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp2_b_time_acqui[F] = np.array([ rec_bp2_T_acqui[i, 0] + rec_bp2_T_acqui[i, 1]*float(2)**(-16) \
                                         for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 1) ])  
        # epoch time of the first sample of the bp2 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp2_b_time_epoch[F] = np.array([ rec_bp2_T_epoch[i] \
                                         for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 1) ])          
        if params:        
            bp2_b_params[F] = [(int(rec_bp2_BW[i]), (int(rec_bp2_SP0[i]), int(rec_bp2_SP1[i])), \
                               (int(rec_bp2_R0[i]), int(rec_bp2_R1[i]), int(rec_bp2_R2[i])))    \
                               for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 1) ]
        if echo:
            print()
            print('bp2_b_time_acqui F%s:'%(F), bp2_b_time_acqui[F].shape)
            print('bp2_b_time_epoch F%s:'%(F), bp2_b_time_epoch[F].shape)      
            print('bp2_b_auto       F%s:'%(F), bp2_b_auto[F].shape)          
            print('bp2_b_cross      F%s:'%(F), bp2_b_cross[F].shape)  
            print('bp2_b_data       F%s:'%(F), bp2_b_data[F].shape)                                  
            print('bp2_b_freq       F%s:'%(F), bp2_b_freq[F].shape)            
    cdf.close()
    return (bp2_n_data, bp2_n_time_acqui, bp2_n_time_epoch, bp2_n_freq, bp2_n_params,
            bp2_b_data, bp2_b_time_acqui, bp2_b_time_epoch, bp2_b_freq, bp2_b_params)       

def load_bp2_CDF_L1(file, denorm=True, echo=False, params=False, params_old=False, params_bias=False, 
                    key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                    
    bp2_n_freq = 3*[None]                            
    bp2_n_auto = 3*[None]
    bp2_n_cross = 3*[None]   
    bp2_n_data = 3*[None]    
    bp2_n_time_epoch = 3*[None]
    bp2_n_time_acqui = 3*[None]
    bp2_n_params = 3*[None]
    bp2_n_params_bias = 3*[None]    
    bp2_b_freq = 2*[None]
    bp2_b_auto = 2*[None]
    bp2_b_cross = 2*[None] 
    bp2_b_data = 2*[None]
    bp2_b_time_epoch = 2*[None]
    bp2_b_time_acqui = 2*[None]
    bp2_b_params = 2*[None]
    bp2_b_params_bias = 2*[None]    
    # sampling frequency F=0,1,2, normal (0) or burst (1) mode, and number of records
    rec_bp2_F = cdf['FREQ'][...]
    rec_bp2_NM = cdf['SURVEY_MODE'][...]        
    rec_bp2_auto = cdf["AUTO"][...]
    rec_bp2_cross_re = cdf["CROSS_RE"][...]
    rec_bp2_cross_im = cdf["CROSS_IM"][...]
    rec_bp2_T_acqui = cdf['ACQUISITION_TIME'][...]
    rec_bp2_T_epoch = cdf.raw_var("Epoch")[...]    
    if params:        
        rec_bp2_R0 = cdf["R0"][...]    
        rec_bp2_R1 = cdf["R1"][...]    
        rec_bp2_R2 = cdf["R2"][...]    
        rec_bp2_SP0 = cdf["SP0"][...]    
        rec_bp2_SP1 = cdf["SP1"][...]    
        rec_bp2_BW = cdf["BW"][...]     
    if params_bias:                       
        rec_bp2_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]           
    # Normal Mode data    
    for F in [0,1,2]:
        # frequency table
        nfreq_n = [11, 13, 12][F]
        df_n = [8*96., 8*16., 8*1.][F]
        fstart_n = [1968, 152, 10.5][F]        
        bp2_n_freq[F] = np.arange(fstart_n, fstart_n+nfreq_n*df_n, df_n) 
        # data
        bp2_n_index = np.logical_and(rec_bp2_F == F, rec_bp2_NM == 0)
        bp2_n_auto[F] = rec_bp2_auto[bp2_n_index, 0:nfreq_n, :]
        bp2_n_cross[F] = rec_bp2_cross_re[bp2_n_index, 0:nfreq_n, :] + 1j*rec_bp2_cross_im[bp2_n_index, 0:nfreq_n, :]                
        # conversion into the 5x5 idl matrix format
        bp2_n_data[F] = lfr_autocross_to_5x5mat(bp2_n_auto[F], bp2_n_cross[F], denorm=denorm)      
         # time of the first sample of the bp2 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp2_n_time_acqui[F] = rec_bp2_T_acqui[bp2_n_index, 0] + rec_bp2_T_acqui[bp2_n_index, 1]*float(2)**(-16)                                         
        # epoch time of the first sample of the bp2 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp2_n_time_epoch[F] = rec_bp2_T_epoch[bp2_n_index]                  
        if params:
            if params_old:
                irec_range = range(len(rec_bp2_F))
                bp2_n_params[F] = [(int(rec_bp2_BW[i]), (int(rec_bp2_SP0[i]), int(rec_bp2_SP1[i])), \
                                   (int(rec_bp2_R0[i]), int(rec_bp2_R1[i]), int(rec_bp2_R2[i])))    \
                                   for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 0)]                                                                  
            else:
                bp2_n_params[F] = [ rec_bp2_BW[bp2_n_index], (rec_bp2_SP0[bp2_n_index], rec_bp2_SP1[bp2_n_index]), 
                                   (rec_bp2_R0[bp2_n_index], rec_bp2_R1[bp2_n_index], rec_bp2_R2[bp2_n_index]) ]  
        if params_bias:                       
            bp2_n_params_bias[F] = [ rec_bp2_bias_mux[bp2_n_index] ]                                                      
        if echo:
            print()
            print('bp2_n_time_acqui F%s:'%(F), bp2_n_time_acqui[F].shape)
            print('bp2_n_time_epoch F%s:'%(F), bp2_n_time_epoch[F].shape)      
            print('bp2_n_auto       F%s:'%(F), bp2_n_auto[F].shape)          
            print('bp2_n_cross      F%s:'%(F), bp2_n_cross[F].shape)  
            print('bp2_n_data       F%s:'%(F), bp2_n_data[F].shape)                      
            print('bp2_n_freq       F%s:'%(F), bp2_n_freq[F].shape)  
    # Burst Mode data               
    for F in [0,1]:
        # frequency table
        nfreq_b = [22, 26][F]
        df_b = [4*96., 4*16.][F]
        fstart_b = [1776, 120.][F]        
        bp2_b_freq[F] = np.arange(fstart_b, fstart_b+nfreq_b*df_b, df_b) 
        # data
        bp2_b_index = np.logical_and(rec_bp2_F == F, rec_bp2_NM == 1)
        bp2_b_auto[F] = rec_bp2_auto[bp2_b_index, 0:nfreq_b, :]
        bp2_b_cross[F] = rec_bp2_cross_re[bp2_b_index, 0:nfreq_b, :] + 1j*rec_bp2_cross_im[bp2_b_index, 0:nfreq_b, :]                
        # conversion into the 5x5 idl matrix format
        bp2_b_data[F] = lfr_autocross_to_5x5mat(bp2_b_auto[F], bp2_b_cross[F], denorm=denorm)      
        # time of the first sample of the bp2 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp2_b_time_acqui[F] = rec_bp2_T_acqui[bp2_b_index, 0] + rec_bp2_T_acqui[bp2_b_index, 1]*float(2)**(-16)                                         
        # epoch time of the first sample of the bp2 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp2_b_time_epoch[F] = rec_bp2_T_epoch[bp2_b_index]                  
        if params:
            if params_old:
                irec_range = range(len(rec_bp2_F))
                bp2_b_params[F] = [(int(rec_bp2_BW[i]), (int(rec_bp2_SP0[i]), int(rec_bp2_SP1[i])), \
                                   (int(rec_bp2_R0[i]), int(rec_bp2_R1[i]), int(rec_bp2_R2[i])))    \
                                   for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 1)]                                                                  
            else:
                bp2_b_params[F] = [ rec_bp2_BW[bp2_b_index], (rec_bp2_SP0[bp2_b_index], rec_bp2_SP1[bp2_b_index]), 
                                   (rec_bp2_R0[bp2_b_index], rec_bp2_R1[bp2_b_index], rec_bp2_R2[bp2_b_index]) ]    
            if params_bias:                       
                bp2_b_params_bias[F] = [ rec_bp2_bias_mux[bp2_b_index] ]                                                
        if echo:
            print()
            print('bp2_b_time_acqui F%s:'%(F), bp2_b_time_acqui[F].shape)
            print('bp2_b_time_epoch F%s:'%(F), bp2_b_time_epoch[F].shape)      
            print('bp2_b_auto       F%s:'%(F), bp2_b_auto[F].shape)          
            print('bp2_b_cross      F%s:'%(F), bp2_b_cross[F].shape)  
            print('bp2_b_data       F%s:'%(F), bp2_b_data[F].shape)                      
            print('bp2_b_freq       F%s:'%(F), bp2_b_freq[F].shape)             
    cdf.close()
    return (bp2_n_data, bp2_n_time_acqui, bp2_n_time_epoch, bp2_n_freq, bp2_n_params, bp2_n_params_bias,
            bp2_b_data, bp2_b_time_acqui, bp2_b_time_epoch, bp2_b_freq, bp2_b_params, bp2_b_params_bias)   
            
def load_sbm_bp2_CDF_L1(file, denorm=True, echo=False, params=False, params_bias=False, 
                        key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)  
    # SBM1 or SBM2 ?
    bname = os.path.basename(file)
    index = bname.find('sbm')
    if index == -1:
        print( 'Fichier {} n\'est pas un ficher SBM ... !'.format(file) )
        return
    else:
        sbm = int(bname[index+3])
        if sbm not in [1, 2]: 
            print( 'Fichier {} n\'est pas un ficher SBM1 ou SBM2 ?'.format(file) )
            return  
    if echo: print("SBM%d data file"%(sbm))                                
    bp2_freq = sbm*[None]                            
    bp2_auto = sbm*[None]
    bp2_cross = sbm*[None]   
    bp2_data = sbm*[None]    
    bp2_time_epoch = sbm*[None]
    bp2_time_acqui = sbm*[None]
    bp2_params = sbm*[None]
    bp2_params_bias = sbm*[None]          
    rec_bp2_auto = cdf["AUTO"][...]
    rec_bp2_cross_re = cdf["CROSS_RE"][...]
    rec_bp2_cross_im = cdf["CROSS_IM"][...]
    rec_bp2_T_acqui = cdf['ACQUISITION_TIME'][...]
    rec_bp2_T_epoch = cdf.raw_var("Epoch")[...]    
    rec_bp2_F = cdf['FREQ'][...] if sbm == 2 else np.zeros(len(rec_bp2_T_epoch), dtype=int)    
    if params:        
        rec_bp2_R0 = cdf["R0"][...]    
        rec_bp2_R1 = cdf["R1"][...]    
        rec_bp2_R2 = cdf["R2"][...]    
        rec_bp2_SP0 = cdf["SP0"][...]    
        rec_bp2_SP1 = cdf["SP1"][...]    
        rec_bp2_BW = cdf["BW"][...]     
    if params_bias:                       
        rec_bp2_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]                   
    # Selected Burst Mode 1 or 2 data  
    for F in range(sbm):
        # frequency table
        nfreq = [22, 26][F]
        df = [4*96., 4*16.][F]
        fstart = [1776, 120.][F]        
        bp2_freq[F] = np.arange(fstart, fstart+nfreq*df, df) 
        # data
        bp2_index = (rec_bp2_F == F)
        bp2_auto[F] = rec_bp2_auto[bp2_index, 0:nfreq, :]
        bp2_cross[F] = rec_bp2_cross_re[bp2_index, 0:nfreq, :] + 1j*rec_bp2_cross_im[bp2_index, 0:nfreq, :]                
        # conversion into the 5x5 idl matrix format
        bp2_data[F] = lfr_autocross_to_5x5mat(bp2_auto[F], bp2_cross[F], denorm=denorm)      
        # time of the first sample of the bp2 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp2_time_acqui[F] = rec_bp2_T_acqui[bp2_index, 0] + rec_bp2_T_acqui[bp2_index, 1]*float(2)**(-16)                                         
        # epoch time of the first sample of the bp2 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp2_time_epoch[F] = rec_bp2_T_epoch[bp2_index]                  
        if params:           
            bp2_params[F] = [ rec_bp2_BW[bp2_index], (rec_bp2_SP0[bp2_index], rec_bp2_SP1[bp2_index]), 
                             (rec_bp2_R0[bp2_index], rec_bp2_R1[bp2_index], rec_bp2_R2[bp2_index]) ]    
            if params_bias:                       
                bp2_params_bias[F] = [ rec_bp2_bias_mux[bp2_index] ]                                                
        if echo:
            print()
            print('bp2_time_acqui F%s:'%(F), bp2_time_acqui[F].shape)
            print('bp2_time_epoch F%s:'%(F), bp2_time_epoch[F].shape)      
            print('bp2_auto       F%s:'%(F), bp2_auto[F].shape)          
            print('bp2_cross      F%s:'%(F), bp2_cross[F].shape)  
            print('bp2_data       F%s:'%(F), bp2_data[F].shape)                      
            print('bp2_freq       F%s:'%(F), bp2_freq[F].shape)             
    cdf.close()        
    return (bp2_data, bp2_time_acqui, bp2_time_epoch, bp2_freq, bp2_params, bp2_params_bias)                 

def load_bp1_CDF_L1_old(file, echo=False, params=True, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                   
    bp1_n_freq = 3*[None]                            
    bp1_n_pb = 3*[None]
    bp1_n_pe = 3*[None]
    bp1_n_dop = 3*[None]
    bp1_n_nvec0 = 3*[None]
    bp1_n_nvec1 = 3*[None]
    bp1_n_nvec2 = 3*[None]
    bp1_n_ellip = 3*[None]
    bp1_n_sx_rea = 3*[None]
    bp1_n_sx_arg = 3*[None]
    bp1_n_vphi_rea = 3*[None]
    bp1_n_vphi_arg = 3*[None]
    bp1_n_time_epoch = 3*[None]
    bp1_n_time_acqui = 3*[None]
    bp1_n_params = 3*[None]    
    bp1_b_freq = 2*[None]                            
    bp1_b_pb = 2*[None]
    bp1_b_pe = 2*[None]
    bp1_b_dop = 2*[None]
    bp1_b_nvec0 = 2*[None]
    bp1_b_nvec1 = 2*[None]
    bp1_b_nvec2 = 2*[None]
    bp1_b_ellip = 2*[None]
    bp1_b_sx_rea = 2*[None]
    bp1_b_sx_arg = 2*[None]
    bp1_b_vphi_rea = 2*[None]
    bp1_b_vphi_arg = 2*[None]
    bp1_b_time_epoch = 2*[None]
    bp1_b_time_acqui = 2*[None]
    bp1_b_params = 2*[None]  
    # sampling frequency F=0,1,2, normal (0) or burst (1) mode, and number of records
    rec_bp1_F = cdf['FREQ']
    rec_bp1_NM = cdf['SURVEY_MODE']        
    irec_range = range(len(rec_bp1_F))  
    rec_bp1_T_acqui = cdf['ACQUISITION_TIME']
    rec_bp1_T_epoch = cdf.raw_var("Epoch")    
    rec_bp1_pb = cdf["PB"]
    rec_bp1_pe = cdf["PE"]
    rec_bp1_dop = cdf["DOP"]
    rec_bp1_nvec0 = cdf["NVEC_V0"]
    rec_bp1_nvec1 = cdf["NVEC_V1"]
    rec_bp1_nvec2 = cdf["NVEC_V2"]
    rec_bp1_ellip = cdf["ELLIP"]
    rec_bp1_sx_rea = cdf["SX_REA"]
    rec_bp1_sx_arg = cdf["SX_ARG"]
    rec_bp1_vphi_rea = cdf["VPHI_REA"]
    rec_bp1_vphi_arg = cdf["VPHI_ARG"]    
    if params:        
        rec_bp1_R0 = cdf["R0"]
        rec_bp1_R1 = cdf["R1"]
        rec_bp1_R2 = cdf["R2"]
        rec_bp1_SP0 = cdf["SP0"]
        rec_bp1_SP1 = cdf["SP1"]
        rec_bp1_BW = cdf["BW"]            
    # Normal Mode data    
    for F in [0,1,2]:
        # frequency table
        nfreq_n = [11, 13, 12][F]
        df_n = [8*96., 8*16., 8*1.][F]
        fstart_n = [1968, 152, 10.5][F]        
        bp1_n_freq[F] = np.arange(fstart_n, fstart_n+nfreq_n*df_n, df_n) 
        # data
        bp1_n_pb[F] = np.array([ rec_bp1_pb[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_pe[F] = np.array([ rec_bp1_pe[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_dop[F] = np.array([ rec_bp1_dop[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_nvec0[F] = np.array([ rec_bp1_nvec0[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_nvec1[F] = np.array([ rec_bp1_nvec1[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_nvec2[F] = np.array([ rec_bp1_nvec2[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_ellip[F] = np.array([ rec_bp1_ellip[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_sx_rea[F] = np.array([ rec_bp1_sx_rea[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_sx_arg[F] = np.array([ rec_bp1_sx_arg[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_vphi_rea[F] = np.array([ rec_bp1_vphi_rea[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        bp1_n_vphi_arg[F] = np.array([ rec_bp1_vphi_arg[i, 0:nfreq_n] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])
        # time of the first sample of the bp1 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp1_n_time_acqui[F] = np.array([ rec_bp1_T_acqui[i, 0] + rec_bp1_T_acqui[i, 1]*float(2)**(-16) \
                                         for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])  
        # epoch time of the first sample of the bp1 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp1_n_time_epoch[F] = np.array([ rec_bp1_T_epoch[i] \
                                         for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0) ])                    
        if params:        
            bp1_n_params[F] = [(int(rec_bp1_BW[i]), (int(rec_bp1_SP0[i]), int(rec_bp1_SP1[i])), \
                               (int(rec_bp1_R0[i]), int(rec_bp1_R1[i]), int(rec_bp1_R2[i])))    \
                               for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0)]   
        if echo:
            print()
            print('bp1_n_time_acqui F%s:'%(F), bp1_n_time_acqui[F].shape)
            print('bp1_n_time_epoch F%s:'%(F), bp1_n_time_epoch[F].shape)      
            print('bp1_n_pb         F%s:'%(F), bp1_n_pb[F].shape)          
            print('bp1_n_pe         F%s:'%(F), bp1_n_pe[F].shape)                                       
            print('bp1_n_dop        F%s:'%(F), bp1_n_dop[F].shape)                                       
            print('bp1_n_nvec0      F%s:'%(F), bp1_n_nvec0[F].shape)                                       
            print('bp1_n_nvec1      F%s:'%(F), bp1_n_nvec1[F].shape)                                       
            print('bp1_n_nvec2      F%s:'%(F), bp1_n_nvec2[F].shape)   
            print('bp1_n_ellip      F%s:'%(F), bp1_n_ellip[F].shape)                                       
            print('bp1_n_sx_rea     F%s:'%(F), bp1_n_sx_rea[F].shape)                                       
            print('bp1_n_sx_arg     F%s:'%(F), bp1_n_sx_arg[F].shape)     
            print('bp1_n_vphi_rea   F%s:'%(F), bp1_n_vphi_rea[F].shape)                                       
            print('bp1_n_vphi_arg   F%s:'%(F), bp1_n_vphi_arg[F].shape)                       
            print('bp1_n_freq       F%s:'%(F), bp1_n_freq[F].shape)  
    # Burst Mode data               
    for F in [0,1]:
        # frequency table
        nfreq_b = [22, 26][F]
        df_b = [4*96., 4*16., 4*1.][F]
        fstart_b = [1776, 120.][F]        
        bp1_b_freq[F] = np.arange(fstart_b, fstart_b+nfreq_b*df_b, df_b) 
        # data
        bp1_b_pb[F] = np.array([ rec_bp1_pb[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_pe[F] = np.array([ rec_bp1_pe[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_dop[F] = np.array([ rec_bp1_dop[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_nvec0[F] = np.array([ rec_bp1_nvec0[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_nvec1[F] = np.array([ rec_bp1_nvec1[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_nvec2[F] = np.array([ rec_bp1_nvec2[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_ellip[F] = np.array([ rec_bp1_ellip[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_sx_rea[F] = np.array([ rec_bp1_sx_rea[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_sx_arg[F] = np.array([ rec_bp1_sx_arg[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_vphi_rea[F] = np.array([ rec_bp1_vphi_rea[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        bp1_b_vphi_arg[F] = np.array([ rec_bp1_vphi_arg[i, 0:nfreq_b] \
                                 for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])
        # time of the first sample of the bp1 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp1_b_time_acqui[F] = np.array([ rec_bp1_T_acqui[i, 0] + rec_bp1_T_acqui[i, 1]*float(2)**(-16) \
                                         for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])  
        # epoch time of the first sample of the bp1 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp1_b_time_epoch[F] = np.array([ rec_bp1_T_epoch[i] \
                                         for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ])          
        if params:        
            bp1_b_params[F] = [(int(rec_bp1_BW[i]), (int(rec_bp1_SP0[i]), int(rec_bp1_SP1[i])), \
                               (int(rec_bp1_R0[i]), int(rec_bp1_R1[i]), int(rec_bp1_R2[i])))    \
                               for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1) ]
        if echo:
            print()
            print('bp1_b_time_acqui F%s:'%(F), bp1_b_time_acqui[F].shape)
            print('bp1_b_time_epoch F%s:'%(F), bp1_b_time_epoch[F].shape)      
            print('bp1_b_pb         F%s:'%(F), bp1_b_pb[F].shape)          
            print('bp1_b_pe         F%s:'%(F), bp1_b_pe[F].shape)                                       
            print('bp1_b_dop        F%s:'%(F), bp1_b_dop[F].shape)                                       
            print('bp1_b_nvec0      F%s:'%(F), bp1_b_nvec0[F].shape)                                       
            print('bp1_b_nvec1      F%s:'%(F), bp1_b_nvec1[F].shape)                                       
            print('bp1_b_nvec2      F%s:'%(F), bp1_b_nvec2[F].shape)   
            print('bp1_b_ellip      F%s:'%(F), bp1_b_ellip[F].shape)                                       
            print('bp1_b_sx_rea     F%s:'%(F), bp1_b_sx_rea[F].shape)                                       
            print('bp1_b_sx_arg     F%s:'%(F), bp1_b_sx_arg[F].shape)     
            print('bp1_b_vphi_rea   F%s:'%(F), bp1_b_vphi_rea[F].shape)                                       
            print('bp1_b_vphi_arg   F%s:'%(F), bp1_b_vphi_arg[F].shape)                       
            print('bp1_b_freq       F%s:'%(F), bp1_b_freq[F].shape)         
    cdf.close()
    return (bp1_n_pb, bp1_n_pe, bp1_n_dop, bp1_n_nvec0, bp1_n_nvec1, bp1_n_nvec2, bp1_n_ellip, 
            bp1_n_sx_rea, bp1_n_sx_arg, bp1_n_vphi_rea, bp1_n_vphi_arg,            
            bp1_n_time_acqui, bp1_n_time_epoch, bp1_n_freq, bp1_n_params,
            bp1_b_pb, bp1_b_pe, bp1_b_dop, bp1_b_nvec0, bp1_b_nvec1, bp1_b_nvec2, bp1_b_ellip, 
            bp1_b_sx_rea, bp1_b_sx_arg, bp1_b_vphi_rea, bp1_b_vphi_arg,            
            bp1_b_time_acqui, bp1_b_time_epoch, bp1_b_freq, bp1_b_params)   

def load_bp1_CDF_L1(file, echo=False, params=False, params_old=False, params_bias=False, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                      
    bp1_n_freq = 3*[None]                            
    bp1_n_pb = 3*[None]
    bp1_n_pe = 3*[None]
    bp1_n_dop = 3*[None]
    bp1_n_nvec0 = 3*[None]
    bp1_n_nvec1 = 3*[None]
    bp1_n_nvec2 = 3*[None]
    bp1_n_ellip = 3*[None]
    bp1_n_sx_rea = 3*[None]
    bp1_n_sx_arg = 3*[None]
    bp1_n_vphi_rea = 3*[None]
    bp1_n_vphi_arg = 3*[None]
    bp1_n_time_epoch = 3*[None]
    bp1_n_time_acqui = 3*[None]
    bp1_n_params = 3*[None]    
    bp1_n_params_bias = 3*[None]
    bp1_b_freq = 2*[None]                            
    bp1_b_pb = 2*[None]
    bp1_b_pe = 2*[None]
    bp1_b_dop = 2*[None]
    bp1_b_nvec0 = 2*[None]
    bp1_b_nvec1 = 2*[None]
    bp1_b_nvec2 = 2*[None]
    bp1_b_ellip = 2*[None]
    bp1_b_sx_rea = 2*[None]
    bp1_b_sx_arg = 2*[None]
    bp1_b_vphi_rea = 2*[None]
    bp1_b_vphi_arg = 2*[None]
    bp1_b_time_epoch = 2*[None]
    bp1_b_time_acqui = 2*[None]
    bp1_b_params = 2*[None]
    bp1_b_params_bias = 2*[None]      
    # sampling frequency F=0,1,2, normal (0) or burst (1) mode, and number of records
    rec_bp1_F = cdf['FREQ'][...]
    rec_bp1_NM = cdf['SURVEY_MODE'][...]        
    rec_bp1_T_acqui = cdf['ACQUISITION_TIME'][...]
    rec_bp1_T_epoch = cdf.raw_var("Epoch")[...]    
    rec_bp1_pb = cdf["PB"][...]
    rec_bp1_pe = cdf["PE"][...]
    rec_bp1_dop = cdf["DOP"][...]
    rec_bp1_nvec0 = cdf["NVEC_V0"][...]
    rec_bp1_nvec1 = cdf["NVEC_V1"][...]
    rec_bp1_nvec2 = cdf["NVEC_V2"][...]
    rec_bp1_ellip = cdf["ELLIP"][...]
    rec_bp1_sx_rea = cdf["SX_REA"][...]
    rec_bp1_sx_arg = cdf["SX_ARG"][...]
    rec_bp1_vphi_rea = cdf["VPHI_REA"][...]
    rec_bp1_vphi_arg = cdf["VPHI_ARG"][...]    
    if params:        
        rec_bp1_R0 = cdf["R0"][...]
        rec_bp1_R1 = cdf["R1"][...]
        rec_bp1_R2 = cdf["R2"][...]
        rec_bp1_SP0 = cdf["SP0"][...]
        rec_bp1_SP1 = cdf["SP1"][...]
        rec_bp1_BW = cdf["BW"][...]    
    if params_bias:                       
        rec_bp1_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]       
    # Normal Mode data    
    for F in [0,1,2]:
        # frequency table
        nfreq_n = [11, 13, 12][F]
        df_n = [8*96., 8*16., 8*1.][F]
        fstart_n = [1968, 152, 10.5][F]        
        bp1_n_freq[F] = np.arange(fstart_n, fstart_n+nfreq_n*df_n, df_n) 
        # data
        bp1_n_index = np.logical_and(rec_bp1_F == F, rec_bp1_NM == 0)
        bp1_n_pb[F] = rec_bp1_pb[bp1_n_index, 0:nfreq_n]
        bp1_n_pe[F] = rec_bp1_pe[bp1_n_index, 0:nfreq_n] 
        bp1_n_dop[F] = rec_bp1_dop[bp1_n_index, 0:nfreq_n] 
        bp1_n_nvec0[F] = rec_bp1_nvec0[bp1_n_index, 0:nfreq_n] 
        bp1_n_nvec1[F] = rec_bp1_nvec1[bp1_n_index, 0:nfreq_n]
        bp1_n_nvec2[F] = rec_bp1_nvec2[bp1_n_index, 0:nfreq_n] 
        bp1_n_ellip[F] = rec_bp1_ellip[bp1_n_index, 0:nfreq_n] 
        bp1_n_sx_rea[F] = rec_bp1_sx_rea[bp1_n_index, 0:nfreq_n]
        bp1_n_sx_arg[F] = rec_bp1_sx_arg[bp1_n_index, 0:nfreq_n] 
        bp1_n_vphi_rea[F] = rec_bp1_vphi_rea[bp1_n_index, 0:nfreq_n] 
        bp1_n_vphi_arg[F] = rec_bp1_vphi_arg[bp1_n_index, 0:nfreq_n]                                   
        # time of the first sample of the bp1 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp1_n_time_acqui[F] = rec_bp1_T_acqui[bp1_n_index, 0] + rec_bp1_T_acqui[bp1_n_index, 1]*float(2)**(-16)                                         
        # epoch time of the first sample of the bp1 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp1_n_time_epoch[F] = rec_bp1_T_epoch[bp1_n_index]                  
        if params:
            if params_old:
                irec_range = range(len(rec_bp1_F))
                bp1_n_params[F] = [(int(rec_bp1_BW[i]), (int(rec_bp1_SP0[i]), int(rec_bp1_SP1[i])), \
                                   (int(rec_bp1_R0[i]), int(rec_bp1_R1[i]), int(rec_bp1_R2[i])))    \
                                   for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0)]                                                                  
            else:
                bp1_n_params[F] = [ rec_bp1_BW[bp1_n_index], (rec_bp1_SP0[bp1_n_index], rec_bp1_SP1[bp1_n_index]), 
                                   (rec_bp1_R0[bp1_n_index], rec_bp1_R1[bp1_n_index], rec_bp1_R2[bp1_n_index]) ]
        if params_bias:                       
            bp1_n_params_bias[F] = [ rec_bp1_bias_mux[bp1_n_index] ]                              
        if echo:
            print()
            print('bp1_n_time_acqui F%s:'%(F), bp1_n_time_acqui[F].shape)
            print('bp1_n_time_epoch F%s:'%(F), bp1_n_time_epoch[F].shape)      
            print('bp1_n_pb         F%s:'%(F), bp1_n_pb[F].shape)          
            print('bp1_n_pe         F%s:'%(F), bp1_n_pe[F].shape)                                       
            print('bp1_n_dop        F%s:'%(F), bp1_n_dop[F].shape)                                       
            print('bp1_n_nvec0      F%s:'%(F), bp1_n_nvec0[F].shape)                                       
            print('bp1_n_nvec1      F%s:'%(F), bp1_n_nvec1[F].shape)                                       
            print('bp1_n_nvec2      F%s:'%(F), bp1_n_nvec2[F].shape)   
            print('bp1_n_ellip      F%s:'%(F), bp1_n_ellip[F].shape)                                       
            print('bp1_n_sx_rea     F%s:'%(F), bp1_n_sx_rea[F].shape)                                       
            print('bp1_n_sx_arg     F%s:'%(F), bp1_n_sx_arg[F].shape)     
            print('bp1_n_vphi_rea   F%s:'%(F), bp1_n_vphi_rea[F].shape)                                       
            print('bp1_n_vphi_arg   F%s:'%(F), bp1_n_vphi_arg[F].shape)                       
            print('bp1_n_freq       F%s:'%(F), bp1_n_freq[F].shape)  
    # Burst Mode data               
    for F in [0,1]:
        # frequency table
        nfreq_b = [22, 26][F]
        df_b = [4*96., 4*16., 4*1.][F]
        fstart_b = [1776, 120.][F]        
        bp1_b_freq[F] = np.arange(fstart_b, fstart_b+nfreq_b*df_b, df_b) 
        # data
        bp1_b_index = np.logical_and(rec_bp1_F == F, rec_bp1_NM == 1)
        bp1_b_pb[F] = rec_bp1_pb[bp1_b_index, 0:nfreq_b]
        bp1_b_pe[F] = rec_bp1_pe[bp1_b_index, 0:nfreq_b] 
        bp1_b_dop[F] = rec_bp1_dop[bp1_b_index, 0:nfreq_b] 
        bp1_b_nvec0[F] = rec_bp1_nvec0[bp1_b_index, 0:nfreq_b] 
        bp1_b_nvec1[F] = rec_bp1_nvec1[bp1_b_index, 0:nfreq_b]
        bp1_b_nvec2[F] = rec_bp1_nvec2[bp1_b_index, 0:nfreq_b] 
        bp1_b_ellip[F] = rec_bp1_ellip[bp1_b_index, 0:nfreq_b] 
        bp1_b_sx_rea[F] = rec_bp1_sx_rea[bp1_b_index, 0:nfreq_b]
        bp1_b_sx_arg[F] = rec_bp1_sx_arg[bp1_b_index, 0:nfreq_b] 
        bp1_b_vphi_rea[F] = rec_bp1_vphi_rea[bp1_b_index, 0:nfreq_b] 
        bp1_b_vphi_arg[F] = rec_bp1_vphi_arg[bp1_b_index, 0:nfreq_b]                                   
        # time of the first sample of the bp1 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp1_b_time_acqui[F] = rec_bp1_T_acqui[bp1_b_index, 0] + rec_bp1_T_acqui[bp1_b_index, 1]*float(2)**(-16)                                         
        # epoch time of the first sample of the bp1 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp1_b_time_epoch[F] = rec_bp1_T_epoch[bp1_b_index]            
        if params:
            if params_old:
                irec_range = range(len(rec_bp1_F))
                bp1_b_params[F] = [(int(rec_bp1_BW[i]), (int(rec_bp1_SP0[i]), int(rec_bp1_SP1[i])), \
                                   (int(rec_bp1_R0[i]), int(rec_bp1_R1[i]), int(rec_bp1_R2[i])))    \
                                   for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1)]                                                                  
            else:
                bp1_b_params[F] = [ rec_bp1_BW[bp1_b_index], (rec_bp1_SP0[bp1_b_index], rec_bp1_SP1[bp1_b_index]), 
                                   (rec_bp1_R0[bp1_b_index], rec_bp1_R1[bp1_b_index], rec_bp1_R2[bp1_b_index]) ]      
        if params_bias:                       
            bp1_b_params_bias[F] = [ rec_bp1_bias_mux[bp1_b_index] ]                                        
        if echo:
            print()
            print('bp1_b_time_acqui F%s:'%(F), bp1_b_time_acqui[F].shape)
            print('bp1_b_time_epoch F%s:'%(F), bp1_b_time_epoch[F].shape)      
            print('bp1_b_pb         F%s:'%(F), bp1_b_pb[F].shape)          
            print('bp1_b_pe         F%s:'%(F), bp1_b_pe[F].shape)                                       
            print('bp1_b_dop        F%s:'%(F), bp1_b_dop[F].shape)                                       
            print('bp1_b_nvec0      F%s:'%(F), bp1_b_nvec0[F].shape)                                       
            print('bp1_b_nvec1      F%s:'%(F), bp1_b_nvec1[F].shape)                                       
            print('bp1_b_nvec2      F%s:'%(F), bp1_b_nvec2[F].shape)   
            print('bp1_b_ellip      F%s:'%(F), bp1_b_ellip[F].shape)                                       
            print('bp1_b_sx_rea     F%s:'%(F), bp1_b_sx_rea[F].shape)                                       
            print('bp1_b_sx_arg     F%s:'%(F), bp1_b_sx_arg[F].shape)     
            print('bp1_b_vphi_rea   F%s:'%(F), bp1_b_vphi_rea[F].shape)                                       
            print('bp1_b_vphi_arg   F%s:'%(F), bp1_b_vphi_arg[F].shape)                       
            print('bp1_b_freq       F%s:'%(F), bp1_b_freq[F].shape)         
    cdf.close()
    return (bp1_n_pb, bp1_n_pe, bp1_n_dop, bp1_n_nvec0, bp1_n_nvec1, bp1_n_nvec2, bp1_n_ellip, 
            bp1_n_sx_rea, bp1_n_sx_arg, bp1_n_vphi_rea, bp1_n_vphi_arg,            
            bp1_n_time_acqui, bp1_n_time_epoch, bp1_n_freq, bp1_n_params, bp1_n_params_bias,
            bp1_b_pb, bp1_b_pe, bp1_b_dop, bp1_b_nvec0, bp1_b_nvec1, bp1_b_nvec2, bp1_b_ellip, 
            bp1_b_sx_rea, bp1_b_sx_arg, bp1_b_vphi_rea, bp1_b_vphi_arg,            
            bp1_b_time_acqui, bp1_b_time_epoch, bp1_b_freq, bp1_b_params, bp1_b_params_bias)            

def load_sbm_bp1_CDF_L1(file, echo=False, params=False, params_bias=False, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)        
    # SBM1 or SBM2 ?
    bname = os.path.basename(file)
    index = bname.find('sbm')
    if index == -1:
        print( 'Fichier {} n\'est pas un ficher SBM ... !'.format(file) )
        return
    else:
        sbm = int(bname[index+3])
        if sbm not in [1, 2]: 
            print( 'Fichier {} n\'est pas un ficher SBM1 ou SBM2 ?'.format(file) )
            return  
    if echo: print("SBM%d data file"%(sbm))                          
    bp1_s_freq = sbm*[None]                            
    bp1_s_pb = sbm*[None]
    bp1_s_pe = sbm*[None]
    bp1_s_dop = sbm*[None]
    bp1_s_nvec0 = sbm*[None]
    bp1_s_nvec1 = sbm*[None]
    bp1_s_nvec2 = sbm*[None]
    bp1_s_ellip = sbm*[None]
    bp1_s_sx_rea = sbm*[None]
    bp1_s_sx_arg = sbm*[None]
    bp1_s_vphi_rea = sbm*[None]
    bp1_s_vphi_arg = sbm*[None]
    bp1_s_time_epoch = sbm*[None]
    bp1_s_time_acqui = sbm*[None]
    bp1_s_params = sbm*[None]    
    bp1_s_params_bias = sbm*[None]    
    rec_bp1_T_acqui = cdf['ACQUISITION_TIME'][...]
    rec_bp1_T_epoch = cdf.raw_var("Epoch")[...]   
    rec_bp1_F = cdf['FREQ'][...] if sbm == 2 else np.zeros(len(rec_bp1_T_epoch), dtype=int)     
    rec_bp1_pb = cdf["PB"][...]
    rec_bp1_pe = cdf["PE"][...]
    rec_bp1_dop = cdf["DOP"][...]
    rec_bp1_nvec0 = cdf["NVEC_V0"][...]
    rec_bp1_nvec1 = cdf["NVEC_V1"][...]
    rec_bp1_nvec2 = cdf["NVEC_V2"][...]
    rec_bp1_ellip = cdf["ELLIP"][...]
    rec_bp1_sx_rea = cdf["SX_REA"][...]
    rec_bp1_sx_arg = cdf["SX_ARG"][...]
    rec_bp1_vphi_rea = cdf["VPHI_REA"][...]
    rec_bp1_vphi_arg = cdf["VPHI_ARG"][...]    
    if params:        
        rec_bp1_R0 = cdf["R0"][...]
        rec_bp1_R1 = cdf["R1"][...]
        rec_bp1_R2 = cdf["R2"][...]
        rec_bp1_SP0 = cdf["SP0"][...]
        rec_bp1_SP1 = cdf["SP1"][...]
        rec_bp1_BW = cdf["BW"][...]    
    if params_bias:                       
        rec_bp1_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]             
    # Selected Burst Mode 1 or 2 data  
    for F in range(sbm):
        # frequency table
        nfreq_s = [22, 26][F]
        df_s = [4*96., 4*16., 4*1.][F]
        fstart_s = [1776, 120.][F]        
        bp1_s_freq[F] = np.arange(fstart_s, fstart_s+nfreq_s*df_s, df_s) 
        # data
        bp1_s_index = (rec_bp1_F == F)
        bp1_s_pb[F] = rec_bp1_pb[bp1_s_index, 0:nfreq_s]
        bp1_s_pe[F] = rec_bp1_pe[bp1_s_index, 0:nfreq_s] 
        bp1_s_dop[F] = rec_bp1_dop[bp1_s_index, 0:nfreq_s] 
        bp1_s_nvec0[F] = rec_bp1_nvec0[bp1_s_index, 0:nfreq_s] 
        bp1_s_nvec1[F] = rec_bp1_nvec1[bp1_s_index, 0:nfreq_s]
        bp1_s_nvec2[F] = rec_bp1_nvec2[bp1_s_index, 0:nfreq_s] 
        bp1_s_ellip[F] = rec_bp1_ellip[bp1_s_index, 0:nfreq_s] 
        bp1_s_sx_rea[F] = rec_bp1_sx_rea[bp1_s_index, 0:nfreq_s]
        bp1_s_sx_arg[F] = rec_bp1_sx_arg[bp1_s_index, 0:nfreq_s] 
        bp1_s_vphi_rea[F] = rec_bp1_vphi_rea[bp1_s_index, 0:nfreq_s] 
        bp1_s_vphi_arg[F] = rec_bp1_vphi_arg[bp1_s_index, 0:nfreq_s]                                   
        # time of the first sample of the bp1 in unit of sec (coarse time) and 2^-16 sec (fine time)
        # conversion to UTC sec from (2000, 1, 1, 0, 0, 0)_UTC    
        bp1_s_time_acqui[F] = rec_bp1_T_acqui[bp1_s_index, 0] + rec_bp1_T_acqui[bp1_s_index, 1]*float(2)**(-16)                                         
        # epoch time of the first sample of the bp1 [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT]    
        bp1_s_time_epoch[F] = rec_bp1_T_epoch[bp1_s_index]            
        if params:            
            bp1_s_params[F] = [ rec_bp1_BW[bp1_s_index], (rec_bp1_SP0[bp1_s_index], rec_bp1_SP1[bp1_s_index]), 
                               (rec_bp1_R0[bp1_s_index], rec_bp1_R1[bp1_s_index], rec_bp1_R2[bp1_s_index]) ]      
        if params_bias:                       
            bp1_s_params_bias[F] = [ rec_bp1_bias_mux[bp1_s_index] ]                                        
        if echo:
            print()
            print('bp1_s_time_acqui F%s:'%(F), bp1_s_time_acqui[F].shape)
            print('bp1_s_time_epoch F%s:'%(F), bp1_s_time_epoch[F].shape)      
            print('bp1_s_pb         F%s:'%(F), bp1_s_pb[F].shape)          
            print('bp1_s_pe         F%s:'%(F), bp1_s_pe[F].shape)                                       
            print('bp1_s_dop        F%s:'%(F), bp1_s_dop[F].shape)                                       
            print('bp1_s_nvec0      F%s:'%(F), bp1_s_nvec0[F].shape)                                       
            print('bp1_s_nvec1      F%s:'%(F), bp1_s_nvec1[F].shape)                                       
            print('bp1_s_nvec2      F%s:'%(F), bp1_s_nvec2[F].shape)   
            print('bp1_s_ellip      F%s:'%(F), bp1_s_ellip[F].shape)                                       
            print('bp1_s_sx_rea     F%s:'%(F), bp1_s_sx_rea[F].shape)                                       
            print('bp1_s_sx_arg     F%s:'%(F), bp1_s_sx_arg[F].shape)     
            print('bp1_s_vphi_rea   F%s:'%(F), bp1_s_vphi_rea[F].shape)                                       
            print('bp1_s_vphi_arg   F%s:'%(F), bp1_s_vphi_arg[F].shape)                       
            print('bp1_s_freq       F%s:'%(F), bp1_s_freq[F].shape)         
    cdf.close()
    return (bp1_s_pb, bp1_s_pe, bp1_s_dop, bp1_s_nvec0, bp1_s_nvec1, bp1_s_nvec2, bp1_s_ellip, 
            bp1_s_sx_rea, bp1_s_sx_arg, bp1_s_vphi_rea, bp1_s_vphi_arg,            
            bp1_s_time_acqui, bp1_s_time_epoch, bp1_s_freq, bp1_s_params, bp1_s_params_bias)     
                            
def load_SCM_TF_mat_CDF(file, temp=None, lfr_on=True, echo=False, key_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file) 
    temperatures = cdf["SCM_TEMPERATURES"][:]
    comments = cdf["Comments"][:]
    tf_index = cdf["TF_INDEX"][:,:]  
    if temp == None:
        i = 0
    else:        
        i = np.argmin(np.abs(temperatures-temp)) 
    j = tf_index[0 if lfr_on else 1, i]    
    nLF = cdf["nLF-Freqs"][j]     
    if echo:
        print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)
        print('temp = %s'%temp)
        print("temp_index = %d, tf_index[%d, %d] = %d"%(i, 0 if lfr_on else 1, i, j))
        print(comments[j])                 
        print("nLF-Freqs[%d]:"%(j), nLF)            
    freqs = cdf["LF-Freqs"][j,0:nLF]        
    rB11_LF = linfromdB(cdf["rB11-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB11-LF-phases"][j,0:nLF])) 
    rB12_LF = linfromdB(cdf["rB12-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB12-LF-phases"][j,0:nLF])) 
    rB13_LF = linfromdB(cdf["rB13-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB13-LF-phases"][j,0:nLF])) 
    rB21_LF = linfromdB(cdf["rB21-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB21-LF-phases"][j,0:nLF])) 
    rB22_LF = linfromdB(cdf["rB22-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB22-LF-phases"][j,0:nLF])) 
    rB23_LF = linfromdB(cdf["rB23-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB23-LF-phases"][j,0:nLF]))
    rB31_LF = linfromdB(cdf["rB31-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB31-LF-phases"][j,0:nLF])) 
    rB32_LF = linfromdB(cdf["rB32-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB32-LF-phases"][j,0:nLF])) 
    rB33_LF = linfromdB(cdf["rB33-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["rB33-LF-phases"][j,0:nLF]))            
    B11_LF = linfromdB(cdf["B11-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B11-LF-phases"][j,0:nLF])) 
    B12_LF = linfromdB(cdf["B12-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B12-LF-phases"][j,0:nLF])) 
    B13_LF = linfromdB(cdf["B13-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B13-LF-phases"][j,0:nLF])) 
    B21_LF = linfromdB(cdf["B21-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B21-LF-phases"][j,0:nLF])) 
    B22_LF = linfromdB(cdf["B22-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B22-LF-phases"][j,0:nLF])) 
    B23_LF = linfromdB(cdf["B23-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B23-LF-phases"][j,0:nLF]))
    B31_LF = linfromdB(cdf["B31-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B31-LF-phases"][j,0:nLF])) 
    B32_LF = linfromdB(cdf["B32-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B32-LF-phases"][j,0:nLF])) 
    B33_LF = linfromdB(cdf["B33-LF-gains"][j,0:nLF]) * np.exp(1j*np.deg2rad(cdf["B33-LF-phases"][j,0:nLF]))    
    mat_V_to_nT = np.transpose( np.array([[rB11_LF, rB12_LF, rB13_LF], 
                                          [rB21_LF, rB22_LF, rB23_LF], 
                                          [rB31_LF, rB32_LF, rB33_LF]]), (2, 0, 1) )    
    mat_nT_to_V = np.transpose( np.array([[B11_LF, B12_LF, B13_LF], 
                                          [B21_LF, B22_LF, B23_LF], 
                                          [B31_LF, B32_LF, B33_LF]]), (2, 0, 1) )
    cdf.close()
    return freqs, mat_V_to_nT, mat_nT_to_V

def load_SCM_TF_comp_CDF(file, temp=None, lfr_on=True, echo=False, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)        
    temperatures = cdf["SCM_TEMPERATURES"][:]
    comments = cdf["Comments"][:]
    tf_index = cdf["TF_INDEX"][:,:]  
    if temp == None:
        i = 0
    else:        
        i = np.argmin(np.abs(temperatures-temp)) 
    j = tf_index[0 if lfr_on else 1, i]    
    nLF = cdf["nLF-Freqs"][j]     
    if echo:
        print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)
        print('temp = %s'%temp)
        print("temp_index = %d, tf_index[%d, %d] = %d"%(i, 0 if lfr_on else 1, i, j))
        print(comments[j])                 
        print("nLF-Freqs[%d]:"%(j), nLF)                     
    TF_components = {"LF-Freqs": cdf["LF-Freqs"][j,0:nLF],
            "rB11-LF-gains": cdf["rB11-LF-gains"][j,0:nLF],
            "rB12-LF-gains": cdf["rB12-LF-gains"][j,0:nLF],
            "rB13-LF-gains": cdf["rB13-LF-gains"][j,0:nLF],
            "rB21-LF-gains": cdf["rB21-LF-gains"][j,0:nLF],
            "rB22-LF-gains": cdf["rB22-LF-gains"][j,0:nLF],
            "rB23-LF-gains": cdf["rB23-LF-gains"][j,0:nLF],
            "rB31-LF-gains": cdf["rB31-LF-gains"][j,0:nLF],
            "rB32-LF-gains": cdf["rB32-LF-gains"][j,0:nLF],
            "rB33-LF-gains": cdf["rB33-LF-gains"][j,0:nLF],
            "rB11-LF-phases": cdf["rB11-LF-phases"][j,0:nLF],
            "rB12-LF-phases": cdf["rB12-LF-phases"][j,0:nLF],
            "rB13-LF-phases": cdf["rB13-LF-phases"][j,0:nLF],
            "rB21-LF-phases": cdf["rB21-LF-phases"][j,0:nLF],
            "rB22-LF-phases": cdf["rB22-LF-phases"][j,0:nLF],
            "rB23-LF-phases": cdf["rB23-LF-phases"][j,0:nLF],
            "rB31-LF-phases": cdf["rB31-LF-phases"][j,0:nLF],
            "rB32-LF-phases": cdf["rB32-LF-phases"][j,0:nLF],
            "rB33-LF-phases": cdf["rB33-LF-phases"][j,0:nLF],
            "B11-LF-gains": cdf["B11-LF-gains"][j,0:nLF],
            "B12-LF-gains": cdf["B12-LF-gains"][j,0:nLF],
            "B13-LF-gains": cdf["B13-LF-gains"][j,0:nLF],
            "B21-LF-gains": cdf["B21-LF-gains"][j,0:nLF],
            "B22-LF-gains": cdf["B22-LF-gains"][j,0:nLF],
            "B23-LF-gains": cdf["B23-LF-gains"][j,0:nLF],
            "B31-LF-gains": cdf["B31-LF-gains"][j,0:nLF],
            "B32-LF-gains": cdf["B32-LF-gains"][j,0:nLF],
            "B33-LF-gains": cdf["B33-LF-gains"][j,0:nLF],
            "B11-LF-phases": cdf["B11-LF-phases"][j,0:nLF],
            "B12-LF-phases": cdf["B12-LF-phases"][j,0:nLF],
            "B13-LF-phases": cdf["B13-LF-phases"][j,0:nLF],
            "B21-LF-phases": cdf["B21-LF-phases"][j,0:nLF],
            "B22-LF-phases": cdf["B22-LF-phases"][j,0:nLF],
            "B23-LF-phases": cdf["B23-LF-phases"][j,0:nLF],
            "B31-LF-phases": cdf["B31-LF-phases"][j,0:nLF],
            "B32-LF-phases": cdf["B32-LF-phases"][j,0:nLF],
            "B33-LF-phases": cdf["B33-LF-phases"][j,0:nLF]}
    cdf.close()
    return TF_components    

def load_asm_CDF_L2(file, echo=False, params=False, params_old=False, params_bias=False, 
                    key_attrs=None, key_field=None,  key_field_attrs=None, SRF=False):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)       
    asm_data = 3*[None]
    asm_time_epoch = 3*[None]
    #asm_time_acqui = 3*[None]    
    asm_freq = 3*[None]                
    asm_params = 3*[None]
    asm_params_bias = 3*[None]
    if params or params_bias:
        # sampling frequency F=0,1,2, and number of records
        rec_asm_F = cdf['FREQ'][...]
    if params:        
        rec_asm_R0 = cdf["R0"][...]
        rec_asm_R1 = cdf["R1"][...]
        rec_asm_R2 = cdf["R2"][...]
        rec_asm_SP0 = cdf["SP0"][...]
        rec_asm_SP1 = cdf["SP1"][...]
        rec_asm_BW = cdf["BW"][...]
        if params_bias:                       
            rec_asm_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]  
    # load data expressed in SRF or in SCM B1-B2-B3 axis system frame      
    ref = "_SRF" if SRF else ''           
    # Normal Mode data      
    for F in [0,1,2]:
        asm_freq[F] = cdf["F%s"%(F)][:]        
        asm_data[F] = cdf["ASM_RE_F%s"%(F)+ref][...] + 1j*cdf["ASM_IM_F%s"%(F)+ref][...]
        asm_time_epoch[F] = cdf.raw_var("Epoch_F%s"%(F))[:]
        #asm_time_acqui[F] = cdf["ACQUISITION_TIME_F%s"%(F)][:,0] + cdf["ACQUISITION_TIME_F%s"%(F)][:,1]*float(2)**(-16)  
        if (params and not params_old) or params_bias:
            asm_index = (rec_asm_F == F)  
        if params:
            if params_old:
                irec_range = range(len(rec_asm_F))
                asm_params[F] = [(int(rec_asm_BW[i]), (int(rec_asm_SP0[i]), int(rec_asm_SP1[i])), \
                                 (int(rec_asm_R0[i]), int(rec_asm_R1[i]), int(rec_asm_R2[i])))    \
                                  for i in irec_range if rec_asm_F[i] == F]                                 
            else:
                asm_params[F] = [ rec_asm_BW[asm_index], (rec_asm_SP0[asm_index], rec_asm_SP1[asm_index]), 
                                 (rec_asm_R0[asm_index], rec_asm_R1[asm_index], rec_asm_R2[asm_index])  ]    
        if params_bias:                       
            asm_params_bias[F] = [ rec_asm_bias_mux[asm_index] ]                                 
        if echo:
            print()
            #print('asm_time_acqui F%s:'%(F), asm_time_acqui[F].shape)
            print('data are expressed in ' + ('SRF' if SRF else 'SCM B1-B2-B3 axis system frame'))
            print('asm_time_epoch F%s:'%(F), asm_time_epoch[F].shape)      
            print('asm_data       F%s:'%(F), asm_data[F].shape)          
            print('asm_freq       F%s:'%(F), asm_freq[F].shape)       
    cdf.close()
    return (asm_data, asm_time_epoch, asm_freq, asm_params, asm_params_bias)        

def load_bp2_CDF_L2(file, echo=False, params=False, params_old=False, params_bias=False, 
                    key_attrs=None, key_field=None,  key_field_attrs=None, SRF=False):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                     
    bp2_n_freq = 3*[None]                            
    bp2_n_data = 3*[None]
    bp2_n_time_epoch = 3*[None]
    #bp2_n_time_acqui = 3*[None]
    bp2_n_params = 3*[None] 
    bp2_n_params_bias = 3*[None]       
    bp2_b_freq = 2*[None]                                
    bp2_b_data = 2*[None]
    bp2_b_time_epoch = 2*[None]
    #bp2_b_time_acqui = 2*[None]
    bp2_b_params = 2*[None]
    bp2_b_params_bias = 2*[None]   
    if params or params_bias:
        # sampling frequency F=0,1,2, normal (0) or burst (1) mode, and number of records
        rec_bp2_F = cdf['FREQ'][...]
        rec_bp2_NM = cdf['SURVEY_MODE'][...]    
    if params:        
        rec_bp2_R0 = cdf["R0"][...]
        rec_bp2_R1 = cdf["R1"][...]
        rec_bp2_R2 = cdf["R2"][...]
        rec_bp2_SP0 = cdf["SP0"][...]
        rec_bp2_SP1 = cdf["SP1"][...]
        rec_bp2_BW = cdf["BW"][...]
    if params_bias:                       
        rec_bp2_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]      
    # load data expressed in SRF or in SCM B1-B2-B3 axis system frame      
    ref = "_SRF" if SRF else ''         
    # Normal Mode data      
    for F in [0,1,2]:
        bp2_n_freq[F] = cdf["N_F%s"%(F)][:]
        bp2_n_data[F] = cdf["BP2_RE_N_F%s"%(F)+ref][...] + 1j*cdf["BP2_IM_N_F%s"%(F)+ref][...]
        bp2_n_time_epoch[F] = cdf.raw_var("Epoch_N_F%s"%(F))[:]  
        #bp2_n_time_acqui[F] = cdf["ACQUISITION_TIME_N_F%s"%(F)][:,0] + cdf["ACQUISITION_TIME_N_F%s"%(F)][:,1]*float(2)**(-16)           
        if (params and not params_old) or params_bias:
            bp2_n_index = np.logical_and(rec_bp2_F == F, rec_bp2_NM == 0)
        if params:
            if params_old:
                irec_range = range(len(rec_bp2_F))
                bp2_n_params[F] = [(int(rec_bp2_BW[i]), (int(rec_bp2_SP0[i]), int(rec_bp2_SP1[i])), \
                                   (int(rec_bp2_R0[i]), int(rec_bp2_R1[i]), int(rec_bp2_R2[i])))    \
                                   for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 0)]                                                                  
            else:

                bp2_n_params[F] = [ rec_bp2_BW[bp2_n_index], (rec_bp2_SP0[bp2_n_index], rec_bp2_SP1[bp2_n_index]), 
                                   (rec_bp2_R0[bp2_n_index], rec_bp2_R1[bp2_n_index], rec_bp2_R2[bp2_n_index]) ]
        if params_bias:                       
            bp2_n_params_bias[F] = [ rec_bp2_bias_mux[bp2_n_index] ]                                 
        if echo:
            print()
            #print('bp2_n_time_acqui F%s:'%(F), bp2_n_time_acqui[F].shape)
            print('data are expressed in ' + ('SRF' if SRF else 'SCM B1-B2-B3 axis system frame'))
            print('bp2_n_time_epoch F%s:'%(F), bp2_n_time_epoch[F].shape)      
            print('bp2_n_data       F%s:'%(F), bp2_n_data[F].shape)          
            print('bp2_n_freq       F%s:'%(F), bp2_n_freq[F].shape)   
    # Burst Mode data               
    for F in [0,1]:
        bp2_b_freq[F] = cdf["B_F%s"%(F)][:]        
        bp2_b_data[F] = cdf["BP2_RE_B_F%s"%(F)+ref][...] + 1j*cdf["BP2_IM_B_F%s"%(F)+ref][...]
        bp2_b_time_epoch[F] = cdf.raw_var("Epoch_B_F%s"%(F))[:]    
        #bp2_b_time_acqui[F] = cdf["ACQUISITION_TIME_B_F%s"%(F)][:,0] + cdf["ACQUISITION_TIME_B_F%s"%(F)][:,1]*float(2)**(-16)           
        if (params and not params_old) or params_bias:
            bp2_b_index = np.logical_and(rec_bp2_F == F, rec_bp2_NM == 1)
        if params:
            if params_old:
                irec_range = range(len(rec_bp2_F))
                bp2_b_params[F] = [(int(rec_bp2_BW[i]), (int(rec_bp2_SP0[i]), int(rec_bp2_SP1[i])), \
                                   (int(rec_bp2_R0[i]), int(rec_bp2_R1[i]), int(rec_bp2_R2[i])))    \
                                   for i in irec_range if (rec_bp2_F[i] == F) and (rec_bp2_NM[i] == 1)]                                                                  
            else:

                bp2_b_params[F] = [ rec_bp2_BW[bp2_b_index], (rec_bp2_SP0[bp2_b_index], rec_bp2_SP1[bp2_b_index]), 
                                   (rec_bp2_R0[bp2_b_index], rec_bp2_R1[bp2_b_index], rec_bp2_R2[bp2_b_index]) ]
        if params_bias:                       
            bp2_b_params_bias[F] = [ rec_bp2_bias_mux[bp2_b_index] ]
        if echo:
            print()
            #print('bp2_b_time_acqui F%s:'%(F), bp2_b_time_acqui[F].shape)
            print('data are expressed in ' + ('SRF' if SRF else 'SCM B1-B2-B3 axis system frame'))            
            print('bp2_b_time_epoch F%s:'%(F), bp2_b_time_epoch[F].shape)      
            print('bp2_b_data       F%s:'%(F), bp2_b_data[F].shape)          
            print('bp2_b_freq       F%s:'%(F), bp2_b_freq[F].shape)         
    cdf.close()
    return (bp2_n_data, bp2_n_time_epoch, bp2_n_freq, bp2_n_params, bp2_n_params_bias,
            bp2_b_data, bp2_b_time_epoch, bp2_b_freq, bp2_b_params, bp2_b_params_bias)  
            
def load_sbm_bp2_CDF_L2(file, echo=False, params=False, params_bias=False, key_attrs=None, key_field=None,  key_field_attrs=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)  
    # SBM1 or SBM2 ?
    bname = os.path.basename(file)
    index = bname.find('sbm')
    if index == -1:
        print( 'Fichier {} n\'est pas un ficher SBM ... !'.format(file) )
        return
    else:
        sbm = int(bname[index+3])
        if sbm not in [1, 2]: 
            print( 'Fichier {} n\'est pas un ficher SBM1 ou SBM2 ?'.format(file) )
            return  
    if echo: print("SBM%d data file"%(sbm))            
    bp2_s_freq = sbm*[None]                                
    bp2_s_data = sbm*[None]
    bp2_s_time_epoch = sbm*[None]
    bp2_s_params = sbm*[None]
    bp2_s_params_bias = sbm*[None]       
    if params:        
        rec_bp2_R0 = cdf["R0"][...]
        rec_bp2_R1 = cdf["R1"][...]
        rec_bp2_R2 = cdf["R2"][...]
        rec_bp2_SP0 = cdf["SP0"][...]
        rec_bp2_SP1 = cdf["SP1"][...]
        rec_bp2_BW = cdf["BW"][...]
    if params_bias:                       
        rec_bp2_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]              
    # Selected Burst Mode 1 or 2 data  
    for F in range(sbm):
        bp2_s_freq[F] = cdf["F%s"%(F)][:]        
        bp2_s_data[F] = cdf["BP2_RE_F%s"%(F)][...] + 1j*cdf["BP2_IM_F%s"%(F)][...]
        bp2_s_time_epoch[F] = cdf.raw_var("Epoch_F%s"%(F))[:] if sbm == 2 else cdf.raw_var("Epoch")[:]           
        if params or params_bias:
            bp2_s_index = (cdf['FREQ'][...] == F) if sbm == 2 else ...
        if params:            
            bp2_s_params[F] = [ rec_bp2_BW[bp2_s_index], (rec_bp2_SP0[bp2_s_index], rec_bp2_SP1[bp2_s_index]), 
                               (rec_bp2_R0[bp2_s_index], rec_bp2_R1[bp2_s_index], rec_bp2_R2[bp2_s_index]) ]
        if params_bias:                       
            bp2_s_params_bias[F] = [ rec_bp2_bias_mux[bp2_s_index] ]
        if echo:
            print()
            print('bp2_s_time_epoch F%s:'%(F), bp2_s_time_epoch[F].shape)      
            print('bp2_s_data       F%s:'%(F), bp2_s_data[F].shape)          
            print('bp2_s_freq       F%s:'%(F), bp2_s_freq[F].shape)         
    cdf.close()
    return (bp2_s_data, bp2_s_time_epoch, bp2_s_freq, bp2_s_params, bp2_s_params_bias)                      

def load_bp1_CDF_L2(file, echo=False, params=False, params_old=False, params_bias=False, key_attrs=None, 
                    key_field=None, key_field_attrs=None, fill_value=None, SRF=False):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                    
    bp1_n_freq = 3*[None]                            
    bp1_n_pb = 3*[None]
    bp1_n_pe = 3*[None]
    bp1_n_dop = 3*[None]
    bp1_n_nvec = 3*[None]
    bp1_n_ellip = 3*[None]
    bp1_n_sx_rea = 3*[None]
    bp1_n_sx_arg = 3*[None]
    bp1_n_vphi_rea = 3*[None]
    bp1_n_vphi_arg = 3*[None]
    bp1_n_time_epoch = 3*[None]
    #bp1_n_time_acqui = 3*[None]
    bp1_n_params = 3*[None]    
    bp1_n_params_bias = 3*[None]
    bp1_b_freq = 2*[None]                            
    bp1_b_pb = 2*[None]
    bp1_b_pe = 2*[None]
    bp1_b_dop = 2*[None]
    bp1_b_nvec = 2*[None]
    bp1_b_ellip = 2*[None]
    bp1_b_sx_rea = 2*[None]
    bp1_b_sx_arg = 2*[None]
    bp1_b_vphi_rea = 2*[None]
    bp1_b_vphi_arg = 2*[None]
    bp1_b_time_epoch = 2*[None]
    #bp1_b_time_acqui = 2*[None]
    bp1_b_params = 2*[None] 
    bp1_b_params_bias = 3*[None]
    if params or params_bias:
        # sampling frequency F=0,1,2, normal (0) or burst (1) mode, and number of records
        rec_bp1_F = cdf['FREQ'][...]
        rec_bp1_NM = cdf['SURVEY_MODE'][...]   
    if params:               
        rec_bp1_R0 = cdf["R0"][...] 
        rec_bp1_R1 = cdf["R1"][...] 
        rec_bp1_R2 = cdf["R2"][...] 
        rec_bp1_SP0 = cdf["SP0"][...] 
        rec_bp1_SP1 = cdf["SP1"][...] 
        rec_bp1_BW = cdf["BW"][...] 
    if params_bias:                       
        rec_bp1_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]        
    # load data expressed in SRF or in SCM B1-B2-B3 axis system frame      
    ref = "_SRF" if SRF else ''                       
    # Normal Mode data       
    for F in [0,1,2]:                
        bp1_n_freq[F] = cdf["N_F%s"%(F)][...]
        bp1_n_pb[F] = cdf["PB_N_F%s"%(F)][...]
        bp1_n_pe[F] = cdf["PE_N_F%s"%(F)][...]
        bp1_n_dop[F] = cdf["DOP_N_F%s"%(F)][...]
        bp1_n_nvec[F] = cdf["NVEC_N_F%s"%(F)+ref][...]        
        bp1_n_ellip[F] = cdf["ELLIP_N_F%s"%(F)][...]
        bp1_n_sx_rea[F] = cdf["SX_REA_N_F%s"%(F)][...]
        bp1_n_sx_arg[F] = cdf["SX_ARG_N_F%s"%(F)][...]
        bp1_n_vphi_rea[F] = cdf["VPHI_REA_N_F%s"%(F)][...]
        bp1_n_vphi_arg[F] = cdf["VPHI_ARG_N_F%s"%(F)][...]                        
        bp1_n_time_epoch[F] = cdf.raw_var("Epoch_N_F%s"%(F))[...]  
        #bp1_n_time_acqui[F] = cdf["ACQUISITION_TIME_N_F%s"%(F)][:,0] + cdf["ACQUISITION_TIME_N_F%s"%(F)][:,1]*float(2)**(-16)  
        if (params and not params_old) or params_bias:
            bp1_n_index = np.logical_and(rec_bp1_F == F, rec_bp1_NM == 0)
        if params:
            if params_old:
                irec_range = range(len(rec_bp1_F))
                bp1_n_params[F] = [(int(rec_bp1_BW[i]), (int(rec_bp1_SP0[i]), int(rec_bp1_SP1[i])), \
                                   (int(rec_bp1_R0[i]), int(rec_bp1_R1[i]), int(rec_bp1_R2[i])))    \
                                   for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 0)]                                                                  
            else:

                bp1_n_params[F] = [ rec_bp1_BW[bp1_n_index], (rec_bp1_SP0[bp1_n_index], rec_bp1_SP1[bp1_n_index]), 
                                   (rec_bp1_R0[bp1_n_index], rec_bp1_R1[bp1_n_index], rec_bp1_R2[bp1_n_index]) ]
        if params_bias:                       
            bp1_n_params_bias[F] = [ rec_bp1_bias_mux[bp1_n_index] ]                             
        if echo:
            print()
            #print('bp1_n_time_acqui F%s:'%(F), bp1_n_time_acqui[F].shape)
            print('bp1_n_time_epoch F%s:'%(F), bp1_n_time_epoch[F].shape)      
            print('bp1_n_pb         F%s:'%(F), bp1_n_pb[F].shape)          
            print('bp1_n_pe         F%s:'%(F), bp1_n_pe[F].shape)          
            print('bp1_n_dop        F%s:'%(F), bp1_n_dop[F].shape)          
            print('bp1_n_nvec       F%s:'%(F), bp1_n_nvec[F].shape)   
            print('bp1_n_ellip      F%s:'%(F), bp1_n_ellip[F].shape)          
            print('bp1_n_sx_rea     F%s:'%(F), bp1_n_sx_rea[F].shape)          
            print('bp1_n_sx_arg     F%s:'%(F), bp1_n_sx_arg[F].shape)  
            print('bp1_n_vphi_rea   F%s:'%(F), bp1_n_vphi_rea[F].shape)          
            print('bp1_n_vphi_arg   F%s:'%(F), bp1_n_vphi_arg[F].shape)        
            print('bp1_n_freq       F%s:'%(F), bp1_n_freq[F].shape)  
            print('bp1_n_nvec is expressed in ' + ('SRF' if SRF else 'SCM B1-B2-B3 axis system frame'))                   
        if fill_value != None:
             for i in ([0, 1, 2] if SRF else [2]):
                bp1_n_nvec[F][...,i][bp1_n_nvec[F][...,i] < fill_value] = np.nan                                           
    # Burst Mode data                
    for F in [0,1]:
        bp1_b_freq[F] = cdf["B_F%s"%(F)][:]
        bp1_b_pb[F] = cdf["PB_B_F%s"%(F)][...]
        bp1_b_pe[F] = cdf["PE_B_F%s"%(F)][...]
        bp1_b_dop[F] = cdf["DOP_B_F%s"%(F)][...]
        bp1_b_nvec[F] = cdf["NVEC_B_F%s"%(F)+ref][...]
        bp1_b_ellip[F] = cdf["ELLIP_B_F%s"%(F)][...]
        bp1_b_sx_rea[F] = cdf["SX_REA_B_F%s"%(F)][...]
        bp1_b_sx_arg[F] = cdf["SX_ARG_B_F%s"%(F)][...]
        bp1_b_vphi_rea[F] = cdf["VPHI_REA_B_F%s"%(F)][...]
        bp1_b_vphi_arg[F] = cdf["VPHI_ARG_B_F%s"%(F)][...]                        
        bp1_b_time_epoch[F] = cdf.raw_var("Epoch_B_F%s"%(F))[:]  
        #bp1_b_time_acqui[F] = cdf["ACQUISITION_TIME_B_F%s"%(F)][:,0] + cdf["ACQUISITION_TIME_B_F%s"%(F)][:,1]*float(2)**(-16)           
        if (params and not params_old) or params_bias:
            bp1_b_index = np.logical_and(rec_bp1_F == F, rec_bp1_NM == 1)
        if params:
            if params_old:
                irec_range = range(len(rec_bp1_F))
                bp1_b_params[F] = [(int(rec_bp1_BW[i]), (int(rec_bp1_SP0[i]), int(rec_bp1_SP1[i])), \
                                   (int(rec_bp1_R0[i]), int(rec_bp1_R1[i]), int(rec_bp1_R2[i])))    \
                                   for i in irec_range if (rec_bp1_F[i] == F) and (rec_bp1_NM[i] == 1)]                                                                  
            else:

                bp1_b_params[F] = [ rec_bp1_BW[bp1_b_index], (rec_bp1_SP0[bp1_b_index], rec_bp1_SP1[bp1_b_index]), 
                                   (rec_bp1_R0[bp1_b_index], rec_bp1_R1[bp1_b_index], rec_bp1_R2[bp1_b_index]) ]
        if params_bias:                       
            bp1_b_params_bias[F] = [ rec_bp1_bias_mux[bp1_b_index] ]                             
        if echo:
            print()
            #print('bp1_b_time_acqui F%s:'%(F), bp1_b_time_acqui[F].shape)
            print('bp1_b_time_epoch F%s:'%(F), bp1_b_time_epoch[F].shape)      
            print('bp1_b_pb         F%s:'%(F), bp1_b_pb[F].shape)          
            print('bp1_b_pe         F%s:'%(F), bp1_b_pe[F].shape)          
            print('bp1_b_dop        F%s:'%(F), bp1_b_dop[F].shape)          
            print('bp1_b_nvec       F%s:'%(F), bp1_b_nvec[F].shape)          
            print('bp1_b_ellip      F%s:'%(F), bp1_b_ellip[F].shape)          
            print('bp1_b_sx_rea     F%s:'%(F), bp1_b_sx_rea[F].shape)          
            print('bp1_b_sx_arg     F%s:'%(F), bp1_b_sx_arg[F].shape)  
            print('bp1_b_vphi_rea   F%s:'%(F), bp1_b_vphi_rea[F].shape)          
            print('bp1_b_vphi_arg   F%s:'%(F), bp1_b_vphi_arg[F].shape)        
            print('bp1_b_freq       F%s:'%(F), bp1_b_freq[F].shape) 
            print('bp1_b_nvec is expressed in ' + ('SRF' if SRF else 'SCM B1-B2-B3 axis system frame'))                               
        if fill_value != None:
            for i in ([0, 1, 2] if SRF else [2]):
                bp1_b_nvec[F][...,i][bp1_b_nvec[F][...,i] < fill_value] = np.nan                  
    cdf.close()
    return (bp1_n_pb, bp1_n_pe, bp1_n_dop, bp1_n_nvec, bp1_n_ellip, 
            bp1_n_sx_rea, bp1_n_sx_arg, bp1_n_vphi_rea, bp1_n_vphi_arg,            
            bp1_n_time_epoch, bp1_n_freq, bp1_n_params, bp1_n_params_bias,
            bp1_b_pb, bp1_b_pe, bp1_b_dop, bp1_b_nvec, bp1_b_ellip, 
            bp1_b_sx_rea, bp1_b_sx_arg, bp1_b_vphi_rea, bp1_b_vphi_arg,            
            bp1_b_time_epoch, bp1_b_freq, bp1_b_params, bp1_b_params_bias) 
            
def load_sbm_bp1_CDF_L2(file, echo=False, params=False, params_bias=False, key_attrs=None, key_field=None, key_field_attrs=None, fill_value=None):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)
    # SBM1 or SBM2 ?
    bname = os.path.basename(file)
    index = bname.find('sbm')
    if index == -1:
        print( 'Fichier {} n\'est pas un ficher SBM ... !'.format(file) )
        return
    else:
        sbm = int(bname[index+3])
        if sbm not in [1, 2]: 
            print( 'Fichier {} n\'est pas un ficher SBM1 ou SBM2 ?'.format(file) )
            return  
    if echo: print("SBM%d data file"%(sbm))                             
    bp1_s_freq = sbm*[None]                            
    bp1_s_pb = sbm*[None]
    bp1_s_pe = sbm*[None]
    bp1_s_dop = sbm*[None]
    bp1_s_nvec = sbm*[None]
    bp1_s_ellip = sbm*[None]
    bp1_s_sx_rea = sbm*[None]
    bp1_s_sx_arg = sbm*[None]
    bp1_s_vphi_rea = sbm*[None]
    bp1_s_vphi_arg = sbm*[None]
    bp1_s_time_epoch = sbm*[None]
    bp1_s_params = sbm*[None] 
    bp1_s_params_bias = sbm*[None]    
    if params:               
        rec_bp1_R0 = cdf["R0"][...] 
        rec_bp1_R1 = cdf["R1"][...] 
        rec_bp1_R2 = cdf["R2"][...] 
        rec_bp1_SP0 = cdf["SP0"][...] 
        rec_bp1_SP1 = cdf["SP1"][...] 
        rec_bp1_BW = cdf["BW"][...] 
    if params_bias:                       
        rec_bp1_bias_mux = cdf["BIAS_MODE_MUX_SET"][...]                                   
    # Selected Burst Mode 1 or 2 data  
    for F in range(sbm):
        bp1_s_freq[F] = cdf["F%s"%(F)][:]
        bp1_s_pb[F] = cdf["PB_F%s"%(F)][...]
        bp1_s_pe[F] = cdf["PE_F%s"%(F)][...]
        bp1_s_dop[F] = cdf["DOP_F%s"%(F)][...]
        bp1_s_nvec[F] = cdf["NVEC_F%s"%(F)][...]
        bp1_s_ellip[F] = cdf["ELLIP_F%s"%(F)][...]
        bp1_s_sx_rea[F] = cdf["SX_REA_F%s"%(F)][...]
        bp1_s_sx_arg[F] = cdf["SX_ARG_F%s"%(F)][...]
        bp1_s_vphi_rea[F] = cdf["VPHI_REA_F%s"%(F)][...]
        bp1_s_vphi_arg[F] = cdf["VPHI_ARG_F%s"%(F)][...]                        
        bp1_s_time_epoch[F] = cdf.raw_var("Epoch_F%s"%(F))[:] if sbm == 2 else cdf.raw_var("Epoch")[:]     
        if params or params_bias:
            bp1_s_index = (cdf['FREQ'][...] == F) if sbm == 2 else ...
        if params:            
            bp1_s_params[F] = [ rec_bp1_BW[bp1_s_index], (rec_bp1_SP0[bp1_s_index], rec_bp1_SP1[bp1_s_index]), 
                               (rec_bp1_R0[bp1_s_index], rec_bp1_R1[bp1_s_index], rec_bp1_R2[bp1_s_index]) ]
        if params_bias:                       
            bp1_s_params_bias[F] = [ rec_bp1_bias_mux[bp1_s_index] ]                             
        if echo:
            print()
            print('bp1_s_time_epoch F%s:'%(F), bp1_s_time_epoch[F].shape)      
            print('bp1_s_pb         F%s:'%(F), bp1_s_pb[F].shape)          
            print('bp1_s_pe         F%s:'%(F), bp1_s_pe[F].shape)          
            print('bp1_s_dop        F%s:'%(F), bp1_s_dop[F].shape)          
            print('bp1_s_nvec       F%s:'%(F), bp1_s_nvec[F].shape)          
            print('bp1_s_ellip      F%s:'%(F), bp1_s_ellip[F].shape)          
            print('bp1_s_sx_rea     F%s:'%(F), bp1_s_sx_rea[F].shape)          
            print('bp1_s_sx_arg     F%s:'%(F), bp1_s_sx_arg[F].shape)  
            print('bp1_s_vphi_rea   F%s:'%(F), bp1_s_vphi_rea[F].shape)          
            print('bp1_s_vphi_arg   F%s:'%(F), bp1_s_vphi_arg[F].shape)        
            print('bp1_s_freq       F%s:'%(F), bp1_s_freq[F].shape) 
        if fill_value != None:
            bp1_s_nvec[F][...,2][bp1_s_nvec[F][...,2] < fill_value] = np.nan           
    cdf.close()
    return (bp1_s_pb, bp1_s_pe, bp1_s_dop, bp1_s_nvec, bp1_s_ellip, 
            bp1_s_sx_rea, bp1_s_sx_arg, bp1_s_vphi_rea, bp1_s_vphi_arg,            
            bp1_s_time_epoch, bp1_s_freq, bp1_s_params, bp1_s_params_bias)             
                           
def load_swf_b_CDF_L2(file, echo=False, key_attrs=None, key_field=None, key_field_attrs=None, reshape=True, fill_value=None,
                      B_RTN=False, L2_QUALITY_BITMASK=False):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                             
    swf_B_UARF = 3*[None]
    swf_time_epoch = 3*[None]   
    if B_RTN: swf_B_RTN = 3*[None] 
    if L2_QUALITY_BITMASK: swf_L2_QUALITY_BITMASK = 3*[None]  
    # sampling frequency F=0,1,2
    rec_swf_Fs = cdf['SAMPLING_RATE'][...]    
    rec_swf_B_UARF = cdf["B"][...]
    rec_swf_T_epoch = cdf.raw_var("Epoch")[...]      
    if B_RTN: rec_swf_B_RTN = cdf["B_RTN"][...]        
    if L2_QUALITY_BITMASK: rec_swf_L2_QUALITY_BITMASK = cdf["L2_QUALITY_BITMASK"][...]   
    for F in [0,1,2]:
        # data
        swf_index = (rec_swf_Fs == LFR_Fs[F])           # in principle, equivalent to:  
        swf_B_UARF[F] = rec_swf_B_UARF[swf_index, ...]  # cdf['B'][2-F::3, ...]                       
        if B_RTN: swf_B_RTN[F] = rec_swf_B_RTN[swf_index, ...]
        if L2_QUALITY_BITMASK: swf_L2_QUALITY_BITMASK[F] = rec_swf_L2_QUALITY_BITMASK[swf_index, ...]    
        # epoch time of the first sample of the swf [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT] 
        swf_t0_epoch = rec_swf_T_epoch[swf_index]       # cdf.raw_var('Epoch')[2-F::3] 
        # time vector
        snap_numb, ncomp, snap_size = swf_B_UARF[F].shape
        ramp_time = np.arange(snap_size) / LFR_Fs[F]
        swf_time_epoch[F] = np.zeros((snap_numb, snap_size), dtype=int)   
        for isnap in range(snap_numb):
            swf_time_epoch[F][isnap, :] = swf_t0_epoch[isnap] + np.asarray(ramp_time*1e9 + 0.5, dtype=int)                          
        if echo:
            print()
            print('swf_time_epoch F%s:'%(F), swf_time_epoch[F].shape)      
            print('swf_B_UARF     F%s:'%(F), swf_B_UARF[F].shape)                            
            if B_RTN: 
                print('swf_B_RTN     F%s:'%(F), swf_B_RTN[F].shape)                                        
        # reshaping if asked
        if reshape:     
            swf_time_epoch[F] = swf_time_epoch[F].flatten(order='C')
            swf_B_UARF[F] = np.reshape(np.swapaxes(swf_B_UARF[F], 1, 0), (ncomp, snap_numb*snap_size), order='C').T
            if B_RTN: swf_B_RTN[F] = np.reshape(np.swapaxes(swf_B_RTN[F], 1, 0), (ncomp, snap_numb*snap_size), order='C').T            
            if echo:
                print('swf_time_epoch F%s:'%(F), swf_time_epoch[F].shape)      
                print('swf_B_UARF     F%s:'%(F), swf_B_UARF[F].shape)
                if B_RTN: 
                    print('swf_B_RTN              F%s:'%(F), swf_B_RTN[F].shape)     
                if L2_QUALITY_BITMASK: 
                    print('swf_L2_QUALITY_BITMASK F%s:'%(F), swf_L2_QUALITY_BITMASK[F].shape)        
        if fill_value != None:
            swf_B_UARF[F][swf_B_UARF[F] < fill_value] = np.nan            
            if B_RTN: swf_B_RTN[F][swf_B_RTN[F] < fill_value] = np.nan   
        field_dict = {} 
        if B_RTN: field_dict['B_RTN'] = swf_B_RTN                      
        if L2_QUALITY_BITMASK: field_dict['L2_QUALITY_BITMASK'] = swf_L2_QUALITY_BITMASK      
    cdf.close()
    return (swf_B_UARF, swf_time_epoch, field_dict)      
    
def load_cwf_b_CDF_L2(file, echo=False, key_attrs=None, key_field=None, key_field_attrs=None, fill_value=None, 
                      B_RTN=False, L2_QUALITY_BITMASK=False):
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                       
    cwf_B_UARF = 4*[None]            
    cwf_time_epoch = 4*[None]   
    if B_RTN: cwf_B_RTN = 4*[None] 
    if L2_QUALITY_BITMASK: cwf_L2_QUALITY_BITMASK = 4*[None]       
    # sampling frequency F=1,2,3
    rec_cwf_Fs = cdf['SAMPLING_RATE'][...]    
    rec_cwf_B_UARF = cdf["B"][...]
    rec_cwf_T_epoch = cdf.raw_var("Epoch")[...]      
    if B_RTN: rec_cwf_B_RTN = cdf["B_RTN"][...]        
    if L2_QUALITY_BITMASK: rec_cwf_L2_QUALITY_BITMASK = cdf["L2_QUALITY_BITMASK"][...]               
    for F in [1,2,3]:
    #for F in [2, 3]:        
        # data
        cwf_index = (rec_cwf_Fs == LFR_Fs[F])       
        cwf_B_UARF[F] = rec_cwf_B_UARF[cwf_index, ...]  
        if B_RTN: cwf_B_RTN[F] = rec_cwf_B_RTN[cwf_index, ...]  
        if L2_QUALITY_BITMASK: cwf_L2_QUALITY_BITMASK[F] = rec_cwf_L2_QUALITY_BITMASK[cwf_index, ...]  
        # epoch time of the cwf samples [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT] 
        cwf_time_epoch[F] = rec_cwf_T_epoch[cwf_index]                                
        if echo:
            print()
            print('cwf_time_epoch         F%s:'%(F), cwf_time_epoch[F].shape)      
            print('cwf_B_UARF             F%s:'%(F), cwf_B_UARF[F].shape)
            if B_RTN: 
                print('cwf_B_RTN              F%s:'%(F), cwf_B_RTN[F].shape)     
            if L2_QUALITY_BITMASK: 
                print('cwf_L2_QUALITY_BITMASK F%s:'%(F), cwf_L2_QUALITY_BITMASK[F].shape)                                               
        if fill_value != None:
            cwf_B_UARF[F][cwf_B_UARF[F] < fill_value] = np.nan            
            if B_RTN: cwf_B_RTN[F][cwf_B_RTN[F] < fill_value] = np.nan          
        field_dict = {} 
        if B_RTN: field_dict['B_RTN'] = cwf_B_RTN                      
        if L2_QUALITY_BITMASK: field_dict['L2_QUALITY_BITMASK'] = cwf_L2_QUALITY_BITMASK                                   
    cdf.close()
    return (cwf_B_UARF, cwf_time_epoch, field_dict)         
    
def load_swf_e_CDF_L2(file, echo=False, key_attrs=None, key_field=None, key_field_attrs=None, reshape=True, fill_value=None):
    """
    VDC = DC singles (BIAS1 BIAS2 BIAS3)
    EDC = DC diffs   (BIAS2 BIAS3)
    EAC = AC diffs   (BIAS4 BIAS5)
    shape of these data: (i_time, i_sample_within_snapshot, i_channel)
    index values for diff channels are always ordered 0=V12, 1=V13, 2=V23
    """
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)                             
    swf_V_DC = 3*[None]
    swf_E_DC = 3*[None]
    swf_E_AC = 3*[None]
    swf_time_epoch = 3*[None]    
    # sampling frequency F=0,1,2
    rec_swf_Fs = cdf['SAMPLING_RATE'][...]    
    rec_swf_V_DC = cdf["VDC"][...]   
    rec_swf_E_DC = cdf["EDC"][...]   
    rec_swf_E_AC = cdf["EAC"][...]   
    rec_swf_T_epoch = cdf.raw_var("Epoch")[...]        
    for F in [0,1,2]:
        # data
        swf_index = (rec_swf_Fs == LFR_Fs[F])       # in principle, equivalent to:  
        swf_V_DC[F] = rec_swf_V_DC[swf_index, ...]  # cdf['V'][2-F::3, ...]         
        swf_E_DC[F] = rec_swf_E_DC[swf_index, ...]  # cdf['E'][2-F::3, ...]       
        swf_E_AC[F] = rec_swf_E_AC[swf_index, ...]  # cdf['EAC'][2-F::3, ...]                          
        # epoch time of the first sample of the swf [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT] 
        swf_t0_epoch = rec_swf_T_epoch[swf_index]   # cdf.raw_var('Epoch')[2-F::3] 
        # time vector
        snap_numb, snap_size, ncomp = swf_V_DC[F].shape
        ramp_time = np.arange(snap_size) / LFR_Fs[F]
        swf_time_epoch[F] = np.zeros((snap_numb, snap_size), dtype=int)   
        for isnap in range(snap_numb):
            swf_time_epoch[F][isnap, :] = swf_t0_epoch[isnap] + np.asarray(ramp_time*1e9 + 0.5, dtype=int)             
        if echo:
            print()
            print('swf_time_epoch F%s:'%(F), swf_time_epoch[F].shape)      
            print('swf_V_DC       F%s:'%(F), swf_V_DC[F].shape)                            
            print('swf_E_DC       F%s:'%(F), swf_E_DC[F].shape)                            
            print('swf_E_AC       F%s:'%(F), swf_E_AC[F].shape)                            
        # reshaping if asked
        if reshape:     
            swf_time_epoch[F] = swf_time_epoch[F].flatten(order='C')
            swf_V_DC[F] = np.reshape(swf_V_DC[F], (swf_V_DC[F].shape[0]*swf_V_DC[F].shape[1], 3), order='C')
            swf_E_DC[F] = np.reshape(swf_E_DC[F], (swf_E_DC[F].shape[0]*swf_E_DC[F].shape[1], 3), order='C')
            swf_E_AC[F] = np.reshape(swf_E_AC[F], (swf_E_AC[F].shape[0]*swf_E_AC[F].shape[1], 3), order='C')
            if echo:
                print('swf_time_epoch F%s:'%(F), swf_time_epoch[F].shape)      
                print('swf_V_DC       F%s:'%(F), swf_V_DC[F].shape)                            
                print('swf_E_DC       F%s:'%(F), swf_E_DC[F].shape)                            
                print('swf_E_AC       F%s:'%(F), swf_E_AC[F].shape) 
        if fill_value != None:
            swf_V_DC[F][swf_V_DC[F] < fill_value] = np.nan
            swf_E_DC[F][swf_E_DC[F] < fill_value] = np.nan 
            swf_E_AC[F][swf_E_AC[F] < fill_value] = np.nan 
    cdf.close()
    return (swf_V_DC, swf_E_DC, swf_E_AC, swf_time_epoch)   
        
def load_cwf_e_CDF_L2(file, echo=False, key_attrs=None, key_field=None, key_field_attrs=None, fill_value=None):
    """
    VDC = DC singles (BIAS1 BIAS2 BIAS3)
    EDC = DC diffs   (BIAS2 BIAS3)
    EAC = AC diffs   (BIAS4 BIAS5)
    shape of these data: (i_time, i_channel)
    index values for diff channels are always ordered 0=V12, 1=V13, 2=V23
    VDC_LABEL: ['Vdc1' 'Vdc2' 'Vdc3']
    EDC_LABEL: ['Vdc12' 'Vdc13' 'Vdc23']
    EAC_LABEL: ['Vac12' 'Vac13' 'Vac23']    
    """
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)     
    cwf_V_DC = 4*[None]
    cwf_E_DC = 4*[None]
    cwf_E_AC = 4*[None]
    cwf_time_epoch = 4*[None]  
    # sampling frequency F=1,2,3
    rec_cwf_Fs = cdf['SAMPLING_RATE'][...]        
    rec_cwf_V_DC = cdf["VDC"][...]   
    rec_cwf_E_DC = cdf["EDC"][...]   
    rec_cwf_E_AC = cdf["EAC"][...]   
    rec_cwf_T_epoch = cdf.raw_var("Epoch")[...]      
    for F in [1,2,3]:
    #for F in [2, 3]:        
        # data
        cwf_index = (rec_cwf_Fs == LFR_Fs[F])       
        cwf_V_DC[F] = rec_cwf_V_DC[cwf_index, ...]  
        cwf_E_DC[F] = rec_cwf_E_DC[cwf_index, ...]  
        cwf_E_AC[F] = rec_cwf_E_AC[cwf_index, ...]  
        # epoch time of the cwf samples [TT2000 ns from (2000, 1, 1, 12, 0, 0)_TT] 
        cwf_time_epoch[F] = rec_cwf_T_epoch[cwf_index]                                
        if echo:
            print()
            print('cwf_time_epoch F%s:'%(F), cwf_time_epoch[F].shape)      
            print('cwf_V_DC       F%s:'%(F), cwf_V_DC[F].shape)
            print('cwf_E_DC       F%s:'%(F), cwf_E_DC[F].shape)
            print('cwf_E_AC       F%s:'%(F), cwf_E_AC[F].shape)
        if fill_value != None:
            cwf_V_DC[F][cwf_V_DC[F] < fill_value] = np.nan            
            cwf_E_DC[F][cwf_E_DC[F] < fill_value] = np.nan            
            cwf_E_AC[F][cwf_E_AC[F] < fill_value] = np.nan            
    cdf.close()
    return (cwf_V_DC, cwf_E_DC, cwf_E_AC, cwf_time_epoch)             
    
def meld_swf_E_bias_L2(swf_E_DC, swf_E_AC, fill_value=-9.99e30):
    swf_E = 3*[None]
    for F in range(3):
        #if not np.logical_xor(swf_E_DC[F] < fill_value, swf_E_AC[F] < fill_value).all():
        #    raise TotoError("swf_E_DC[%s] is not complementary to swf_E_AC[%s] !!?"%(F, F))
        if np.logical_and(swf_E_DC[F] >= fill_value, swf_E_AC[F] >= fill_value).any():
            raise TotoError("swf_E_DC[%s] is simultaneous to swf_E_AC[%s] !!?"%(F, F))             
        E_DC = np.copy(swf_E_DC[F])
        E_AC = np.copy(swf_E_AC[F])  
        both_nan = np.logical_and(swf_E_DC[F] < fill_value, swf_E_AC[F] < fill_value)                   
        # when only one E is a nan (E_DC xor E_AC): set to 0 for summation ... 
        E_DC[E_DC < fill_value] = 0.                 
        E_AC[E_AC < fill_value] = 0. 
        # but when both are nan, i.e. no E data at all, overwrite and set to nan
        E_DC[both_nan] = np.nan
        E_AC[both_nan] = np.nan   
        swf_E[F] = E_DC[:, ::2] + E_AC[:, ::2]
    return swf_E 

def meld_cwf_E_bias_L2(cwf_E_DC, cwf_E_AC, fill_value=-9.99e30):    
    #if not np.logical_xor(cwf_E_DC < fill_value, cwf_E_AC < fill_value).all():
    #    raise TotoError("cwf_E_DC is not complementary to cwf_E_AC !!?")     
    if np.logical_and(cwf_E_DC >= fill_value, cwf_E_AC >= fill_value).any():
        raise TotoError("cwf_E_DC is simultaneous to cwf_E_AC !!?")     
    E_DC = np.copy(cwf_E_DC)
    E_AC = np.copy(cwf_E_AC)     
    both_nan = np.logical_and(cwf_E_DC < fill_value, cwf_E_AC < fill_value)                   
    # when only one E is a nan (E_DC xor E_AC): set to 0 for summation ... 
    E_DC[E_DC < fill_value] = 0. 
    E_AC[E_AC < fill_value] = 0. 
    # but when both are nan, i.e. no E data at all, overwrite and set to nan
    E_DC[both_nan] = np.nan
    E_AC[both_nan] = np.nan 
    # the 2 electric components retained are V12 and V23        
    cwf_E = E_DC[:, ::2] + E_AC[:, ::2]
    return cwf_E     
    
def load_bias_ne_CDF_L3(file, echo=False, key_attrs=None, key_field=None, key_field_attrs=None, fill_value=None):
    """
    density from S/C potential measurement 
    fill_value=-9.9e30
    """
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)   
    density_scpot = cdf['DENSITY'][...]    
    time_epoch = cdf.raw_var("Epoch")[:]                                       
    if echo:
        print()        
        print('time_epoch:  ', time_epoch.shape)      
        print('density_scpot:', density_scpot.shape)  
        print('Sampling frequency:', 1/np.median(np.gradient(1e-9*time_epoch))) 
    if fill_value != None:
            density_scpot[density_scpot < fill_value] = np.nan            
    cdf.close()
    return (density_scpot, time_epoch)    
    
def load_bias_efield_CDF_L3(file, echo=False, key_attrs=None, key_field=None, key_field_attrs=None, fill_value=None):
    """
    E field in SRF elaborated from ANT/BIAS/LFR DC data (and MAG data ?)      
    fill_value=-9.9e30
    """
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)   
    EDC_SRF = cdf['EDC_SRF'][...]    
    time_epoch = cdf.raw_var("Epoch")[:]                                       
    if echo:
        print()        
        print('time_epoch:  ', time_epoch.shape)      
        print('EDC_SRF:', EDC_SRF.shape)  
        print('Sampling frequency:', 1/np.median(np.gradient(1e-9*time_epoch))) 
    if fill_value != None:
            EDC_SRF[EDC_SRF < fill_value] = np.nan            
    cdf.close()
    return (EDC_SRF, time_epoch)      
    
def load_bias_vht_CDF_L3(file, echo=False, key_attrs=None, key_field=None, key_field_attrs=None, fill_value=None):
    """
    de Hoffmann-Teller (HT) solar wind velocity in the SRF X direction (km/s)      
    fill_value=-9.9e30
    """
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)   
    VX_SRF = cdf['VX_SRF'][...]    
    time_epoch = cdf.raw_var("Epoch")[:]                                       
    if echo:
        print()        
        print('time_epoch:  ', time_epoch.shape)      
        print('VX_SRF:', VX_SRF.shape)  
        print('Sampling frequency:', 1/np.median(np.gradient(1e-9*time_epoch))) 
    if fill_value != None:
            VX_SRF[VX_SRF < fill_value] = np.nan            
    cdf.close()
    return (VX_SRF, time_epoch)       
    
def load_tnr_fp_CDF_L3(file, echo=False, key_attrs=None, key_field=None, key_field_attrs=None, fill_value=None):
    """
    data from TNR peak tracking
    plasma frequency in kHz
    """
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://pythonhosted.org/SpacePy/    
    # pycdf - Python interface to CDF files        
    cdf = pycdf.CDF(file)    
    if echo: print_cdf(cdf, key_attrs=key_attrs, key_field=key_field, key_field_attrs=key_field_attrs)   
    plasma_freq = cdf['PLASMA_FREQ'][...]    
    time_epoch = cdf.raw_var("Epoch")[:]                                       
    if echo:
        print()        
        print('time_epoch:  ', time_epoch.shape)      
        print('plasma_freq:', plasma_freq.shape)                       
    cdf.close()
    return (plasma_freq, time_epoch)      
    
    
############### miscellaneous ASCII files #######################    
    
def load_SCM_stimuli_TF(file, header=4 , sep=';'):
    if os.path.exists(file):
        SCMstimuli = pd.read_csv(file, header=header-1, sep=sep, index_col=0, names=['Gain','Phase'])
        return SCMstimuli   
        
def load_SCM_TF(file, header=22 , sep=';'):
    return np.genfromtxt(file, skip_header=header, delimiter=sep, dtype='f8,f8,f8', names=['Freq', 'Gain','Phase'])          

def load_BIAS_TF(file):
    return np.genfromtxt(file, comments='m', delimiter=None, dtype='f8,f8,f8', names=['Freq', 'Gain','Phase'])
    
def load_LFR_TF_ascii_6(file):
    """    
      B1 B2 B3 E1 E2  V  
    B1 0  0  1  2  3  4
    B2    1  5  6  7  8
    B3       2  9 10 11
    E1          3 12 13
    E2             4 14
     V                5
    """
    return np.genfromtxt(file, skip_header=4, dtype='S44,'+36*'f8,'+'f8', \
                 names=['source file', 'ifreq', 'auto 0', 'auto 1', 'auto 2', 'auto 3', 'auto 4', 'auto 5', \
                        'rho 0', 'rho 1', 'rho 2', 'rho 3', 'rho 4', 'rho 5', 'rho 6', 'rho 7', 'rho 8', 'rho 9', \
                        'rho 10', 'rho 11', 'rho 12', 'rho 13', 'rho 14', \
                        'phi 0', 'phi 1', 'phi 2', 'phi 3', 'phi 4', 'phi 5', 'phi 6', 'phi 7', 'phi 8', 'phi 9', \
                        'phi 10', 'phi 11', 'phi 12', 'phi 13', 'phi 14'])    
                        
def load_EGSE_stimuli_AWG0(file):
    return np.genfromtxt(file, skip_header=3, delimiter=None, dtype='f8', names=['Amplitude'])
    
def load_EGSE_stimuli_AWG(file, fromtime=0., totime=0., fs=1., t0=0., header=3, bytes_per_item=4, everyN=None, echo=True):
    if not t0 <= fromtime <= totime: 
        raise TotoError('problematic time input: t0 <= fromtime <= totime IS NOT TRUE !?')
    i1 = int((fromtime-t0)*fs) + header
    i2 = int(np.ceil((totime-t0)*fs)) + header        
    ftype = file[-3:]
    if echo:     
        if ftype == 'csv':       
            header_lines = readlines(file, i1=0, i2=header-1)
            nb_of_pts = int(header_lines[1].split(',')[1])                               
            print('Header:', header_lines)
        if ftype in ['dat', 'txt']:                                
            nb_of_pts = os.path.getsize(file) / bytes_per_item                   
        print('Total number of points :', nb_of_pts)        
        print('Sampling frequency (Hz):', fs)
        print('Total wf duration  (s) :', nb_of_pts/fs)
    if ftype == 'csv':                               
        selected_wf = np.array(readlines(file, i1=i1, i2=i2)).astype(float)
    if ftype in ['dat', 'txt']:                            
        selected_wf = np.array(seeklines(file, i1=i1, i2=i2, bytes_per_item=bytes_per_item, ctype='f', 
                                         option='struct', endianness='<', sep='\n'))        
    if i2-i1+1 != len(selected_wf):
        raise TotoError('problematic selection: i2-i1+1={} IS NOT EQUAL TO len(selected_wf)={}' \
                        .format(i2-i1+1, len(selected_wf)))
    time = t0 + np.arange(i1-header, i2+1-header) / fs            
    return {'Amplitude': selected_wf[::everyN], 'Time': time[::everyN]}    
    
def load_tnr_plasma_frequency(file):
    """
    data from Milan TNR peak tracking
    """
    return np.genfromtxt(file, skip_header=1, delimiter=None, dtype=float, 
                         names=['Hour_of_the_day', 'Fp_kHz','Quality_factor'])    
                         
def load_bias_ne_scpot(file):
    """
    data from Yuri S/C potential measurements 
    """
    return np.genfromtxt(file, skip_header=11, delimiter=None, dtype=float, 
                         names=['sec', 'NeScp_cc'])                                                  

############### miscellaneous XML files ####################### 
   
def load_stimuli_mysterious_console_XML(file, echo=False, j=-1, ncomp=6):
    import jxmlease
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://www.oreilly.com/learning/jxmlease-python-xml-conversion-data-structures
    # https://docs.python.org/3/library/xml.etree.elementtree.html 
    # fichier console de Xavier pour les stimuli mysterious signals
    with open(file, "r") as f_in:
        xml_str = f_in.read()
    root = jxmlease.parse(xml_str)
    if echo:
        root.prettyprint()
    stimuli = []
    for i in range(ncomp): 
        if echo:
            print(root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j].get_xml_attrs())
            print(root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j], 'att:',
                  root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j]['Amplitude'].get_xml_attrs()['att'])
        stimuli.append({'Waveform':    root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j].get_xml_attrs()['waveform'],
                        'Time':        root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j].get_xml_attrs()['time'],
                        'Amplitude':   root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j]['Amplitude'][:],
                        'Attenuation': root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j]['Amplitude'].get_xml_attrs()['att'],
                        'SampleFreq':  root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j]['SampleFreq'][:],
                        'Offset':      root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j]['Offset'][:],
                        'NbCycles':    root['RocEgse']['EgseOutputs']['Output'][i]['Signal'][j]['NbCycles'][:]})
    return stimuli
    
def load_stimuli_lfr_sweep_F0_F1_F2_console_XML(file, echo=False, raw_echo=False, index=[0,1,2,3]):
    import jxmlease
    if not os.path.exists(file):
        print( 'Fichier {} introuvable ... !'.format(file) )
        return
    # https://www.oreilly.com/learning/jxmlease-python-xml-conversion-data-structures
    # https://docs.python.org/3/library/xml.etree.elementtree.html 
    # fichier console pour les stimuli injectés en 3 blocs lors d'un sweep LFR F0 F1 F2
    with open(file, "r") as f_in:
        xml_str = f_in.read()
    root = jxmlease.parse(xml_str)
    if raw_echo: root.prettyprint()
    stimuli = {}
    for i in index: 
        Output_i = root['RocEgse']['EgseOutputs']['Output'][i]
        Out = Output_i.get_xml_attrs()['id']
        if echo: print('\nOuput id = ', Out)
        blocks = []
        for j in range(len(Output_i['Signal'])):
            if Output_i['Signal'][j].get_xml_attrs()['action'] == 'StartAWG': 
                if echo: 
                    print(Output_i['Signal'][j].get_xml_attrs())            
                    print(Output_i['Signal'][j], 'att:', Output_i['Signal'][j]['Amplitude'].get_xml_attrs()['att'])
                blocks.append({'Waveform':    Output_i['Signal'][j].get_xml_attrs()['waveform'],
                               'Time':        Output_i['Signal'][j].get_xml_attrs()['time'],
                               'Amplitude':   Output_i['Signal'][j]['Amplitude'][:],
                               'Attenuation': Output_i['Signal'][j]['Amplitude'].get_xml_attrs()['att'],
                               'SampleFreq':  Output_i['Signal'][j]['SampleFreq'][:],
                               'Offset':      Output_i['Signal'][j]['Offset'][:],
                               'NbCycles':    Output_i['Signal'][j]['NbCycles'][:]})
        #stimuli.append({'0ut{}'.format(out) : blocks})  
        stimuli['Out{}'.format(Out)] = blocks          
    return stimuli    
    
                    
############### Bruno's decom ASCII files #######################        
    
def get_lfr_decom_l1_swf(file):
    """    
    Routine qui lit les fichiers ascii .sfi (i.e. snapshot waveforms SWF, i= 0, 1 ou 2)
    crées par la routine de decom L1 LFR_packet_decom-win-i686.exe de Bruno Katra    
    
INPUT:
    file:           file name (ex: 'data/2015_10_19_15_28_08_packet_record_NORMAL.sf0')           

OUTPUT:
    data_block:     v, e1, e2, b1, b2, b3 (in count unit => 16-bit integer array) 
                    [ndarray(nb_of_data, 6), with nb_of_data = number of samples by component]
                    
    packet_headers: headers of the packets read in the file (string array)
                    [ndarray(nb_of_packets, 6)] 
                    cnt   => number of packets for a snapshot
                    nr    => numbering of the packets for a given snapshot
                    nb    => number of samples in the packet
                    syn_t => bit of synchronization of the packet (0 if synchronized)
                    int_t => time of the first sample of the packet in unit of 2^-16 seconds (integer)
                    sec_t => same but in unit of seconds (floating)      
                    example : cnt  nr  nb   syn_t   int_t (2^⁻16 sec)      sec_t (seconds)
                              7    1   304  1       93255699               1422.9690399169921875 
                                                  
    relative_time:  time of the data in unit of seconds counted from the first sample (float array)
                    [ndarray(nb_of_data)]
    start_time:     time of the first sample (seconds from 2000/00/01 00:00)
    
    index_snapshot: index of the first element of each snapshot in data_block and time (integer array)
                    [ndarray(nb_of_snapshots+1)]
                    i. e., for k from 0 to nb_of_snapshots-1, snapshot(k) and its time(k) are given by
                           data_block[index_snapshot[k]:index_snapshot[k+1], :] and
                           relative_time[index_snapshot[k]:index_snapshot[k+1]]
                          
    fe:             fréquence d'échantillonnage (sampling frequency) read from the file name
    fromfile:       file name  
    names:          string enumerating the data field components            

    {'data_block':      data_block,                 \
     'packet_headers':  packet_headers,             \
     'relative_time':   relative_time,              \
     'start_time':      start_time,                 \
     'index_snapshot':  index_snapshot,             \     
     'fe':              fe,                         \
     'fromfile':        file,                       \
     'names':           'V, E1, E2, B1, B2, B3'}
                
    version 1.0: first version with Python (!) from the last version of the idl routine     [25 décembre 2015]         
    version 1.1: this doctring + np.float64 changed by np.float                             [2 février 2016]
    version 1.2: python3 compatibility ...                                                  [19 mars 2016]
    version 1.3: add TotoError Exception class                                              [12 avril 2016]   
    version 1.4: np.float changed by float (because of jsonpickle ! ...)                    [22 juillet 2016]
    """
        
    # reading the ascii file using loadtxt (faster than genfromtxt because read the file just once but no names ...)
    # here just the data (skip the comments) 
    data_block = np.loadtxt(file, delimiter=None, comments='#', dtype=np.int16)
    # data_block = np.genfromtxt(file, delimiter=None, comments='#', dtype=np.int16, names='V, E1, E2, B1, B2, B3')    
    nb_of_data = data_block.shape[0]

    # reading now all the lines as an array of strings for packet headers ...
    with open(file, 'r') as file_in:
        block_lines = np.array(file_in.read().strip().split("\n"))
    first_char = np.array([str[0:21] for str in block_lines])
    index_cnt_line  = np.where(first_char == '##### PA_LFR_PKT_CNT:')[0]
    index_nr_line   = index_cnt_line + 1
    index_nb_line   = index_cnt_line + 2
    index_synt_line = index_cnt_line + 4
    index_intt_line = index_cnt_line + 5
    index_sect_line = index_cnt_line + 6
    packet_headers_str = block_lines[ np.array([index_cnt_line, index_nr_line, index_nb_line, \
                                                index_synt_line, index_intt_line, index_sect_line]) ].transpose()
    cnt = np.array([str[22:22+2] for str in packet_headers_str[:,0]])
    nr  = np.array([str[21:21+2] for str in packet_headers_str[:,1]])
    nb  = np.array([str[41:41+3] for str in packet_headers_str[:,2]])
    # CUC format = 6 bytes =  1 bit  (synchro) 
    #                      + 31 bits (coarse time in integer secondes ) 
    #                      + 16 bits (fine time in fractional secondes)
    syn_t = np.array([str[17:17+1]  for str in packet_headers_str[:,3]]) # 1 bit synchro
    int_t = np.array([str[44:44+15] for str in packet_headers_str[:,4]]) # max (2^-16) sec = 2^47-1 = 140737488355327    (15 decimal)
    sec_t = np.array([str[16:16+27] for str in packet_headers_str[:,5]]) # max coarse  sec = 2^31-1 =      2147483647    (10 decimal)
                                                                         # min fine    sec = 2^-16  = 0.0000152587890625 (16 decimal)
    packet_headers = np.array([cnt, nr, nb, syn_t, int_t, sec_t]).transpose()
    nb_of_packets = cnt.size
    
    index_packet_nrEQ1  = np.where(nr.astype(int) == 1 )[0]
    index_packet_nrEQ1 = np.hstack((index_packet_nrEQ1, nb_of_packets))
    nb_of_snapshots = index_packet_nrEQ1.size - 1
    snapshot_length = np.zeros(nb_of_snapshots, dtype=int)
    for k in range(nb_of_snapshots):
        snapshot_length[k] = sum(packet_headers[index_packet_nrEQ1[k]:index_packet_nrEQ1[k+1],2].astype(int))
    index_snapshot = np.zeros(nb_of_snapshots+1, dtype=int)
    for k in range(nb_of_snapshots):
        index_snapshot[k+1] = index_snapshot[k] + snapshot_length[k]
    nb_of_data_expected = index_snapshot[nb_of_snapshots]
        
    if nb_of_data != nb_of_data_expected:
        raise TotoError('NUMBER OF data IS NOT EQUAL TO THAT EXPECTED !!!!' )
   
    # creation of the time series
    f_ = file[-2:]
    if f_ == 'f0':
        fe = float(24576)
    elif f_ == 'f1':
        fe = float(4096)
    elif f_ == 'f2':
        fe = float(256)
    # sinon on lève une exception
    else:
        raise TotoError('nom de fichier problématique ...' )
    
    ramp = np.arange(int(max(nb))) / fe
    start_time = int_t[0].astype(int) * float(2)**(-16)
    relative_tid = (int_t.astype(int) - int_t[0].astype(int)) * float(2)**(-16)
    #print( relative_tid )
    relative_time = np.zeros(nb_of_data, dtype=float)
    i_packet = np.zeros(nb_of_packets+1, dtype=int)
    for i in range(nb_of_packets):
        relative_time[i_packet[i]:i_packet[i]+int(nb[i])] = ramp[0:int(nb[i])]+relative_tid[i]
        i_packet[i+1] = i_packet[i]+int(nb[i])

    return {'data_block': data_block,         \
            'packet_headers': packet_headers, \
            'relative_time': relative_time,   \
            'index_snapshot': index_snapshot, \
            'start_time': start_time,         \
            'fe': fe,                         \
            'fromfile': file,                 \
            'names': 'V, E1, E2, B1, B2, B3'}    
        
def get_lfr_decom_l1_cwf(file):
    """    
    Routine qui lit les fichiers ascii .cfi (i.e. continuous waveforms CWF, i= 1, 2 ou 3)
    crées par la routine de decom L1 LFR_packet_decom-win-i686.exe de Bruno Katra    
    
INPUT:
    file:           file name (ex: 'data/2015_10_02_10_50_48_packet_record_SBM1.cf1')           

OUTPUT:
    data_block:     v, e1, e2, b1, b2, b3 [or v, e1, e2] (in count unit => 16-bit integer array) 
                    [ndarray(nb_of_data, 6 [or 3]), with nb_of_data = number of samples by component]
                    
    packet_headers: headers of the packets read in the file (string array)
                    [ndarray(nb_of_packets, 4)] 
                    nb    => number of samples in the packet
                    syn_t => bit of synchronization of the packet (0 if synchronized)
                    int_t => time of the first sample of the packet in unit of 2^-16 seconds (integer)
                    sec_t => same but in unit of seconds (floating)      
                    example : nb   syn_t   int_t (2^⁻16 sec)      sec_t (seconds)
                              336  1       92209163               1407.0001678466796875                                      
                                                  
    relative_time:  time of the data in unit of seconds counted from the first sample (float array)
                    [ndarray(nb_of_data)]
    start_time:     time of the first sample (seconds from 2000/00/01 00:00)
                              
    fe:             fréquence d'échantillonnage (sampling frequency) read from the file name
    fromfile:       file name  
    names:          string enumerating the data field components            

    {'data_block':      data_block,                 \
     'packet_headers':  packet_headers,             \
     'relative_time':   relative_time,              \
     'start_time':      start_time,                 \
     'fe':              fe,                         \
     'fromfile':        file,                       \
     'names':           'V, E1, E2, B1, B2, B3' [or 'V, E1, E2']}
                
    version 1.0: first version with Python (!) from the last version of the idl routine     [27 décembre 2015]         
    version 1.1: this doctring + np.float64 changed by np.float                             [2 février 2016]  
                 + suppression of case f0, addition of case f3                                                   
    version 1.2: python3 compatibility ...                                                  [2 avril 2016]
    version 1.3: add TotoError Exception class                                              [12 avril 2016] 
    version 1.4: np.float changed by float (because of jsonpickle ! ...)                    [22 juillet 2016]
    """
            
    # reading the ascii file using loadtxt (faster than genfromtxt because read the file just once but no names ...)
    # here just the data (skip the comments) 
    data_block = np.loadtxt(file, delimiter=None, comments='#', dtype=np.int16)
    # data_block = np.genfromtxt(file, delimiter=None, comments='#', dtype=np.int16, names='V, E1, E2, B1, B2, B3')    
    nb_of_data = data_block.shape[0]
    nb_of_comp = data_block.shape[1]

    # reading now all the lines as an array of strings for packet headers ...
    with open(file, 'r') as file_in:
        block_lines = np.array(file_in.read().strip().split("\n"))
    first_char = np.array([str[0:38] for str in block_lines])
    index_nb_line  = np.where(first_char == '##### Number of blocks/samples to read')[0]
    index_synt_line = index_nb_line + 2
    index_intt_line = index_nb_line + 3
    index_sect_line = index_nb_line + 4
    packet_headers_str = block_lines[ np.array([index_nb_line, index_synt_line, index_intt_line, index_sect_line]) ].transpose()
    nb  = np.array([str[41:41+3] for str in packet_headers_str[:,0]])
    # CUC format = 6 bytes =  1 bit  (synchro) 
    #                      + 31 bits (coarse time in integer secondes ) 
    #                      + 16 bits (fine time in fractional secondes)
    syn_t = np.array([str[17:17+1]  for str in packet_headers_str[:,1]]) # 1 bit synchro
    int_t = np.array([str[44:44+15] for str in packet_headers_str[:,2]]) # max (2^-16) sec = 2^47-1 = 140737488355327    (15 decimal)
    sec_t = np.array([str[16:16+27] for str in packet_headers_str[:,3]]) # max coarse  sec = 2^31-1 =      2147483647    (10 decimal)
                                                                         # min fine    sec = 2^-16  = 0.0000152587890625 (16 decimal)
    packet_headers = np.array([nb, syn_t, int_t, sec_t]).transpose()
    nb_of_packets = nb.size
    nb_of_data_expected = sum(nb.astype(int))
            
    if nb_of_data != nb_of_data_expected:
        raise TotoError('NUMBER OF data IS NOT EQUAL TO THAT EXPECTED !!!!')
   
    # creation of the time series
    f_ = file[-2:]
    if f_ == 'f1':
        fe = float(4096)
    elif f_ == 'f2':
        fe = float(256)
    elif f_ == 'f3':
        fe = float(16)
    # sinon on lève une exception
    else:
        raise TotoError('nom de fichier problématique ...')
    
    ramp = np.arange(int(max(nb))) / fe
    start_time = int_t[0].astype(int) * float(2)**(-16)
    relative_tid = (int_t.astype(int) - int_t[0].astype(int)) * float(2)**(-16)
    #print relative_tid
    relative_time = np.zeros(nb_of_data, dtype=np.float)
    i_packet = np.zeros(nb_of_packets+1, dtype=int)
    for i in range(nb_of_packets):
        relative_time[i_packet[i]:i_packet[i]+int(nb[i])] = ramp[0:int(nb[i])]+relative_tid[i]
        i_packet[i+1] = i_packet[i]+int(nb[i])

    return {'data_block': data_block,         \
            'packet_headers': packet_headers, \
            'relative_time': relative_time,   \
            'start_time': start_time,         \
            'fe': fe,                         \
            'fromfile': file,                 \
            'names': 'V, E1, E2, B1, B2, B3' if nb_of_comp == 6 else 'V, E1, E2'}    
    
def get_lfr_decom_l1_asm(file):
    """    
    Routine qui lit les fichiers ascii .afi (i.e. time-averaged spectral matrix ASM, i= 0, 1 ou 2)
    crées par la routine de decom L1 LFR_packet_decom-win-i686.exe de Bruno Katra    
    
INPUT:
    file:           file name (ex: 'data/2015_10_19_15_28_08_packet_record_NORMAL.af0')           

OUTPUT:
    asm_idl:        [B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2]) (in count^2 unit with 32-bit float => complex64 array)                  
                    [ndarray(nspec, nfreq, dim_vector, dim_vector), with nspec = nb of spectral matrices 
                                                                     and nfreq = nb of frequency bins]    
                                                                                        
    packet_headers: headers of the packets read in the file (string array)
                    [ndarray(nb_of_packets, 6)] 
                    cnt   => number of packets for a spectral matrix (ASM)
                    nr    => numbering of the packets for a given spectral matrix
                    nb    => number of bins in the packet
                    syn_t => bit of synchronization of the packet (0 if synchronized)
                    int_t => time of a given spectral matrix in unit of 2^-16 seconds (integer)
                             [corresponds to the time of the first sample of the first underlying FFT]
                    sec_t => same but in unit of seconds (floating)      
                    example : cnt  nr  nb   syn_t   int_t (2^⁻16 sec)      sec_t (seconds)
                              3    2   32   0       32674944516096         498580086.0000000000000000
                                                  
    relative_time:  time of the data in unit of seconds counted from the first spectral matrix (float array)
                    [ndarray(nb_of_data)]
    start_time:     time of the first spectral matrix (seconds from 2000/00/01 00:00)
    freq:           frequency bins (Hz) [ndarray(nfreq)]
                          
    fe:             fréquence d'échantillonnage (sampling frequency) read from the file name
    fromfile:       file name  
    names:          string enumerating the data field components            

    {'asm_idl':         asm_idl,                    
     'packet_headers':  packet_headers,             
     'relative_time':   relative_time,              
     'start_time':      start_time,                 
     'freq':            freq                        
     'fe':              fe,                         
     'mode':            mode,                          
     'fromfile':        file,                       
     'names':           '[B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])'}
                
    version 1.0: first version with Python (!) from the last version of the idl routine     [27 décembre  2015]         
    version 1.1: this doctring + np.float64 changed by np.float                             [2  février   2016]
    version 1.2: python3 compatibility ...                                                  [13 septembre 2016]
    version 1.3: change shape (5, 5, nspec, nfreq) to shape (nspec, nfreq, 5, 5)            [5  janvier   2017]
    version 1.4: add 'mode' as for bp2 and bp1                                              [19 décembre  2018]
    """
            
    # reading the ascii file using loadtxt (faster than genfromtxt because read the file just once but no names ...)
    # here just the data (skip the comments) 
    data_block = np.loadtxt(file, delimiter=None, comments='#', dtype=np.float32)
    # data_block = np.genfromtxt(file, delimiter=None, comments='#', dtype=np.float32, \
    #              names='B1B1*, B1B2*, B1B3*, B1E1*, B1E2*, B2B2*, B2B3*, B2E1*, B2E2*, B3B3*, B3E1*, B3E2*, E1E1*, E1E2*, E2E2*')    
    nb_of_data = data_block.shape[0]

    # reading now all the lines as an array of strings for packet headers ...
    with open(file, 'r') as file_in:
        block_lines = np.array(file_in.read().strip().split("\n"))
    first_char = np.array([str[0:25] for str in block_lines])
    index_cnt_line  = np.where(first_char == '##### PA_LFR_PKT_CNT_ASM:')[0]
    index_nr_line   = index_cnt_line + 1
    index_nb_line   = index_cnt_line + 2
    index_synt_line = index_cnt_line + 4
    index_intt_line = index_cnt_line + 5
    index_sect_line = index_cnt_line + 6
    packet_headers_str = block_lines[ np.array([index_cnt_line, index_nr_line, index_nb_line, \
                                                index_synt_line, index_intt_line, index_sect_line]) ].transpose()
    cnt = np.array([str[26:26+2] for str in packet_headers_str[:,0]])
    nr  = np.array([str[25:25+2] for str in packet_headers_str[:,1]])
    nb  = np.array([str[41:41+3] for str in packet_headers_str[:,2]])
    # CUC format = 6 bytes =  1 bit  (synchro) 
    #                      + 31 bits (coarse time in integer secondes ) 
    #                      + 16 bits (fine time in fractional secondes)
    syn_t = np.array([str[17:17+1]  for str in packet_headers_str[:,3]])# 1 bit synchro
    int_t = np.array([str[44:44+15] for str in packet_headers_str[:,4]]) # max (2^-16) sec = 2^47-1 = 140737488355327    (15 decimal)
    sec_t = np.array([str[16:16+27] for str in packet_headers_str[:,5]]) # max coarse  sec = 2^31-1 =      2147483647    (10 decimal)
                                                                         # min fine    sec = 2^-16  = 0.0000152587890625 (16 decimal)
    packet_headers = np.array([cnt, nr, nb, syn_t, int_t, sec_t]).transpose()
    nb_of_packets = cnt.size
    
   
    R = 2                              # R2 FSW (default)
    index_packet_nrEQ1  = np.where(nr.astype(int) == 1 )[0]    
    index_packet_nrEQ1 = np.hstack((index_packet_nrEQ1, nb_of_packets))        
    nb_of_asm1 = index_packet_nrEQ1.size - 1
    index_packet_nrEQ2  = np.where(nr.astype(int) == 2 )[0]
    index_packet_nrEQ2 = np.hstack((index_packet_nrEQ2, nb_of_packets))  
    nb_of_asm2 = index_packet_nrEQ2.size - 1                      
    if nb_of_asm1 == nb_of_asm2:
        nspec = nb_of_asm1 
    else:
        print('number of asm1 = {}'.format(nb_of_asm1))
        print('number of asm2 = {}'.format(nb_of_asm2))
        raise TypeError('nb_of_asm1  NOT EQUAL to nb_of_asm2 !')       
    
    cnt_val = int(cnt[0])
    if cnt_val == 3:
        R = 3                           # R3 FSW
        index_packet_nrEQ3  = np.where(nr.astype(int) == 3 )[0]
        index_packet_nrEQ3 = np.hstack((index_packet_nrEQ3, nb_of_packets))  
        nb_of_asm3 = index_packet_nrEQ3.size - 1                                 
        if nb_of_asm3 != nb_of_asm1:
            print('number of asm1 = {}'.format(nb_of_asm1))
            print('number of asm2 = {}'.format(nb_of_asm2))
            print('number of asm3 = {}'.format(nb_of_asm3))           
            raise TypeError('nb_of_asm3  NOT EQUAL to nb_of_asm1 !')

    asm_nfreq = np.zeros(nspec, dtype=int)
    for k in range(nspec):
        asm_nfreq[k] = sum(packet_headers[index_packet_nrEQ1[k]:index_packet_nrEQ1[k+1],2].astype(int))
    index = np.where(asm_nfreq != asm_nfreq[0])[0]
    if len(index) == 0:
        nfreq = asm_nfreq[0]
    else:
        print(' asm_nfreq, index :',  asm_nfreq, index)
        raise TypeError('{} values of asm_nfreq different from  asm_nfreq(0) !'.format(len(index)))         
    nb_of_data_expected = nspec*nfreq
        
    if nb_of_data != nb_of_data_expected:
        raise TypeError('NUMBER OF data IS NOT EQUAL TO THAT EXPECTED !!!!')
   
    # creation of the time series and the frequency table
    mode = {'L': 'NORMAL',
            '1': 'SBM1',
            '2': 'SBM2',
            'T': 'BURST'}[file[-5]]
    f_ = file[-2:]
    if f_ == 'f0':
        fe = np.float(24576)
        # 88 frequency bins
        freq = [1632., 1728., 1824., 1920., 2016., 2112., 2208., 2304., \
                2400., 2496., 2592., 2688., 2784., 2880., 2976., 3072., \
                3168., 3264., 3360., 3456., 3552., 3648., 3744., 3840., \
                3936., 4032., 4128., 4224., 4320., 4416., 4512., 4608., \
                4704., 4800., 4896., 4992., 5088., 5184., 5280., 5376., \
                5472., 5568., 5664., 5760., 5856., 5952., 6048., 6144., \
                6240., 6336., 6432., 6528., 6624., 6720., 6816., 6912., \
                7008., 7104., 7200., 7296., 7392., 7488., 7584., 7680., \
                7776., 7872., 7968., 8064., 8160., 8256., 8352., 8448., \
                8544., 8640., 8736., 8832., 8928., 9024., 9120., 9216., \
                9312., 9408., 9504., 9600., 9696., 9792., 9888., 9984.]
    elif f_ == 'f1':
        fe = np.float(4096)
        # 104 frequency bins        
        freq = [96.00, 112.0, 128.0, 144.0, 160.0, 176.0, 192.0, 208.0, \
                224.0, 240.0, 256.0, 272.0, 288.0, 304.0, 320.0, 336.0, \
                352.0, 368.0, 384.0, 400.0, 416.0, 432.0, 448.0, 464.0, \
                480.0, 496.0, 512.0, 528.0, 544.0, 560.0, 576.0, 592.0, \
                608.0, 624.0, 640.0, 656.0, 672.0, 688.0, 704.0, 720.0, \
                736.0, 752.0, 768.0, 784.0, 800.0, 816.0, 832.0, 848.0, \
                864.0, 880.0, 896.0, 912.0, 928.0, 944.0, 960.0, 976.0, \
                992.0, 1008., 1024., 1040., 1056., 1072., 1088., 1104., \
                1120., 1136., 1152., 1168., 1184., 1200., 1216., 1232., \
                1248., 1264., 1280., 1296., 1312., 1328., 1344., 1360., \
                1376., 1392., 1408., 1424., 1440., 1456., 1472., 1488., \
                1504., 1520., 1536., 1552., 1568., 1584., 1600., 1616., \
                1632., 1648., 1664., 1680., 1696., 1712., 1728., 1744.]
    elif f_ == 'f2':
        fe = np.float(256)
        # 96 frequency bins        
        freq = [7.00, 8.00, 9.00, 10.0, 11.0, 12.0, 13.0, 14.0, \
                15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, \
                23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0, 30.0, \
                31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, \
                39.0, 40.0, 41.0, 42.0, 43.0, 44.0, 45.0, 46.0, \
                47.0, 48.0, 49.0, 50.0, 51.0, 52.0, 53.0, 54.0, \
                55.0, 56.0, 57.0, 58.0, 59.0, 60.0, 61.0, 62.0, \
                63.0, 64.0, 65.0, 66.0, 67.0, 68.0, 69.0, 70.0, \
                71.0, 72.0, 73.0, 74.0, 75.0, 76.0, 77.0, 78.0, \
                79.0, 80.0, 81.0, 82.0, 83.0, 84.0, 85.0, 86.0, \
                87.0, 88.0, 89.0, 90.0, 91.0, 92.0, 93.0, 94.0, \
                95.0, 96.0, 97.0, 98.0, 99.0, 100., 101., 102.]
    # sinon on lève une exception
    else:
        raise TotoError('nom de fichier problématique ...' )        
    
    start_time = int_t[0].astype(int) * np.float(2)**(-16)
    relative_tid = (int_t.astype(int) - int_t[0].astype(int)) * np.float(2)**(-16)
    #print relative_tid
    relative_time = np.zeros(nspec, dtype=np.float)
    relative_time2 = np.zeros(nspec, dtype=np.float)
    if R == 3:
        relative_time3 = np.zeros(nspec, dtype=np.float)
    for i in range(nspec):    
        relative_time[i] = relative_tid[cnt_val*i]
        relative_time2[i] = relative_tid[cnt_val*i+1]
        if R == 3:
            relative_time3[i] = relative_tid[cnt_val*i+2]
            if relative_time[i] != relative_time3[i]:
                raise TypeError('##### WARNING : for i = {} time(i) NE time3(i) !!! #####'.format(i))        
        if relative_time[i] != relative_time2[i]:
                raise TypeError('##### WARNING : for i = {} time(i) NE time2(i) !!! #####'.format(i))     

    # the 25 real matrix components implemented by lfr (asm_c):
    #     B1      B2     B3     E1     E2  
    #  B1 00      01-02  03-04  05-06  07-08
    #  B2 cc-cc   09     10-11  12-13  14-15
    #  B3 cc-cc   cc-cc  16     17-18  19-20
    #  E1 cc-cc   cc-cc  cc-cc  21     22-23
    #  E2 cc-cc   cc-cc  cc-cc  cc-cc  24
    # conversion into the idl matrix format:
    asm_idl = np.zeros((nspec, nfreq, 5, 5), dtype=np.complex64)
    for l in range(nspec):
        for k in range(nfreq):  
            asm_idl[l,k,0,0] = data_block[l*nfreq+k,0 ] 
            asm_idl[l,k,1,1] = data_block[l*nfreq+k,9 ]
            asm_idl[l,k,2,2] = data_block[l*nfreq+k,16]
            asm_idl[l,k,3,3] = data_block[l*nfreq+k,21]
            asm_idl[l,k,4,4] = data_block[l*nfreq+k,24]
        
            asm_idl[l,k,0,1] = data_block[l*nfreq+k,1 ] + 1j*data_block[l*nfreq+k,2 ]
            asm_idl[l,k,0,2] = data_block[l*nfreq+k,3 ] + 1j*data_block[l*nfreq+k,4 ]
            asm_idl[l,k,0,3] = data_block[l*nfreq+k,5 ] + 1j*data_block[l*nfreq+k,6 ]
            asm_idl[l,k,0,4] = data_block[l*nfreq+k,7 ] + 1j*data_block[l*nfreq+k,8 ]
            asm_idl[l,k,1,2] = data_block[l*nfreq+k,10] + 1j*data_block[l*nfreq+k,11]
            asm_idl[l,k,1,3] = data_block[l*nfreq+k,12] + 1j*data_block[l*nfreq+k,13]
            asm_idl[l,k,1,4] = data_block[l*nfreq+k,14] + 1j*data_block[l*nfreq+k,15]
            asm_idl[l,k,2,3] = data_block[l*nfreq+k,17] + 1j*data_block[l*nfreq+k,18] 
            asm_idl[l,k,2,4] = data_block[l*nfreq+k,19] + 1j*data_block[l*nfreq+k,20]
            asm_idl[l,k,3,4] = data_block[l*nfreq+k,22] + 1j*data_block[l*nfreq+k,23] 
        
            asm_idl[l,k,1,0] = np.conj(asm_idl[l,k,0,1]) 
            asm_idl[l,k,2,0] = np.conj(asm_idl[l,k,0,2])
            asm_idl[l,k,3,0] = np.conj(asm_idl[l,k,0,3])  
            asm_idl[l,k,4,0] = np.conj(asm_idl[l,k,0,4])  
            asm_idl[l,k,2,1] = np.conj(asm_idl[l,k,1,2]) 
            asm_idl[l,k,3,1] = np.conj(asm_idl[l,k,1,3]) 
            asm_idl[l,k,4,1] = np.conj(asm_idl[l,k,1,4])  
            asm_idl[l,k,3,2] = np.conj(asm_idl[l,k,2,3])  
            asm_idl[l,k,4,2] = np.conj(asm_idl[l,k,2,4])  
            asm_idl[l,k,4,3] = np.conj(asm_idl[l,k,3,4])                  

    return {'asm_idl': asm_idl, 
            'packet_headers':  packet_headers, 
            'relative_time':   relative_time, 
            'start_time':      start_time, 
            'freq':            freq, 
            'fe':              fe, 
            'mode':            mode,                                 
            'fromfile':        file, 
            'names':           '[B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])'}
        
def get_lfr_decom_l1_hk(file):
    """    
    Routine qui lit les fichiers ascii .hk (i.e. HK parameters)
    crées par la routine de decom L1 LFR_packet_decom-linux-x86_64 de Bruno Katra    
    
INPUT:
    file:           file name (ex: 'data/2015_10_02_10_50_48_packet_record_SBM1.cf1')    
    
##### SOURCE data BEGIN - Offset in the file (byte pos): 17 #####
TIME : 
Sync bit : 1
Raw Integer :         4328245
Seconds :         66.0437774658203125
# LFR Mode | Spw enabled | Spw link state |  SC potential flag | PAS filter enabled | WD enabled | Calib enabled | Reset cause    
     0            1               5                 1                     0                1             0              1
SW version : 3.2.0.3
FPGA version : 1.1.91
# SCM T° | PCB T° | FPGA T°
 -24.78    -5.69     -2.09
# SC V F3 | SC E1 F3 | SC E2 F3
     431        426        386
# RW1 F1 flag | RW1 F2 flag | RW1 F3 flag | RW1 F4 flag
       0             0             0             0
# RW2 F1 flag | RW2 F2 flag | RW2 F3 flag | RW2 F4 flag
       0             0             0             0
# RW3 F1 flag | RW3 F2 flag | RW3 F3 flag | RW3 F4 flag
       0             0             0             0
# RW4 F1 flag | RW4 F2 flag | RW4 F3 flag | RW4 F4 flag
       0             0             0             0                

OUTPUT:

    {'VE1E2':           VE1E2_block,                
     'packet_headers':  packet_headers,             
     'relative_time':   relative_time,              
     'start_time':      start_time,                 
     'fe':              1.,                         
     'fromfile':        file,                       
     'names':           ''}
                
    version 1.0:      [17 mars 2017]             
    """
    
    # reading all the lines as an array of strings 
    with open(file, 'r') as file_in:
        block_lines = np.array(file_in.read().strip().split("\n"))    
        
    VE1E2_block_lines = block_lines[12::21]
    VE1E2_block = np.array([np.array(l.split()).astype(int) for l in VE1E2_block_lines])
    
    time_lines = block_lines[4::21]
    time = np.array([l.split()[2] for l in time_lines]).astype(float)    
    
    return  {'VE1E2':           VE1E2_block,        
             'relative_time':   time - time[0],          
             'start_time':      time[0],                 
             'fe':              1.,                         
             'fromfile':        file,                       
             'names':           ''}    

def bp2_denormalization(bp2_idl):
    nspec, nfreq, dim, _ = bp2_idl.shape
    for l in range(nspec):
        for i in range(dim):
            for j in range(dim):                    
                if i != j:
                    bp2_idl[l,:,i,j] = bp2_idl[l,:,i,j] * np.sqrt( bp2_idl[l,:,i,i].real * bp2_idl[l,:,j,j].real ) 
                                 
def get_lfr_decom_l1_bp2(file, denorm=True):
    """    
    Routine qui lit les fichiers ascii .2fi (i.e. normalized spectral matrices BP2, i= 0, 1 ou 2)
    créés par la routine de decom L1 LFR_packet_decom-linux-x86_64 de Bruno Katra    
    
INPUT:
    file:           file name (ex: 'data/2018_12_03_16_52_20_packet_record_NORMAL.2f0')           

OUTPUT:
    bp2_idl:        [B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])  (complex, in count^2 unit and denormalized by default)          
                    [ndarray(nspec, nfreq, dim_vector, dim_vector), with nspec = nb of spectral matrices 
                                                                     and nfreq = nb of frequency bins]                          
                    
    packet_headers: headers of the packets read in the file (string array)
                    [ndarray(nb_of_packets, 4)] 
                    nb    => number of bins in the packet (corresponding to one BP2)
                    syn_t => bit of synchronization of the packet
                    int_t => time of the first sample of the packet in unit of 2^-16 seconds (integer)
                             [corresponds to the time of the first sample of the first underlying FFT]
                    sec_t => same but in unit of secondes (floating)      
                    example: nb   syn_t   int_t (2^⁻16 sec)      sec_t (secondes)
                             11   1       1507320                22.9998779296875000    
                                                  
    relative_time:  time of the data in unit of seconds counted from the first BP2 (float array)
                    [ndarray(nspec)]
    start_time:     time of the first BP2 defined by the first sample of the first associated FFT 
                    (seconds from 2000/00/01 00:00)
    freq:           frequency bins (Hz) [ndarray(nfreq)]
                          
    fe:             fréquence d'échantillonnage (sampling frequency) read from the file name
    fromfile:       file name  
    names:          string enumerating the data field components            

    {'bp2_idl':         bp2_idl,                    
     'packet_headers':  packet_headers,             
     'relative_time':   relative_time,              
     'start_time':      start_time,                 
     'freq':            freq                        
     'fe':              fe,  
     'mode':            mode,                                                 
     'fromfile':        file,                       
     'names':           '[B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])'}
                
    version 1.0: first version with Python from the last version of the idl routine     [7 et 10 décembre 2018]             
    """                
        
    # reading all the lines as an array of strings
    with open(file, 'r') as file_in:
        block_lines = np.array(file_in.read().strip().split("\n"))
        
    # extracting the packet headers ...    
    first_char = np.array([str[0:40] for str in block_lines])
    index_nb_line  = np.where(first_char == '##### Number of blocks/samples to read :')[0]
    index_synt_line = index_nb_line + 2
    index_intt_line = index_nb_line + 3
    index_sect_line = index_nb_line + 4
    packet_headers_str = block_lines[ np.array([index_nb_line, index_synt_line, 
                                                index_intt_line, index_sect_line]) ].transpose()    
    nb  = np.array([str[40:40+3] for str in packet_headers_str[:,0]])
    # CUC format = 6 bytes =  1 bit  (synchro) 
    #                      + 31 bits (coarse time in integer secondes ) 
    #                      + 16 bits (fine time in fractional secondes)
    syn_t = np.array([str[17:17+1]  for str in packet_headers_str[:,1]]) # 1 bit synchro
    int_t = np.array([str[44:44+15] for str in packet_headers_str[:,2]]) # max (2^-16) sec = 2^47-1 = 140737488355327    (15 decimal)
    sec_t = np.array([str[16:16+27] for str in packet_headers_str[:,3]]) # max coarse  sec = 2^31-1 =      2147483647    (10 decimal)
                                                                         # min fine    sec = 2^-16  = 0.0000152587890625 (16 decimal)
    packet_headers = np.array([nb, syn_t, int_t, sec_t]).transpose()
    nb_of_packets = len(nb)
    bp2_nfreq = int(nb[0])
    nb_of_data_expected = nb_of_packets*bp2_nfreq*3  # AUTO + CROSS_RE + IM_RE => 3 lines per frequency bin  
    
    # extracting the data ...
    first_char = np.array([str[0:1] for str in block_lines])
    block_lines_data = block_lines[ np.where(first_char != '#')[0] ] 
    nb_of_data = len(block_lines_data)
    if nb_of_data != nb_of_data_expected:
        raise TypeError('NUMBER OF data IS NOT EQUAL TO THAT EXPECTED !!!!')
    
    block_lines_auto     = block_lines_data[0::3]
    block_lines_cross_re = block_lines_data[1::3]
    block_lines_cross_im = block_lines_data[2::3]
    data_block_auto =     np.array([l.split() for l in block_lines_auto]).astype(float)
    data_block_cross_re = np.array([l.split() for l in block_lines_cross_re]).astype(float)
    data_block_cross_im = np.array([l.split() for l in block_lines_cross_im]).astype(float)
    data_block_cross = data_block_cross_re + 1j*data_block_cross_im
    
    # the 25 real matrix components implemented by lfr (asm_c => same for bp2_c):
    #     B1      B2     B3     E1     E2  
    #  B1 00      00     01     02     03
    #  B2 cc      01     04     05     06
    #  B3 cc      cc     02     07     08
    #  E1 cc      cc     cc     03     09
    #  E2 cc      cc     cc     cc     04
    # conversion into the idl matrix format:
    nfreq = bp2_nfreq
    nspec = nb_of_packets 
    bp2_idl = np.zeros((nspec, nfreq, 5, 5), dtype=np.complex)
    for l in range(nspec):
        for k in range(nfreq):             
            bp2_idl[l,k,0,0] = data_block_auto[l*nfreq+k,0] 
            bp2_idl[l,k,1,1] = data_block_auto[l*nfreq+k,1]
            bp2_idl[l,k,2,2] = data_block_auto[l*nfreq+k,2]
            bp2_idl[l,k,3,3] = data_block_auto[l*nfreq+k,3] 
            bp2_idl[l,k,4,4] = data_block_auto[l*nfreq+k,4]

            bp2_idl[l,k,0,1] = data_block_cross[l*nfreq+k,0]    
            bp2_idl[l,k,0,2] = data_block_cross[l*nfreq+k,1] 
            bp2_idl[l,k,0,3] = data_block_cross[l*nfreq+k,2]
            bp2_idl[l,k,0,4] = data_block_cross[l*nfreq+k,3] 
            bp2_idl[l,k,1,2] = data_block_cross[l*nfreq+k,4] 
            bp2_idl[l,k,1,3] = data_block_cross[l*nfreq+k,5] 
            bp2_idl[l,k,1,4] = data_block_cross[l*nfreq+k,6] 
            bp2_idl[l,k,2,3] = data_block_cross[l*nfreq+k,7] 
            bp2_idl[l,k,2,4] = data_block_cross[l*nfreq+k,8] 
            bp2_idl[l,k,3,4] = data_block_cross[l*nfreq+k,9]

            bp2_idl[l,k,1,0] = np.conj(bp2_idl[l,k,0,1]) 
            bp2_idl[l,k,2,0] = np.conj(bp2_idl[l,k,0,2])
            bp2_idl[l,k,3,0] = np.conj(bp2_idl[l,k,0,3])  
            bp2_idl[l,k,4,0] = np.conj(bp2_idl[l,k,0,4])  
            bp2_idl[l,k,2,1] = np.conj(bp2_idl[l,k,1,2]) 
            bp2_idl[l,k,3,1] = np.conj(bp2_idl[l,k,1,3]) 
            bp2_idl[l,k,4,1] = np.conj(bp2_idl[l,k,1,4])  
            bp2_idl[l,k,3,2] = np.conj(bp2_idl[l,k,2,3])  
            bp2_idl[l,k,4,2] = np.conj(bp2_idl[l,k,2,4])  
            bp2_idl[l,k,4,3] = np.conj(bp2_idl[l,k,3,4])
            
    if denorm: bp2_denormalization(bp2_idl)
                                
    # the frequency table
    mode = {'L': 'NORMAL',
            '1': 'SBM1',
            '2': 'SBM2',
            'T': 'BURST'}[file[-5]]
    F = int(file[-1])
    fe = LFR_Fs[F]
    if mode == 'NORMAL':
        if F == 0:         
            deltaf = 768.  # fe = 24576Hz, df = fe/256 = 96Hz and deltaf = 8*df = 768Hz                   
            freq = [1968., # 1632., 1728., 1824., 1920., 2016., 2112., 2208., 2304., 
                    2736., # 2400., 2496., 2592., 2688., 2784., 2880., 2976., 3072., 
                    3504., # 3168., 3264., 3360., 3456., 3552., 3648., 3744., 3840., 
                    4272., # 3936., 4032., 4128., 4224., 4320., 4416., 4512., 4608., 
                    5040., # 4704., 4800., 4896., 4992., 5088., 5184., 5280., 5376., 
                    5808., # 5472., 5568., 5664., 5760., 5856., 5952., 6048., 6144., 
                    6576., # 6240., 6336., 6432., 6528., 6624., 6720., 6816., 6912., 
                    7344., # 7008., 7104., 7200., 7296., 7392., 7488., 7584., 7680., 
                    8112., # 7776., 7872., 7968., 8064., 8160., 8256., 8352., 8448., 
                    8880., # 8544., 8640., 8736., 8832., 8928., 9024., 9120., 9216., 
                    9648.] # 9312., 9408., 9504., 9600., 9696., 9792., 9888., 9984.] 
        elif F == 1:       
            deltaf = 128.  # fe = 4096Hz, df = 16Hz and deltaf = 8*df = 128Hz                   
            freq = [152.0, # 96.00, 112.0, 128.0, 144.0, 160.0, 176.0, 192.0, 208.0, 
                    280.0, # 224.0, 240.0, 256.0, 272.0, 288.0, 304.0, 320.0, 336.0, 
                    408.0, # 352.0, 368.0, 384.0, 400.0, 416.0, 432.0, 448.0, 464.0, 
                    536.0, # 480.0, 496.0, 512.0, 528.0, 544.0, 560.0, 576.0, 592.0, 
                    664.0, # 608.0, 624.0, 640.0, 656.0, 672.0, 688.0, 704.0, 720.0, 
                    792.0, # 736.0, 752.0, 768.0, 784.0, 800.0, 816.0, 832.0, 848.0, 
                    920.0, # 864.0, 880.0, 896.0, 912.0, 928.0, 944.0, 960.0, 976.0, 
                    1048., # 992.0, 1008., 1024., 1040., 1056., 1072., 1088., 1104., 
                    1176., # 1120., 1136., 1152., 1168., 1184., 1200., 1216., 1232., 
                    1304., # 1248., 1264., 1280., 1296., 1312., 1328., 1344., 1360., 
                    1432., # 1376., 1392., 1408., 1424., 1440., 1456., 1472., 1488., 
                    1560., # 1504., 1520., 1536., 1552., 1568., 1584., 1600., 1616., 
                    1688.] # 1632., 1648., 1664., 1680., 1696., 1712., 1728., 1744.]                 
        elif F == 2:                
            deltaf = 8.    # fe = 256Hz, df = 1Hz and deltaf = 8*df = 8Hz                 
            freq = [10.5,  # 7.00, 8.00, 9.00, 10.0, 11.0, 12.0, 13.0, 14.0, 
                    18.5,  # 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 
                    26.5,  # 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0, 30.0, 
                    34.5,  # 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 
                    42.5,  # 39.0, 40.0, 41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 
                    50.5,  # 47.0, 48.0, 49.0, 50.0, 51.0, 52.0, 53.0, 54.0, 
                    58.5,  # 55.0, 56.0, 57.0, 58.0, 59.0, 60.0, 61.0, 62.0, 
                    66.5,  # 63.0, 64.0, 65.0, 66.0, 67.0, 68.0, 69.0, 70.0, 
                    74.5,  # 71.0, 72.0, 73.0, 74.0, 75.0, 76.0, 77.0, 78.0, 
                    82.5,  # 79.0, 80.0, 81.0, 82.0, 83.0, 84.0, 85.0, 86.0, 
                    90.5,  # 87.0, 88.0, 89.0, 90.0, 91.0, 92.0, 93.0, 94.0, 
                    98.5]  # 95.0, 96.0, 97.0, 98.0, 99.0, 100., 101., 102.]
        else: raise TotoError('nom de fichier problématique ...') 
    if mode == 'SBM1':
        if F == 0:         
            deltaf = 384.          # fe = 24576Hz, df = fe/256 = 96Hz and deltaf = 4*df = 384Hz                                          
            freq = [1776., 2160.,  #1632., 1728., 1824., 1920., 2016., 2112., 2208., 2304., 
                    2544., 2928.,  #2400., 2496., 2592., 2688., 2784., 2880., 2976., 3072., 
                    3312., 3696.,  #3168., 3264., 3360., 3456., 3552., 3648., 3744., 3840., 
                    4080., 4464.,  #3936., 4032., 4128., 4224., 4320., 4416., 4512., 4608., 
                    4848., 5232.,  #4704., 4800., 4896., 4992., 5088., 5184., 5280., 5376., 
                    5616., 6000.,  #5472., 5568., 5664., 5760., 5856., 5952., 6048., 6144., 
                    6384., 6768.,  #6240., 6336., 6432., 6528., 6624., 6720., 6816., 6912., 
                    7152., 7536.,  #7008., 7104., 7200., 7296., 7392., 7488., 7584., 7680., 
                    7920., 8304.,  #7776., 7872., 7968., 8064., 8160., 8256., 8352., 8448., 
                    8688., 9072.,  #8544., 8640., 8736., 8832., 8928., 9024., 9120., 9216., 
                    9456., 9840.]  #9312., 9408., 9504., 9600., 9696., 9792., 9888., 9984.]    
        else: raise TotoError('nom de fichier problématique ...') 
    if mode in ['SBM2', 'BURST']:
        if F == 0:         
            deltaf = 384.          # fe = 24576Hz, df = fe/256 = 96Hz and deltaf = 4*df = 384Hz                                          
            freq = [1776., 2160.,  #1632., 1728., 1824., 1920., 2016., 2112., 2208., 2304., 
                    2544., 2928.,  #2400., 2496., 2592., 2688., 2784., 2880., 2976., 3072., 
                    3312., 3696.,  #3168., 3264., 3360., 3456., 3552., 3648., 3744., 3840., 
                    4080., 4464.,  #3936., 4032., 4128., 4224., 4320., 4416., 4512., 4608., 
                    4848., 5232.,  #4704., 4800., 4896., 4992., 5088., 5184., 5280., 5376., 
                    5616., 6000.,  #5472., 5568., 5664., 5760., 5856., 5952., 6048., 6144., 
                    6384., 6768.,  #6240., 6336., 6432., 6528., 6624., 6720., 6816., 6912., 
                    7152., 7536.,  #7008., 7104., 7200., 7296., 7392., 7488., 7584., 7680., 
                    7920., 8304.,  #7776., 7872., 7968., 8064., 8160., 8256., 8352., 8448., 
                    8688., 9072.,  #8544., 8640., 8736., 8832., 8928., 9024., 9120., 9216., 
                    9456., 9840.]  #9312., 9408., 9504., 9600., 9696., 9792., 9888., 9984.]    
        elif F == 1:                                                                        
            deltaf = 64.           # fe = 4096Hz, df = 16Hz and deltaf = 4*df = 64Hz
            freq = [120.0, 184.0,  #96.00, 112.0, 128.0, 144.0, 160.0, 176.0, 192.0, 208.0, 
                    248.0, 312.0,  #224.0, 240.0, 256.0, 272.0, 288.0, 304.0, 320.0, 336.0, 
                    376.0, 440.0,  #352.0, 368.0, 384.0, 400.0, 416.0, 432.0, 448.0, 464.0, 
                    504.0, 568.0,  #480.0, 496.0, 512.0, 528.0, 544.0, 560.0, 576.0, 592.0, 
                    632.0, 696.0,  #608.0, 624.0, 640.0, 656.0, 672.0, 688.0, 704.0, 720.0, 
                    760.0, 824.0,  #736.0, 752.0, 768.0, 784.0, 800.0, 816.0, 832.0, 848.0, 
                    888.0, 952.0,  #864.0, 880.0, 896.0, 912.0, 928.0, 944.0, 960.0, 976.0, 
                    1016., 1080.,  #992.0, 1008., 1024., 1040., 1056., 1072., 1088., 1104., 
                    1144., 1208.,  #1120., 1136., 1152., 1168., 1184., 1200., 1216., 1232., 
                    1272., 1336.,  #1248., 1264., 1280., 1296., 1312., 1328., 1344., 1360., 
                    1400., 1464.,  #1376., 1392., 1408., 1424., 1440., 1456., 1472., 1488., 
                    1528., 1592.,  #1504., 1520., 1536., 1552., 1568., 1584., 1600., 1616., 
                    1656., 1720.]  #1632., 1648., 1664., 1680., 1696., 1712., 1728., 1744.]                           
        else: raise TotoError('nom de fichier problématique ...')                 

    # the time series
    start_time = int_t[0].astype(int) * (2.)**(-16)
    relative_time = (int_t.astype(int) - int_t[0].astype(int)) * (2.)**(-16)
                 
    return {'bp2_idl':         bp2_idl,                    
            'packet_headers':  packet_headers,             
            'relative_time':   relative_time,              
            'start_time':      start_time,                 
            'freq':            freq,                      
            'fe':              fe,
            'mode':            mode,
            'fromfile':        file,                       
            'names':           '[B1 B2 B3 E1 E2] # CONJ([B1 B2 B3 E1 E2])'}   
            
def get_lfr_decom_l1_bp1(file):
    """    
    Routine qui lit les fichiers ascii .1fi (i.e. basic parameters BP1, i= 0, 1 ou 2)
    créés par la routine de decom L1 LFR_packet_decom-linux-x86_64 de Bruno Katra    
    
INPUT:
    file:           file name (ex: 'data/2018_12_04_11_41_08_packet_record_NORMAL.1f0')           

OUTPUT:
    psd_be:         B and E power spectral densities  (positive float) [ndarray(nspec,nfreq,2)] (in count^2 unit)
    dop:            degree of polarisation (0 ≤ float ≤ 1)             [ndarray(nspec,nfreq)]
    ellip:          wave ellipticity estimator (0 ≤ float ≤ 1)         [ndarray(nspec,nfreq)]
    nvec:           wave normal vector components (-1 ≤ float ≤ 1)     [ndarray(nspec,nfreq,3)]
    sx:             x-component of the Poynting vector (float in count^2 unit) +
                    1 argument bit value (complex plane divided into two sectors) [ndarray(nspec,nfreq,2)]
    vphi:           phase velocity estimator  (float) +        
                    1 argument bit value (complex plane divided into two sectors) [ndarray(nspec,nfreq,2)] 
                    [nspec = nb of spectral matrices, nfreq = nb of frequency bins]
                                  
    packet_headers: headers of the packets read in the file (string array)
                    [ndarray(nb_of_packets, 4)] 
                    nb    => number of bins in the packet (corresponding to one BP1)
                    syn_t => bit of synchronization of the packet
                    int_t => time of the first sample of the packet in unit of 2^-16 seconds (integer)
                             [corresponds to the time of the first sample of the first underlying FFT]
                    sec_t => same but in unit of secondes (floating)      
                    example: nb   syn_t   int_t (2^⁻16 sec)      sec_t (secondes)
                             11   1       1507320                22.9998779296875000  
                             
    relative_time:  time of the data in unit of seconds counted from the first BP1 (float array)
                    [ndarray(nspec)]
    start_time:     time of the first BP1 defined by the first sample of the first associated FFT 
                    (seconds from 2000/00/01 00:00)
    freq:           frequency bins (Hz) [ndarray(nfreq)]
                          
    fe:             fréquence d'échantillonnage (sampling frequency) read from the file name
    fromfile:       file name  
    names:          string enumerating the data field components      

    {'psd_be':          psd_be, 
     'dop':             dop, 
     'ellip':           ellip, 
     'nvec':            nvec, 
     'sx':              sx, 
     'vphi':            vphi,    
     'packet_headers':  packet_headers,             
     'relative_time':   relative_time,              
     'start_time':      start_time,                 
     'freq':            freq                        
     'fe':              fe,         
     'mode':            mode,                     
     'fromfile':        file,                       
     'names':           'psd_be, dop, ellip, nvec, sx, vphi'}
                
    version 1.0: first version with Python from the last version of the idl routine     [13,14 décembre 2018]             
    """
        
    # reading all the lines as an array of strings
    with open(file, 'r') as file_in:
        block_lines = np.array(file_in.read().strip().split("\n"))
        
    # extracting the packet headers ...    
    first_char = np.array([str[0:40] for str in block_lines])
    index_nb_line  = np.where(first_char == '##### Number of blocks/samples to read :')[0]
    index_synt_line = index_nb_line + 2
    index_intt_line = index_nb_line + 3
    index_sect_line = index_nb_line + 4
    packet_headers_str = block_lines[ np.array([index_nb_line, index_synt_line, 
                                                index_intt_line, index_sect_line]) ].transpose()    
    nb  = np.array([str[40:40+3] for str in packet_headers_str[:,0]])
    # CUC format = 6 bytes =  1 bit  (synchro) 
    #                      + 31 bits (coarse time in integer secondes ) 
    #                      + 16 bits (fine time in fractional secondes)
    syn_t = np.array([str[17:17+1]  for str in packet_headers_str[:,1]]) # 1 bit synchro
    int_t = np.array([str[44:44+15] for str in packet_headers_str[:,2]]) # max (2^-16) sec = 2^47-1 = 140737488355327    (15 decimal)
    sec_t = np.array([str[16:16+27] for str in packet_headers_str[:,3]]) # max coarse  sec = 2^31-1 =      2147483647    (10 decimal)
                                                                         # min fine    sec = 2^-16  = 0.0000152587890625 (16 decimal)
    packet_headers = np.array([nb, syn_t, int_t, sec_t]).transpose()
    nb_of_packets = len(nb)
    bp1_nfreq = int(nb[0])
    nb_of_data_expected = nb_of_packets*bp1_nfreq*11 #  PE + PB + NVEC_V0 + NVEC_V1 + NVEC_V2 + DOP + ELLIP + 
                                                     #  SX + SX Arg + VPHI + VPHI Arg => 11 lines per frequency bin 
    
    # extracting the data ...
    first_char = np.array([str[0:1] for str in block_lines])
    block_lines_data = block_lines[ np.where(first_char != '#')[0] ] 
    nb_of_data = len(block_lines_data)
    if nb_of_data != nb_of_data_expected:
        raise TypeError('NUMBER OF data IS NOT EQUAL TO THAT EXPECTED !!!!')
    
    data_block_pe       = np.array(block_lines_data[0::11]).astype(float)
    data_block_pb       = np.array(block_lines_data[1::11]).astype(float)
    data_block_nvec0    = np.array(block_lines_data[2::11]).astype(float)
    data_block_nvec1    = np.array(block_lines_data[3::11]).astype(float)
    data_block_nvec2    = np.array(block_lines_data[4::11]).astype(float)
    data_block_ellip    = np.array(block_lines_data[5::11]).astype(float)
    data_block_dop      = np.array(block_lines_data[6::11]).astype(float)
    data_block_sx       = np.array(block_lines_data[7::11]).astype(float)
    data_block_sx_arg   = np.array(block_lines_data[8::11]).astype(float)
    data_block_vphi     = np.array(block_lines_data[9::11]).astype(float)
    data_block_vphi_arg = np.array(block_lines_data[10::11]).astype(float)
    
    nfreq = bp1_nfreq
    nspec = nb_of_packets
    dop   = np.reshape(data_block_dop, (nspec, nfreq))
    ellip = np.reshape(data_block_ellip, (nspec, nfreq))    
    psd_be = np.stack((data_block_pb, data_block_pe), axis=1).reshape((nspec, nfreq, 2))
    nvec   = np.stack((data_block_nvec0, data_block_nvec1, data_block_nvec2), axis=1).reshape((nspec, nfreq, 3))
    sx     = np.stack((data_block_sx, data_block_sx_arg), axis=1).reshape((nspec, nfreq, 2))
    vphi   = np.stack((data_block_vphi, data_block_vphi_arg), axis=1).reshape((nspec, nfreq, 2)) 
    
    # take into account the precision uncertainty ~1/255 :
    # if n3~0 one may have 1-n1^2-n2^2 < 0 hence the NaN values ...
    indices_nan = np.nonzero(np.isnan(nvec))
    nvec[indices_nan] = 0.
    
    # the frequency table
    mode = {'L': 'NORMAL',
            '1': 'SBM1',
            '2': 'SBM2',
            'T': 'BURST'}[file[-5]]
    F = int(file[-1])
    fe = LFR_Fs[F]
    if mode == 'NORMAL':
        if F == 0:         
            deltaf = 768.  # fe = 24576Hz, df = fe/256 = 96Hz and deltaf = 8*df = 768Hz                   
            freq = [1968., # 1632., 1728., 1824., 1920., 2016., 2112., 2208., 2304., 
                    2736., # 2400., 2496., 2592., 2688., 2784., 2880., 2976., 3072., 
                    3504., # 3168., 3264., 3360., 3456., 3552., 3648., 3744., 3840., 
                    4272., # 3936., 4032., 4128., 4224., 4320., 4416., 4512., 4608., 
                    5040., # 4704., 4800., 4896., 4992., 5088., 5184., 5280., 5376., 
                    5808., # 5472., 5568., 5664., 5760., 5856., 5952., 6048., 6144., 
                    6576., # 6240., 6336., 6432., 6528., 6624., 6720., 6816., 6912., 
                    7344., # 7008., 7104., 7200., 7296., 7392., 7488., 7584., 7680., 
                    8112., # 7776., 7872., 7968., 8064., 8160., 8256., 8352., 8448., 
                    8880., # 8544., 8640., 8736., 8832., 8928., 9024., 9120., 9216., 
                    9648.] # 9312., 9408., 9504., 9600., 9696., 9792., 9888., 9984.] 
        elif F == 1:       
            deltaf = 128.  # fe = 4096Hz, df = 16Hz and deltaf = 8*df = 128Hz                   
            freq = [152.0, # 96.00, 112.0, 128.0, 144.0, 160.0, 176.0, 192.0, 208.0, 
                    280.0, # 224.0, 240.0, 256.0, 272.0, 288.0, 304.0, 320.0, 336.0, 
                    408.0, # 352.0, 368.0, 384.0, 400.0, 416.0, 432.0, 448.0, 464.0, 
                    536.0, # 480.0, 496.0, 512.0, 528.0, 544.0, 560.0, 576.0, 592.0, 
                    664.0, # 608.0, 624.0, 640.0, 656.0, 672.0, 688.0, 704.0, 720.0, 
                    792.0, # 736.0, 752.0, 768.0, 784.0, 800.0, 816.0, 832.0, 848.0, 
                    920.0, # 864.0, 880.0, 896.0, 912.0, 928.0, 944.0, 960.0, 976.0, 
                    1048., # 992.0, 1008., 1024., 1040., 1056., 1072., 1088., 1104., 
                    1176., # 1120., 1136., 1152., 1168., 1184., 1200., 1216., 1232., 
                    1304., # 1248., 1264., 1280., 1296., 1312., 1328., 1344., 1360., 
                    1432., # 1376., 1392., 1408., 1424., 1440., 1456., 1472., 1488., 
                    1560., # 1504., 1520., 1536., 1552., 1568., 1584., 1600., 1616., 
                    1688.] # 1632., 1648., 1664., 1680., 1696., 1712., 1728., 1744.]                 
        elif F == 2:                
            deltaf = 8.    # fe = 256Hz, df = 1Hz and deltaf = 8*df = 8Hz                 
            freq = [10.5,  # 7.00, 8.00, 9.00, 10.0, 11.0, 12.0, 13.0, 14.0, 
                    18.5,  # 15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 
                    26.5,  # 23.0, 24.0, 25.0, 26.0, 27.0, 28.0, 29.0, 30.0, 
                    34.5,  # 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 
                    42.5,  # 39.0, 40.0, 41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 
                    50.5,  # 47.0, 48.0, 49.0, 50.0, 51.0, 52.0, 53.0, 54.0, 
                    58.5,  # 55.0, 56.0, 57.0, 58.0, 59.0, 60.0, 61.0, 62.0, 
                    66.5,  # 63.0, 64.0, 65.0, 66.0, 67.0, 68.0, 69.0, 70.0, 
                    74.5,  # 71.0, 72.0, 73.0, 74.0, 75.0, 76.0, 77.0, 78.0, 
                    82.5,  # 79.0, 80.0, 81.0, 82.0, 83.0, 84.0, 85.0, 86.0, 
                    90.5,  # 87.0, 88.0, 89.0, 90.0, 91.0, 92.0, 93.0, 94.0, 
                    98.5]  # 95.0, 96.0, 97.0, 98.0, 99.0, 100., 101., 102.]
        else: raise TotoError('nom de fichier problématique ...') 
    if mode == 'SBM1':
        if F == 0:         
            deltaf = 384.          # fe = 24576Hz, df = fe/256 = 96Hz and deltaf = 4*df = 384Hz                                          
            freq = [1776., 2160.,  #1632., 1728., 1824., 1920., 2016., 2112., 2208., 2304., 
                    2544., 2928.,  #2400., 2496., 2592., 2688., 2784., 2880., 2976., 3072., 
                    3312., 3696.,  #3168., 3264., 3360., 3456., 3552., 3648., 3744., 3840., 
                    4080., 4464.,  #3936., 4032., 4128., 4224., 4320., 4416., 4512., 4608., 
                    4848., 5232.,  #4704., 4800., 4896., 4992., 5088., 5184., 5280., 5376., 
                    5616., 6000.,  #5472., 5568., 5664., 5760., 5856., 5952., 6048., 6144., 
                    6384., 6768.,  #6240., 6336., 6432., 6528., 6624., 6720., 6816., 6912., 
                    7152., 7536.,  #7008., 7104., 7200., 7296., 7392., 7488., 7584., 7680., 
                    7920., 8304.,  #7776., 7872., 7968., 8064., 8160., 8256., 8352., 8448., 
                    8688., 9072.,  #8544., 8640., 8736., 8832., 8928., 9024., 9120., 9216., 
                    9456., 9840.]  #9312., 9408., 9504., 9600., 9696., 9792., 9888., 9984.]    
        else: raise TotoError('nom de fichier problématique ...') 
    if mode in ['SBM2', 'BURST']:
        if F == 0:         
            deltaf = 384.          # fe = 24576Hz, df = fe/256 = 96Hz and deltaf = 4*df = 384Hz                                          
            freq = [1776., 2160.,  #1632., 1728., 1824., 1920., 2016., 2112., 2208., 2304., 
                    2544., 2928.,  #2400., 2496., 2592., 2688., 2784., 2880., 2976., 3072., 
                    3312., 3696.,  #3168., 3264., 3360., 3456., 3552., 3648., 3744., 3840., 
                    4080., 4464.,  #3936., 4032., 4128., 4224., 4320., 4416., 4512., 4608., 
                    4848., 5232.,  #4704., 4800., 4896., 4992., 5088., 5184., 5280., 5376., 
                    5616., 6000.,  #5472., 5568., 5664., 5760., 5856., 5952., 6048., 6144., 
                    6384., 6768.,  #6240., 6336., 6432., 6528., 6624., 6720., 6816., 6912., 
                    7152., 7536.,  #7008., 7104., 7200., 7296., 7392., 7488., 7584., 7680., 
                    7920., 8304.,  #7776., 7872., 7968., 8064., 8160., 8256., 8352., 8448., 
                    8688., 9072.,  #8544., 8640., 8736., 8832., 8928., 9024., 9120., 9216., 
                    9456., 9840.]  #9312., 9408., 9504., 9600., 9696., 9792., 9888., 9984.]    
        elif F == 1:                                                                        
            deltaf = 64.           # fe = 4096Hz, df = 16Hz and deltaf = 4*df = 64Hz
            freq = [120.0, 184.0,  #96.00, 112.0, 128.0, 144.0, 160.0, 176.0, 192.0, 208.0, 
                    248.0, 312.0,  #224.0, 240.0, 256.0, 272.0, 288.0, 304.0, 320.0, 336.0, 
                    376.0, 440.0,  #352.0, 368.0, 384.0, 400.0, 416.0, 432.0, 448.0, 464.0, 
                    504.0, 568.0,  #480.0, 496.0, 512.0, 528.0, 544.0, 560.0, 576.0, 592.0, 
                    632.0, 696.0,  #608.0, 624.0, 640.0, 656.0, 672.0, 688.0, 704.0, 720.0, 
                    760.0, 824.0,  #736.0, 752.0, 768.0, 784.0, 800.0, 816.0, 832.0, 848.0, 
                    888.0, 952.0,  #864.0, 880.0, 896.0, 912.0, 928.0, 944.0, 960.0, 976.0, 
                    1016., 1080.,  #992.0, 1008., 1024., 1040., 1056., 1072., 1088., 1104., 
                    1144., 1208.,  #1120., 1136., 1152., 1168., 1184., 1200., 1216., 1232., 
                    1272., 1336.,  #1248., 1264., 1280., 1296., 1312., 1328., 1344., 1360., 
                    1400., 1464.,  #1376., 1392., 1408., 1424., 1440., 1456., 1472., 1488., 
                    1528., 1592.,  #1504., 1520., 1536., 1552., 1568., 1584., 1600., 1616., 
                    1656., 1720.]  #1632., 1648., 1664., 1680., 1696., 1712., 1728., 1744.]                           
        else: raise TotoError('nom de fichier problématique ...')                 

    # the time series
    start_time = int_t[0].astype(int) * (2.)**(-16)
    relative_time = (int_t.astype(int) - int_t[0].astype(int)) * (2.)**(-16)
            
    return {'psd_be':          psd_be, 
            'dop':             dop, 
            'ellip':           ellip, 
            'nvec':            nvec, 
            'sx':              sx, 
            'vphi':            vphi,    
            'packet_headers':  packet_headers,             
            'relative_time':   relative_time,              
            'start_time':      start_time,                 
            'freq':            freq,                      
            'fe':              fe,     
            'mode':            mode,                                                     
            'fromfile':        file,                       
            'names':           'psd_be, dop, ellip, nvec, sx, vphi'}                       
    
    
