# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt
import copy

from lib_lfr.global_params import *
from lib_lfr.time_routines import index_from_date

from lib_signal.filters import filtre_passe_bande
from lib_signal.spectra import computeFFT

def select_oneSWF(snap_id, swf_datetime, swf_B, swf_E, swf_V, F_list=[2,1,0], 
                  bname=None, names=None, echo=True):
    """
    select one snapshot identified either by its index (int) or a time indication (datetime)
    """    
    snap_size = 2048    
    oneSWF = {'B': 3*[None], 
               'E': 3*[None],
               'V': 3*[None],
               'datetime': 3*[None],                
               'isnap': 3*[None], 
               'fromfile': None if bname == None else bname, 
               'names': names 
              }        
    for F in F_list:        
        if isinstance(snap_id, (int,  np.int64)):
              isnap = snap_id
        else:
              isnap = index_from_date(snap_id, swf_datetime[F]) // 2048  
        itime = isnap * snap_size    
        if echo:
            print('F = %s'%(F))
            print('snap_id: %s'%(snap_id))      
            print('isnap: %d, tsnap: %s'%(isnap, swf_datetime[F][itime]))                   
            print()
        oneSWF['B'][F] = swf_B[F][itime:itime+snap_size, :]
        oneSWF['E'][F] = swf_E[F][itime:itime+snap_size, :]
        oneSWF['V'][F] = swf_V[F][itime:itime+snap_size, :]
        oneSWF['datetime'][F] = swf_datetime[F][itime:itime+snap_size]        
        oneSWF['isnap'][F] = isnap
    return oneSWF

def filtering_oneSWF(snapBEV, f1=None, f2=None, F_list=[2,1,0], twindow='trapezoid5', rm_dc=True, echo=False):
    oneSWF = copy.deepcopy(snapBEV)
    for F in F_list:
        if echo: print('F = %s'%(F))
        oneSWF['B'][F] = filtre_passe_bande(oneSWF['B'][F], LFR_Fs[F], f1=f1, f2=f2, 
                                             fwindow='rectangular', twindow=twindow, 
                                             rm_dc=rm_dc, echo=echo)
        oneSWF['E'][F] = filtre_passe_bande(oneSWF['E'][F], LFR_Fs[F], f1=f1, f2=f2, 
                                             fwindow='rectangular', twindow=twindow, 
                                             rm_dc=rm_dc, echo=echo)        
        oneSWF['V'][F] = filtre_passe_bande(oneSWF['V'][F], LFR_Fs[F], f1=f1, f2=f2, 
                                             fwindow='rectangular', twindow=twindow, 
                                             rm_dc=rm_dc, echo=echo)
        if echo: print()
    return oneSWF       

def fig_oneSWF(snapBEV, level='L2', date_fmt_s=None, spectra=False, window=False, removeMean=False, 
               ampl_range=None, freq_range=None, loglog=True, figsize=(9,12), hspace=0.05, wspace=None, 
               ylabels_l2_wf=None, ylabels_l1_wf=None, ylabels_l2_spectra=None, ylabels_l1_spectra=None,
               fname=None, dpi=None):
    """
    make a figure with all the B & E waveform components of a given snapshot @F2 and @F1, by default,
    in SRF for L2 data, in native frame for L1 data; optionnally plot the corresponding spectra
    """    
    def set_loglog_or_semilogy(loglog, ax):
        if loglog: ax.loglog()
        else: ax.semilogy()   
    def set_ylabel_l2_or_l1(level, label_l2, label_l1, ax):            
        if level == 'L2': ax.set_ylabel(label_l2)   
        else: ax.set_ylabel(label_l1)            
    if date_fmt_s != None:
        default_date_fmt_s = plt.rcParams["date.autoformatter.second"]        
        plt.rcParams["date.autoformatter.second"] = date_fmt_s
    if ampl_range == None: ampl_range = 2*[[None, None]]
    if freq_range == None: freq_range = 2*[None]            
    if ylabels_l2_wf == None:
        ylabels_l2_wf = ['$B_X$ (SRF)\nnT', '$B_Y$ (SRF)\nnT', '$B_Z$ (SRF)\nnT', 
                         '$E_Y$ (SRF)\nmV/m', '$E_Z$ (SRF)\nmV/m']
    if ylabels_l1_wf == None:
        ylabels_l1_wf = ['$B_1$\ncount', '$B_2$\ncount', '$B_3$\ncount', 
                         '$E_1$\ncount', '$E_2$\ncount']                             
    if ylabels_l2_spectra == None: ylabels_l2_spectra = ['log$_{10}$('+'nT'+'$^2$/Hz)', 
                                                         'log$_{10}$('+'[V/m]'+'$^2$/Hz)']
    if ylabels_l1_spectra == None: ylabels_l1_spectra = 'log$_{10}$('+'count'+'$^2$/Hz)'                                                                                                     
    fig, axarr = plt.subplots(3+2, 2 if spectra else 1, squeeze=False, figsize=figsize, 
                              sharex='col' if spectra else True, sharey=False)
    plt.subplots_adjust(hspace=hspace, wspace=wspace)     
    F = 2
    for j in [0, 1, 2]:
        axarr[j, 0].plot(snapBEV['datetime'][F], snapBEV['B'][F][:, j], label='F%d'%(F))
        axarr[j, 0].xaxis.set_ticks_position('both')    
        axarr[j, 0].yaxis.set_ticks_position('both')        
        set_ylabel_l2_or_l1(level, ylabels_l2_wf[j], ylabels_l1_wf[j], axarr[j, 0])    
        if spectra:
            fft = computeFFT(snapBEV['B'][F][:, j], fs=LFR_Fs[F], window=window, removeMean=removeMean) 
            df = LFR_Fs[F] / len(snapBEV['datetime'][F])            
            psd = fft[1]**2 / df
            axarr[j, 1].plot(fft[0], psd, label='F%d'%(F))
            axarr[j, 1].xaxis.set_ticks_position('both')    
            axarr[j, 1].yaxis.set_ticks_position('both')              
            set_ylabel_l2_or_l1(level, ylabels_l2_spectra[0], ylabels_l1_spectra, axarr[j, 1])                    
            set_loglog_or_semilogy(loglog, axarr[j, 1])           
    for j in [3, 4]:
        k = 1e3 if level == 'L2' else 1
        axarr[j, 0].plot(snapBEV['datetime'][F], snapBEV['E'][F][:, j-3]*k, label='F%d'%(F))
        axarr[j, 0].xaxis.set_ticks_position('both')    
        axarr[j, 0].yaxis.set_ticks_position('both')         
        set_ylabel_l2_or_l1(level, ylabels_l2_wf[j], ylabels_l1_wf[j], axarr[j, 0])                
        if spectra:
            fft = computeFFT(snapBEV['E'][F][:, j-3], fs=LFR_Fs[F], window=window, removeMean=removeMean) 
            psd = fft[1]**2 / df
            axarr[j, 1].plot(fft[0], psd, label='F%d'%(F))
            axarr[j, 1].xaxis.set_ticks_position('both')    
            axarr[j, 1].yaxis.set_ticks_position('both')                                                                 
            set_ylabel_l2_or_l1(level, ylabels_l2_spectra[1], ylabels_l1_spectra, axarr[j, 1])                                    
            set_loglog_or_semilogy(loglog, axarr[j, 1])                                       
    F = 1
    for j in [0, 1, 2]:
        axarr[j, 0].plot(snapBEV['datetime'][F], snapBEV['B'][F][:, j], label='F%d'%(F))
        if spectra:
            fft = computeFFT(snapBEV['B'][F][:, j], fs=LFR_Fs[F], window=window, removeMean=removeMean) 
            df = LFR_Fs[F] / len(snapBEV['datetime'][F])            
            psd = fft[1]**2 / df
            axarr[j, 1].plot(fft[0], psd, label='F%d'%(F))    
            axarr[j, 1].set_ylim(ampl_range[0])  
    for j in [3, 4]:
        axarr[j, 0].plot(snapBEV['datetime'][F], snapBEV['E'][F][:, j-3]*k, label='F%d'%(F))   
        if spectra:
            fft = computeFFT(snapBEV['E'][F][:, j-3], fs=LFR_Fs[F], window=window, removeMean=removeMean) 
            psd = fft[1]**2 / df
            axarr[j, 1].plot(fft[0], psd, label='F%d'%(F))
            axarr[j, 1].set_ylim(ampl_range[1])    
    axarr[0, 0].legend(loc='best')      
    axarr[-1, 0].set_xlabel(str(snapBEV['datetime'][2][0])[0:10], fontsize=12)          
    if spectra:
        axarr[0, 1].legend(loc='best') 
        axarr[0, 1].set_xlim(freq_range)          
        plt.setp(axarr[-1, 0].get_xticklabels(), rotation=30, ha="right", rotation_mode='anchor')
        fig.suptitle('%s    (snapshot index = %d)'%(snapBEV['fromfile'], snapBEV['isnap'][2]), y=0.91)
        axarr[-1, 1].set_xlabel('frequency (Hz)', fontsize=11)         
    else:
        fig.autofmt_xdate(rotation=30, ha='right')
        axarr[0, 0].set_title('%s    (snapshot index = %d)'%(snapBEV['fromfile'], snapBEV['isnap'][2])) 
    if date_fmt_s != None:        
        plt.rcParams["date.autoformatter.second"] = default_date_fmt_s   
    # saving      
    if fname != None:
        default_dpi = plt.rcParams["savefig.dpi"]        
        plt.rcParams["savefig.dpi"] = 'figure' if dpi == None else dpi 
        plt.savefig(fname, bbox_inches='tight', pad_inches=0.2)
        plt.rcParams["savefig.dpi"] = default_dpi    
