# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from lib_lfr.global_params import *
from datetime import timedelta
                
def fig_TF_S1S2Cal(calib, params=(None,None,None), echo=True, fname=None, \
                   phi_range=None, ampl_range=None, freq_range=None):
    F = int(calib[0]['datatype'][-1]) 
    dtype = calib[0]['datatype'][:3]    
    if echo:
        print("nb of frequencies:  %d"%len(calib))
        print('calib[0]:')
        print(calib[0])
    chs = calib[0]['comp']        
    if 'calib' not in chs:
        return print("ABSOLUTE phase calibration is needed ...")
    isg = [name2index_sm(ch) for ch in chs]        
    f_injected = np.array([cal['f_injected'] for cal in calib])
    calib_V2count = np.array([cal['calib_V2count'] for cal in calib])
    cross_norm = np.array([cal['cross_norm'] for cal in calib])
    delta_phi_add = np.array([cal['delta_phi_add'] if (cal['delta_phi_add'] != None) else 0. for cal in calib])        
    status_auto = [cal['status_auto'] for cal in calib]
    #status_cross = [cal['status_cross'] for cal in calib]
    #status_param = [cal['status_param'] for cal in calib]
    #phi_S1S2Cal = [phi_S1Cal, phi_S2Cal, phi_S1S2]    
    phi_S1S2Cal = np.array([[np.angle(cross_n[1],deg=False) + np.deg2rad(dphi2add), \
                             np.angle(cross_n[2],deg=False) + np.deg2rad(dphi2add), \
                             np.angle(cross_n[0],deg=False) ] \
                            for cross_n, dphi2add in zip(cross_norm, delta_phi_add)])
    for j in range(3):
        phi_S1S2Cal[:,j] = np.rad2deg(np.unwrap(phi_S1S2Cal[:,j]))   
    fig, axarr = plt.subplots(2,1, figsize=(16,16))  
    plt.subplots_adjust(hspace=.05)    
    axarr[0].set_title("Transfer Function @F%d (%s)"%(F,dtype), fontsize=18)      
    for j in range(2):
        axarr[0].plot(f_injected, calib_V2count[:,j], linewidth=2, marker=markers_sm[isg[j]], \
                      label=chs_sm[isg[j]], color=colors_sm[isg[j]])          
    axarr[0].legend(loc='best')
    axarr[0].set_ylabel('Amplitude (count/V)', fontsize=14)
    axarr[0].text(0.05, 0.1, 'BW = {}\nR0, R1, R2 = {}\nSP0, SP1 = {}'.format(*params), \
                  horizontalalignment='left', transform=axarr[0].transAxes, fontsize=14)      
    #axarr[0].set_xlabel('Frequency (Hz)', fontsize=12)
    #axarr[0].semilogy()
    #axarr[0].loglog()
    if freq_range == None:
        xrange = (0,LFR_Fs[F]/2)
    else:
        xrange = freq_range            
    axarr[0].set_xlim(xrange)    
    if F==0:
        axarr[0].set_ylim(400,9300)
    if F==1:
        axarr[0].set_ylim(1000,9000)        
    if F==2:
        axarr[0].set_ylim(1000,9000)
    if ampl_range != None:
        axarr[0].set_ylim(ampl_range)
    for j in [0, 1]:
        axarr[1].plot(f_injected, phi_S1S2Cal[:,j], linewidth=2, marker=markers_sm[isg[j]], \
                      label=chs_sm[isg[j]], color=colors_sm[isg[j]])   
    axarr[1].legend(loc='best')    
    axarr[1].set_ylabel('Phase (°)', fontsize=14)
    axarr[1].set_xlabel('Frequency (Hz)', fontsize=12)
    axarr[1].set_xlim(xrange)       
    if F==0:
        axarr[1].set_ylim(-650,50)
    if F==1:
        axarr[1].set_ylim(-400,10)
    if F == 2:
        axarr[1].set_ylim(-400,20)   
    if phi_range != None:
        axarr[1].set_ylim(phi_range)             
    if fname != None:    
        if dtype == 'SWF':
            if calib[-2]['part_snap']:
                fname = fname.replace('.svg','_partsnap{}.svg'.format(calib[-2]['part_snap']))            
        plt.savefig(fname)
    plt.show()        
    if echo:
        print("TF amplitudes (count/V), autocross status (OK/KO), correlation coefficients (normalized) and phases (°):")        
        print("     Freq           {S1:2s}      {S2:2s}     auto   rho{S1}Cal rho{S2}Cal rho{S1}{S2}    phi{S1}    phi{S2}    phi{S1}{S2}".\
              format(S1=chs[0], S2=chs[1]))
        for f, cal , s_auto,  cross_n, phi in zip(f_injected, calib_V2count, status_auto, cross_norm, phi_S1S2Cal):
            print("{:9.3f} Hz => [{:7.2f} {:7.2f}] [{:s} {:s}] [{:7.5f} {:7.5f} {:7.5f}] [{:8.3f} {:8.3f} {:8.3f}]". \
            format(f, cal[0], cal[1], s_auto[0], s_auto[1], abs(cross_n[1]), abs(cross_n[2]), abs(cross_n[0]), *phi))
                                 
def fig_TF_allCal(calib, params=(None,None,None), echo=True, fname=None, dB=False, logx=False, 
                  phi_range=None, ampl_range=None, freq_range=None, unwrap_plus=None, figsize=(10,16)):
    F = int(calib[0]['datatype'][-1]) 
    dtype = calib[0]['datatype'][:3]
    if echo:
        print("nb of frequencies:  %d"%len(calib))
        print('calib[0]:')
        print(calib[0])
    chs = calib[0]['comp']       
    if 'calib' not in chs:
        return print("ABSOLUTE phase calibration is needed ...")    
    icomp = [name2index_sm(ch) for ch in chs]        
    f_injected = np.array([cal['f_injected'] for cal in calib])
    calib_V2count = np.array([cal['calib_V2count'] for cal in calib])
    cross_norm = np.array([cal['cross_norm'] for cal in calib])
    delta_phi_add = np.array([cal['delta_phi_add'] if (cal['delta_phi_add'] != None) else 0. for cal in calib])    
    status_auto = [cal['status_auto'] for cal in calib]
    status_param = [cal['status_param'] for cal in calib]    
    # phi_allCal is a subset of [phi_B1Cal, phi_B2Cal, phi_B3Cal, phi_E1Cal, phi_E2Cal, phi_VCal]        
    l = 0 
    l_allCal = []
    for icomp_i in icomp:
        for icomp_j in icomp:
            if icomp_j > icomp_i:
                if icomp_j == 6:
                    l_allCal.append(l)                                
                l = l + 1
    ncompCal = len(l_allCal)
    phi_allCal = np.array([[np.angle(cross_n[l], deg=False) + np.deg2rad(dphi2add) for l in l_allCal] \
                           for cross_n, dphi2add in zip(cross_norm, delta_phi_add)])        
    for j in range(ncompCal):
        phi_allCal[:,j] = np.rad2deg(np.unwrap(phi_allCal[:,j], discont=np.pi))    
        if unwrap_plus != None:
            index_toapply = unwrap_plus[0]
            m_2pi_toadd = unwrap_plus[1]              
            phi_allCal[index_toapply,j] = phi_allCal[index_toapply,j] + m_2pi_toadd
    fig, axarr = plt.subplots(2, 1, figsize=figsize)  
    plt.subplots_adjust(hspace=.05)    
    axarr[0].set_title("Transfer Function @F%d (%s)"%(F,dtype), fontsize=14)  
    for j in range(ncompCal):
        axarr[0].plot(f_injected, 20*np.log10(calib_V2count[:,j]) if dB else calib_V2count[:,j], \
                      linewidth=2, marker=markers_sm[icomp[j]], label=chs_sm[icomp[j]], color=colors_sm[icomp[j]])
    axarr[0].legend(loc='best')
    axarr[0].set_ylabel('Amplitude (count/V)' if not dB else 'Amplitude (dB[count/V])', fontsize=14)
    axarr[0].text(0.05, 0.1, 'BW = {}\nR0, R1, R2 = {}\nSP0, SP1 = {}'.format(*params), \
                  horizontalalignment='left', transform=axarr[0].transAxes, fontsize=14)      
    if logx:
        for j in range(0,2):
            axarr[j].semilogx()
            #axarr[j].loglog()
    if freq_range == None:
        xrange = (0,LFR_Fs[F]/2)
    else:
        xrange = freq_range            
    axarr[0].set_xlim(xrange)
    if ampl_range == None:
        ampl_range = [[400,9300], [1000,9000], [1000,9000], [1000,9000]][F]
    axarr[0].set_ylim(ampl_range if not dB else np.rint(20*np.log10(ampl_range)+np.array([-0.5,0.5])))
    for j in range(ncompCal):
        axarr[1].plot(f_injected, phi_allCal[:,j], linewidth=1, marker=markers_sm[icomp[j]], \
                      label=chs_sm[icomp[j]], color=colors_sm[icomp[j]])    
    axarr[1].legend(loc='best')    
    axarr[1].set_ylabel('Phase (°)', fontsize=14)
    axarr[1].set_xlabel('Frequency (Hz)', fontsize=12)
    axarr[1].set_xlim(xrange)
    if phi_range == None:
        phi_range = [[-650,50], [-400,20], [-400,20], [-10, 0]][F]  
    axarr[1].set_ylim(phi_range)            
    if fname != None:    
        if dtype == 'SWF':
            if calib[-2]['part_snap']:
                fname = fname.replace('.svg','_partsnap{}.svg'.format(calib[-2]['part_snap']))
        plt.savefig(fname.replace('.svg','_dBlog.svg') if (dB and logx) else \
                    fname.replace('.svg','_dB.svg') if dB else fname.replace('.svg','_logx.svg') if logx else \
                    fname, bbox_inches='tight', pad_inches=0.2)
    plt.show()        
    if echo:
        print("TF amplitudes (count/V):")                
        str_head = "     Freq     "
        str_cal = "{:9.3f} Hz => ["
        str_s_auto = "["                
        for j in range(ncompCal):
            str_head += "      {:2s}"
            str_cal += " {:7.2f}"
            str_s_auto += " {:s}"                                                
        str_head += "        auto status " + (ncompCal)*" " + " param status"   
        str_cal += " ] "
        str_s_auto += " ] "
        str_s_param = (6-ncompCal)*" " + "  [ {:s} ]"
        print(str_head.format(*chs[:-1])) 
        for f, cal , s_auto, s_param in zip(f_injected, calib_V2count, status_auto, status_param):
            print(str_cal.format(f, *cal[:-1]) + str_s_auto.format(*s_auto[:-1]) + str_s_param.format(s_param))  
        print()    
        print("Signal/calib correlation coefficients (normalized) and absolute phases (°):")              
        str_head1 = "     Freq        "
        str_head2 = "  "        
        str_rho = "{:9.3f} Hz => ["
        str_phi = "["
        for j in range(ncompCal):
            str_head1 += "   {:2s}"
            str_head2 += "      {:2s}"            
            str_rho += " {:4.2f}"
            str_phi += " {:7.2f}"
        str_rho += " ] "
        str_phi += " ]"
        # rho_allCal  is a subset of [rho_B1Cal, rho_B2Cal, rho_B3Cal, rho_E1Cal, rho_E2Cal, rho_VCal]
        rho_allCal = np.array([[abs(cross_n[l]) for l in l_allCal] for cross_n in cross_norm])        
        print(str_head1.format(*chs[:-1]) + str_head2.format(*chs[:-1]))                
        for f, rho, phi in zip(f_injected, rho_allCal, phi_allCal):        
            print(str_rho.format(f, *rho) + str_phi.format(*phi))
            
def fig_TF_RPW_allRel(calib, params=None, echo=True, fname=None, dB=None, 
                      phi_range=None, ampl_range=None, freq_range=None,   
                      phi_range1=None, phi_range2=None, phi_range3=None,
                      scaleE=1., logx=None, test_phase=None):
    F = int(calib[0]['datatype'][-1]) 
    dtype = calib[0]['datatype'][:3]
    if params == None: params = {'LFR':(1, 0, 0), 
                                 'BIAS':('None', 'None', 'None', 1, None),              
                                 'SCM':('None', 'None', 'None')}  
    if echo:
        print("nb of frequencies:  %d"%len(calib))
        print('calib[0]:')
        print(calib[0])
    chs = calib[0]['comp'] 
    icomp = [name2index_sm(ch) for ch in chs]        
    f_injected = np.array([cal['f_injected'] for cal in calib])
    calib_V2count = np.array([cal['calib_V2count'] for cal in calib])
    cross_norm = np.array([cal['cross_norm'] for cal in calib])
    status_auto = [cal['status_auto'] for cal in calib]
    status_param = [cal['status_param'] for cal in calib]        
    ncomp = len(icomp)   
    fig1, axarr1 = plt.subplots(2, 1, figsize=(16,16))
    fig1.subplots_adjust(hspace=.05)    
    fig2, axarr2 = plt.subplots(3, 1, figsize=(16,16*29.7/21))      
    fig2.subplots_adjust(hspace=.1)  
    axarr1[0].set_title("RPW SCM and BIAS Transfer Functions @F%d (%s)"%(F,dtype), fontsize=18)
    for j in range(ncomp):
        if dB:
            axarr1[0].plot(f_injected, 20*np.log10(calib_V2count[:,j]/params['BIAS'][3]/scaleE if icomp[j] in [3,4,5] else \
                                                   calib_V2count[:,j]), linewidth=2, marker=markers_sm[icomp[j]], 
                           label=chs_sm[icomp[j]], color=colors_sm[icomp[j]])
        else:    
            axarr1[0].plot(f_injected, calib_V2count[:,j]/params['BIAS'][3]/scaleE if icomp[j] in [3,4,5] else \
                                       calib_V2count[:,j], linewidth=2, marker=markers_sm[icomp[j]], 
                           label=chs_sm[icomp[j]], color=colors_sm[icomp[j]])                
    axarr1[0].text(0.15, 0.08, 'BW = {}\nR0, R1, R2 = {}\nSP0, SP1 = {}'.format(*params['LFR']), 
                   horizontalalignment='left', transform=axarr1[0].transAxes, fontsize=14)
    str_bias = 'BIAS1 = {}\nBIAS2 = {}\nBIAS3 = {}\nGain = {}\nIMP = {}' if params['LFR'][1]  == 1 else \
               'BIAS1 = {}\nBIAS4 = {}\nBIAS5 = {}\nGain = {}\nIMP = {}'
    axarr1[0].text(0.35, 0.08, str_bias.format(*params['BIAS']), 
                   horizontalalignment='left', transform=axarr1[0].transAxes, fontsize=14)
    if test_phase != None: axarr1[0].text(0.55, 0.08, '\n\n\n{}'.format(test_phase), 
                           horizontalalignment='left', transform=axarr1[0].transAxes, fontsize=14)    
    #axarr1[0].semilogy()
    #axarr1[0].loglog()  
    if logx:
        for j in range(0,2):
            axarr1[j].semilogx()
        for j in range(0,3):
            axarr2[j].semilogx()
    if freq_range == None:
        xrange = (0,LFR_Fs[F]/2)
    else:
        xrange = freq_range
    if ampl_range == None:
        if F==0:
            ampl_range = [4000,9000]                      
        if F==1:
            ampl_range = [1000,9000]        
        if F==2:
            ampl_range = [10,9000]        
    if dB:
        axarr1[0].set_ylim(np.rint(20*np.log10(ampl_range)+np.array([-0.5,0.5])))
        axarr1[0].set_ylabel('Amplitude (dB[{:5.3f} x count/V] or dB[count/nT])'.format(params['BIAS'][3]*scaleE), fontsize=14)
    else:
        axarr1[0].set_ylim(ampl_range)
        axarr1[0].set_ylabel('Amplitude ({:5.3f} x count/V or count/nT)'.format(params['BIAS'][3]*scaleE), fontsize=14)        
    cross_names = [chs_sm[i]+chs_sm[j] for i in icomp for j in icomp if j > i ]
    ncomp_cross = len(cross_names)    
    cross_markers = ncomp_cross*['']
    phi_cross = np.array([[np.angle(cross_n[l], deg=False) for l in range(ncomp_cross)] for cross_n in cross_norm])  
    for l in range(ncomp_cross):
        cross_markers[l] = '8' if 'B' not in cross_names[l] else '+'
        if 'B' in cross_names[l] and ('E' in cross_names[l] or 'V' in cross_names[l]) :
            cross_markers[l] = ' '             
        phi_cross[:,l] = np.rad2deg(np.unwrap(phi_cross[:,l]))
        axarr1[1].plot(f_injected, phi_cross[:,l], linewidth=2, label=cross_names[l], marker=cross_markers[l])        
        for j in range(0,3):
            axarr2[j].plot(f_injected, phi_cross[:,l], linewidth=2, label=cross_names[l], marker=cross_markers[l])                   
    axarr1[1].set_ylabel('Relative phase (°)', fontsize=14)
    axarr1[1].set_xlabel('Frequency (Hz)', fontsize=12)    
    axarr2[0].set_ylabel('Zoom 1 (°)', fontsize=14)
    axarr2[0].set_xlabel('Frequency (Hz)', fontsize=12)
    axarr2[1].set_ylabel('Zoom 2 (°)', fontsize=14)
    axarr2[1].set_xlabel('Frequency (Hz)', fontsize=12)    
    axarr2[2].set_ylabel('Zoom 3 (°)', fontsize=14)
    axarr2[2].set_xlabel('Frequency (Hz)', fontsize=12)            
    for j in range(0,2):
        axarr1[j].set_xlim(xrange)
        axarr1[j].legend(loc='best')
    for j in range(0,3):
        axarr2[j].set_xlim(xrange)
        axarr2[j].legend(loc='best')    
    if F == 0:
        axarr1[1].set_ylim(-200,+200)
        axarr2[0].set_ylim(+50,+185)
        axarr2[1].set_ylim(-10,+20)
        axarr2[2].set_ylim(-200,-160)        
    if F == 1:
        axarr1[1].set_ylim(-200,+200)
        axarr2[0].set_ylim(+50,+185)
        axarr2[1].set_ylim(-20,+20)
        axarr2[2].set_ylim(-200,-80)   
    if F == 2:
        axarr1[1].set_ylim(-200,+200)
        axarr2[0].set_ylim(+50,+185)
        axarr2[1].set_ylim(-20,+20)
        axarr2[2].set_ylim(-200,-80)
    if phi_range != None:
        axarr1[1].set_ylim(phi_range)
    if phi_range1 != None:
        axarr2[0].set_ylim(phi_range1)
    if phi_range2 != None:
        axarr2[1].set_ylim(phi_range2)
    if phi_range3 != None:
        axarr2[2].set_ylim(phi_range3)       
    if fname != None:
        fname = fname.replace('.svg','_F%d.svg'%F)
        fig1.savefig(fname.replace('.svg','_dBlog.svg') if (dB and logx) else \
                     fname.replace('.svg','_dB.svg') if dB else fname.replace('.svg','_logx.svg') if logx else \
                     fname, bbox_inches='tight', pad_inches=0.2)
        fig2.savefig(fname.replace('.svg','_phase_zoom_logx.svg') if logx else \
                     fname.replace('.svg','_phase_zoom.svg'), bbox_inches='tight', pad_inches=0.2)        
    plt.show()        
    if echo:
        print("Transfert fonction amplitudes (count/V or count/nT):")                
        str_head = "     Freq     "
        str_cal = "{:9.3f} Hz => ["
        str_s_auto = "["                
        for j in range(ncomp):
            str_head += "      {:2s}"
            str_cal += " {:7.2f}"
            str_s_auto += " {:s}"                                                
        str_head += "        auto status " + (ncomp)*" " + " param status"   
        str_cal += " ] "
        str_s_auto += " ] "
        str_s_param = (6-ncomp)*" " + "  [ {:s} ]"
        print(str_head.format(*chs)) 
        for f, cal , s_auto, s_param in zip(f_injected, calib_V2count, status_auto, status_param):
            print(str_cal.format(f, *cal) + str_s_auto.format(*s_auto) + str_s_param.format(s_param))  
        str_head_rho = "     Freq        "
        str_head_phi = "     Freq        "
        str_rho = "{:9.3f} Hz => ["
        str_phi = "{:9.3f} Hz => ["        
        for l in range(ncomp_cross):
            str_head_rho += "  {:4s}"
            str_head_phi += "    {:4s}"            
            str_rho += " {:5.3f}"
            str_phi += " {:7.2f}"
        str_rho += " ] "
        str_phi += " ]"
        rho_cross = np.array([[abs(cross_n[l]) for l in range(ncomp_cross)] for cross_n in cross_norm])
        print()            
        print("Normalized crosscorrelation coefficients:")                      
        print(str_head_rho.format(*cross_names))
        for f, rho in zip(f_injected, rho_cross):        
            print(str_rho.format(f, *rho))  
        print()    
        print("Relative phases (°):")                          
        print(str_head_phi.format(*cross_names))                
        for f, phi in zip(f_injected, phi_cross):        
            print(str_phi.format(f, *phi))
            
def fig_TF_RPW_allAbs(calib, params=None, echo=True, fname=None, dB=None, logx=None, scaleE=1.,
                      phi_range=None, ampl_range=None, freq_range=None, test_phase=None, freqs_drop=[]):
    F = int(calib[0]['datatype'][-1]) 
    dtype = calib[0]['datatype'][:3]
    if echo:
        print("nb of frequencies:  %d"%len(calib))
        print('calib[0]:')
        print(calib[0])
    chs = calib[0]['comp']
    icomp = [name2index_sm(ch) for ch in chs]  
    ncomp = len(icomp)       
    f_injected = np.array([cal['f_injected'] for cal in calib if cal['f_injected'] not in freqs_drop])
    calib_V2count = np.array([cal['calib_V2count'] for cal in calib if cal['f_injected'] not in freqs_drop])
    calib_phi = np.array([cal['calib_phi'] for cal in calib if cal['f_injected'] not in freqs_drop])
    fig1, axarr1 = plt.subplots(2, 1, figsize=(16,16))
    fig1.subplots_adjust(hspace=.05)        
    axarr1[0].set_title("RPW SCM and BIAS Transfer Functions @F%d (%s)"%(F,dtype), fontsize=18)
    for j in range(ncomp):
        if dB: axarr1[0].plot(f_injected, 
                              20*np.log10(calib_V2count[:,j]/params['BIAS'][3]/scaleE if icomp[j] in [3,4,5] else \
                                          calib_V2count[:,j]), linewidth=2, marker=markers_sm[icomp[j]], 
                                          label=chs_sm[icomp[j]], color=colors_sm[icomp[j]])
        else: axarr1[0].plot(f_injected, calib_V2count[:,j]/params['BIAS'][3]/scaleE if icomp[j] in [3,4,5] else \
                                         calib_V2count[:,j], linewidth=2, marker=markers_sm[icomp[j]], 
                                         label=chs_sm[icomp[j]], color=colors_sm[icomp[j]])
        axarr1[1].plot(f_injected, np.rad2deg(np.unwrap(np.deg2rad(calib_phi[:,j]))), 
                       linewidth=2, marker=markers_sm[icomp[j]], label=chs_sm[icomp[j]], color=colors_sm[icomp[j]])    
    axarr1[0].text(0.15, 0.08, 'BW = {}\nR0, R1, R2 = {}\nSP0, SP1 = {}'.format(*params['LFR']), 
                   horizontalalignment='left', transform=axarr1[0].transAxes, fontsize=14)
    str_bias = 'BIAS1 = {}\nBIAS2 = {}\nBIAS3 = {}\nGain = {}\nIMP = {}' if params['LFR'][1]  == 1 else \
               'BIAS1 = {}\nBIAS4 = {}\nBIAS5 = {}\nGain = {}\nIMP = {}'
    axarr1[0].text(0.35, 0.08, str_bias.format(*params['BIAS']), 
                   horizontalalignment='left', transform=axarr1[0].transAxes, fontsize=14)
    if test_phase != None: axarr1[0].text(0.55, 0.08, '\n\n\n{}'.format(test_phase), 
                           horizontalalignment='left', transform=axarr1[0].transAxes, fontsize=14)        
    if logx: 
        for j in range(0,2): axarr1[j].semilogx()
    xrange = (0,LFR_Fs[F]/2) if freq_range == None else freq_range
    if ampl_range == None: ampl_range = [[4000,9000], [1000,9000], [10,9000]][F]           
    if dB:
        axarr1[0].set_ylim(np.rint(20*np.log10(ampl_range)+np.array([-0.5,0.5])))
        axarr1[0].set_ylabel('Amplitude (dB[{:5.3f} x count/V] or dB[count/nT])'.format(params['BIAS'][3]*scaleE),
                             fontsize=14)
    else:
        axarr1[0].set_ylim(ampl_range)
        axarr1[0].set_ylabel('Amplitude ({:5.3f} x count/V or count/nT)'.format(params['BIAS'][3]*scaleE),
                             fontsize=14)
    axarr1[1].set_ylim([[-200,+200], [-200,+200], [-200,+200]][F])    
    axarr1[1].set_ylabel('Phase (°)', fontsize=14)
    axarr1[1].set_xlabel('Frequency (Hz)', fontsize=12)                   
    for j in range(0,2):
        axarr1[j].set_xlim(xrange)
        axarr1[j].legend(loc='best')      
    if phi_range != None:
        axarr1[1].set_ylim(phi_range)        
    if fname != None:
        fname = fname.replace('.svg','_F%d.svg'%F)
        if calib[0]['hanning']: fname = fname.replace('.svg','_win.svg')
        if calib[0]['expected']: fname = fname.replace('.svg','_exp.svg')        
        fname = fname.replace('.svg', '_'+calib[0]['everyN']+'.svg')    
        fig1.savefig(fname.replace('.svg','_dBlog.svg') if (dB and logx) else \
                     fname.replace('.svg','_dB.svg')    if  dB           else \
                     fname.replace('.svg','_logx.svg')  if  logx         else \
                     fname, bbox_inches='tight', pad_inches=0.2)       
    plt.show()        
    if echo:
        print("Transfert fonction amplitudes (count/V or count/nT):")                
        str_head = "     Freq     "
        str_cal = "{:9.3f} Hz => ["
        for j in range(ncomp):
            str_head += " {:>8s}"
            str_cal += " {:8.2f}"
        str_cal += " ] "
        print(str_head.format(*chs)) 
        for f, cal in zip(f_injected, calib_V2count): print(str_cal.format(f, *cal))  
        str_head_phi = "     Freq     "
        str_phi = "{:9.3f} Hz => ["        
        for l in range(ncomp):
            str_head_phi += " {:>7s}"            
            str_phi += " {:7.2f}"
        str_phi += " ]"
        print()                    
        print("Transfert fonction phases (°):")                          
        print(str_head_phi.format(*chs))                
        for f, phi in zip(f_injected, calib_phi): print(str_phi.format(f, *phi))

def fig_spectrogram_auto(sm, time, freq, datatype='ASM_F0', log=True, ampl_range=None, units=None, psd=False,
                         fname=None, freq_range=None, figsize=(16,20), hspace=0.05, fontsize=12, comment=None,
                         sharex=True, sharey=True, cmap=plt.cm.jet, vline_time=None):     
    nspec, nfreq, dim, _ = sm.shape
    F = int(datatype[-1]) 
    dtype = datatype[:3]
    # configuration for the dim components    
    fig, axarr = plt.subplots(dim, 1, sharex=sharex, sharey=sharey, figsize=figsize)  
    plt.subplots_adjust(hspace=hspace)  
    tiarr = ['B1B1*', 'B2B2*', 'B3B3*','E1E1*','E2E2*', 'VV*']
    log = dim*[True] if log else dim*[False]        
    if ampl_range == None:
        varr = [[None, None] for i in range(dim)]
    else:
        varr = [np.log10(ampl_range[i]) if (log[i] and ampl_range[i][0] != None) \
                    else ampl_range[i] for i in range(dim)]     
    yrange = (freq[1] if freq[0] == 0 else freq[0] if log else 0, LFR_Fs[F]/2) if freq_range == None else freq_range
    # generate the two 2D grids for the x & y bounds of the color spectrograms
    dt = time[1] - time[0]           
    df = freq[1] - freq[0]    
    x, y = np.meshgrid(np.append(time, time[-1]+dt), np.append(freq, freq[-1]+df)-df/2)
    # plotting
    if units == None: units = dim*['count']
    for i in range(dim):
        z = np.log10(abs(sm[:, :,i, i])) if log[i] else abs(sm[:, :,i, i])
        spectro = axarr[i].pcolormesh(x, y, z.T, vmin=varr[i][0], vmax=varr[i][1], cmap=cmap)
        axarr[i].set_ylim(yrange)  
        axarr[i].set_ylabel('frequency (Hz)', fontsize=fontsize)
        axarr[i].xaxis.set_ticks_position('both')
        axarr[i].yaxis.set_ticks_position('both')
        if vline_time != None:
            for t in vline_time:
                axarr[i].axvline(t, color='white', lw=1)
        if (comment != None) and (i == 0): 
            axarr[i].set_title('{} @F{:d} {} ($\Delta f$ = {:.2f} Hz, $\Delta t$ = {:.1f} s)' \
                           .format(dtype, F, comment, df, dt.total_seconds() if isinstance(dt, timedelta) \
                               else dt) , fontsize=fontsize)                     
        cbar = fig.colorbar(spectro, ax=axarr[i])
        if psd:
            cbar.set_label(('log10({}$^2$/ Hz)\n'+tiarr[i]).format(units[i]) if log[i] else ('{}$^2$/ Hz\n'+tiarr[i]).format(units[i]), fontsize=fontsize)
        else:
            cbar.set_label(('log10({}$^2$)\n'+tiarr[i]).format(units[i]) if log[i] else ('{}$^2$\n'+tiarr[i]).format(units[i]), fontsize=fontsize)        
    if isinstance(dt, timedelta): 
        fig.autofmt_xdate()
        #plt.xticks(rotation=70)
    else:
        axarr[dim-1].set_xlabel('time (s)', fontsize=fontsize)               
    # saving      
    if fname != None:
        plt.savefig(fname, bbox_inches='tight', pad_inches=0.2)
    plt.show()         
    
def fig_spectra_auto(auto_arr, freq, datatype='ASM_F0', freq_range=None, logx=False, logy=True, ampl_range=None,
                     units=None, psd=False, fname=None, figsize=(12,16), hspace=0.15, comment=None,  
                     ycmt=0.06, pcmt=0, fontsize=12, label_list=None):     
    nfreq, dim, _ = auto_arr.shape
    F = int(datatype[-1]) 
    dtype = datatype[:3]     
    fig, axarr = plt.subplots(dim, 1, sharex=True, figsize=figsize)  
    plt.subplots_adjust(hspace=hspace)  
    if label_list == None:
        tiarr = ['B1B1*', 'B2B2*', 'B3B3*','E1E1*','E2E2*', 'VV*']
    else:
        tiarr = label_list
    logy = dim*[True] if logy else dim*[False]    
    df = freq[1] - freq[0]
    if ampl_range == None:
        varr = [[None, None] for i in range(dim)]
    else:
        varr = [np.log10(ampl_range[i]) if (logy[i] and ampl_range[i][0] != None) \
                    else ampl_range[i] for i in range(dim)]                     
    if units == None: units = dim*['count']
    xrange = (freq[1] if freq[0] == 0 else freq[0] if logx else 0, LFR_Fs[F]/2) if freq_range == None else freq_range
    axarr[0].set_title('power spectral densities from {}     @F{:d}     ($\Delta f$ = {:.2f} Hz)' \
                           .format(dtype, F, df) , fontsize=fontsize)    
    for i in range(dim):
        axarr[i].plot(freq, np.log10(abs(auto_arr[:,i,i])) if logy[i] else abs(auto_arr[:,i,i]),
                      linewidth=2, marker=markers_sm[i], label=tiarr[i], color=colors_sm[i])
        axarr[i].set_xlim(xrange)
        if logx: axarr[i].semilogx()
        axarr[i].legend(loc='best')              
        axarr[i].set_ylim(varr[i])
        #axarr[i].xaxis.set_ticks_position('both')
        axarr[i].yaxis.set_ticks_position('both')        
        if psd:
            axarr[i].set_ylabel('log10({}$^2$/ Hz)'.format(units[i]) if logy[i] else '{}$^2$/ Hz'.format(units[i]), fontsize=fontsize)
        else:
            axarr[i].set_ylabel('log10({}$^2$)'.format(units[i]) if logy[i] else '{}$^2$'.format(units[i]), fontsize=fontsize)        
    axarr[dim-1].set_xlabel('frequency (Hz)', fontsize=fontsize)
    if comment != None: axarr[pcmt].text(0.5, ycmt, comment, horizontalalignment='center', transform=axarr[pcmt].transAxes, 
                                         fontsize=fontsize)    
    if fname != None:
        suffix = fname[fname.find('.',-5):]
        plt.savefig(fname.replace(suffix,'_logx'+suffix) if logx else fname, bbox_inches='tight', pad_inches=0.2)
    plt.show()              
                        
def fig_spectra_auto_allF(sm_data, sm_freq, sm_ik, date_comment, datatype='ASM', freq_range=None, logx=True, logy=True, 
                          ampl_range=None, units=None, psd=False, fname=None, figsize=(10,12), hspace=0.05, 
                          comment=None, ycmt=0.06, pcmt=0, fontsize=12, label_list=None):     
    nspec, nfreq, dim, _ = sm_data[0].shape    
    dtype = datatype[:3]     
    fig, axarr = plt.subplots(dim, 1, sharex=True, figsize=figsize)  
    plt.subplots_adjust(hspace=hspace)  
    if label_list == None:
        tiarr = ['B1B1*', 'B2B2*', 'B3B3*','E1E1*','E2E2*', 'VV*']
    else:
        tiarr = label_list
    logy = dim*[True] if logy else dim*[False]
    if units == None: units = dim*['count']    
    colorlist = ['blue', 'orange', 'green']    
    axarr[0].set_title('power spectral densities from {}   F0 F1 F2    [{}]'.format(dtype, str(date_comment)), fontsize=fontsize)            
    for i in range(dim):
        for F in [0,1,2]:
            axarr[i].plot(sm_freq[F], np.log10(abs(sm_data[F][sm_ik[F],:,i,i])) if logy[i] else \
                                               abs(sm_data[F][sm_ik[F],:,i,i]),
                          linewidth=1., label=tiarr[i] if F==0 else None, color=colorlist[F])                     
        if logx: axarr[i].semilogx()
        axarr[i].legend(loc='best')
        #axarr[i].xaxis.set_ticks_position('both')
        axarr[i].yaxis.set_ticks_position('both')        
        if psd:
            axarr[i].set_ylabel('log10({}$^2$/ Hz)'.format(units[i]) if logy[i] else '{}$^2$/ Hz'.format(units[i]), fontsize=fontsize)
        else:
            axarr[i].set_ylabel('log10({}$^2$)'.format(units[i]) if logy[i] else '{}$^2$'.format(units[i]), fontsize=fontsize)        
    axarr[dim-1].set_xlabel('frequency (Hz)', fontsize=fontsize)
    if comment != None: axarr[pcmt].text(0.5, ycmt, comment, horizontalalignment='center', transform=axarr[pcmt].transAxes, 
                                         fontsize=fontsize)    
    if fname != None:
        suffix = fname[fname.find('.',-5):]
        plt.savefig(fname.replace(suffix,'_logx'+suffix) if logx else fname, bbox_inches='tight', pad_inches=0.2)
    plt.show()                             

