# -*- coding: utf-8 -*-

import jsonpickle
import jsonpickle.ext.numpy as jsonpickle_numpy
import numpyson
import numpy as np

def save2json(one_set_of_data, fname):
    with open(fname, "w") as f_out:
        f_out.write(jsonpickle.encode(one_set_of_data, keys=True, warn=True))
        
def json2data(fname):
    with open(fname, "r") as f_in:
        return jsonpickle.decode(f_in.read(), keys=True)     

def save2numpyson(one_set_of_data, fname):
    with open(fname, "wb") as f_out:
        f_out.write(numpyson.dumps(one_set_of_data))

def numpyson2data(fname):
    with open(fname, "rb") as f_in:
        return numpyson.loads(f_in.read())                                 

def save2Dtxt(*dataset, fname='', fmt='%.18e', header='', T=False, delimiter=' ', comments='# '):
    """
    *dataset is either a unique 2D array_like of shape (m, n) for m columns and n lines
    or a succession of such 2D objects with same dimension n and/or of 1D-objects with n elements  
    if fmt is a list, its length shall match the number of total columns m
    if T=True the interpretation of m and n is inversed (i.e., m lines and n columns)
    """
    if len(dataset) > 1:
        darray = np.concatenate([np.array(subset, ndmin=2) for subset in dataset])
    else:
        darray = np.array(dataset[0], ndmin=2) 
    np.savetxt(fname, darray if T else darray.T, fmt=fmt, header=header, delimiter=delimiter, comments=comments)

def load2Dtxt(fname):
    return np.loadtxt(fname, delimiter=None, comments='#', dtype='float')

