# -*- coding: utf-8 -*-

import numpy as np
import struct

def readlines(file, i1=0, i2=0):
    lines = []
    with open(file, 'r') as f_in:    
        for i, line in enumerate(f_in):
            if i >= i1: lines.append(line.strip())
            if i == i2: break
    return lines
 
def rewrite(csvfile, header=3, output_ftype='.dat', ctype='f', option='struct', endianness='<', format_spec='9.6f'):
    newfile = csvfile.replace('.csv', output_ftype)   
    dtype = {'f': np.float32, 'd': float}[ctype]         
    if output_ftype == '.txt':
        with open(newfile, 'w') as output_file:
            with open(csvfile, 'r') as input_file: 
                for i in range(header): print(input_file.readline().strip())        
                for line in input_file: 
                    output_file.write(('{:'+format_spec+'}\n').format(dtype(line)))
    if output_ftype == '.dat':
        with open(newfile, 'wb') as output_file:
            with open(csvfile, 'r') as input_file: 
                for i in range(header): print(input_file.readline().strip())     
                if option == 'tofile':    
                    for line in input_file: 
                        np.array(line, dtype=dtype).tofile(output_file)
                if option == 'struct':  
                    fmt = endianness + ctype
                    for line in input_file:                     
                        output_file.write(struct.pack(fmt, dtype(line)))                                        
                    
def seeklines(file, i1=0, i2=0, bytes_per_item=4, ctype='f', option='struct', endianness='<', sep='\n'):   
    ftype = file[-3:]
    n = i2 - i1 + 1
    if ftype == 'dat':             
        with open(file, 'rb') as input_file:
            input_file.seek(i1*bytes_per_item)
            if option == 'tofile':
                dtype = {'f': np.float32, 'd': float}[ctype]
                return np.fromfile(input_file, dtype=dtype, count=n)
            if option == 'struct':                
                fmt = endianness + str(n) + ctype
                return struct.unpack(fmt, input_file.read(n*bytes_per_item))
    if ftype == 'txt':
        with open(file, 'r') as input_file:
            input_file.seek(i1*bytes_per_item)
            return input_file.read(n*bytes_per_item).strip().split(sep)  
            
                                            
def loadtxt2(filename, ncols=None, dtyp=None, commentchars=["#","@"]):
    """
    http://blog.juliusschulz.de/blog/ultimate-ipython-notebook#reading-large-text-files
    """
    global data
    if ncols==None:
        with open(filename) as f:
            l = f.readline()
            while l[0] in commentchars:
                print(l)
                l = f.readline()
            ncols = len(l.split())
            data = np.fromfile(filename, sep=" ", dtype=dtyp)
    return data.reshape((len(data)/ncols, ncols))        
                    
